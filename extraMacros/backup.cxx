//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// functions
void doMassPlot();
void doFEBTimePlot();
void doCellTimePlot();
void doAngularPlot();
void doEFracPlot();
void doETimePlot();
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
void drawFitInfo(int gain, int sl);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1);

// for calculating correcitons at each pass
double febTime_0[2][47+17][620]; // [2] gain [47] runs [620] febs for pass 0
double ftTime_0[2][47+17][104]; // [2] gain [47] runs [620] ft for pass 0
double febTime_1[2][620];     // [2] gain [620] febs for pass 1
double chTime[2][79360];      // [2] gain [79360] channels for pass 2
double enFit[2][22][6];       // [2] gain [22] slots [6] energy fit params
double dphiFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[2][22][2];       // [2] gain [22] slots [2] df1 fit params
double f3Fit[2][22][2];       // [2] gain [22] slots [2] df3 fit params

// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","Medium","Low"};
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
std::vector< int > runNumberList;

// histograms
TH1F *h_cell_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_feb_run[2][5][2]; // EMBA/C, EMECA/C, all
TH2F *h_phi_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_dphi_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_eta_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_deta_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_f1_t[2][5][2];    // EMBA/C, EMECA/C, all
TH2F *h_f3_t[2][5][2];    // EMBA/C, EMECA/C, all
TH2F *h_e_t[2][5][2];     // EMBA/C, EMECA/C, all

// Configuration
const int  NRUNS   = 150; // 47 for IOVconst int  NRUNS2  = 17; // 17 for IOV2 const 
bool saveEPS = false;
const int  NRUNS2  = 0; // 17 for IOV2
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void LArWeek(int pn=0, int mode=0){

#ifdef __CINT__
  gROOT->LoadMacro("../utils/AtlasLabels.C");
  gROOT->LoadMacro("../utils/AtlasStyle.C");
#endif
   SetAtlasStyle();  
  // Set Pass
  setPass(pn, mode);

  // Set Run List
  setRunList();

  
  // Make plots
  if( doPlot ) {
   //doMassPlot();
   //doCellTimePlot();
//   doFEBTimePlot();
 setTRange(-0.45,1);   
 // doETimePlot();
  // setTRange(-0.5, 0.5);

 //  doAngularPlot();
//setTRange(-1.5,1.5); 
    setTRange(-.3, .45);
    doEFracPlot();
   return;    
  }

}


//___________________
// 
// Make mass plots
//_____________________
void doMassPlot(){

	std::cout << "Making W/Z mass plots\n\n";

	TFile *file;

	// Open files
	file   = TFile::Open("../files/2016_all/pass0.root");
	//file2b = TFile::Open(Form("/afs/cern.ch/user/r/rcarbone/eos/atlas/user/r/rcarbone/WTiming/slims/slimZ_dec16_IOV2/IOV2_Z_slim.root"));
	TH1F *h_w[3];
	TH1F *h_t[3]; 
	TH1F *h_w_all;
	TH1F *h_t_all; 

	for(int g=0; g<3; g++){
		std::string hname  = Form("h_transverseMassWenu%s",gain[g].c_str());
		std::string hname2  = Form("h_maxEcellEnergy%s",gain[g].c_str());

		// Get mass distributions
		h_w[g] = (TH1F*)file->Get(hname.c_str());
		if(g==0) h_w_all=(TH1F*)file->Get(hname.c_str());   
		else
		h_w_all->Add(h_w[g]);
		//TH1F* h_wb = (TH1F*)fileb->Get(hname.c_str());
		//TH1F* h_z  = (TH1F*)file2->Get(hname.c_str());
		//TH1F* h_zb = (TH1F*)file2b->Get(hname.c_str());
	 	 h_t[g] = (TH1F*)file->Get(hname2.c_str());
		 if(g==0) h_t_all=(TH1F*)file->Get(hname2.c_str()); 
		 else
		h_t_all->Add(h_t[g]);
		//TH1F* h_tb = (TH1F*)file3b->Get(hname2.c_str());

		// Add IOV1+IOV2
		//  h_w->Add(h_wb);
		//  h_z->Add(h_zb);
		//  h_t->Add(h_tb);

		// W Plot
		TCanvas *cw = new TCanvas("c_massw","c_massw",800,700);
		h_w[g]->GetXaxis()->SetTitle("m_{T}[GeV]");
		h_w[g]->GetYaxis()->SetTitle("Events/1.0 GeV");
		h_w[g]->GetXaxis()->SetRangeUser(40,140);
		h_w[g]->GetYaxis()->SetRangeUser(0,h_w[g]->GetMaximum()/0.78);
		h_w[g]->Draw();
		int entries=h_w[g]->GetEntries();
		// Put info on plot
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
		myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
		myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
		// Save plot
		if( saveEPS )cw->SaveAs(Form("Mass_w_%s.png",gain[g].c_str()));
		cw->SaveAs(Form("Mass_w_%s.png",gain[g].c_str()));
		delete cw;
		TCanvas *ct = new TCanvas("c_massw","c_massw",800,700);
		h_t[g]->GetXaxis()->SetTitle("Max cell Energy[GeV]");
		h_t[g]->GetYaxis()->SetTitle("Events/1.0 GeV");
		if(g==0)
			h_t[g]->GetXaxis()->SetRangeUser(0,80);
		if(g==1) 
			h_t[g]->GetXaxis()->SetRangeUser(10,100);
		h_t[g]->GetYaxis()->SetRangeUser(0,h_t[g]->GetMaximum()/0.7);
		h_t[g]->Draw();
		entries=h_t[g]->GetEntries();
		// Put info on plot
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
		myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
		myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
		// Save plot
		if( saveEPS )ct->SaveAs(Form("energy_w_%s.png",gain[g].c_str()));
		ct->SaveAs(Form("energy_w_%s.png",gain[g].c_str()));
		delete ct;

	}
	TCanvas *cw_all = new TCanvas("c_massw","c_massw",800,700);
	h_w_all->GetXaxis()->SetTitle("m_{T}[GeV]");
	h_w_all->GetYaxis()->SetTitle("Events/1.0 GeV");
	h_w_all->GetXaxis()->SetRangeUser(40,140);
	h_w_all->GetYaxis()->SetRangeUser(0,h_w_all->GetMaximum()/0.78);
	h_w_all->Draw();
        int	entries=h_w_all->GetEntries();
	// Put info on plot
	ATLASLabel(0.2,0.88,pInternal);
	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
	myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
	//  myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
	// Save plot
	if( saveEPS )cw_all->SaveAs("Mass_w.png");
	cw_all->SaveAs("Mass_w.png");
	delete cw_all;
	TCanvas *ct_all = new TCanvas("c_massw","c_massw",800,700);
	h_t_all->GetXaxis()->SetTitle("Max cell Energy[GeV]");
	h_t_all->GetYaxis()->SetTitle("Events/1.0 GeV");
	h_t_all->GetXaxis()->SetRangeUser(10,100);
	h_t_all->GetYaxis()->SetRangeUser(0,h_t_all->GetMaximum()/0.7);
	h_t_all->Draw();
	entries=h_t_all->GetEntries();
	// Put info on plot
	ATLASLabel(0.2,0.88,pInternal);
	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
	myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
	// myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
	// Save plot
	if( saveEPS )ct_all->SaveAs("energy_w.png");
	ct_all->SaveAs("energy_w.png");
	delete ct_all;



	//  // Z Plot
	//  TCanvas *cz = new TCanvas("c_massz","c_massz",800,700);
	//  h_z->GetXaxis()->SetTitle("m_{ee}[GeV]");
	//  h_z->GetYaxis()->SetTitle("Events/1.0 GeV");
	//  h_z->GetXaxis()->SetRangeUser(68,108);
	//  //h_z->GetYaxis()->SetRangeUser(0,h_z->GetMaximum()/0.7);
	//  h_z->Draw();
	//  // Put info on plot
	//  ATLASLabel(0.2,0.88,pInternal);
	//  myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	//  myText(0.22, 0.76, kBlack, Form("Z#rightarrow ee"), 0.7);
	//  // Save plot
	//  if( saveEPS )cz->SaveAs(Form("Mass_z.png"));
	//  cz->SaveAs(Form("Mass_z.png"));
	//  delete cz;
	//  
	//  // Low gain Plot
	//  TCanvas *c = new TCanvas("c_lowz","c_lowz",800,700);
	//  h_t->GetXaxis()->SetTitle("Time [ns]");
	//  h_t->GetYaxis()->SetTitle("Events/ 0.16 ns");
	//  h_t->Rebin(8);
	//  h_t->Draw();
	//  
	//  double mean = h_t->GetMean();
	//  double rms = h_t->GetRMS();
	//  int entries = h_t->GetEntries();
	//  h_t = fitGaus(h_t, mean , rms, 0.68);
	//  //h_t->GetYaxis()->SetRangeUser(0.5,h_t->GetMaximum()/.05);
	//  //c->SetLogy();
	//  
	//  // Put info on plot
	//  ATLASLabel(0.2,0.88,pInternal);
	//  myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	//  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
	//  myText(0.22, 0.72, kBlack, Form("Low Gain"), 0.7);
	//  myText(0.73, 0.81, kBlack, Form("Entries: %d",entries),.7 );
	//  myText(0.73, 0.77, kBlack, Form("Mean: %.3f", mean), .7);
	//  myText(0.73, 0.73, kBlack, Form("RMS: %.3f", rms),.7);
	//  // Save plot
	//  if( saveEPS )c->SaveAs(Form("time_low.png"));
	//  c->SaveAs(Form("time_low.png"));
	//  delete c;


	return;
}

// TEST FIXME TEST
void doFEBTimePlot(){

	// For storing run corrections
	TH2F *hp[2][2][2];  //[0-1][][] = high,med;  [][0-1][] = emb, emec [][][0-1] =pass0,1

	// open data files
	TFile *f1;
	TFile *f2;
	TFile *f3;
	TFile *f4;
	f1 = TFile::Open(Form("../files/2016_all/pass0.root"),"READ");
	// f2 = TFile::Open(Form("../files/IOV_2/pass0.root"),"READ");
	f3 = TFile::Open(Form("../files/2016_all/pass1.root"),"READ");
	// f4 = TFile::Open(Form("../files/IOV_2/pass1.root"),"READ");

	// Initialize the histograms
	for( int g=0; g<2; g++){ 
		for(int benc=0; benc<2; benc++){
			for( int pass=0; pass<2; pass++){
				hp[g][benc][pass]   = new TH2F(Form("hp%d%d%d",g,benc,pass),Form("hp%d%d%d",g,benc,pass),150,0,150,500,-5,5);
			}
		}
	}

	for( int rn=0; rn<NRUNS; rn++){
		TH2F *h1[2]; // iov1 pass0
		TH2F *h2[2]; // iov2 pass0
		TH2F *h3[2]; // iov1 pass1
		TH2F *h4[2]; // iov2 pass1
		TProfile *tp1[2]; // iov1 pass0
		TProfile *tp2[2]; // iov2 pass0
		TProfile *tp3[2]; // iov1 pass1
		TProfile *tp4[2]; // iov2 pass1
		for( int g=0; g<2; g++){
			// Load histograms for each run
			std::string hname  = Form("h_RunAvgFTTime_%s%d",gain[g].c_str(),rn);
			std::string pname  = Form("Prof run %s%d1",gain[g].c_str(),rn);
			//     std::string pnameb  = Form("Prof run %s%d2",gain[g].c_str(),rn+NRUNS);
			std::string pname2 = Form("Prof run %s%d1 pass1",gain[g].c_str(),rn);
			//     std::string pname2b = Form("Prof run %s%d2 pass1",gain[g].c_str(),rn+NRUNS);
			h1[g] = (TH2F*)f1->Get(hname.c_str());
			h3[g] = (TH2F*)f3->Get(hname.c_str());
			//      if( rn < NRUNS2){
			//        h2[g] = (TH2F*)f2->Get(hname.c_str());
			//        h4[g] = (TH2F*)f4->Get(hname.c_str());
			//      }
			//      if( !h1[g]->GetEntries() || ! h2[g]->GetEntries() ){
			//        std::cout << "  >> Run index: " << rn << " has no data for "
			//          << " some gain, no data to be plotted!\n";
			//        continue;
			//      }
			//
			//      // Make profile plot

			tp1[g] = h1[g]->ProfileX(pname.c_str(),-1,-1,"o");
			tp3[g] = h3[g]->ProfileX(pname2.c_str(),-1,-1,"o");
			//      if( rn < NRUNS2){
			//        tp2[g] = h2[g]->ProfileX(pnameb.c_str(),-1,-1,"o");
			//        tp4[g] = h4[g]->ProfileX(pname2b.c_str(),-1,-1,"o");
			//      }
			for( int ft=0; ft<114; ft++){
				// Make sure ft has data
				//        bool fill1 = false;
				//        bool fill2 = false;
//        bool fill3 = false;
//        bool fill4 = false;
//        if( tp1[g]->GetBinContent(ft+1) )
//          fill1 = true;
//        if( tp3[g]->GetBinContent(ft+1) )
//          fill3 = true;
//        if( rn < NRUNS2 ){
//          if( tp2[g]->GetBinContent(ft+1) )
//            fill2  = true;
//          if( tp4[g]->GetBinContent(ft+1) )
//            fill4 = true;
//        }
        // Fill plot
        if( ft < 64){ // emb
          //if( fill1 )
            hp[g][0][0]->Fill(rn, tp1[g]->GetBinContent(ft+1)); 
          //if( fill2)
          //  hp[g][0][0]->Fill(rn+NRUNS, tp2[g]->GetBinContent(ft+1));
          //if( fill3)
            hp[g][0][1]->Fill(rn, tp3[g]->GetBinContent(ft+1));
          //if( fill4)
           // hp[g][0][1]->Fill(rn+NRUNS, tp4[g]->GetBinContent(ft+1));
        }
        else{ // emec
          //if( fill1 )
            hp[g][1][0]->Fill(rn, tp1[g]->GetBinContent(ft+1));
          //if( fill2 )
          //  hp[g][1][0]->Fill(rn+NRUNS, tp2[g]->GetBinContent(ft+1));
          //if( fill3 )
            hp[g][1][1]->Fill(rn, tp3[g]->GetBinContent(ft+1));
          //if( fill4 )
           // hp[g][1][1]->Fill(rn+NRUNS, tp4[g]->GetBinContent(ft+1));
        }
      } // end loop over fts
    } // end loop over gains
  } // end loop over runs
  
  for( int g=0; g<2; g++){
    // EMB
    // Run by Run
    TCanvas *c1 = new TCanvas("cfeb","cfeb",800,700);
    TProfile *emb  = hp[g][0][0]->ProfileX();
    TProfile *emb2 = hp[g][0][1]->ProfileX();
    emb->Draw();
    emb2->SetLineColor(kGreen-3);
    emb2->SetMarkerColor(kGreen-3);
    emb2->Draw("same");
    // Run number labels
    for( int bin=0; bin<NRUNS; bin++){
      int s_run =  runNumberList[bin] ;
      emb->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
    }
    emb->GetXaxis()->SetRangeUser(0,NRUNS);
    emb->GetXaxis()->LabelsOption("v");
    emb->GetXaxis()->SetLabelSize(0.03);
    emb->GetXaxis()->SetTitle("Run Number");
    emb->GetYaxis()->SetRangeUser(-1,1);
    emb->GetYaxis()->SetTitle("Time [ns]");
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
    myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
    myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
    myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
    myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
    myText(0.73, 0.76, kGreen-3,  Form("Pass 1"), 0.6);
    // Save plots
    c1->SaveAs(Form("EMB_pass0_pass1_%s.png",gain[g].c_str()));
    delete c1;

    // EMEC
    // Run by Run
    TCanvas *c2 = new TCanvas("c2","c2",800,700);
    TProfile *emec = hp[g][1][0]->ProfileX();
    TProfile *emec2 = hp[g][1][1]->ProfileX();
    emec->Draw();
    emec2->SetLineColor(kGreen-3);
    emec2->SetMarkerColor(kGreen-3);
    emec2->Draw("same");
    // Write run number as bin label
    for( int bin=0; bin<NRUNS+NRUNS2; bin++){
      int s_run =  runNumberList[bin] ;
      emec->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
    }
    emec->GetXaxis()->SetRangeUser(0,NRUNS+NRUNS2);
    emec->GetXaxis()->LabelsOption("v");
    emec->GetXaxis()->SetLabelSize(0.03);
    emec->GetXaxis()->SetTitle("Run Number");
    emec->GetYaxis()->SetRangeUser(-1.5,1.5);
    emec->GetYaxis()->SetTitle("Time [ns]");
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
    myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
    myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
    myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
    myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
    myText(0.73, 0.76, kGreen-3,  Form("Pass 1"), 0.6);
    // Save plots
    c2->SaveAs(Form("EMEC_pass0_pass1_%s.png",gain[g].c_str()));
    delete c2;


    //All
    TCanvas *c3 = new TCanvas("c3","c3",800,700);              
    emec->Add(emb);
    emec2->Add(emb2);
    emec->Draw();
    emec2->SetLineColor(kGreen-3);
    emec2->SetMarkerColor(kGreen-3);
    emec2->Draw("same");
    // Write run number as bin label
    for( int bin=0; bin<NRUNS+NRUNS2; bin++){
      int s_run =  runNumberList[bin] ;
      emec->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
    }
    emec->GetXaxis()->SetRangeUser(0,NRUNS+NRUNS2);
    emec->GetXaxis()->LabelsOption("v");
    emec->GetXaxis()->SetLabelSize(0.03);
    emec->GetXaxis()->SetTitle("Run Number");
    emec->GetYaxis()->SetRangeUser(-1.5,1.5);
    emec->GetYaxis()->SetTitle("Time [ns]");
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
    myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
    myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
    myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
    myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
    myText(0.73, 0.76, kGreen-3,  Form("Pass 1"), 0.6);
    // Save plots
   c3->SaveAs(Form("All_pass0_pass1_%s.png",gain[g].c_str()));
   delete c3;
  }

  f1->Close();
  //f2->Close();
  f3->Close();
  //f4->Close();



}
//_____________________________________________
//
// Plot avg time vs energy 
// by subdetector/slot
//_____________________________________________
void doETimePlot(){
  
  std::cout << "\nMaking plots for average energy timing\n\n";
  
  // Initialize the histograms
  for( int i=0; i<5; i++){
    for( int j=0; j<2; j++){
      for( int k=0; k<2; k++){
        h_e_t[j][i][k]  = new TH2F(Form("h_e_t%d%d%d",i,j,k),Form("Cell Time %s; Energy [GeV]; Time [ns]",subDet[i].c_str()),250,0,250.,500,-5,5);
      }
    }
  }
  
  // Open the data file
  TFile *file;
  TFile *fileb;
  TFile *file2;
  TFile *file2b;
  file = TFile::Open(Form("../files/2016_all/pass3.root"),"READ");
//  fileb = TFile::Open(Form("../files/IOV_2/pass3.root"),"READ");
  file2 = TFile::Open(Form("../files/2016_all/pass4.root"),"READ");
//  file2b = TFile::Open(Form("../files/IOV_2/pass4.root"),"READ");
  
  // Loop over gains
  for( int g=0; g<2; g++){  
    // Loop over slots
    for(int sl=0; sl<22; sl++){

      // Make names for the histograms to open
      std::string ename  = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl);

      TH2F *t_e  = (TH2F*)file->Get(ename.c_str());
      //TH2F *t_eb  = (TH2F*)fileb->Get(ename.c_str());
      TH2F *t_e2  = (TH2F*)file2->Get(ename.c_str());
      //TH2F *t_e2b  = (TH2F*)file2b->Get(ename.c_str());

      if( !t_e->GetEntries() ) {
        std::cout << "  >> Slot index: " << sl << " has no data for " 
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }

      // Summary plots
      h_e_t[g][4][0]->Add(t_e);
     // h_e_t[g][4][0]->Add(t_eb);
      h_e_t[g][4][1]->Add(t_e2);
     // h_e_t[g][4][1]->Add(t_e2b);
      if( sl < 4 || sl == 8 ){ 
        h_e_t[g][0][0]->Add(t_e);
      //  h_e_t[g][0][0]->Add(t_eb);
        h_e_t[g][0][1]->Add(t_e2);
      //  h_e_t[g][0][1]->Add(t_e2b);
      }
      else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
        h_e_t[g][1][0]->Add(t_e);
       // h_e_t[g][1][0]->Add(t_eb);
        h_e_t[g][1][1]->Add(t_e2);
      //  h_e_t[g][1][1]->Add(t_e2b);
      }
      else if ( sl > 9 && sl < 16 ){
        h_e_t[g][2][0]->Add(t_e);
    //   // h_e_t[g][2][0]->Add(t_eb);
        h_e_t[g][2][1]->Add(t_e2);
      //  h_e_t[g][2][1]->Add(t_e2b);
      }
      else if ( sl > 15 ){
        h_e_t[g][3][0]->Add(t_e);
      //  h_e_t[g][3][0]->Add(t_eb);
        h_e_t[g][3][1]->Add(t_e2);
      //  h_e_t[g][3][1]->Add(t_e2b);
      }

      
    }// end loop over slots
    

      double nFrac = ( g == 0 ? 100 : 75 );
     //nFrac=1;
    // Make all summary plots
      // e
      TCanvas *ccall = new TCanvas(Form("cccallEMB_%d",g),Form("ce_%d",g),800,700);
      // rebin
    // // h_e_t[4][0]->RebinX(100);
    //  h_e_t[g][4][0]->GetXaxis()->SetRangeUser(5,100);  
    //  h_e_t[g][4][0]->Draw();

    //  ccall->SaveAs(Form("2d_All_pass3_pass4_%s.png",gain[g].c_str()));
      h_e_t[g][4][0] = rebin( h_e_t[g][4][0], h_e_t[g][4][0]->GetEntries()/nFrac, 5);
      h_e_t[g][4][1] = rebin( h_e_t[g][4][1], h_e_t[g][4][1]->GetEntries()/nFrac, 5);

    //  TCanvas *ccall1 = new TCanvas(Form("cccallEMB_%d",g),Form("ce_%d",g),800,700);
      // rebin
     // h_e_t[4][0]->RebinX(100);
    //  h_e_t[g][4][0]->GetXaxis()->SetRangeUser(5,100);  
    //  h_e_t[g][4][0]->Draw();
    //  ccall1->SaveAs(Form("rebin_2d_All_pass3_pass4_%s.png",gain[g].c_str()));
      //delete ccall1;

    //  h_e_t[g][4][0]->Draw();
      TProfile *ff_e_all = h_e_t[g][4][0]->ProfileX(Form("ff_e_alla%d0",g),-1,-1,"o");
      TProfile *ff_e_all2 = h_e_t[g][4][1]->ProfileX(Form("ff_e_alla%d1",g),-1,-1,"o");
      ff_e_all->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_e_all->GetYaxis()->SetTitle("Time [ns]");
      ff_e_all->SetErrorOption("g"); 
      ff_e_all2->SetErrorOption("g"); 
      
      if( g ==V 0){
        ff_e_all->GetXaxis()->SetRangeUser(5,ff_e_all->GetXaxis()->GetBinLowEdge(ff_e_all->GetXaxis()->FindBin(249.)-1));
      } else{
        ff_e_all->GetXaxis()->SetRangeUser(ff_e_all->GetXaxis()->GetBinLowEdge(ff_e_all->GetXaxis()->FindBin(5.)+1),250.);
      }
     //  ff_e_all->Sumw2();
      ff_e_all->Draw();
       ///CJ///
      ff_e_all->GetXaxis()->SetRangeUser(5,100);
      ff_e_all2->SetLineColor(kGreen-3);
      ff_e_all2->SetMarkerColor(kGreen-3);
      ff_e_all2->Draw("same");
 
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 3"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 4"), 0.6);
      ccall->SetLogx(1);   
      if( saveEPS )ccall->SaveAs(Form("All_pass3_pass4_%s.png",gain[g].c_str()));
      ccall->SaveAs(Form("All_pass3_pass4_%s.png",gain[g].c_str()));
      delete ccall;
    
    // Make EMB summary plots
      // e
      TCanvas *cce = new TCanvas(Form("ccceEMB_%d",g),Form("ce_%d",g),800,700);
      h_e_t[g][0][0]->Add( h_e_t[g][1][0]); 
      h_e_t[g][0][1]->Add( h_e_t[g][1][1]);
      // rebin
      h_e_t[g][0][0] = rebin( h_e_t[g][0][0], h_e_t[g][0][0]->GetEntries()/nFrac, 5);
      h_e_t[g][0][1] = rebin( h_e_t[g][0][1], h_e_t[g][0][1]->GetEntries()/nFrac, 5);
      TProfile *ff_e = h_e_t[g][0][0]->ProfileX(Form("ff_ea%d0",g),-1,-1,"o");
      TProfile *ff_e2 = h_e_t[g][0][1]->ProfileX(Form("ff_ea%d1",g),-1,-1,"o");
      ff_e->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_e->GetYaxis()->SetTitle("Time [ns]");
      if( g == 0){
        ff_e->GetXaxis()->SetRangeUser(5,ff_e->GetXaxis()->GetBinLowEdge(ff_e->GetXaxis()->FindBin(249.)-1));
      } else{
        ff_e->GetXaxis()->SetRangeUser(ff_e->GetXaxis()->GetBinLowEdge(ff_e->GetXaxis()->FindBin(5.)+1),250.);
      }
      ff_e->Draw();
      ff_e2->SetLineColor(kGreen-3);
      ff_e2->SetMarkerColor(kGreen-3);
      ff_e2->Draw("same");
 
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 3"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 4"), 0.6);
      cce->SetLogx(1);   
      if( saveEPS )cce->SaveAs(Form("EMB_pass3_pass4_%s.png",gain[g].c_str()));
      cce->SaveAs(Form("EMB_pass3_pass4_%s.png",gain[g].c_str()));
      delete cce;
    
      // Make EMEC summary plots
      // e
      TCanvas *ccd = new TCanvas(Form("cccdEMEC_%d",g),Form("ced_%d",g),800,700);
      h_e_t[g][2][0]->Add( h_e_t[g][3][0]); 
      h_e_t[g][2][1]->Add( h_e_t[g][3][1]);
      // rebin
      h_e_t[g][2][0] = rebin( h_e_t[g][2][0], h_e_t[g][2][0]->GetEntries()/nFrac, 5);
      h_e_t[g][2][1] = rebin( h_e_t[g][2][1], h_e_t[g][2][1]->GetEntries()/nFrac, 5);
      

      TProfile *ff_eb = h_e_t[g][2][0]->ProfileX(Form("ff_eab%d0",g),-1,-1,"o");
      TProfile *ff_e2b = h_e_t[g][2][1]->ProfileX(Form("ff_eab%d1",g),-1,-1,"o");
      ff_eb->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_eb->GetYaxis()->SetTitle("Time [ns]");
      ff_eb->SetErrorOption("g"); 
      ff_e2b->SetErrorOption("g"); 
      if( g == 0){
        ff_eb->GetXaxis()->SetRangeUser(5,ff_eb->GetXaxis()->GetBinLowEdge(ff_eb->GetXaxis()->FindBin(249.)-1));
      }else{
        ff_eb->GetXaxis()->SetRangeUser(ff_eb->GetXaxis()->GetBinLowEdge(ff_eb->GetXaxis()->FindBin(5.)+1),250.);
      }
      ff_eb->Draw();
      ff_e2b->SetLineColor(kGreen-3);
      ff_e2b->SetMarkerColor(kGreen-3);
      ff_e2b->Draw("same");
 
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 3"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 4"), 0.6);
      ccd->SetLogx(1);
      if( saveEPS )ccd->SaveAs(Form("EMEC_pass3_pass4_%s.png",gain[g].c_str()));
      ccd->SaveAs(Form("EMEC_pass3_pass4_%s.png",gain[g].c_str()));
      delete ccd;
  }// end loop over gains

  file->Close();
 // fileb->Close();
  file2->Close();
 // file2b->Close();
  return;
}

//_____________________________________________
//
// Plot avg time vs Phi/Eta 
// by subdetector/slot
//_____________________________________________
void doAngularPlot(){
  
  std::cout << "\nMaking plots for angular cell position timing\n\n";
  
  // Initialize the histograms
  for( int i=0; i<5; i++){
    for( int j=0; j<2; j++){
      for( int k=0; k<2; k++){
        h_dphi_t[j][i][k] = new TH2F(Form("h_dphi_t%d%d%d",i,j,k),Form("Cell Time %s; #delta#phi[0.0245 units]; Time [ns]",subDet[i].c_str()),400,-2,2,500,-5,5);
        h_deta_t[j][i][k] = new TH2F(Form("h_deta_t%d%d%d",i,j,k),Form("Cell Time %s; #delta#eta[0.025 units]; Time [ns]",subDet[i].c_str()),100,-1,1,500,-5,5);
      }
    }
  }
  
  // Open the data file
  TFile *file;
  TFile *fileb;
  TFile *file2;
  TFile *file2b;
  file = TFile::Open(Form("../files/2016_all/pass4.root"),"READ");
//  fileb = TFile::Open(Form("../files/IOV_2/pass4.root"),"READ");
  file2 = TFile::Open(Form("../files/2016_all/pass5.root"),"READ");
//  file2b = TFile::Open(Form("../files/IOV_2/pass5.root"),"READ");
  
// Loop over gains
  for( int g=0; g<2; g++){  
	  // Loop over slots
	  for(int sl=0; sl<22; sl++){

		  // Make names for the histograms to open
		  std::string dpname = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),sl);
		  std::string dename = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),sl);
		  //       
		  TH2F *t_dp   = (TH2F*)file->Get(dpname.c_str());
		  //// TH2F *t_dpb  = (TH2F*)fileb->Get(dpname.c_str());
		  TH2F *t_dp2  = (TH2F*)file2->Get(dpname.c_str());
		  //// TH2F *t_dp2b = (TH2F*)file2b->Get(dpname.c_str());
		  TH2F *t_de   = (TH2F*)file->Get(dename.c_str());
		  //// TH2F *t_deb  = (TH2F*)fileb->Get(dename.c_str());
		  TH2F *t_de2  = (TH2F*)file2->Get(dename.c_str());
		  //// TH2F *t_de2b = (TH2F*)file2b->Get(dename.c_str());

		  if( !t_dp->GetEntries() ) {
			  std::cout << "  >> Slot index: " << sl << " has no data for " 
				  << gain[g] << " gain, no plots will be made!\n";
			  continue;
		  }

		  // Summary plots
		  h_dphi_t[g][4][0]->Add(t_dp);
		  ////     h_dphi_t[g][4][0]->Add(t_dpb);
		  h_dphi_t[g][4][1]->Add(t_dp2);
		  //// h_dphi_t[g][4][1]->Add(t_dp2b);
		  h_deta_t[g][4][0]->Add(t_de);
		  //// h_deta_t[g][4][0]->Add(t_deb);
		  h_deta_t[g][4][1]->Add(t_de2);
		  //// h_deta_t[g][4][1]->Add(t_de2b);

		  if( sl < 4 || sl == 8 ){ 
		  h_dphi_t[g][0][0]->Add(t_dp);
		  ////   h_dphi_t[g][0][0]->Add(t_dpb);
		  h_dphi_t[g][0][1]->Add(t_dp2);
		  ////   h_dphi_t[g][0][1]->Add(t_dp2b);
		  h_deta_t[g][0][0]->Add(t_de);
		  ////   h_deta_t[g][0][0]->Add(t_deb);
		  h_deta_t[g][0][1]->Add(t_de2);
		  //       h_deta_t[g][0][1]->Add(t_de2b);
	  }

		  else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
			  h_dphi_t[g][1][0]->Add(t_dp);
			  ////        h_dphi_t[g][1][0]->Add(t_dpb);
			  h_dphi_t[g][1][1]->Add(t_dp2);
			  ////        h_dphi_t[g][1][1]->Add(t_dp2b);
			  h_deta_t[g][1][0]->Add(t_de);
			  ////        h_deta_t[g][1][0]->Add(t_deb);
			  h_deta_t[g][1][1]->Add(t_de2);
			  ////        h_deta_t[g][1][1]->Add(t_de2b);
		  }

		  else if ( sl > 9 && sl < 16 ){
			  h_dphi_t[g][2][0]->Add(t_dp);
			  ////   h_dphi_t[g][2][0]->Add(t_dpb);
			  h_dphi_t[g][2][1]->Add(t_dp2);
			  ////   h_dphi_t[g][2][1]->Add(t_dp2b);
			  h_deta_t[g][2][0]->Add(t_de);
			  ////   h_deta_t[g][2][0]->Add(t_deb);
			  h_deta_t[g][2][1]->Add(t_de2);
			  ////       h_deta_t[g][2][1]->Add(t_de2b);
		  }

		  else if ( sl > 15 ){
			  h_dphi_t[g][3][0]->Add(t_dp);
			  ////        h_dphi_t[g][3][0]->Add(t_dpb);
			  h_dphi_t[g][3][1]->Add(t_dp2);
			  //        h_dphi_t[g][3][1]->Add(t_dp2b);
			  h_deta_t[g][3][0]->Add(t_de);
			  //        h_deta_t[g][3][0]->Add(t_deb);
			  h_deta_t[g][3][1]->Add(t_de2);
			  //        h_deta_t[g][3][1]->Add(t_de2b);
		  }

	  }// end loop over slots


	  // Make summary plots all
	  // dPhi


	  TCanvas *cdp_all = new TCanvas(Form("cdp_all_%d",g),Form("c_%d",g),800,700);
//	  h_dphi_t[g][4][0]->Draw();
// cdp_all->SaveAs(Form("rebin_pass4_pass5_%s.png",gain[g].c_str())); 
  //
  //    delete cdp_all;

	  TProfile *ff_dp_all = h_dphi_t[g][4][0]->ProfileX(Form("ff_dp_all%d0",g),-1,-1,"o");
	  TProfile *ff_dp_all2 = h_dphi_t[g][4][1]->ProfileX(Form("ff_dp_all%d1",g),-1,-1,"o");
	//  ff_dp_all->Rebin(2);
	 // ff_dp_all2->Rebin(2);
//	  ff_dp_all->GetXaxis()->SetRangeUser(-1,1);
	  ff_dp_all->GetYaxis()->SetRangeUser(tmin,tmax);
	  ff_dp_all->GetYaxis()->SetTitle("Time [ns]");
	  ff_dp_all->Draw();
	  ff_dp_all2->SetLineColor(kGreen-3);
	  ff_dp_all2->SetMarkerColor(kGreen-3);
	  ff_dp_all2->Draw("same");
	  cout<<"phi"<<ff_dp_all-> GetXaxis()->GetNbins()<<"\t"<<ff_dp_all->GetEntries()<<endl;

	  // Put info on plot
	  ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 5"), 0.6);

      if( saveEPS )cdp_all->SaveAs(Form("All_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      cdp_all->SaveAs(Form("All_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      delete cdp_all;
      // dEta
      TCanvas *cdall = new TCanvas(Form("cdall_%d",g),Form("c_%d",g),800,700);
      TProfile *ff_dall = h_deta_t[g][4][0]->ProfileX(Form("ff_dall%d0",g),-1,-1,"o");
      TProfile *ff_dall2 = h_deta_t[g][4][1]->ProfileX(Form("ff_dall%d1",g),-1,-1,"o");
      ff_dall->GetXaxis()->SetRangeUser(-0.6,0.6);
      ff_dall->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_dall->GetYaxis()->SetTitle("Time [ns]");
      ff_dall->Draw();
      ff_dall2->SetLineColor(kGreen-3);
      ff_dall2->SetMarkerColor(kGreen-3);
      ff_dall2->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 5"), 0.6);

      if( saveEPS )cdall->SaveAs(Form("All_deta_pass4_pass5_%s.png",gain[g].c_str()));
      cdall->SaveAs(Form("all_deta_pass4_pass5_%s.png",gain[g].c_str()));

      delete cdall;
     
    // Make summary plots EMB
       // dPhi
      TCanvas *cdp = new TCanvas(Form("cdp_%d",g),Form("c_%d",g),800,700);
      TProfile *ff_dp = h_dphi_t[g][0][0]->ProfileX(Form("ff_dp%d0",g),-1,-1,"o");
      TProfile *ff_dpc = h_dphi_t[g][1][0]->ProfileX(Form("ff_dpc%d0",g),-1,-1,"o");
      TProfile *ff_dp2 = h_dphi_t[g][0][1]->ProfileX(Form("ff_dp%d1",g),-1,-1,"o");
      TProfile *ff_dp2c = h_dphi_t[g][1][1]->ProfileX(Form("ff_dpc%d1",g),-1,-1,"o");
      ff_dp->Add(ff_dpc);
      ff_dp2->Add(ff_dp2c);
      ff_dp->Rebin(2);
      ff_dp2->Rebin(2);
      ff_dp->GetXaxis()->SetRangeUser(-1,1);
      ff_dp->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_dp->GetYaxis()->SetTitle("Time [ns]");
      ff_dp->Draw();
      ff_dp2->SetLineColor(kGreen-3);
      ff_dp2->SetMarkerColor(kGreen-3);
      ff_dp2->Draw("same");

      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen, Form("Pass 5"), 0.6);

      if( saveEPS )cdp->SaveAs(Form("EMB_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      cdp->SaveAs(Form("EMB_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      delete cdp;

      // dEta
      TCanvas *cde = new TCanvas(Form("cde_%d",g),Form("c_%d",g),800,700);
      TProfile *ff_de = h_deta_t[g][0][0]->ProfileX(Form("ff_de%d0",g),-1,-1,"o");
      TProfile *ff_dec = h_deta_t[g][1][0]->ProfileX(Form("ff_dec%d0",g),-1,-1,"o");
      TProfile *ff_de2 = h_deta_t[g][0][1]->ProfileX(Form("ff_de%d1",g),-1,-1,"o");
      TProfile *ff_de2c = h_deta_t[g][1][1]->ProfileX(Form("ff_dec%d1",g),-1,-1,"o");
      ff_de->Add(ff_dec);
      ff_de2->Add(ff_de2c);
      ff_de->GetXaxis()->SetRangeUser(-0.6,0.6);
      ff_de->GetYaxis()->SetRangeUser(-.1,.2);
      ff_de->GetYaxis()->SetTitle("Time [ns]");
      ff_de->Draw();
      ff_de2->SetLineColor(kGreen-3);
      ff_de2->SetMarkerColor(kGreen-3);
      ff_de2->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen, Form("Pass 5"), 0.6);

      if( saveEPS )cde->SaveAs(Form("EMB_deta_pass4_pass5_%s.png",gain[g].c_str()));
      cde->SaveAs(Form("EMB_deta_pass4_pass5_%s.png",gain[g].c_str()));

      delete cde;
     
     
      // Make summary plots EMEC
       // dPhi
      TCanvas *cdpb = new TCanvas(Form("cdpb_%d",g),Form("cb_%d",g),800,700);
      TProfile *ff_dpb = h_dphi_t[g][2][0]->ProfileX(Form("ff_dp%d0",g),-1,-1,"o");
      TProfile *ff_dpcb = h_dphi_t[g][3][0]->ProfileX(Form("ff_dpc%d0",g),-1,-1,"o");
      TProfile *ff_dp2b = h_dphi_t[g][2][1]->ProfileX(Form("ff_dp%d1",g),-1,-1,"o");
      TProfile *ff_dp2cb = h_dphi_t[g][3][1]->ProfileX(Form("ff_dpc%d1",g),-1,-1,"o");
      ff_dpb->Add(ff_dpcb);
      ff_dp2b->Add(ff_dp2cb);
      ff_dpb->Rebin(2);
      ff_dp2b->Rebin(2);
      ff_dpb->GetXaxis()->SetRangeUser(-1,1);
      ff_dpb->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_dpb->GetYaxis()->SetTitle("Time [ns]");
      ff_dpb->Draw();
      ff_dp2b->SetLineColor(kGreen-3);
      ff_dp2b->SetMarkerColor(kGreen-3);
      ff_dp2b->Draw("same");

      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 5"), 0.6);

      if( saveEPS )cdpb->SaveAs(Form("EMEC_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      cdpb->SaveAs(Form("EMEC_dphi_pass4_pass5_%s.png",gain[g].c_str()));
      delete cdpb;

      // dEta
      TCanvas *cdeb = new TCanvas(Form("cdeb_%d",g),Form("c_%d",g),800,700);
      TProfile *ff_deb = h_deta_t[g][2][0]->ProfileX(Form("ff_de%d0",g),-1,-1,"o");
      TProfile *ff_decb = h_deta_t[g][3][0]->ProfileX(Form("ff_dec%d0",g),-1,-1,"o");
      TProfile *ff_de2b = h_deta_t[g][2][1]->ProfileX(Form("ff_de%d1",g),-1,-1,"o");
      TProfile *ff_de2cb = h_deta_t[g][3][1]->ProfileX(Form("ff_dec%d1",g),-1,-1,"o");
      ff_deb->Add(ff_decb);
      ff_de2b->Add(ff_de2cb);
      ff_deb->GetXaxis()->SetRangeUser(-0.6,0.6);
      ff_deb->GetYaxis()->SetRangeUser(-.1,.2);
      ff_deb->GetYaxis()->SetTitle("Time [ns]");
      ff_deb->Draw();
      ff_de2b->SetLineColor(kGreen-3);
      ff_de2b->SetMarkerColor(kGreen-3);
      ff_de2b->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 4"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 5"), 0.6);

      if( saveEPS )cdeb->SaveAs(Form("EMEC_deta_pass4_pass5_%s.png",gain[g].c_str()));
      cdeb->SaveAs(Form("EMEC_deta_pass4_pass5_%s.png",gain[g].c_str()));

      delete cdeb;
  }// end loop over gains

  file->Close();
  return;
}


//_____________________________________________
//
// Plot avg time vs f1/f3 
// by subdetector/slot
//_____________________________________________
void doEFracPlot(){
  
  std::cout << "\nMaking plots for fractional energy deposit timing\n\n";
  
  // Initialize the histograms
  for( int i=0; i<5; i++){
    for( int j=0; j<2; j++){
      for( int k=0; k<2; k++){
        h_f1_t[j][i][k]  = new TH2F(Form("h_f1_t%d%d%d",i,j,k),Form("Cell Time %s; f1; Time [ns]",subDet[i].c_str()), 400,-0.05, .6, 500, -5, 5);//220,-0.10,1.,500,-5,5);
        h_f3_t[j][i][k]  = new TH2F(Form("h_f3_t%d%d%d",i,j,k),Form("Cell Time %s; f3; Time [ns]",subDet[i].c_str()), 400,-0.05, .2, 500, -5, 5);
      }
    }
  }
  
  // Open the data file
  TFile *file;
  TFile *file2;
  file = TFile::Open(Form("../files/2016_all/pass5.root"),"READ");
//  fileb = TFile::Open(Form("../files/IOV_2/pass5.root"),"READ");
//  file2 = TFile::Open(Form("../files/2016_all/pass6.root"),"READ");
  file2 = TFile::Open(Form("../files/pass6.root"),"READ");
//  file2b = TFile::Open(Form("../files/IOV_2/pass6.root"),"READ");
  
//  // Loop over gains
  for( int g=0; g<2; g++){  
//    // Loop over slots
    for(int sl=0; sl<22; sl++){
//     
      // Make names for the histograms to open
      std::string f1name  = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),sl);
      std::string f3name  = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),sl);
//      
      TH2F *t_f1    = (TH2F*)file->Get(f1name.c_str());
//      TH2F *t_f1b    = (TH2F*)fileb->Get(f1name.c_str());
      TH2F *t_f1_2  = (TH2F*)file2->Get(f1name.c_str());
//      TH2F *t_f1_2b  = (TH2F*)file2b->Get(f1name.c_str());
      TH2F *t_f3    = (TH2F*)file->Get(f3name.c_str());
//      TH2F *t_f3b    = (TH2F*)fileb->Get(f3name.c_str());
      TH2F *t_f3_2  = (TH2F*)file2->Get(f3name.c_str());
//      TH2F *t_f3_2b  = (TH2F*)file2b->Get(f3name.c_str());
     
      if( !t_f1->GetEntries() ) {
      std::cout << "  >> Slot index: " << sl << " has no data for " 
                  << gain[g] << " gain, no plots will be made!\n";
      continue;
      }

//      // Summary plots
      h_f1_t[g][4][0]->Add(t_f1);
//      h_f1_t[g][4][0]->Add(t_f1b);
      h_f1_t[g][4][1]->Add(t_f1_2);
//      h_f1_t[g][4][1]->Add(t_f1_2b);
      h_f3_t[g][4][0]->Add(t_f3);
//      h_f3_t[g][4][0]->Add(t_f3b);
      h_f3_t[g][4][1]->Add(t_f3_2);
//      h_f3_t[g][4][1]->Add(t_f3_2b);

      if( sl < 4 || sl == 8 ){ 
        h_f1_t[g][0][0]->Add(t_f1);
//        h_f1_t[g][0][0]->Add(t_f1b);
        h_f1_t[g][0][1]->Add(t_f1_2);
//        h_f1_t[g][0][1]->Add(t_f1_2b);
        h_f3_t[g][0][0]->Add(t_f3);
//        h_f3_t[g][0][0]->Add(t_f3b);
        h_f3_t[g][0][1]->Add(t_f3_2);
//        h_f3_t[g][0][1]->Add(t_f3_2b);
      }
     else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
        h_f1_t[g][1][0]->Add(t_f1);
//        h_f1_t[g][1][0]->Add(t_f1b);
        h_f1_t[g][1][1]->Add(t_f1_2);
//        h_f1_t[g][1][1]->Add(t_f1_2b);
        h_f3_t[g][1][0]->Add(t_f3);
//        h_f3_t[g][1][0]->Add(t_f3b);
        h_f3_t[g][1][1]->Add(t_f3_2);
//        h_f3_t[g][1][1]->Add(t_f3_2b);
      }
      else if ( sl > 9 && sl < 16 ){
        h_f1_t[g][2][0]->Add(t_f1);
//        h_f1_t[g][2][0]->Add(t_f1b);
        h_f1_t[g][2][1]->Add(t_f1_2);
//        h_f1_t[g][2][1]->Add(t_f1_2b);
        h_f3_t[g][2][0]->Add(t_f3);
//        h_f3_t[g][2][0]->Add(t_f3b);
        h_f3_t[g][2][1]->Add(t_f3_2);
//        h_f3_t[g][2][1]->Add(t_f3_2b);
      }
      else if ( sl > 15 ){
        h_f1_t[g][3][0]->Add(t_f1);
//        h_f1_t[g][3][0]->Add(t_f1b);
        h_f1_t[g][3][1]->Add(t_f1_2);
//        h_f1_t[g][3][1]->Add(t_f1_2b);
        h_f3_t[g][3][0]->Add(t_f3);
//        h_f3_t[g][3][0]->Add(t_f3b);
        h_f3_t[g][3][1]->Add(t_f3_2);
//        h_f3_t[g][3][1]->Add(t_f3_2b);
      }
//          
    }// end loop over slots
    
  // Make summary plots EMB
  double nFrac =  ( g == 0 ? 100 : 75);  
 // if(g==1) continue;
    // double nFrac =10;
  // f1
 
      TCanvas *cf1 = new TCanvas(Form("cf1_%d",g),Form("cf1_%d",g),800,700);
    // rebin
      h_f1_t[g][0][0]->Add(h_f1_t[g][1][0]);
      h_f1_t[g][0][1]->Add(h_f1_t[g][1][1]);
      h_f1_t[g][0][0] = rebin( h_f1_t[g][0][0], h_f1_t[g][0][0]->GetEntries()/nFrac, h_f1_t[g][0][0]->GetXaxis()->FindBin(0.)-1);
      h_f1_t[g][0][1] = rebin( h_f1_t[g][0][1], h_f1_t[g][0][1]->GetEntries()/nFrac, h_f1_t[g][0][1]->GetXaxis()->FindBin(0.)-1);
      //TProfile *ff_f1= new TProfile();
     // TProfile *ff_f1_2 =new TProfile();
      TProfile *ff_f1 = h_f1_t[g][0][0]->ProfileX(Form("ff_f1%d0",g),-1,-1,"o");
      TProfile *ff_f1_2 = h_f1_t[g][0][1]->ProfileX(Form("ff_f1%d1",g),-1,-1,"o");
    //  ff_f1->Sumw2();
   //   ff_f1_2->Sumw2();
  //    ff_f1=h_f1_t[g][0][0]->ProfileX();//Form("ff_f1%d0",g),-1,-1,"o");
      ff_f1->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f1->GetXaxis()->SetRangeUser(0,.6);
      ff_f1->GetYaxis()->SetTitle("Time [ns]");
    //  ff_f1_2 = h_f1_t[g][0][1]->ProfileX();//Form("ff_f1%d1",g),-1,-1,"o");
      ff_f1_2->SetLineColor(kGreen-3);
      ff_f1_2->SetMarkerColor(kGreen-3);
      ff_f1->Draw();
      ff_f1_2->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);
	cout<<"emb"<<g<<"\t"<<ff_f1->GetEntries()<<endl;
      if( saveEPS )cf1->SaveAs(Form("EMB_f1_pass5_pass6_%s.png",gain[g].c_str()));
          cf1->SetLogx(1);  
      
      cf1->SaveAs(Form("EMB_f1_pass5_pass6_%s.png",gain[g].c_str()));
      delete cf1;
      // f3
      TCanvas *cf3 = new TCanvas(Form("cf3_%d",g),Form("cf3_%d",g),800,700);
      // rebin
      h_f3_t[g][0][0]->Add( h_f3_t[g][1][0]);
      h_f3_t[g][0][1]->Add( h_f3_t[g][1][1]);
     h_f3_t[g][0][0] = rebin( h_f3_t[g][0][0], h_f3_t[g][0][0]->GetEntries()/nFrac, h_f3_t[g][0][0]->GetXaxis()->FindBin(0.)-1);
     h_f3_t[g][0][1] = rebin( h_f3_t[g][0][1], h_f3_t[g][0][1]->GetEntries()/nFrac, h_f3_t[g][0][1]->GetXaxis()->FindBin(0.)-1);
      TProfile *ff_f3 = h_f3_t[g][0][0]->ProfileX(Form("ff_f3%d0",g),-1,-1,"o");
      TProfile *ff_f3_2 = h_f3_t[g][0][1]->ProfileX(Form("ff_f3%d1",g),-1,-1,"o");
      ff_f3->GetYaxis()->SetRangeUser(tmin,tmax);
      //ff_f3->GetXaxis()->SetRangeUser(-.05,.1);
      ff_f3->GetXaxis()->SetRangeUser(-1e-3,.2);
      ff_f3->GetYaxis()->SetTitle("Time [ns]");
      ff_f3->Draw("g");
      ff_f3_2->SetLineColor(kGreen-3);
      ff_f3_2->SetMarkerColor(kGreen-3);
      ff_f3_2->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);
      cf3->SetLogx(1);

      if( saveEPS )cf3->SaveAs(Form("EMB_f3_pass5_pass6_%s.png",gain[g].c_str()));
      cf3->SaveAs(Form("EMB_f3_pass5_pass6_%s.png",gain[g].c_str()));
      
      delete cf3;
      // Make summary plots EMEC
    // f1
      TCanvas *cf1b = new TCanvas(Form("cf1b_%d",g),Form("cf1b_%d",g),800,700);
      // rebin
      h_f1_t[g][2][0]->Add(h_f1_t[g][3][0]);
      h_f1_t[g][2][1]->Add(h_f1_t[g][3][1]);
      h_f1_t[g][2][0] = rebin( h_f1_t[g][2][0], h_f1_t[g][2][0]->GetEntries()/nFrac, h_f1_t[g][2][0]->GetXaxis()->FindBin(0.)-1);
      h_f1_t[g][2][1] = rebin( h_f1_t[g][2][1], h_f1_t[g][2][1]->GetEntries()/nFrac, h_f1_t[g][2][1]->GetXaxis()->FindBin(0.)-1);
      TProfile *ff_f1b = h_f1_t[g][2][0]->ProfileX(Form("ff_f1%d0",g),-1,-1,"o");
      TProfile *ff_f1_2b = h_f1_t[g][2][1]->ProfileX(Form("ff_f1%d1",g),-1,-1,"o");
	cout<<"emec"<<g<<"\t"<<ff_f1b->GetEntries()<<endl;
      ff_f1b->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f1b->GetXaxis()->SetRangeUser(0,.6);
      ff_f1b->GetYaxis()->SetTitle("Time [ns]");
      ff_f1b->Draw("g");
      ff_f1_2b->SetLineColor(kGreen-3);
      ff_f1_2b->SetMarkerColor(kGreen-3);
      ff_f1_2b->Draw("same");

      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);

      if( saveEPS )cf1b->SaveAs(Form("EMEC_f1_pass5_pass6_%s.png",gain[g].c_str()));
      cf1b->SaveAs(Form("EMEC_f1_pass5_pass6_%s.png",gain[g].c_str()));
      delete cf1b;
      // f3
      TCanvas *cf3b = new TCanvas(Form("cf3b_%d",g),Form("cf3_%d",g),800,700);
      // rebin
      h_f3_t[g][2][0]->Add( h_f3_t[g][3][0]);
      h_f3_t[g][2][1]->Add( h_f3_t[g][3][1]);
      h_f3_t[g][2][0] = rebin( h_f3_t[g][2][0], h_f3_t[g][2][0]->GetEntries()/nFrac, h_f3_t[g][2][0]->GetXaxis()->FindBin(0.)-1);
      h_f3_t[g][2][1] = rebin( h_f3_t[g][2][1], h_f3_t[g][2][1]->GetEntries()/nFrac, h_f3_t[g][2][1]->GetXaxis()->FindBin(0.)-1);
      TProfile *ff_f3b = h_f3_t[g][2][0]->ProfileX(Form("ff_f3%d0",g),-1,-1,"o");
      TProfile *ff_f3_2b = h_f3_t[g][2][1]->ProfileX(Form("ff_f3%d1",g),-1,-1,"o");
      ff_f3b->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f3b->GetXaxis()->SetRangeUser(0,.2);
      ff_f3b->GetYaxis()->SetTitle("Time [ns]");
      ff_f3b->Draw("g");
      ff_f3_2b->SetLineColor(kGreen-3);
      ff_f3_2b->SetMarkerColor(kGreen-3);
      ff_f3_2b->Draw("same");
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);
      
      cf3b->SetLogx(1);

      if( saveEPS )cf3b->SaveAs(Form("EMEC_f3_pass5_pass6_%s.png",gain[g].c_str()));
      cf3b->SaveAs(Form("EMEC_f3_pass5_pass6_%s.png",gain[g].c_str()));
      
      delete cf3;

	//all f1
      TCanvas *cf1all = new TCanvas(Form("cf1all_%d",g),Form("cf1all_%d",g),800,700);
      // rebin
     h_f1_t[g][4][0] = rebin( h_f1_t[g][4][0], h_f1_t[g][4][0]->GetEntries()/nFrac, h_f1_t[g][4][0]->GetXaxis()->FindBin(0.)-1);
     h_f1_t[g][4][1] = rebin( h_f1_t[g][4][1], h_f1_t[g][4][1]->GetEntries()/nFrac, h_f1_t[g][4][1]->GetXaxis()->FindBin(0.)-1);
     h_f1_t[g][4][0]->GetXaxis()->SetRangeUser(0,.6); 
      TProfile *ff_f1all = h_f1_t[g][4][0]->ProfileX(Form("ff_f1%d0",g),-1,-1,"o");
      TProfile *ff_f1_2all = h_f1_t[g][4][1]->ProfileX(Form("ff_f1%d1",g),-1,-1,"o");
      ff_f1all->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f1all->GetXaxis()->SetRangeUser(0,.6);
      ff_f1all->GetYaxis()->SetTitle("Time [ns]");
      
      cout<<"f1"<<ff_f1all-> GetXaxis()->GetNbins()<<"\t"<<ff_f1all->GetEntries()<<endl;
     // ff_f1all->Sumw2();
      ff_f1all->Draw("g");
      ff_f1_2all->SetLineColor(kGreen-3);
      ff_f1_2all->SetMarkerColor(kGreen-3);
      ff_f1_2all->Draw("same");
//
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);

      if( saveEPS )cf1all->SaveAs(Form("All_f1_pass5_pass6_%s.png",gain[g].c_str()));
      cf1all->SaveAs(Form("All_f1_pass5_pass6_%s.png",gain[g].c_str()));
      delete cf1all;
	//all f3
      TCanvas *cf3all = new TCanvas(Form("cf3all_%d",g),Form("cf3all_%d",g),800,700);
      // rebin
      h_f3_t[g][4][0] = rebin( h_f3_t[g][4][0], h_f3_t[g][4][0]->GetEntries()/nFrac, h_f3_t[g][4][0]->GetXaxis()->FindBin(0.)-1);
      h_f3_t[g][4][1] = rebin( h_f3_t[g][4][1], h_f3_t[g][4][1]->GetEntries()/nFrac, h_f3_t[g][4][1]->GetXaxis()->FindBin(0.)-1);
      TProfile *ff_f3all = h_f3_t[g][4][0]->ProfileX(Form("ff_f3%d0",g),-1,-1,"o");
      TProfile *ff_f3_2all = h_f3_t[g][4][1]->ProfileX(Form("ff_f3%d1",g),-1,-1,"o");
      ff_f3all->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f3all->GetXaxis()->SetRangeUser(0,.2);
      ff_f3all->GetYaxis()->SetTitle("Time [ns]");
      ff_f3all->Draw("g");
      ff_f3_2all->SetLineColor(kGreen-3);
      ff_f3_2all->SetMarkerColor(kGreen-3);
      ff_f3_2all->Draw("same");

      cf3all->SetLogx(1);
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
      myText(0.22, 0.72, kBlack, Form("EMB+EMEC"), 0.7);
      myText(0.73, 0.83, kBlack, Form("%s Gain",gain[g].c_str()), 0.7);
      myText(0.73, 0.79, kBlack, Form("Pass 5"), 0.6);
      myText(0.73, 0.76, kGreen-3, Form("Pass 6"), 0.6);

      if( saveEPS )cf3all->SaveAs(Form("All_f3_pass5_pass6_%s.png",gain[g].c_str()));
      cf3all->SaveAs(Form("All_f3_pass5_pass6_%s.png",gain[g].c_str()));
      delete cf3all;
  }// end loop over gains

  file->Close();
  return;
}


//_____________________________________________
//
// Plot avg Cell time 
// by subdetector/slot
//_____________________________________________
void doCellTimePlot(){
  
  std::cout << "\nMaking plots for average Cell Timing\n\n";
  // FIXME ill advised loo
  //for( int zzz = 0; zzz < 8; zzz++){
  //std::cout << "Pass " << zzz << std::endl;
 
  // Initialize the histograms
  for( int i=0; i<5; i++){
    for( int j=0; j<3; j++){
      for(int k=0; k<2; k++){
        h_cell_t[j][i][k] = new TH1F(Form("h_cell_t%d%d%d",i,j,k),Form("Cell Time %s; Time [ns]; Events / 0.02 ns",subDet[i].c_str()),1000,-25,25);
      }
    }
  }
  
  // Open the data file
  TFile *file;
//  TFile *fileb;
  TFile *file2;
  file  = TFile::Open("../files/2016_all/pass0.root","READ"); 
  //fileb = TFile::Open("../files/IOV_2/pass0.root","READ"); 
  //file  = TFile::Open(Form("files/2016_all/pass%d.root",zzz),"READ"); 
  //fileb = TFile::Open(Form("files/IOV_2/pass%d.root",zzz),"READ"); 
  //file2 = TFile::Open("../files/combinedZ/pass8_comb.root","READ"); 
  // For pass7 FIXME
  TFile *file3;
  //TFile *file3b;
  file3  = TFile::Open("../files/2016_all/pass7.root","READ"); 
//  file3b = TFile::Open("../files/IOV_2/pass7.root","READ"); 
  
  // Loop over gains
  for( int g=0; g<2; g++){  
    // Loop over slots
    for(int sl=0; sl<22; sl++){
     
      // Make names for the histograms to open
      std::string tname = Form("h_cellTimeAll%s%d",gain[g].c_str(),sl);
      TH1F *f_t = (TH1F*)file->Get(tname.c_str());
 //     TH1F *f_tb = (TH1F*)fileb->Get(tname.c_str());
      // FIXME put back f_t2
      //TH1F *f_t2 = (TH1F*)file2->Get(tname.c_str());
      TH1F *f_t2 = (TH1F*)file3->Get(tname.c_str());
 //     TH1F *f_t2 = (TH1F*)file3b->Get(tname.c_str());
 //     f_t2->Add(f_t3);

      if( !f_t->GetEntries() ) {
        std::cout << "  >> Slot index: " << sl << " has no data for " 
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }
      
      // Summary plots
      h_cell_t[g][4][0]->Add(f_t);
////      h_cell_t[g][4][0]->Add(f_tb);
      h_cell_t[g][4][1]->Add(f_t2);
      if( sl < 4 || sl == 8 ){ 
        h_cell_t[g][0][0]->Add(f_t);
//        h_cell_t[g][0][0]->Add(f_tb);
        h_cell_t[g][0][1]->Add(f_t2);
      }
      else if ( (sl > 3 && sl < 8) || sl == 9 ) {
        h_cell_t[g][1][0]->Add(f_t);
 //       h_cell_t[g][1][0]->Add(f_tb);
        h_cell_t[g][1][1]->Add(f_t2);
      }
      else if ( sl > 9 && sl < 16 ){
        h_cell_t[g][2][0]->Add(f_t);
 //       h_cell_t[g][2][0]->Add(f_tb);
        h_cell_t[g][2][1]->Add(f_t2);
        }
      else if ( sl > 15 ){
        h_cell_t[g][3][0]->Add(f_t);
 //       h_cell_t[g][3][0]->Add(f_tb);
        h_cell_t[g][3][1]->Add(f_t2);
      }
    } // end loop over slots

//    // Make summary plots
    for( int dd=0; dd<3; dd++){
      int d;
      if( dd == 0 ){ 
        d = 0;
        h_cell_t[g][d][0]->Add( h_cell_t[g][d+1][0]);
        h_cell_t[g][d][1]->Add( h_cell_t[g][d+1][1]);
      }else if( dd == 1){
        d = 2;
        h_cell_t[g][d][0]->Add( h_cell_t[g][d+1][0]);
        h_cell_t[g][d][1]->Add( h_cell_t[g][d+1][1]);
      }else if( dd == 2){
        d = 4;
      }
      // separate plots
      // pass0
      TCanvas *gg = new TCanvas(Form("c_gg%d_%d",g,d),"ggg",800,700);
      h_cell_t[g][d][0]->Draw();
      double rms  = h_cell_t[g][d][0]->GetRMS();
      double mean = h_cell_t[g][d][0]->GetMean();
      int entries = h_cell_t[g][d][0]->GetEntries();
      
      //FIXME fit info
      //if (d ==0){
      //  std::cout << "EMB:   Mean: " << mean <<"; RMS: " <<  rms << std::endl;
     // }
      //if( d== 2){ 
       // std::cout << "EMEC:  Mean: " << mean <<"; RMS: " <<  rms << std::endl;
     // }
     if(g==2)
	    cout<<"findme"<<d<<"\t"<<entries<<endl;
      
      myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
      myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",h_cell_t[g][d][0]->GetMean()),0.6);
      myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",h_cell_t[g][d][0]->GetRMS()),0.6);
      h_cell_t[g][d][0] = fitGaus( h_cell_t[g][d][0], mean, rms,.72 );
      ATLASLabel(0.2, 0.88,pInternal);
      if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
      else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
      else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
      myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
      myText(0.22, 0.72,kBlack,Form("Pass 0 (W#rightarrow e#nu)"),0.6); 
      double maxY = h_cell_t[g][d][0]->GetBinContent( h_cell_t[g][d][0]->GetMaximumBin() );
      h_cell_t[g][d][0]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
      gg->SetLogy(1);
      if( d== 0)gg->SaveAs(Form("Log_pass0_EMB_Celltime_%s.png",gain[g].c_str())); 
      else if( d==2)gg->SaveAs(Form("Log_pass0_EMEC_Celltime_%s.png",gain[g].c_str())); 
      else if ( d==4)gg->SaveAs(Form("Log_pass0_All_Celltime_%s.png",gain[g].c_str()));
      delete gg;


      // pass8
      TCanvas *ggg = new TCanvas(Form("c_ggg%d_%d",g,d),"ggg",800,700);
      h_cell_t[g][d][1]->Draw();
      rms  = h_cell_t[g][d][1]->GetRMS();
      mean = h_cell_t[g][d][1]->GetMean();
      entries = h_cell_t[g][d][1]->GetEntries();
      myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
      myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",h_cell_t[g][d][1]->GetMean()),0.6);
      myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",h_cell_t[g][d][1]->GetRMS()),0.6);
      h_cell_t[g][d][1] = fitGaus( h_cell_t[g][d][1], mean, rms,.72 );
      ATLASLabel(0.2, 0.88,pInternal);
      if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
      else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
      else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
      myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
      //myText(0.22, 0.72,kBlack,Form("Pass 8 (Z#rightarrow ee)"),0.6); FIXME
      myText(0.22, 0.72,kBlack,Form("Pass 7 (W#rightarrow e#nu)"),0.6);
      maxY = h_cell_t[g][d][1]->GetBinContent( h_cell_t[g][d][1]->GetMaximumBin() );
      h_cell_t[g][d][1]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
      ggg->SetLogy(1);
      if( d== 0)ggg->SaveAs(Form("Log_pass7_EMB_Celltime_%s.png",gain[g].c_str()));
      else if( d==2)ggg->SaveAs(Form("Log_pass7_EMEC_Celltime_%s.png",gain[g].c_str()));
      else if ( d==4)ggg->SaveAs(Form("Log_pass7_All_Celltime_%s.png",gain[g].c_str()));
      delete ggg;

      // Both on same plot
      h_cell_t[g][d][0]->GetXaxis()->SetRangeUser(-15,15);

      TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      h_cell_t[g][d][0]->Draw();
      rms  = h_cell_t[g][d][0]->GetRMS();
      mean = h_cell_t[g][d][0]->GetMean();
      entries = h_cell_t[g][d][0]->GetEntries();
      myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
      h_cell_t[g][d][0] = fitGaus( h_cell_t[g][d][0], mean, rms,.78 );
      h_cell_t[g][d][1]->SetLineColor(kGreen-3);
      h_cell_t[g][d][1]->Draw("same");
      rms  = h_cell_t[g][d][1]->GetRMS();
      mean = h_cell_t[g][d][1]->GetMean();
   //   entries = h_cell_t[g][d][1]->GetEntries();
      h_cell_t[g][d][1]->Scale(entries/h_cell_t[g][d][1]->GetEntries());
      //myText(0.73, 0.74,kGreen-3,Form("Entries: %d",entries),0.6); //FIXME uncomment for pass8
     h_cell_t[g][d][1] = fitGaus( h_cell_t[g][d][1], mean, rms,0.68 ,413);
      
      ATLASLabel(0.2, 0.88,pInternal);
      if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
      else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
      else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
      myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
      myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
      myText(0.22, 0.72,kBlack,Form("Pass 0 (W#rightarrow e#nu)"),0.6);
      //myText(0.22, 0.68,kGreen-3,Form("Pass 8 (Z#rightarrow ee)"),0.6); // FIXME
      myText(0.22, 0.68,kGreen-3,Form("Pass 7 (W#rightarrow e#nu)"),0.6);
      
      maxY = h_cell_t[g][d][0]->GetBinContent( h_cell_t[g][d][0]->GetMaximumBin() );
      h_cell_t[g][d][0]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
     c3->SetLogy();
      // FIXME
      if( d== 0)c3->SaveAs(Form("Log_pass0_pass7_EMB_Celltime_%s.png",gain[g].c_str()));
      else if( d==2)c3->SaveAs(Form("Log_pass0_pass7_EMEC_Celltime_%s.png",gain[g].c_str()));
      else if ( d==4)c3->SaveAs(Form("Log_pass0_pass7_All_Celltime_%s.png",gain[g].c_str()));
      delete c3;

    }
  
  } // end loop over gains

  file->Close();
  file3->Close();
 // file2->Close();

 // } // end illadvised loop

  
  return;
}






//_____________________________________________
//
// Set PassNumber and mode
//_____________________________________________
void setPass(int pn, int mode){
  
  passNumber  = pn;
  sPassNumber = Form("pass%d",passNumber);
  switch(mode){
    case 0: 
      doPlot = true;
      doCorr = true;
      break;
    case 1:
      doPlot = true;
      doCorr = false;
      break;
    case 2:
      doPlot = false;
      doCorr = true;
      break;
  }

  std::cout << "  > Pass number has been set: " << passNumber << std::endl;
  std::cout << "  > doPlot: " << doPlot << std::endl;
  std::cout << "  > doCorr: " << doCorr << std::endl;

  return;
}


//_____________________________________________
//
// Read list of run numbers from config file
//_____________________________________________
void setRunList(){
  
  // Run number config file
  ifstream f1("../config/RunNumberList.txt");
  int tempRunNum;

  // Store each run into local vector
  while( f1 >> tempRunNum ){
      runNumberList.push_back(tempRunNum);
  }
  
  f1.close();

  std::cout << "  > Added " << runNumberList.size() << " runs successfully from "
            << runNumberList[0] << " to " << runNumberList.back() << "\n\n";
  return;
}


//_____________________________________________
//
// Set tmin/tmax for plotting
//_____________________________________________
void setTRange(double t_l, double t_h){
  
  tmin = t_l;
  tmax = t_h;
  
  return;
}


//_____________________________________________
//
// Make a gaussian fit for sub range,
// Show dotted line for full range,
// put fit params
//_____________________________________________
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y, int color ){
  
  // Hide fit stats
  gStyle->SetOptFit(0);
 
  // Gaus fit (limited range around peak)
  TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
  //fg->SetParameter(0,0); // not sure if this is necessary
  
  // guess starting point for fit
  fg->SetParameter(1,mean);
  fg->SetParameter(2,rms);
  
  // put limits on the paramters
  fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
  fg->SetParLimits(2,0.5*rms,2*rms);
  
  // Draw the fit in subrange
  fg->SetLineColor(kRed);
  h_cell->Fit("fg","BR");
  fg->Draw("same");
  
  // Make the fit extend and draw as dotted line
  TF1 *f2 = new TF1("f2","gaus",-25,25);
  f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
  f2->SetLineColor(kRed);
  f2->SetLineStyle(7); //dashed line
  f2->Draw("same");
  
  // Put parameters on the plot
  double pos_x = 0.73;
  myText(pos_x, pos_y,color,Form("#mu: %.3f ns",fg->GetParameter(1)),0.8);
  myText(pos_x, pos_y-0.04,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.8);
 

  //FIXME printing info
  //std::cout << "    Mu: " << fg->GetParameter(1) << "; Sigma: "<<fg->GetParameter(2) <<std::endl;
  return h_cell;

}


// Rebin the 2D histogram so that each bin
// has at least entPerBin entries
// firstBin is the first bin you want to consider
//_____________________________________________
TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

  // hold the bin edge information
  std::vector< double > xbins;
  // Combine all bins below first bin
  // Cut at 5 GeV so this bin is empty
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

  // keep track of last bin with at least entPerBin
  int lastFullIndex = firstBin; // for energy 5
  // Get the xaxis
  TAxis *axis = h->GetXaxis();

  // Loop over bins in xaxis
  // start after 5GeV bin
  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {
    // Get entries in this bin, and width
    int y = h->Integral(i,i);
    double w = axis->GetBinWidth(i);

    // If not enough entries, need to combine bins
    if (y <= entPerBin){
      // Find integral from last combined bin
      double integral = h->Integral(lastFullIndex+1, i);
      if (integral <= entPerBin ) continue;
      // if above threshold, mark as new bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w);
    }
    else{
      // above threshold, mark as bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w );
    }

  }

  // put bin edges into an array
  xbins.push_back( axis->GetXmax() );
  size_t s = xbins.size();
  double *xbinsFinal = &xbins[0];
cout<<"s"<<s<<endl;
  // create new histo with new bin edges
  TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
  hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());

hnew->Sumw2();
 // cout<<
  // fill new histo with old values
  for( int i=1; i<=h->GetNbinsX(); i++){
    for( int j=1; j<=h->GetNbinsY(); j++){
	    for(int a=1;a<=h->GetBinContent(i,j);a++)
      //hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j), h->GetBinContent(i,j));
      hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
  //    if(i==200)
//	      cout<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<"\t"<<h->GetBinError(i,j)<<endl;
     // hnew->
//if(i<=h->GetNbinsX()/2&&i>=h->GetNbinsX()/2-1)
    }
  }
//cout<<"h"<<"\t"<<h->GetEntries()<<endl;
//cout<<"h"<<"\t"<<h->GetBinContent(200,250)<<endl;
//or(j =1; j<=hnew->GetNbinsY();j++)
//	if( hnew-> GetBinContent(38,j)!=0)
//cout<<"h_new"<<"\t"<<hnew->GetXaxis()->GetBinCenter(38)<<"\t"<<""<<endl;
//cout<<"h_new"<<"\t"<<hnew->GetEntries()<<endl;
//
//double stats[7];
//hnew->GetStats(stats);
//for(i=0;i<7;i++)
//	cout<<"star"<<stats[7]<<endl;
  TH2F* h_return = (TH2F*)hnew->Clone("");
  delete hnew;

  // return histo
  return h_return;
}





//__________________________________________
//
// Put info for fit plots
// g=gain sl=slot index
//__________________________________________
void drawFitInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

  return;

}


//____________________________________________
//
// Put info for run by run plots
// g=gain, rn = run number index
//____________________________________________
void drawRunInfo(int g, int rn){
  
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

  return;

}

//_________________________________________
//
// Put info for slot by slot plots
// g=gain index, sl=slot index
//_________________________________________
void drawSlInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
  
  return;

}
//_________________________________________
//
// Put info for subdetector summary plots
// g=gain index, d=subdetector index
//_________________________________________
void drawSubDetInfo(int g, int d){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
  
  return;

}

//_________________________________________
//
// Put info for subdetector summary plots
// ent=entries, m=mean, r=rms
//_________________________________________
void drawStats(int ent, double m, double rms){

  myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
  myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
  myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
 
  return;

}

//_________________________________________
//
// Put info for 3 fits on plot
// p0, p4, p0
//_________________________________________
void drawStatBox(double par[7],std::string var){
  myText(0.55,0.89,kGreen-3,Form("%s < -0.5",var.c_str()),0.6);
  myText(0.55,0.86,kGreen-3,Form("p0: %.3f",par[0]),0.6);
  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
  myText(0.83,0.89,kGreen-3,Form("0.5 < %s",var.c_str()),0.6);
  myText(0.83,0.86,kGreen-3,Form("p0: %.3f",par[6]),0.6); 
  return;
}
