//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// functions
void doEFracPlot();
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
void drawFitInfo(int gain, int sl);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1);

// for calculating correcitons at each pass
double febTime_0[2][47+17][620]; // [2] gain [47] runs [620] febs for pass 0
double ftTime_0[2][47+17][104]; // [2] gain [47] runs [620] ft for pass 0
double febTime_1[2][620];     // [2] gain [620] febs for pass 1
double chTime[2][79360];      // [2] gain [79360] channels for pass 2
double enFit[2][22][6];       // [2] gain [22] slots [6] energy fit params
double dphiFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[2][22][2];       // [2] gain [22] slots [2] df1 fit params
double f3Fit[2][22][2];       // [2] gain [22] slots [2] df3 fit params

// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","Medium","Low"};
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
std::vector< int > runNumberList;

// histograms
TH1F *h_cell_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_feb_run[2][5][2]; // EMBA/C, EMECA/C, all
TH2F *h_phi_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_dphi_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_eta_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_deta_t[2][5][2];  // EMBA/C, EMECA/C, all
TH2F *h_f1_t[2][5][2];    // EMBA/C, EMECA/C, all
TH2F *h_f3_t[2][5][2];    // EMBA/C, EMECA/C, all
TH2F *h_e_t[2][5][2];     // EMBA/C, EMECA/C, all

// Configuration
const int  NRUNS   = 150; // 47 for IOVconst int  NRUNS2  = 17; // 17 for IOV2 const 
bool saveEPS = false;
const int  NRUNS2  = 0; // 17 for IOV2
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -0.5;
double tmax        = 0.5;

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void rebin(int pn=0, int mode=0){

#ifdef __CINT__
  gROOT->LoadMacro("../utils/AtlasLabels.C");
  gROOT->LoadMacro("../utils/AtlasStyle.C");
#endif
   SetAtlasStyle();  
   
    doEFracPlot();

}

//
// Plot avg time vs f1/f3 
// by subdetector/slot
//_____________________________________________
void doEFracPlot(){
  
  std::cout << "\nMaking plots for fractional energy deposit timing\n\n";
  
  // Initialize the histograms
  for( int i=0; i<5; i++){
    for( int j=0; j<2; j++){
      for( int k=0; k<2; k++){
        h_f1_t[j][i][k]  = new TH2F(Form("h_f1_t%d%d%d",i,j,k),Form("Cell Time %s; f1; Time [ns]",subDet[i].c_str()), 400,-0.05, .6, 500, -5, 5);//220,-0.10,1.,500,-5,5);
      }
    }
  }
  
  // Open the data file
  TFile *file;
  file = TFile::Open(Form("../files/2016_all/pass5.root"),"READ");
//  // Loop over gains
//    // Loop over slots
   int g=0;
    for(int sl=0; sl<22; sl++){
      // Make names for the histograms to open
      std::string f1name  = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),sl);

//      
      TH2F *t_f1    = (TH2F*)file->Get(f1name.c_str());
        h_f1_t[g][0][0]->Add(t_f1);
//          
    }// end loop over slots
    h_f1_t[g][0][0]->Sumw2();
h_f1_t[g][0][0]->GetXaxis()->SetRangeUser(0,0.6);
   h_f1_t[g][0][0]->Rebin2D(40,100); 
  // Make summary plots EMB
  double nFrac =  ( g == 0 ? 100 : 75);  
      nFrac =10;
  // f1
      TCanvas *cf1 = new TCanvas(Form("cf1_%d",g),Form("cf1_%d",g),800,700);
    // rebin
      TProfile *ff_f= new TProfile();  
     for(int j=1;j<6;j++)
	     cout<<h_f1_t[g][0][0]->GetBinContent(5,j)<<"\t"<<h_f1_t[g][0][0]->GetYaxis()->GetBinCenter(j)<<"\t"<<h_f1_t[g][0][0]->GetBinError(5,j)<<endl;
 ff_f=h_f1_t[g][0][0]->ProfileX();
cout<<"whichbin"<<ff_f->GetXaxis()->FindBin(0.22)<<endl;
cout<<"bin5_e"<<"\t"<<ff_f->GetBinEntries(5)<<"\t"<<ff_f->GetBinContent(5)<<"\t"<<ff_f->GetBinError(5)<<endl; 
      ff_f->SetErrorOption("g");
cout<<"bin5_g"<<"\t"<<ff_f->GetBinEntries(5)<<"\t"<<ff_f->GetBinContent(5)<<"\t"<<ff_f->GetBinError(5)<<endl; 
      ff_f->SetErrorOption("s");
cout<<"bin5_s"<<"\t"<<ff_f->GetBinEntries(5)<<"\t"<<ff_f->GetBinContent(5)<<"\t"<<ff_f->GetBinError(5)<<endl; 
     for(int j=1;j<6;j++)
	     cout<<h_f1_t[g][0][0]->GetBinContent(6,j)<<"\t"<<h_f1_t[g][0][0]->GetYaxis()->GetBinCenter(j)<<"\t"<<h_f1_t[g][0][0]->GetBinError(6,j)<<endl;
 ff_f=h_f1_t[g][0][0]->ProfileX();
cout<<"bin6_e"<<"\t"<<ff_f->GetBinEntries(6)<<"\t"<<ff_f->GetBinContent(6)<<"\t"<<ff_f->GetBinError(6)<<endl; 
      ff_f->SetErrorOption("g");
cout<<"bin6_g"<<"\t"<<ff_f->GetBinEntries(6)<<"\t"<<ff_f->GetBinContent(6)<<"\t"<<ff_f->GetBinError(6)<<endl; 
      ff_f->SetErrorOption("s");
cout<<"bin6_s"<<"\t"<<ff_f->GetBinEntries(6)<<"\t"<<ff_f->GetBinContent(6)<<"\t"<<ff_f->GetBinError(6)<<endl; 
//cout<<"find"<<ff_f->GetXaxis()->FindBin(0.314)<<"\t"<<ff_f->GetBinContent(56)<<"\t"<<ff_f->GetBinError(56)<<endl; 


      cout<<"beforEntries"<<h_f1_t[g][0][0]->GetEntries()<<"\t"<<h_f1_t[g][0][0]->GetEffectiveEntries()<<endl;
h_f1_t[g][0][0] = rebin( h_f1_t[g][0][0], h_f1_t[g][0][0]->GetEntries()/nFrac, h_f1_t[g][0][0]->GetXaxis()->FindBin(0.)-1);
for(int j=1;j<6;j++) 
cout<<h_f1_t[g][0][0]->GetBinContent(h_f1_t[g][0][0]->GetBin(4,j))<<"\t"<<h_f1_t[g][0][0]->GetYaxis()->GetBinCenter(j)<<"\t"<<h_f1_t[g][0][0]->GetBinError(4,j)<<"\t"<<h_f1_t[g][0][0]->GetNbinsX()<<endl;

//cout<<"2derror"<<h_f1_t[g][0][0]->GetBinContent(5,8)<<"\t"<<h_f1_t[g][0][0]->GetBinError(5,8)<<"\t"<<h_f1_t[g][0][0]->GetNbinsY()<<endl;
      TProfile *ff_f1= new TProfile();
ff_f1->Sumw2();
      cout<<"afterEntries"<<h_f1_t[g][0][0]->GetEntries()<<"\t"<<h_f1_t[g][0][0]->GetEffectiveEntries()<<endl;

   //   ff_f1->Sumw2();
      ff_f1=h_f1_t[g][0][0]->ProfileX();//Form("ff_f1%d0",g),-1,-1,"o");
      cout<<"entries"<<ff_f1->GetBinEntries(4)<<"\t"<<ff_f1->GetBinEffectiveEntries(4)<<endl;
      ff_f1->SetErrorOption("");
      ff_f1->SetLineColor(kRed);
//fif_f1->GetYaxis()->SetRangeUser(-0.2,0.2);
      cout<<"whichbin"<<ff_f1->GetXaxis()->FindBin(0.22)<<"\t"<<ff_f1->GetBinContent(4)<<endl;
	      cout<<"bin4_e"<<"\t"<<ff_f1->GetBinError(4)<<"\t"<<ff_f1->GetBinEntries(4)<<endl;
      ff_f1->SetErrorOption("g");
      double eg=ff_f1->GetBinError(4);
      cout<<"bin4_g"<<"\t"<<ff_f1->GetBinError(4)<<endl;
      ff_f1->SetErrorOption("s");
      double es=ff_f1->GetBinError(4);
      cout<<"bin4_s"<<"\t"<<ff_f1->GetBinError(3)<<endl;
 //     cout<<
  //    cout<<"Entries"<<ff_f1->GetEntries()<<"\t"<<ff_f->GetEntries()<<"\t"<<ff_f1->GetEffectiveEntries()<<endl;
///cout<<"find"<<ff_f1->GetXaxis()->FindBin(0.314)<<"\t"<<ff_f1->GetBinError(6)<<endl; 
      ff_f1->Draw("g");
//ff_f->Draw("same");
      // Put info on plot
      cf1->SaveAs(Form("EMB_f1_pass5_pass6_%s.png",gain[g].c_str()));
      delete cf1;
  file->Close();
  return;
}

//_____________________________________________
TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

  // hold the bin edge information
  std::vector< double > xbins;
  // Combine all bins below first bin
  // Cut at 5 GeV so this bin is empty
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

  // keep track of last bin with at least entPerBin
  int lastFullIndex = firstBin; // for energy 5

  // Get the xaxis
  TAxis *axis = h->GetXaxis();

  // Loop over bins in xaxis
  // start after 5GeV bin
  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i=i+2) {

    // Get entries in this bin, and width
    int y = h->Integral(i,i);
    double w1 = axis->GetBinWidth(i);
       double w2 = axis->GetBinWidth(i+1);  
      xbins.push_back( axis->GetBinLowEdge(i) + w1+w2 );

  }

  // put bin edges into an array
  xbins.push_back( axis->GetXmax() );
  size_t s = xbins.size();
  double *xbinsFinal = &xbins[0];

  // create new histo with new bin edges
  TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
  hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());
  hnew->Sumw2();
int k=0;
 // cout<<
 // fill new histo with old values
for( int i=1; i<=h->GetNbinsX(); i++){
	//	  if(i==5||i==6)
	//	  cout<<h->GetXaxis()->GetBinLowEdge(i) <<endl;
	for( int j=1; j<=h->GetNbinsY(); j++){

//		for(int a=1;a<=h->GetBinContent(i,j);a++)
          hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j), h->GetBinContent(i,j));

			//     cout<<"fill"<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<endl;
//			hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
		//    if(i==200)
		//	      cout<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<"\t"<<h->GetBinError(i,j)<<endl;
		// hnew->
		//if(i<=h->GetNbinsX()/2&&i>=h->GetNbinsX()/2-1)
	}
}
//cout<<"h"<<"\t"<<h->GetEntries()<<endl;
//cout<<"h"<<"\t"<<h->GetBinContent(200,250)<<endl;
//for(j =1; j<=hnew->GetNbinsX();j++)
//	if( hnew-> GetBinContent(38,j)!=0)
//if(j==3)
//cout<<xbins[j]<<endl;
//cout<<"h_new"<<"\t"<<hnew->GetXaxis()->GetBinCenter(38)<<"\t"<<""<<endl;
//cout<<"h_new"<<"\t"<<hnew->GetEntries()<<endl;
//
double stats[7];
//hnew->GetStats(stats);
//for(i=0;i<7;i++)
//	cout<<"star"<<stats[i]<<endl;
  TH2F* h_return = (TH2F*)hnew->Clone("");
  delete hnew;

  // return histo
  return h_return;
}





//__________________________________________
//
// Put info for fit plots
// g=gain sl=slot index
//__________________________________________
void drawFitInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

  return;

}


//____________________________________________
//
// Put info for run by run plots
// g=gain, rn = run number index
//____________________________________________
void drawRunInfo(int g, int rn){
  
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

  return;

}

//_________________________________________
//
// Put info for slot by slot plots
// g=gain index, sl=slot index
//_________________________________________
void drawSlInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
  
  return;

}
//_________________________________________
//
// Put info for subdetector summary plots
// g=gain index, d=subdetector index
//_________________________________________
void drawSubDetInfo(int g, int d){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
  
  return;

}

//_________________________________________
//
// Put info for subdetector summary plots
// ent=entries, m=mean, r=rms
//_________________________________________
void drawStats(int ent, double m, double rms){

  myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
  myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
  myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
 
  return;

}

//_________________________________________
//
// Put info for 3 fits on plot
// p0, p4, p0
//_________________________________________
void drawStatBox(double par[7],std::string var){
  myText(0.55,0.89,kGreen-3,Form("%s < -0.5",var.c_str()),0.6);
  myText(0.55,0.86,kGreen-3,Form("p0: %.3f",par[0]),0.6);
  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
  myText(0.83,0.89,kGreen-3,Form("0.5 < %s",var.c_str()),0.6);
  myText(0.83,0.86,kGreen-3,Form("p0: %.3f",par[6]),0.6); 
  return;
}
