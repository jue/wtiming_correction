// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
#include "../help/runPassCalc.h"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// Functions
void pass0Pull();
void pass1Pull();
void pass2Pull();
void setRunList(); 

// strings
const std::string gain[2]       = {"High","Medium"};
char Intern[15]                 = "Internal";
char * pInternal                = Intern;

// constants
const int  NRUNS   = 47; // 47 for IOV1
const int  NRUNS2  = 17; // 17 for IOV2

// run numbers
std::vector< int > runNumberList;

//
// Make pull plots for different passes to assess
// if the corrections are independent of gain
//______________________________________________
void pullPlots(){

#ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
#endif
  setRunList();
  pass0Pull();
//  pass1Pull();
//  pass2Pull();

}

//
// Pass 2 - using Ch histos
// (just iov1 for now)
//__________________________________
void pass2Pull(){
  
  std::cout << "\nStarting Pass 2 pull plots\n";

  // For storing run corrections 
  TH2F *hp[2];  // 0 = emb, 1 = emec (by FEB)
  TH1F *hp1D[2];  // as 1D histo 

  // open data files for each iov
  TFile *f1;
  f1 = TFile::Open(Form("../files/IOV_1/pass2.root"),"READ");

  // Initialize the histograms
  for(int benc=0; benc<2; benc++){
    hp[benc]     = new TH2F(Form("hp2%d",benc),Form("hp2%d",benc), 79360,-0.5,79359.5,500,-10,10);
   // hp1D[benc]   = new TH1F(Form("hp1d2%d",benc),Form("hp1d2%d",benc),200,-100,100);
    hp1D[benc]   = new TH1F(Form("hp1d2%d",benc),Form("hp1d2%d",benc),200,-10,10);
  }
  
  // Load histograms for each run
  std::string hname  = Form("h_AvgChannelTime_%s",gain[0].c_str());
  std::string mname  = Form("h_AvgChannelTime_%s",gain[1].c_str());
  TH2F *h1[2];
  h1[0] = (TH2F*)f1->Get(hname.c_str());
  h1[1] = (TH2F*)f1->Get(mname.c_str());
    
  // Make profile plot
  TProfile *tp1[2];
  tp1[0] = h1[0]->ProfileX("H_high",-1,-1,"o");
  tp1[1] = h1[1]->ProfileX("H_medium",-1,-1,"o");
  
  // loop over ch
  for( int ch=0; ch<79360; ch++){
        bool fill1 = true;
        double diff1 = tp1[0]->GetBinContent(ch+1) - tp1[1]->GetBinContent(ch+1);
        double denom1 = sqrt( tp1[0]->GetBinError(ch+1)*tp1[0]->GetBinError(ch+1) + 
                              tp1[1]->GetBinError(ch+1)*tp1[1]->GetBinError(ch+1 ));
        if( ch < 10 ){
          std::cout<< "Bin error high:  " << tp1[0]->GetBinError(ch+1)
                   << "; medium:   " << tp1[1]->GetBinError(ch+1) << std::endl;
        }

        // dont fill 0, unless both independently non-zero
        if( diff1 == 0 ){
          if( tp1[0]->GetBinContent(ch+1) == 0 || tp1[1]->GetBinContent(ch+1) == 0)
            fill1 = false;
          else{
            std::cout << "!! Diff: " << diff1
                    << "; CH: " << ch
                    << "; High: " << tp1[0]->GetBinContent(ch+1)
                    << "; Med:  "<< tp1[1]->GetBinContent(ch+1) << std::endl;
          }
        }
        denom1 = 1.0;
        if( ch < 320*128){
          if( fill1 ){
            hp[0]->Fill(ch, diff1);
            hp1D[0]->Fill(diff1/denom1);
          }
        }
        else{
          if( fill1 ){
            hp[1]->Fill(ch, diff1);
            hp1D[1]->Fill(diff1/denom1);
          }
        }
    } // end loop over fts

  // EMB
  // By Ch
  TCanvas *c1 = new TCanvas("c1","c1",800,700);
  TH2F *emb = (TH2F*)hp[0]->Clone();
  emb->Draw();
  emb->GetXaxis()->SetRangeUser(-0.5, 79359.5);
  emb->GetXaxis()->SetTitle("Ch Number");
  emb->GetYaxis()->SetRangeUser(-5,5);
  emb->GetYaxis()->SetTitle("t_{High}-t_{Medium} [ns]");
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 2"), 0.6);
  c1->SaveAs("../plots/emb_ch_pull2D.png");
  // 1D
  TCanvas *c3 = new TCanvas("c3","c3",800,700);
  hp1D[0]->Draw();
  //hp1D[0]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[0]->GetXaxis()->SetTitle("t_{High}-t_{Medium}");
  hp1D[0]->GetYaxis()->SetRangeUser(0.5, hp1D[0]->GetMaximum()/0.08);
  int entries = hp1D[0]->GetEntries();
  double mean = hp1D[0]->GetMean();
  double rms  = hp1D[0]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 2"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f ns",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f ns",rms), 0.6);
  c3->SetLogy(1);
  c3->SaveAs("../plots/emb_ch_pull1D_dt.png");

  // EMEC
  // By ch
  TCanvas *c2 = new TCanvas("c2","c2",800,700);
  TH2F* emec = (TH2F*)hp[1]->Clone();
  emec->Draw();
  emec->GetXaxis()->SetRangeUser(-0.5,79359.5);
  emec->GetXaxis()->SetTitle("Ch Number");
  emec->GetYaxis()->SetRangeUser(-5,5);
  emec->GetYaxis()->SetTitle("t_{High}-t_{Medium} [ns]");
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 2"), 0.6);
  c2->SaveAs("../plots/emec_ch_pull2D.png");
  // 1D
  TCanvas *c4 = new TCanvas("c4","c4",800,700);
  hp1D[1]->Draw();
  //hp1D[1]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[1]->GetXaxis()->SetTitle("t_{High}-t_{Medium}");
  hp1D[1]->GetYaxis()->SetRangeUser(0.5, hp1D[1]->GetMaximum()/0.08);
  entries = hp1D[1]->GetEntries();
  mean    = hp1D[1]->GetMean();
  rms     = hp1D[1]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 2"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f ns",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f ns",rms), 0.6);
  c4->SetLogy(1);
  c4->SaveAs("../plots/emec_ch_pull1D_dt.png");

  f1->Close();

}


//
// Pass 1 - using FEB histos
// (just iov1 for now)
//__________________________________
void pass1Pull(){
  
  std::cout << "\nStarting Pass 1 pull plots\n";
  
  // For storing run corrections 
  TH2F *hp[2];  // 0 = emb, 1 = emec (by FEB)
  TH1F *hp1D[2];  // as 1D histo 

  // open data files for each iov
  TFile *f1;
  f1 = TFile::Open(Form("../files/IOV_1/pass1.root"),"READ");

  // Initialize the histograms
  for(int benc=0; benc<2; benc++){
    hp[benc]     = new TH2F(Form("hp1%d",benc),Form("hp%d",benc), 620,-0.5,619.5,500,-5,5);
    //hp1D[benc]   = new TH1F(Form("hp1%d",benc),Form("hp%d",benc),200,-200,200);
    hp1D[benc]   = new TH1F(Form("hp1%d",benc),Form("hp%d",benc),200,-10,10);
  }

  // Load histograms for each run
  std::string hname  = Form("h_AvgFebTime_%s",gain[0].c_str());
  std::string mname  = Form("h_AvgFebTime_%s",gain[1].c_str());
  TH2F *h1[2];
  h1[0] = (TH2F*)f1->Get(hname.c_str());
  h1[1] = (TH2F*)f1->Get(mname.c_str());
  
  // Make profile plot
  TProfile *tp1[2];
  tp1[0] = h1[0]->ProfileX("H1_high",-1,-1,"o");
  tp1[1] = h1[1]->ProfileX("H1_medium",-1,-1,"o");
  
  // loop over febs
  for( int feb=0; feb<620; feb++){
        bool fill1 = true;
        double diff1 = tp1[0]->GetBinContent(feb+1) - tp1[1]->GetBinContent(feb+1);
        double denom1 = sqrt( tp1[0]->GetBinError(feb+1)*tp1[0]->GetBinError(feb+1) + 
                              tp1[1]->GetBinError(feb+1)*tp1[1]->GetBinError(feb+1 ));

        // dont fill 0, unless both independently non-zero
        if( diff1 == 0 ){
          if( tp1[0]->GetBinContent(feb+1) == 0 || tp1[1]->GetBinContent(feb+1) == 0)
            fill1 = false;
          else{
            std::cout << "!! Diff: " << diff1
                    << "; FEB: " << feb
                    << "; High: " << tp1[0]->GetBinContent(feb+1)
                    << "; Med:  "<< tp1[1]->GetBinContent(feb+1) << std::endl;
          }
        }
         denom1 = 1.0;
        if( feb < 320){
          if( fill1 ){
            hp[0]->Fill(feb, diff1);
            hp1D[0]->Fill(diff1/denom1);
          }
        }
        else{
          if( fill1 ){
            hp[1]->Fill(feb, diff1);
            hp1D[1]->Fill(diff1/denom1);
          }
        }
    } // end loop over fts

  // EMB
  // By FEB
  TCanvas *c1 = new TCanvas("c1","c1",800,700);
  TH2F *emb = (TH2F*)hp[0]->Clone();
  emb->Draw();
  emb->GetXaxis()->SetRangeUser(-0.5, 619.5);
  emb->GetXaxis()->SetTitle("FEB Number");
  emb->GetYaxis()->SetRangeUser(-5,5);
  emb->GetYaxis()->SetTitle("t_{High}-t_{Medium} [ns]");
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 1"), 0.6);
  c1->SaveAs("../plots/emb_feb_pull2D.png");
  // 1D
  TCanvas *c3 = new TCanvas("c3","c3",800,700);
  hp1D[0]->Draw();
  hp1D[0]->Rebin(2);
 // hp1D[0]->GetXaxis()->SetRangeUser(-100,100);
  //hp1D[0]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[0]->GetXaxis()->SetTitle("t_{High}-t_{Medium}");
  hp1D[0]->GetYaxis()->SetRangeUser(0.5, hp1D[0]->GetMaximum()/0.08);
  int entries = hp1D[0]->GetEntries();
  double mean = hp1D[0]->GetMean();
  double rms  = hp1D[0]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 1"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f ns",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f ns",rms), 0.6);
  c3->SetLogy(1);
  c3->SaveAs("../plots/emb_feb_pull1D_dt.png");

  // EMEC
  // by FEB
  TCanvas *c2 = new TCanvas("c2","c2",800,700);
  TH2F* emec = (TH2F*)hp[1]->Clone();
  emec->Draw();
  emec->GetXaxis()->SetRangeUser(-0.5,619.5);
  emec->GetXaxis()->SetTitle("FEB Number");
  emec->GetYaxis()->SetRangeUser(-5,5);
  emec->GetYaxis()->SetTitle("t_{High}-t_{Medium} [ns]");
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 1"), 0.6);
  c2->SaveAs("../plots/emec_feb_pull2D.png");
  // 1D
  TCanvas *c4 = new TCanvas("c4","c4",800,700);
  hp1D[1]->Draw();
//  hp1D[1]->Rebin(4);
  hp1D[1]->Rebin(2);
  //hp1D[1]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[1]->GetXaxis()->SetTitle("t_{High}-t_{Medium}");
  hp1D[1]->GetYaxis()->SetRangeUser(0.5, hp1D[1]->GetMaximum()/0.08);
  entries = hp1D[1]->GetEntries();
  mean    = hp1D[1]->GetMean();
  rms     = hp1D[1]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 1"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f ns",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f ns",rms), 0.6);
  c4->SetLogy(1);
  c4->SaveAs("../plots/emec_feb_pull1D_dt.png");

  f1->Close();

}




//
// Pass 0 - using FT histos
//__________________________________
void pass0Pull(){
  
  std::cout << "\nStarting Pass 0 pull plots\n";
  
  // For storing run corrections 
  TH2F *hp[2];  // 0 = emb, 1 = emec (by run number)

  // open data files for each iov
  TFile *f1;
  TFile *f2;
  f1 = TFile::Open(Form("../files/IOV_1/pass7.root"),"READ");
  f2 = TFile::Open(Form("../files/IOV_2/pass7.root"),"READ");

  // Initialize the histograms
  for(int benc=0; benc<2; benc++){
    hp[benc]   = new TH2F(Form("hp%d",benc),Form("hp%d",benc),64,0,64,500,-5,5);
  }
  
    TH2F *h1[2];
    TH2F *h2[2];
    TProfile *tp1[2];
    TProfile *tp2[2];
  // loop over runs
    for(g=0;g<2;g++){
	    cout<<gain[g].c_str()<<endl;
  for( int rn=0; rn<NRUNS; rn++){
    
    // Load histograms for each run
    std::string hname  = Form("h_RunChannelTime_%s%d",gain[g].c_str(),rn);
    std::string pname  = Form("Prof run %s%d1",gain[g].c_str(),rn);
    std::string pnameb = Form("Prof run %s%d1",gain[g].c_str(),rn+NRUNS);
    h1[g] = (TH2F*)f1->Get(hname.c_str());
    if( rn < NRUNS2){
      h2[g] = (TH2F*)f2->Get(hname.c_str());
    }
    //if(h2[0]->GetEntries()) cout<<"suc"<<endl;


    // Make profile plot
    tp1[g] = h1[g]->ProfileX(pname.c_str(),-1,-1,"o");
    if( rn < NRUNS2){
      tp2[g] = h2[g]->ProfileX(pnameb.c_str(),-1,-1,"o");
    }
    for( int ft=0; ft<1; ft++){
        bool fill1 = true;
        bool fill2 = false;
	double entry= tp1[g]->GetBinContent(ft+1); 
	if( rn < NRUNS2 ){
		fill2= true;
		double entry  = tp2[g]->GetBinContent(ft+1);
        }
        // dont fill 0, unless both independently non-zero
        //fill2 = false;
          if( fill1 ){
		  if(entry!=0)
            hp[g]->Fill(rn,entry );
	    cout<<"entry"<<entry<<endl;
          }
	  if(fill2){
		  if(entry!=0)
            hp[g]->Fill(rn+NRUNS, entry);
	    cout<<"entry"<<entry<<endl;
          }
	
    } // end loop over fts
  } // end loop over runs

  TCanvas *c1 = new TCanvas("c1","c1",800,700);
  TProfile *emb = hp[g]->ProfileX();
  emb->Draw("P");
  for( int bin=0; bin<NRUNS+NRUNS2; bin++){
    int s_run =  runNumberList[bin] ;
    emb->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
  }
  emb->GetXaxis()->SetRangeUser(0,NRUNS+NRUNS2);
  emb->GetXaxis()->LabelsOption("v");
  emb->GetXaxis()->SetLabelSize(0.03);
  emb->GetXaxis()->SetTitle("Run Number");
  emb->GetYaxis()->SetRangeUser(-1,1);
  emb->GetYaxis()->SetTitle("t [ns]");
  drawChannelInfo(g, 1);  
  //ATLASLabel(0.2,0.88,pInternal);
//  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
//  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("%s gain", gain[g].c_str()), 0.7);
  myText(0.73, 0.79, kBlack, Form("Pass 7"), 0.6);
  c1->SaveAs(Form("../plots/channel_%s.png", gain[g].c_str()));
  }
  // 1D
  /*
  TCanvas *c3 = new TCanvas("c3","c3",800,700);
  hp1D[0]->Draw();
  hp1D[0]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[0]->GetYaxis()->SetRangeUser(0.5, hp1D[0]->GetMaximum()/0.08);
  int entries = hp1D[0]->GetEntries();
  double mean = hp1D[0]->GetMean();
  double rms  = hp1D[0]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMB"), 0.7);
  //myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f",rms), 0.6);
  c3->SetLogy(1);
  c3->SaveAs("../plots/emb_ft_pull1D.png");

  // EMEC
  // Run by Run
  TCanvas *c2 = new TCanvas("c2","c2",800,700);
  TProfile *emec = hp[1]->ProfileX();
  emec->Draw();
  // Write run number as bin label
  for( int bin=0; bin<NRUNS+NRUNS2; bin++){
    int s_run =  runNumberList[bin] ;
    emec->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
  }
  emec->GetXaxis()->SetRangeUser(0,NRUNS+NRUNS2);
  emec->GetXaxis()->LabelsOption("v");
  emec->GetXaxis()->SetLabelSize(0.03);
  emec->GetXaxis()->SetTitle("Run Number");
  emec->GetYaxis()->SetRangeUser(-1,1);
  emec->GetYaxis()->SetTitle("t_{High}-t_{Medium} [ns]");
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
  c2->SaveAs("../plots/emec_ft_pullRun.png");
  // 1D
  TCanvas *c4 = new TCanvas("c4","c4",800,700);
  hp1D[1]->Draw();
  hp1D[1]->GetXaxis()->SetTitle("t_{High}-t_{Medium}/#sigma");
  hp1D[1]->GetYaxis()->SetRangeUser(0.5, hp1D[1]->GetMaximum()/0.08);
  entries = hp1D[1]->GetEntries();
  mean    = hp1D[1]->GetMean();
  rms     = hp1D[1]->GetRMS();
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
  myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
  myText(0.22, 0.72, kBlack, Form("EMEC"), 0.7);
  //myText(0.73, 0.83, kBlack, Form("IOV1"), 0.6);
  myText(0.73, 0.79, kBlack, Form("Pass 0"), 0.6);
  myText(0.73, 0.75, kBlack, Form("Entries: %d",entries), 0.6);
  myText(0.73, 0.71, kBlack, Form("Mean: %.3f",mean), 0.6);
  myText(0.73, 0.67, kBlack, Form("RMS: %.3f",rms), 0.6);
  c4->SetLogy(1);
  c4->SaveAs("../plots/emec_ft_pull1D.png");
*/
  f1->Close();
  f2->Close();

}

//_____________________________________________
//
// Read list of run numbers from config file
//_____________________________________________
void setRunList(){

  // Run number config file
  ifstream f1("../config/RunNumberList.txt");
  int tempRunNum;

  // Store each run into local vector
  while( f1 >> tempRunNum ){
      runNumberList.push_back(tempRunNum);
  }

  f1.close();

  std::cout << "  > Added " << runNumberList.size() << " runs successfully from "
            << runNumberList[0] << " to " << runNumberList.back() << "\n\n";
  ifstream f2("config/channel.txt");  
  int i=0, tempch, tempid;
  while( f2 >>tempch>>tempid){
  channel[i] = tempch;
  onlineid[i] = tempid;
  //cout<<channel[i]<<"\t"<<onlineid[i]<<endl;
  i++;
  }
  f2.close();
  return;
}








