//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooChi2Var.h"
#include "RooDataSet.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooCBShape.h"
#include "RooExponential.h"
 #include "RooDataSet.h"
#include "RooDataHist.h"
#include "TMinuit.h"

#include "TCanvas.h"
#include "RooFFTConvPdf.h"
//#include "TDataSet.h"
#include "TVirtualFFT.h"
#include "RooPlot.h"
#include "TAxis.h"
#include  "RooDecay.h"
using namespace RooFit ;
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// functions
void doMassPlot();
void doFEBTimePlot();
void domuPlot(); 
void doCellTimePlot();
void doAngularPlot();
void doEFracPlot();
void doETimePlot();
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
void drawFitInfo(int gain, int sl);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1,int pass=0);

TCanvas *fitroofit_all(TH1F* h_cell, double mean, double rms, double pos_y, int color,int pass);

// for calculating correcitons at each pass
double febTime_0[2][47+17][620]; // [2] gain [47] runs [620] febs for pass 0
double ftTime_0[2][47+17][104]; // [2] gain [47] runs [620] ft for pass 0
double febTime_1[2][620];     // [2] gain [620] febs for pass 1
double chTime[2][79360];      // [2] gain [79360] channels for pass 2
double enFit[2][22][6];       // [2] gain [22] slots [6] energy fit params
double dphiFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[2][22][2];       // [2] gain [22] slots [2] df1 fit params
double f3Fit[2][22][2];       // [2] gain [22] slots [2] df3 fit params
int pass_Color[12] ={921,1, 417,800, 600, 397,416,880, 432, 821, 632,839};//, "kGreen", "kRed", "kViolet"}


//int pass_Color[3] ={921,1, 417};//, "kGreen", "kRed", "kViolet"};
//int pass_Color[2] ={1, 417};//, "kGreen", "kRed", "kViolet"};

//string pass_Color[6] ={kgray" "kBlack", "kOrange","kBlue",,"kYellow", "kGreen", "kViolet","kcyan","kSpring","kRed", "kTeal"};

// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","Medium","Low"};
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
std::vector< int > runNumberList;
//const std::string Correction[8] = {"Pass 0","Pass 1: FT","Pass 2: FEB", "Pass 3: Channel","Pass 4: Energy","Pass 5: Angular","Pass 6: f1/f3","Pass 7: 2nd Channel"};  
const int passN=10;

//const std::string Correction[passN] = {"Pass 1: After FT", "Pass 3: After Channel", "Pass 3 separate iovs"};//,"Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 2", "Pass 3: After Channel", "Pass 4: After Energy","Pass 3 separate iovs","Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs", "Pass 5: after Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: After 2nd Channel", "Pass 7: separate iovs"};
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 5: After Angular","Pass 6: After f1/f3", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"}/
///Efrac
//const std::string Correction[passN] = { "Pass 3: After Channel", "Pass 3 separate iovs", "Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};
//const std::string Correction[passN] = { "Pass 1: After FT", "Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs"};
//const std::string Correction[passN] = {"Pass 0", "Pass 1: After FT","Pass 2: After Feb", "Pass 3 separate iovs", "Pass 4 separate iovs","Pass 5: separate iovs",  "Pass 6: separate iovs", "Pass 7: separate iovs in Pass 2"};
//const std::string Correction[passN] = {"Pass 3(After Channel)","Pass3 (IOV-dependent)" };//"Pass 1: After FT","Pass 2: After Feb", "Pass 3 separate iovs", "Pass 4 separate iovs","Pass 5: separate iovs",  "Pass 6: separate iovs", "Pass 7: separate iovs in Pass 2"};
//const std::string Correction[passN] = {"Pass 3","pass 4(After Energy correction)", "Pass 3(Subslot)"};//,"Pass 8"};//,"Pass 8(After Pile-up Correction)" };

//const std::string Correction[passN] = {"Pass 6","pass 7 (After Bunch position correction)"};//, "Pass 3(Subslot)"};//,"Pass 8"};//,"Pass 8(After Pile-up Correction)" };
//const std::string Correction[passN] = {"Pass 0","pass 8(After All correcitons)", "Pass 8(probamatic channel removed)"};//,"Pass 8"};//,"Pass 8(After Pile-up Correction)" };
//const std::string Correction[passN] = {"Pass 2","Pass 3(After Channel correcitons)", "Pass 3(Separate IOVs)"};//,"Pass 8"};//,"Pass 8(After Pile-up Correction)" };
				
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 4 separate iovs","Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};
const std::string Correction[passN] = {"Pass 0", "Pass 1: After FT","Pass 2: After Feb", "Pass 3 ", "Pass 4 ","Pass 5:", "Pass 6: After f1/f3", "Pass 7: after 2nd Channel(all together)", "Pass 8","Pass 9"}; // { "Pass 4: After energy","Pass 4 separate iovs","Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};


// histogramsi
TH1F *h_cell_t_pass[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_feb_run[2][5][2]; // EMBA/C, EMECA/C, all
TH2F *h_phi_t[passN][5];   // EMBA/C, EMECA/C, all
TH2F *h_dphi_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_eta_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_deta_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_f1_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_f3_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_e_t_pass[passN][2][5];     // EMBA/C, EMECA/C, all
TH2F *h_mu_t_pass[passN][2];     // EMBA/C, EMECA/C, all
TH2F *hp_pass[passN][2][3];

// Configuration
const int  NRUNS   = 150; // 47 for IOVconst int  NRUNS2  = 17; // 17 for IOV2 const 
bool saveEPS = false;
const int  NRUNS2  = 0; // 17 for IOV2
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void LArWeek(int pn=0, int mode=0){
#ifdef __CINT__
  gROOT->LoadMacro("../utils/AtlasLabels.C");
  gROOT->LoadMacro("../utils/AtlasStyle.C");
#endif
  SetAtlasStyle();  
  // Set Run List
   setRunList();
  setTRange(-0.04, 0.04);
  //  domuPlot();
  // Make plots
  //doMassPlot();
 // doCellTimePlot();
  //doETimePlot();
//  setTRange(-0.5, 1);
//
//  doAngularPlot();
   doFEBTimePlot();
//  doEFracPlot();
   return;    
}


//___________________
// 
// Make mass plots
//_____________________
void doMassPlot(){

	std::cout << "Making W/Z mass plots\n\n";

	TFile *file;
	// Open files
	file   = TFile::Open("../files/2016_all/pass0.root");
	TH1F *h_w[3];
	TH1F *h_t[3]; 
	TH1F *h_w_all;
	TH1F *h_t_all; 

	for(int g=0; g<3; g++){
		std::string hname  = Form("h_transverseMassWenu%s",gain[g].c_str());
		std::string hname2  = Form("h_maxEcellEnergy%s",gain[g].c_str());

		// Get mass distributions
		h_w[g] = (TH1F*)file->Get(hname.c_str());
		if(g==0) h_w_all=(TH1F*)file->Get(hname.c_str());   
		else
		h_w_all->Add(h_w[g]);
		//TH1F* h_wb = (TH1F*)fileb->Get(hname.c_str());
		//TH1F* h_z  = (TH1F*)file2->Get(hname.c_str());
		//TH1F* h_zb = (TH1F*)file2b->Get(hname.c_str());
	 	 h_t[g] = (TH1F*)file->Get(hname2.c_str());
		 if(g==0) h_t_all=(TH1F*)file->Get(hname2.c_str()); 
		 else
		h_t_all->Add(h_t[g]);
		//TH1F* h_tb = (TH1F*)file3b->Get(hname2.c_str());

		// Add IOV1+IOV2
		//  h_w->Add(h_wb);
		//  h_z->Add(h_zb);
		//  h_t->Add(h_tb);

		// W Plot
		TCanvas *cw = new TCanvas("c_massw","c_massw",800,700);
		h_w[g]->GetXaxis()->SetTitle("m_{T}[GeV]");
		h_w[g]->GetYaxis()->SetTitle("Events/1.0 GeV");
		h_w[g]->GetXaxis()->SetRangeUser(40,140);
		h_w[g]->GetYaxis()->SetRangeUser(0,h_w[g]->GetMaximum()/0.78);
		h_w[g]->Draw();
		int entries=h_w[g]->GetEntries();
		// Put info on plot
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
		myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
		myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
		// Save plot
		if( saveEPS )cw->SaveAs(Form("Mass_w_%s.png",gain[g].c_str()));
		cw->SaveAs(Form("Mass_w_%s.png",gain[g].c_str()));
		delete cw;
		TCanvas *ct = new TCanvas("c_massw","c_massw",800,700);
		h_t[g]->GetXaxis()->SetTitle("Max cell Energy[GeV]");
		h_t[g]->GetYaxis()->SetTitle("Events/1.0 GeV");
		if(g==0)
			h_t[g]->GetXaxis()->SetRangeUser(0,80);
		if(g==1) 
			h_t[g]->GetXaxis()->SetRangeUser(10,100);
		h_t[g]->GetYaxis()->SetRangeUser(0,h_t[g]->GetMaximum()/0.7);
		h_t[g]->Draw();
		entries=h_t[g]->GetEntries();
		// Put info on plot
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
		myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
		myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
		// Save plot
		if( saveEPS )ct->SaveAs(Form("energy_w_%s.png",gain[g].c_str()));
		ct->SaveAs(Form("energy_w_%s.png",gain[g].c_str()));
		delete ct;

	}
	TCanvas *cw_all = new TCanvas("c_massw","c_massw",800,700);
	h_w_all->GetXaxis()->SetTitle("m_{T}[GeV]");
	h_w_all->GetYaxis()->SetTitle("Events/1.0 GeV");
	h_w_all->GetXaxis()->SetRangeUser(40,140);
	h_w_all->GetYaxis()->SetRangeUser(0,h_w_all->GetMaximum()/0.78);
	h_w_all->Draw();
        int	entries=h_w_all->GetEntries();
	// Put info on plot
	ATLASLabel(0.2,0.88,pInternal);
	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
	myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
	//  myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
	// Save plot
	if( saveEPS )cw_all->SaveAs("Mass_w.png");
	cw_all->SaveAs("Mass_w.png");
	delete cw_all;
	TCanvas *ct_all = new TCanvas("c_massw","c_massw",800,700);
	h_t_all->GetXaxis()->SetTitle("Max cell Energy[GeV]");
	h_t_all->GetYaxis()->SetTitle("Events/1.0 GeV");
	h_t_all->GetXaxis()->SetRangeUser(10,100);
	h_t_all->GetYaxis()->SetRangeUser(0,h_t_all->GetMaximum()/0.7);
	h_t_all->Draw();
	entries=h_t_all->GetEntries();
	// Put info on plot
	ATLASLabel(0.2,0.88,pInternal);
	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
	myText(0.22, 0.76, kBlack, Form("W#rightarrow e#nu"), 0.7);
	myText(0.72, 0.83,kBlack,Form("Entries: %d",entries ),0.6);
	// myText( 0.72, 0.78, kRed, Form("%s Gain",gain[g].c_str()), 0.7); 
	// Save plot
	if( saveEPS )ct_all->SaveAs("energy_w.png");
	ct_all->SaveAs("energy_w.png");
	delete ct_all;

	return;
}

void doFEBTimePlot(){

	// Initialize the histograms
	for( int g=0; g<2; g++){ 
		for( int pass=0; pass<passN; pass++){
			
			for(int i=0; i<3; i++)
				hp_pass[pass][g][i]   = new TH2F(Form("hp%d%d%d",g,pass,i),Form("hp%d%d%d",g,pass,i),150,0,150,500,-5,5);
		}
	}

	TFile *file;

	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			//if(pass!=3&&pass!=4) continue;
				switch(pass){
					case 0: file= TFile::Open("../files/recheck_2016_all/pass3_iovs.root","READ");   break; 
					case 1: file= TFile::Open("../files/recheck_2016_all/pass4_iovs.root","READ"); break;    
					case 2: file= TFile::Open("../files/pass8_new_iovs_dis_front.root","READ");   break; 
					case 3: file= TFile::Open("../files/pass9_new_iovs_dis_front.root","READ"); break;    
			}
				int old=0;
			for( int rn=0; rn<NRUNS; rn++){
				std::string pname  = Form("Prof run %s%d",gain[g].c_str(),rn);
				std::string tname = Form("h_RunAvgFTTime_%s%d",gain[g].c_str(),rn);
				TH2F *f_t_t = (TH2F*)file->Get(tname.c_str());
				TProfile *f_t=f_t_t->ProfileX(pname.c_str(), -1, -1, "o" );
				if( !f_t->GetEntries() ) {
					std::cout << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				for( int ft=0; ft<114; ft++){
					// Summary plots
					if(pass==3&&f_t->GetBinContent(ft+1)<-0.01)
						continue;
					else{
					hp_pass[pass][g][2]->Fill(rn, f_t->GetBinContent(ft+1));
					}
					if( ft < 64){ // emb
						hp_pass[pass][g][0]->Fill(rn, f_t->GetBinContent(ft+1));
					}
					else{
						hp_pass[pass][g][1]->Fill(rn, f_t->GetBinContent(ft+1));
					}
				}
			}

		}//loop over pass
		for(int d=0;d<3;d++){
			TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
			TProfile *ff_e[passN];
			for(int pass=0; pass<passN; pass++){

			//	if(pass!=3&&pass!=4) continue;
				int entries = hp_pass[0][g][d]->GetEntries();
				ff_e[pass] = hp_pass[pass][g][d]->ProfileX(Form("ff_e%d%d%d",pass,d,g),-1,-1,"o");
				cout<<pass<<"\t"<<d<<"\t"<<g<<"\t"<< hp_pass[pass][g][d]->GetEntries()<<endl;
				if(pass==0)
					ff_e[pass]->Draw();
				for( int bin=0; bin<NRUNS; bin++){
			//	for( int bin=60; bin<NRUNS; bin++){
					int s_run =  runNumberList[bin] ;
					ff_e[pass]->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
				}
				ff_e[pass]->Draw("same");
			cout<<"lala"<<endl;
				//	ff_e[pass]->SetErrorOption("g");
			ff_e[pass]->GetYaxis()->SetRangeUser(tmin,tmax);
			//	ff_e[pass]->GetYaxis()->SetRangeUser(tmin+0.015,tmax-0.08);
				ff_e[pass]->GetXaxis()->SetRangeUser(0,NRUNS);  
			//	ff_e[pass]->GetXaxis()->SetRangeUser(-1,1);  
				ff_e[pass]->SetLineColor(pass_Color[pass]);
				ff_e[pass]->SetMarkerColor(pass_Color[pass]);
				ff_e[pass]->GetXaxis()->LabelsOption("v");
				ff_e[pass]->GetXaxis()->SetLabelSize(0.03);
				ff_e[pass]->GetXaxis()->SetTitle("Run Number");
				ff_e[pass]->GetYaxis()->SetTitle("Time [ns]");
				if(pass==0)
					ff_e[pass]->Draw();
				ff_e[pass]->Draw("same");
				if(pass==0){
					ATLASLabel(0.2, 0.88,pInternal);
					//	myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
					if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
					else if( d == 1 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
					else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
					myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
					myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
					TLine *l = new TLine(0,0,NRUNS,0);
					l->SetLineColor(kRed);
					l->SetLineWidth(1);
					l->Draw("same");
					TLine *l1 = new TLine(16,-0.015,16,0.02);
					l1->SetLineColor(kRed);
					l1->SetLineWidth(1);
					l1->Draw("same");
					TLine *l2 = new TLine(30,-0.015,30,0.02);
					l2->SetLineColor(kRed);
					l2->SetLineWidth(1);
					l2->Draw("same");
					TLine *l3 = new TLine(78,-0.015,78,0.02);
					l3->SetLineColor(kRed);
					l3->SetLineWidth(1);
					l3->Draw("same");
					TLine *l4 = new TLine(119,-0.015,119,0.02);
					l4->SetLineColor(kRed);
					l4->SetLineWidth(1);
					l4->Draw("same");
				}
				else
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);

			}
			if( d== 0)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/bunch_position/pass7_pass8_EMB_ft_%s.png",gain[g].c_str()));
			else if( d==1)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/bunch_position/pass7_pass8_EMEC_ft_%s.png",gain[g].c_str()));
			else if ( d==2)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/bunch_position/pass7_pass8_All_ft_%s.png",gain[g].c_str()));
			//delete ff_e[passN];
			delete c3;
		}//loop over d
	}//loop over gain
	file->Close();

	return;
}
void domuPlot(){

	std::cout << "\nMaking plots for average mu timing\n\n";

	// Initialize the histograms
	for(int k=0; k<passN; k++){
		for( int j=0; j<2; j++){
		h_mu_t_pass[k][j]= new TH2F(Form("h_e_t%d%d",j,k),Form("Cell Time Mu; Time [ns]"),1000,-10,60,1000,-25,25);
		}  }
	TFile *file;

	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			switch(pass){
				case 0: file= TFile::Open("../files/pass6_iovs_actual_plot_mu_bad_channel.root","READ");break;    
				case 1: file= TFile::Open("../files/pass7_iovs_actual_plot_mu_bad_channel.root","READ"); break;    
			}
			std::string tname = Form("h_timeVsmu%s",gain[g].c_str());
			TH2F *f_t = (TH2F*)file->Get(tname.c_str());
			h_mu_t_pass[pass][g]->Add(f_t);
		}
	double nFrac = ( g == 0 ? 50 : 75 );
	TProfile *ff_e[passN];
	TCanvas *c3 = new TCanvas(Form("c_%d",g),Form("c_%d",g),800,700);
	for(int pass=0; pass<passN; pass++){
		int entries = h_mu_t_pass[0][g]->GetEntries();
                h_mu_t_pass[pass][g] = rebin( h_mu_t_pass[pass][g], h_mu_t_pass[pass][g]->GetEntries()/nFrac, 5);
		ff_e[pass] = h_mu_t_pass[pass][g]->ProfileX(Form("ff_e%d",pass),-1,-1,"o");
		if(pass==0)
			ff_e[pass]->Draw();
		ff_e[pass]->Draw("same");
				ff_e[pass]->SetErrorOption("g"); 
		ff_e[pass]->GetYaxis()->SetRangeUser(tmin,tmax);
		ff_e[pass]->GetXaxis()->SetRangeUser(0,60);
			ff_e[pass]->GetXaxis()->SetTitle("Actual Interactions Per Crossing");
			ff_e[pass]->GetYaxis()->SetTitle("time(ns)");
		ff_e[pass]->SetLineColor(pass_Color[pass]);
		ff_e[pass]->SetMarkerColor(pass_Color[pass]);
		if(pass==0){
			ATLASLabel(0.2, 0.88,pInternal);
			myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
			myText(0.73, 0.81+0.03,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.70, 0.76+0.03,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			myText(0.70, 0.72+0.03-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
		}
		else
			myText(0.22, 0.72+0.03-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);

	}
	c3->SaveAs(Form("/a/home/kolya/jchen/WWW/mu/new/actual_mu_pass7_pass8_mu%s.png",gain[g].c_str()));
	delete c3;
}//loop over gain

file->Close();
return;
}
//_____________________________________________
//
// Plot avg time vs energy 
// by subdetector/slot
//_____________________________________________
void doETimePlot(){

	std::cout << "\nMaking plots for average energy timing\n\n";
	

	// Initialize the histograms
	for(int k=0; k<passN; k++){
		for( int j=0; j<2; j++){
			for( int i=0; i<5; i++){
				h_e_t_pass[k][j][i]  = new TH2F(Form("h_e_t%d%d%d",i,j,k),Form("Cell Time %s; Energy [GeV]; Time [ns]",subDet[i].c_str()),250,0,250.,500,-5,5);
				//     hist[i][j] = new TH1F(Form("hist%d%d",i,j),"Title",nbins,lowX,hiX); 
			}}  }
	TFile *file;

	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			switch(pass){
				case 0: file= TFile::Open("../files/pass3_no_subslot_iovs_dis_front.root","READ");break;    
				case 1: file= TFile::Open("../files/pass4_no_subslot_iovs_dis_front.root","READ");break;    
				case 2: file= TFile::Open("../files/pass4_W_clean_new_iovs_dis_front.root","READ"); 
			}
			// Loop over slots
			for(int sl=0; sl<22+7; sl++){
				if(g==1||pass!=2&&sl>=22) continue;
				// Make names for the histograms to open
				std::string tname = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl);
				TH2F *f_t = (TH2F*)file->Get(tname.c_str());

				if( !f_t->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}
				//find the Energy boundary between H/M gain
			cout<<sl<<"\t"<<f_t->GetXaxis()->GetBinLowEdge(f_t->FindFirstBinAbove(f_t-> GetEntries()/10000))<<endl;
				// Summary plots
				h_e_t_pass[pass][g][4]->Add(f_t);
				if( sl < 4 || sl == 8 ){ 
					h_e_t_pass[pass][g][0]->Add(f_t);
				}
				else if ( (sl > 3 && sl < 8) || sl == 9 ) {
					h_e_t_pass[pass][g][1]->Add(f_t);
				}
				else if ( sl > 9 && sl < 16 ||sl>21&&sl<26){
					h_e_t_pass[pass][g][2]->Add(f_t);
				}
				else if ( sl > 15 &&sl <21||sl>25){
					h_e_t_pass[pass][g][3]->Add(f_t);
				}
			}
			// Make EMB/EMEC
			//LOW gain info
			//  if(g==2)
			//	    //cout<<"findme"<<"pass"<<pass<<"d"<<d<<"\t"<<entries<<endl;
			// separate plots
			// pass0
			/*
			   TCanvas *gg = new TCanvas(Form("c_gg%d_%d",g,d),"ggg",800,700);
			   h_e_t_pass[pass][g][d]->Draw();
			//FIXME fit info
			//if (d ==0){
			//  std:://cout << "EMB:   Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			//if( d== 2){ 
			// std:://cout << "EMEC:  Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			myText(0.73, 0.83,kBlack,Form("Entries: %d",h_e_t_pass[pass][g][d]->GetEntries()),0.6);
			myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",h_e_t_pass[pass][g][d]->GetMean()),0.6);
			myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",h_e_t_pass[pass][g][d]->GetRMS()),0.6);
			h_e_t_pass[pass][g][d] = fitGaus( h_e_t_pass[pass][g][d],h_e_t_pass[pass][g][d]->GetMean() ,h_e_t_pass[pass][g][d]->GetRMS(),.72 );
			ATLASLabel(0.2, 0.88,pInternal);
			if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
			else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
			else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
			myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			myText(0.22, 0.72,kBlack,Form("Pass %d (W#rightarrow e#nu)",pass),0.6); 
			h_e_t_pass[pass][g][d]->GetYaxis()->SetRangeUser(0.5,h_e_t_pass[pass][g][d]->GetBinContent( h_e_t_pass[g][d][0]->GetMaximumBin() )/0.08);
			// h_e_t_pass[pass][g][d]->GetXaxis()->SetRangeUser(-15,15);
			gg->SetLogy(1);
			if( d== 0)gg->SaveAs(Form("Log_pass%d_EMB_Celltime_%s.png",pass,gain[g].c_str())); 
			else if( d==2)gg->SaveAs(Form("Log_pass%d_EMEC_Celltime_%s.png",pass,gain[g].c_str())); 
			else if ( d==4)gg->SaveAs(Form("Log_pass%d_All_Celltime_%s.png",pass,gain[g].c_str()));
			delete gg;
			*/
			h_e_t_pass[pass][g][0]->Add( h_e_t_pass[pass][g][0+1]);
			h_e_t_pass[pass][g][2]->Add( h_e_t_pass[pass][g][2+1]);

		}//loop over pass
		for(int d=0;d<=4;d=d+2){
			int dd=d/2;
			double nFrac = ( g == 0 ? 150 :100 );
			TProfile *ff_e[passN];
			TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
			for(int pass=0; pass<passN; pass++){
			//	if(pass!=3&&pass!=4) continue;
				int entries = h_e_t_pass[0][g][d]->GetEntries();
				h_e_t_pass[pass][g][d] = rebin( h_e_t_pass[pass][g][d], h_e_t_pass[pass][g][d]->GetEntries()/nFrac, 5);

				ff_e[pass] = h_e_t_pass[pass][g][d]->ProfileX(Form("ff_e%d",pass),-1,-1,"o");
				//	 int pass][g=p*2+g;
				cout<<pass<<"\t"<<d<<"\t"<<g<<"\t"<< h_e_t_pass[pass][g][d]->GetEntries()<<endl;
				if(pass==3)
					ff_e[pass]->Draw();
				ff_e[pass]->Draw("same");
				///for rebin
				ff_e[pass]->SetErrorOption("g"); 

				if(g==0){
					ff_e[pass]->GetXaxis()->SetRangeUser(ff_e[pass]->GetXaxis()->GetBinLowEdge(ff_e[pass]->GetXaxis()->FindBin(5)+1),ff_e[pass]->GetXaxis()->GetBinLowEdge(ff_e[pass]->GetXaxis()->FindBin(250.)));
				} else{
					ff_e[pass]->GetXaxis()->SetRangeUser(ff_e[pass]->GetXaxis()->GetBinLowEdge(ff_e[pass]->GetXaxis()->FindBin(10.)+1),ff_e[pass]->GetXaxis()->GetBinLowEdge(ff_e[pass]->GetXaxis()->FindBin(300.)));
				}
				
		
				ff_e[pass]->GetYaxis()->SetRangeUser(tmin,tmax);
				ff_e[pass]->SetLineColor(pass_Color[pass]);
				ff_e[pass]->SetMarkerColor(pass_Color[pass]);
				if(pass==0)
					ff_e[pass]->Draw();
				ff_e[pass]->Draw("same");
				//c3->SetLogx(1);   
				if(pass==3){
					ATLASLabel(0.2, 0.88,pInternal);
				//	myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
					if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
					else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
					else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
					myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
					myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
				}
				else
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);
			 
			}
			if( d== 0)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/mu/new/pass1_pass7_EMB_energy_%s.png",gain[g].c_str()));
			else if( d==2)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/mu/new/pass1_pass7_EMEC_energy_%s.png",gain[g].c_str()));
			else if ( d==4)c3->SaveAs(Form("/a/home/kolya/jchen/WWW/mu/new/pass1_pass7_All_energy_%s.png",gain[g].c_str()));
			//delete ff_e[passN];
			delete c3;
		}//loop over d
	}//loop over gain

	file->Close();
	return;
}
//_____________________________________________
//
// Plot avg time vs Phi/Eta 
// by subdetector/slot
//_____________________________________________
void doAngularPlot(){

	std::cout << "\nMaking plots for angular cell position timing\n\n";

	// Initialize the histograms
	for(int k=0; k<passN; k++){
		for( int j=0; j<2; j++){
			for( int i=0; i<5; i++){
				h_dphi_t[k][j][i] = new TH2F(Form("h_dphi_t%d%d%d",i,j,k),Form("Cell Time %s; #delta#phi[0.0245 units]; Time [ns]",subDet[i].c_str()),400,-2,2,500,-5,5);
				h_deta_t[k][j][i] = new TH2F(Form("h_deta_t%d%d%d",i,j,k),Form("Cell Time %s; #delta#eta[0.025 units]; Time [ns]",subDet[i].c_str()),100,-1,1,500,-5,5);
			}}  }
	TFile *file;

	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			switch(pass){
				case 0: file= TFile::Open("../files/recheck_2016_all/pass1.root","READ");break;    
				case 1: file= TFile::Open("../files/recheck_2016_all/pass3.root","READ");break;    
				case 2: file= TFile::Open("../files/pass3_iovs.root","READ");break;    
				case 3: file= TFile::Open("../files/recheck_2016_all/pass4.root","READ");break;    
				case 4: file= TFile::Open("../files/pass4_iovs.root","READ");break;    
				case 5: file= TFile::Open("../files/pass5_iovs.root","READ");   break; 
				case 6: file= TFile::Open("../files/recheck_2016_all/pass6.root","READ");break;    
				case 7: file= TFile::Open("../files/pass6_iovs.root","READ");
			}
			// Loop over slots
			for(int sl=0; sl<22; sl++){
				// Make names for the histograms to open
				std::string dpname = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),sl);
				std::string dename = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),sl);
				TH1F *f_dp= (TH1F*)file->Get(dpname.c_str());
				TH1F *f_de= (TH1F*)file->Get(dename.c_str());

				if( !f_dp->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				// Summary plots
				h_dphi_t[pass][g][4]->Add(f_dp);
				h_deta_t[pass][g][4]->Add(f_de);
				if( sl < 4 || sl == 8 ){ 
					h_dphi_t[pass][g][0]->Add(f_dp);
					h_deta_t[pass][g][0]->Add(f_de);
				}
				else if ( (sl > 3 && sl < 8) || sl == 9 ) {
					h_dphi_t[pass][g][1]->Add(f_dp);
					h_deta_t[pass][g][1]->Add(f_de);
				}
				else if ( sl > 9 && sl < 16 ){
					h_dphi_t[pass][g][2]->Add(f_dp);
					h_deta_t[pass][g][2]->Add(f_de);
				}
				else if ( sl > 15 ){
					h_dphi_t[pass][g][3]->Add(f_dp);
					h_deta_t[pass][g][3]->Add(f_de);
				}
			}
			// Make EMB/EMEC
			//LOW gain info
			//  if(g==2)
			//	    //cout<<"findme"<<"pass"<<pass<<"d"<<d<<"\t"<<entries<<endl;
			// separate plots
			// pass0
			/*
			   TCanvas *gg = new TCanvas(Form("c_gg%d_%d",g,d),"ggg",800,700);
			   h_dphi_t[pass][g][d]->Draw();
			//FIXME fit info
			//if (d ==0){
			//  std:://cout << "EMB:   Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			//if( d== 2){ 
			// std:://cout << "EMEC:  Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			myText(0.73, 0.83,kBlack,Form("Entries: %d",h_dphi_t[pass][g][d]->GetEntries()),0.6);
			myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",h_dphi_t[pass][g][d]->GetMean()),0.6);
			myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",h_dphi_t[pass][g][d]->GetRMS()),0.6);
			h_dphi_t[pass][g][d] = fitGaus( h_dphi_t[pass][g][d],h_dphi_t[pass][g][d]->GetMean() ,h_dphi_t[pass][g][d]->GetRMS(),.72 );
			ATLASLabel(0.2, 0.88,pInternal);
			if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
			else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
			else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
			myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			myText(0.22, 0.72,kBlack,Form("Pass %d (W#rightarrow e#nu)",pass),0.6); 
			h_dphi_t[pass][g][d]->GetYaxis()->SetRangeUser(0.5,h_dphi_t[pass][g][d]->GetBinContent( h_dphi_t[g][d][0]->GetMaximumBin() )/0.08);
			// h_dphi_t[pass][g][d]->GetXaxis()->SetRangeUser(-15,15);
			gg->SetLogy(1);
			if( d== 0)gg->SaveAs(Form("Log_pass%d_EMB_Celltime_%s.png",pass,gain[g].c_str())); 
			else if( d==2)gg->SaveAs(Form("Log_pass%d_EMEC_Celltime_%s.png",pass,gain[g].c_str())); 
			else if ( d==4)gg->SaveAs(Form("Log_pass%d_All_Celltime_%s.png",pass,gain[g].c_str()));
			delete gg;
			*/
			//   file[pass]->Close();
			h_dphi_t[pass][g][0]->Add( h_dphi_t[pass][g][0+1]);
			h_dphi_t[pass][g][2]->Add( h_dphi_t[pass][g][2+1]);
			h_deta_t[pass][g][0]->Add( h_deta_t[pass][g][0+1]);
			h_deta_t[pass][g][2]->Add( h_deta_t[pass][g][2+1]);

		}//loop over pass
		for(int d=0;d<=4;d=d+2){
			int dd=d/2;
			TProfile *ff_e[2][passN];
			string angle[2]={"phi","eta"};

			for (int a=0; a<2; a++){
				TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
				for(int pass=0; pass<passN; pass++){
					int entries =  h_dphi_t[0][g][d]->GetEntries();
					if(a==0)
						ff_e[a][pass] = h_dphi_t[pass][g][d]->ProfileX(Form("ff_e[a]%d",pass),-1,-1,"o");
					else ff_e[a][pass] = h_deta_t[pass][g][d]->ProfileX(Form("ff_e[a]%d",pass),-1,-1,"o");      
					cout<<pass<<"\t"<<d<<"\t"<<g<<"\t"<< h_dphi_t[pass][g][d]->GetEntries()<<endl;
					if(pass==0)
						ff_e[a][pass]->Draw();
					ff_e[a][pass]->Draw("same");
					ff_e[a][pass]->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_e[a][pass]->GetXaxis()->SetRangeUser(-1,1);
					if(a==1)
						ff_e[a][pass]->GetXaxis()->SetRangeUser(-0.6,0.6);
					ff_e[a][pass]->GetYaxis()->SetRangeUser(tmin,1.5);
					ff_e[a][pass]->SetLineColor(pass_Color[pass]);
					ff_e[a][pass]->SetMarkerColor(pass_Color[pass]);
					if(pass==0)
						ff_e[a][pass]->Draw();
					ff_e[a][pass]->Draw("same");
					//	c3->SetLogx(1);   
					if(pass==0){
						ATLASLabel(0.2, 0.88,pInternal);
						//myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
						if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
						else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
						else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
						myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
						myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
						myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
					}
					else
						myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);

				}
				if( d== 0)c3->SaveAs(Form("pass1_pass7_EMB_%s_%s.png",angle[a].c_str(),gain[g].c_str()));
				else if( d==2)c3->SaveAs(Form("pass1_pass7_EMEC_%s_%s.png",angle[a].c_str(),gain[g].c_str()));
				else if ( d==4)c3->SaveAs(Form("pass1_pass7_All_%s_%s.png",angle[a].c_str(),gain[g].c_str()));
				//delete ff_e[passN];
				delete c3;
			}//loop over a
		}//loop over d
	}//loop over gain

	file->Close();
	return;
}
//_____________________________________________
//
// Plot avg time vs f1/f3 
// by subdetector/slot
//_____________________________________________
void doEFracPlot(){

	std::cout << "\nMaking plots for fractional energy deposit timing\n\n";



	// Initialize the histograms
	for(int k=0; k<passN; k++){
		for( int j=0; j<2; j++){
			for( int i=0; i<5; i++){
				h_f1_t[k][j][i] = new TH2F(Form("h_f1_t%d%d%d",i,j,k),Form("Cell Time %s;f1; Time [ns]",subDet[i].c_str()), 400,-0.05, .6, 500, -5, 5 );
				h_f3_t[k][j][i] = new TH2F(Form("h_f3_t%d%d%d",i,j,k),Form("Cell Time %s;f3; Time [ns]",subDet[i].c_str()), 400,-0.05, .2, 500, -5, 5 );
			}}  }
	TFile *file;

	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			switch(pass){
				case 0: file= TFile::Open("../files/recheck_2016_all/pass1.root","READ");break;    
				case 1: file= TFile::Open("../files/recheck_2016_all/pass3.root","READ");break;    
				case 2: file= TFile::Open("../files/pass3_iovs.root","READ");break;    
				case 3: file= TFile::Open("../files/recheck_2016_all/pass4.root","READ");break;    
				case 4: file= TFile::Open("../files/pass4_iovs.root","READ");break;    
				case 5: file= TFile::Open("../files/pass5_iovs.root","READ");   break; 
				case 6: file= TFile::Open("../files/recheck_2016_all/pass6.root","READ");break;    
				case 7: file= TFile::Open("../files/pass6_iovs.root","READ");
			}
			// Loop over slots
			for(int sl=0; sl<22; sl++){
				// Make names for the histograms to open
				std::string dpname = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),sl);
				std::string dename = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),sl);
				TH1F *f_f1= (TH1F*)file->Get(dpname.c_str());
				TH1F *f_f3= (TH1F*)file->Get(dename.c_str());

				if( !f_f1->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				// Summary plots
				h_f1_t[pass][g][4]->Add(f_f1);
				h_f3_t[pass][g][4]->Add(f_f3);
				if( sl < 4 || sl == 8 ){ 
					h_f1_t[pass][g][0]->Add(f_f1);
					h_f3_t[pass][g][0]->Add(f_f3);
				}
				else if ( (sl > 3 && sl < 8) || sl == 9 ) {
					h_f1_t[pass][g][1]->Add(f_f1);
					h_f3_t[pass][g][1]->Add(f_f3);
				}
				else if ( sl > 9 && sl < 16 ){
					h_f1_t[pass][g][2]->Add(f_f1);
					h_f3_t[pass][g][2]->Add(f_f3);
				}
				else if ( sl > 15 ){
					h_f1_t[pass][g][3]->Add(f_f1);
					h_f3_t[pass][g][3]->Add(f_f3);
				}
			}
			h_f1_t[pass][g][0]->Add( h_f1_t[pass][g][0+1]);
			h_f1_t[pass][g][2]->Add( h_f1_t[pass][g][2+1]);
			h_f3_t[pass][g][0]->Add( h_f3_t[pass][g][0+1]);
			h_f3_t[pass][g][2]->Add( h_f3_t[pass][g][2+1]);
		}

		for(int d=0;d<=4;d=d+2){
			int dd=d/2;
			TProfile *ff_efrac[2][passN];
			string efrac[2]={"f1","f3"};
			double nFrac =  ( g == 0 ? 100 : 75);  
			//loop of f1 f3
			for (int a=0; a<2; a++){
				TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
				for(int pass=0; pass<passN; pass++){
					h_f1_t[pass][g][d] = rebin( h_f1_t[pass][g][d], h_f1_t[pass][g][d]->GetEntries()/nFrac, h_f1_t[pass][g][d]->GetXaxis()->FindBin(0.)-1);
					h_f3_t[pass][g][d] = rebin( h_f3_t[pass][g][d], h_f3_t[pass][g][d]->GetEntries()/nFrac, h_f3_t[pass][g][d]->GetXaxis()->FindBin(0.)-1);

					if(a==0)
						ff_efrac[a][pass] = h_f1_t[pass][g][d]->ProfileX(Form("ff_efrac[a]%d",pass),-1,-1,"o");
					else ff_efrac[a][pass] = h_f3_t[pass][g][d]->ProfileX(Form("ff_efrac[a]%d",pass),-1,-1,"o");      
					if(pass==0)
						ff_efrac[a][pass]->Draw();
					ff_efrac[a][pass]->Draw("same");
					ff_efrac[a][pass]->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_efrac[a][pass]->SetErrorOption("g");
					ff_efrac[a][pass]->GetXaxis()->SetRangeUser(0,0.6);
					if(a==1)
						ff_efrac[a][pass]->GetXaxis()->SetRangeUser(ff_efrac[a][pass]->GetXaxis()->FindBin(5e-3),ff_efrac[a][pass]->GetXaxis()->FindBin(0.2));
					//		ff_efrac[a][pass]->GetXaxis()->SetRangeUser(0,80);   
					ff_efrac[a][pass]->SetLineColor(pass_Color[pass]);
					ff_efrac[a][pass]->SetMarkerColor(pass_Color[pass]);
					if(pass==0)
						ff_efrac[a][pass]->Draw();
					ff_efrac[a][pass]->Draw("same");
					//	c3->SetLogx(1);   
					if(pass==0){
						ATLASLabel(0.2, 0.88,pInternal);
						//			myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
						if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
						else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
						else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
						myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
						myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
						myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
					}
					else
						myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);

				}
				if( d== 0)     c3->SaveAs(Form("pass1_pass7_EMB_%s_%s.png",efrac[a].c_str(),gain[g].c_str()));
				else if( d==2) c3->SaveAs(Form("pass1_pass7_EMEC_%s_%s.png",efrac[a].c_str(),gain[g].c_str()));
				else if ( d==4)c3->SaveAs(Form("pass1_pass7_All_%s_%s.png",efrac[a].c_str(),gain[g].c_str()));
				//delete ff_efrac[passN];
				delete c3;
			}//loop over a
		}//loop over d
	}//loop over gain

	file->Close();
	return;
}

void doCellTimePlot(){

	std::cout << "\nMaking plots for average Cell Timing\n\n";
	for(int k=0; k<passN; k++)
		for( int j=0; j<2; j++)
			for( int i=0; i<5; i++)
				h_cell_t_pass[k][j][i] = new TH1F(Form("h_cell_t_pass%d%d%d",k,j,i),Form("Cell Time %s; Time [ns]; Events / 0.02 ns",subDet[i].c_str()),1000,-25,25);
	TFile *file;
	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			switch(pass){
				//	case 0: file= TFile::Open("../files/recheck_2016_all/pass2.root","READ");break;    
				case 0: file= TFile::Open("../files/pass7_new_iovs_dis_front.root","READ");break;    
				case 1: file= TFile::Open("../files/pass8_new_iovs_dis_front.root","READ");break;    
			//	case 2: file= TFile::Open("../files/pass8_new_iovs_dis_front.root","READ"); 
			//	case 1: file= TFile::Open("../files/pass6_iovs_actual_plot_mu_bad_channel.root","READ");break;    
			//	case 2: file= TFile::Open("../files/pass7_iovs_actual_plot_mu_bad_channel.root","READ");break;    
			}
			// Loop over slots
			for(int sl=0; sl<22; sl++){
				
				// Make names for the histograms to open
				std::string tname = Form("h_cellTimeAll%s%d",gain[g].c_str(),sl);
				TH1F *f_t = (TH1F*)file->Get(tname.c_str());

				if( !f_t->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				// Summary plots
				h_cell_t_pass[pass][g][4]->Add(f_t);
				if( sl < 4 || sl == 8 ){ 
					h_cell_t_pass[pass][g][0]->Add(f_t);
				}
				else if ( (sl > 3 && sl < 8) || sl == 9 ) {
					h_cell_t_pass[pass][g][1]->Add(f_t);
				}
				else if ( sl > 9 && sl < 16 ){
					h_cell_t_pass[pass][g][2]->Add(f_t);
				}
				else if ( sl > 15 ){
					h_cell_t_pass[pass][g][3]->Add(f_t);
				}
			}
			// Make EMB/EMEC
			//LOW gain info
			//  if(g==2)
			//	    //cout<<"findme"<<"pass"<<pass<<"d"<<d<<"\t"<<entries<<endl;
			// separate plots
			// pass0
			/*
			   TCanvas *gg = new TCanvas(Form("c_gg%d_%d",g,d),"ggg",800,700);
			   h_cell_t_pass[pass][g][d]->Draw();
			//FIXME fit info
			//if (d ==0){
			//  std:://cout << "EMB:   Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			//if( d== 2){ 
			// std:://cout << "EMEC:  Mean: " << mean <<"; RMS: " <<  rms << std::endl;
			// }
			myText(0.73, 0.83,kBlack,Form("Entries: %d",h_cell_t_pass[pass][g][d]->GetEntries()),0.6);
			myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",h_cell_t_pass[pass][g][d]->GetMean()),0.6);
			myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",h_cell_t_pass[pass][g][d]->GetRMS()),0.6);
			h_cell_t_pass[pass][g][d] = fitGaus( h_cell_t_pass[pass][g][d],h_cell_t_pass[pass][g][d]->GetMean() ,h_cell_t_pass[pass][g][d]->GetRMS(),.72 );
			ATLASLabel(0.2, 0.88,pInternal);
			if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
			else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
			else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
			myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			myText(0.22, 0.72,kBlack,Form("Pass %d (W#rightarrow e#nu)",pass),0.6); 
			h_cell_t_pass[pass][g][d]->GetYaxis()->SetRangeUser(0.5,h_cell_t_pass[pass][g][d]->GetBinContent( h_cell_t_pass[g][d][0]->GetMaximumBin() )/0.08);
			// h_cell_t_pass[pass][g][d]->GetXaxis()->SetRangeUser(-15,15);
			gg->SetLogy(1);
			if( d== 0)gg->SaveAs(Form("Log_pass%d_EMB_Celltime_%s.png",pass,gain[g].c_str())); 
			else if( d==2)gg->SaveAs(Form("Log_pass%d_EMEC_Celltime_%s.png",pass,gain[g].c_str())); 
			else if ( d==4)gg->SaveAs(Form("Log_pass%d_All_Celltime_%s.png",pass,gain[g].c_str()));
			delete gg;
			*/
			file->Close();
			h_cell_t_pass[pass][g][0]->Add( h_cell_t_pass[pass][g][0+1]);
			h_cell_t_pass[pass][g][2]->Add( h_cell_t_pass[pass][g][2+1]);

			cout<<"debug"<<"\t"<<"0"<<"\t"<<" p"<<"\t"<< pass<<"\t"<<h_cell_t_pass[pass][g][2]->GetEntries()<<endl;
			cout<<"debug"<<"\t"<<"1"<<"\t"<<" p"<<"\t"<< pass<<"\t"<<h_cell_t_pass[pass][g][2]->GetEntries()<<endl;
		}//loop oveor pass

		//	h_cell_t_pass[1][1][4]->Add(h_cell_t_pass[0][1][4],-1); 
		for(int d=0;d<=4;d=d+2){
			TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
			for(int pass=0; pass<passN; pass++){
				cout<<pass<<"\t"<<d<<"\t"<<g<<"\t"<< h_cell_t_pass[pass][g][d]->GetEntries()<<endl;
				int entries = 104318999;
				if(pass==0)
					h_cell_t_pass[pass][g][d]->Draw();
				entries = h_cell_t_pass[pass][g][d]->GetEntries();
				h_cell_t_pass[pass][g][d]->Draw("same");
				h_cell_t_pass[pass][g][d]->GetXaxis()->SetRangeUser(-25,25);//5555
				double rms  = h_cell_t_pass[pass][g][d]->GetRMS();
				double mean = h_cell_t_pass[pass][g][d]->GetMean();
				h_cell_t_pass[pass][g][d] = fitGaus( h_cell_t_pass[pass][g][d], mean, rms,.78 , pass_Color[pass], pass);
				//	c3= fitroofit_all( h_cell_t_pass[pass][g][d], mean, rms,.78 , pass_Color[pass], pass);
				h_cell_t_pass[pass][g][d]->SetLineColor(pass_Color[pass]);
				if(pass==0){
					ATLASLabel(0.2, 0.88,pInternal);
					myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
					if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
					else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
					else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
					myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
					myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);

					double maxY = h_cell_t_pass[pass][g][d]->GetBinContent( h_cell_t_pass[pass][g][d]->GetMaximumBin() );
					h_cell_t_pass[pass][g][d]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
				}
				else
					myText(0.22, 0.72-pass*0.03,pass_Color[pass],Form("%s", Correction[pass].c_str()),0.5);
			}
			c3->SetLogy();
			// FIXME
			if( d== 0)c3->SaveAs(Form("~/WWW/bunch_position/pass7_EMB_Celltime_%s.png",gain[g].c_str()));
			else if( d==2) c3->SaveAs(Form("~/WWW/bunch_position/pass7_EMEC_Celltime_%s.png",gain[g].c_str()));
			else if ( d==4)c3->SaveAs(Form("~/WWW/bunch_position/pass7_All_Celltime_%s.png",gain[g].c_str()));
			delete c3;
		}
	}//loop over gain

	return;
}

//_____________________________________________
//
// Read list of run numbers from config file
//_____________________________________________
void setRunList(){

	// Run number config file
	ifstream f1("../config/RunNumberList.txt");
	int tempRunNum;

	// Store each run into local vector
	while( f1 >> tempRunNum ){
		runNumberList.push_back(tempRunNum);
	}

	cout<<"run number list"<<endl;
	f1.close();

	std::cout << "  > Added " << runNumberList.size() << " runs successfully from "
		<< runNumberList[0] << " to " << runNumberList.back() << "\n\n";
	return;
}


//_____________________________________________
//
// Set tmin/tmax for plotting
//_____________________________________________
void setTRange(double t_l, double t_h){

	tmin = t_l;
	tmax = t_h;

	return;
}

TCanvas *fitroofit_all(TH1F* h_cell, double mean, double rms, double pos_y, int color,int pass)
{
	gStyle->SetOptFit(1112); 
	TCanvas *ggg = new TCanvas(Form("c_gg1_%d", pass),"ggg",800,700);
	//	cout<<"order"<<"\t"<<order<<"\t"<<d<<"\t"<<rms<<endl;
	RooRealVar x("mes","time (ns)",-15,15) ;
	// --- Build Gaussian signal PDF ---
	RooRealVar sigmean("#mu","b^{#pm} mass",mean,mean-rms,mean+rms) ;
	RooRealVar sigwidth("#sigma","B^{#pm} width",rms,0,2*rms) ; 
	RooGaussian gauss("gauss","gaussian PDF",x,sigmean,sigwidth) ;
	RooDataHist data("data", "dataset", x, h_cell);
	RooPlot* mesframe = x.frame() ;

	gauss.fitTo(data,Range(mean-5*rms,mean+5*rms) );
	data.plotOn(mesframe, Name("data")) ;
	gauss.plotOn(mesframe, Name("model")) ; 

	float  signalchi = mesframe->chiSquare("model","data",3);

	gauss.paramOn(mesframe,Parameters( RooArgSet(sigmean,sigwidth)), Format("NELU", 2), Layout(0.7, 0.95, 0.75-pass*0.3) );
	mesframe->getAttText()->SetTextSize(0.03) ; 
	TPaveText *box= new TPaveText(0.73, 0.835, 0.95, 0.875,"BRNDC");
	box->SetFillColor(10);
	box->SetBorderSize(1);
	box->SetTextAlign(12);
	box->SetTextSize(0.03F);
	box->SetFillStyle(1001);
	box->SetFillColor(10);
	TText *text = 0;
	Char_t buf[30];
	sprintf( buf,  "#chi^{2}/ndf = %f", signalchi);
	text = box->AddText( buf );
	mesframe->addObject(box);
	mesframe-> SetMinimum(1);
	if(pass==0)

		mesframe->Draw("");
	else
		mesframe->Draw("same");

	ggg->Modified();
	ggg->Update();

	return ggg;


}

TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y, int color, int pass ){
	  
	  // Hide fit stats
	  gStyle->SetOptFit(0);
	 
	  // Gaus fit (limited range around peak)
	 TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
	  //fg->SetParameter(0,0); // not sure if this is necessary
	  
	  // guess starting point for fit
	  fg->SetParameter(1,mean);
	  fg->SetParameter(2,rms);
	  
	  // put limits on the paramters
	  fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
	 fg->SetParLimits(2,0.5*rms,4*rms);
	  
	  // Draw the fit in subrange
	  fg->SetLineColor(kRed);
	  h_cell->Fit("fg","BR");
	double chi_old = 1.1e5;
	double chi_new = 1e5;
	while(chi_old-chi_new>1e-7){
		//      for(int j=0;j<100;j++){ 
		chi_old = chi_new;
		cout<<"chi_old"<<chi_old<<endl;

		chi_new = fg->GetChisquare()/fg->GetNDF();
		cout<<"chi_new"<<chi_new<<endl;
		fg->SetParameters(fg->GetParameters());  
		h_cell->Fit("fg","R");
	}
//	  fg->Draw("same");
	  
	  // Make the fit extend and draw as dotted line
	  TF1 *f2 = new TF1("f2","gaus",-25,25);
	  f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
	  f2->SetLineColor(kRed);
	  f2->SetLineStyle(7); //dashed line
	  f2->Draw("same");
	  
	  // Put parameters on the plot
	  double pos_x = 0.78;
	  myText(pos_x, pos_y-0.06*pass,color,Form("#mu: %.3f ns",fg->GetParameter(1)),0.6);
	  myText(pos_x, pos_y-0.06*pass-0.03,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.6);

	  //FIXME printing info
	  //std::cout << "    Mu: " << fg->GetParameter(1) << "; Sigma: "<<fg->GetParameter(2) <<std::endl;
	  return h_cell;

	}


	// Rebin the 2D histogram so that each bin
	// has at least entPerBin entries
	// firstBin is the first bin you want to consider
	//_____________________________________________
	TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

	  // hold the bin edge information
	  std::vector< double > xbins;
	  // Combine all bins below first bin
	  // Cut at 5 GeV so this bin is empty
	  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
	  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

	  // keep track of last bin with at least entPerBin
	  int lastFullIndex = firstBin; // for energy 5
	  // Get the xaxis
	  TAxis *axis = h->GetXaxis();

	  // Loop over bins in xaxis
	  // start after 5GeV bin
	  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {
	    // Get entries in this bin, and width
	    int y = h->Integral(i,i);
	    double w = axis->GetBinWidth(i);

	    // If not enough entries, need to combine bins
	    if (y <= entPerBin){
	      // Find integral from last combined bin
	      double integral = h->Integral(lastFullIndex+1, i);
	      if (integral <= entPerBin ) continue;
	      // if above threshold, mark as new bin
	      lastFullIndex = i;
	      xbins.push_back( axis->GetBinLowEdge(i) + w);
	    }
	    else{
	      // above threshold, mark as bin
	      lastFullIndex = i;
	      xbins.push_back( axis->GetBinLowEdge(i) + w );
	    }

	  }

	  // put bin edges into an array
	  xbins.push_back( axis->GetXmax() );
	  size_t s = xbins.size();
	  double *xbinsFinal = &xbins[0];
	cout<<"s"<<s<<endl;
	  // create new histo with new bin edges
	  TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
	  hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());

	  hnew->Sumw2();
	 // cout<<
	  // fill new histo with old values
	  for( int i=1; i<=h->GetNbinsX(); i++){
	    for( int j=1; j<=h->GetNbinsY(); j++){
		//    for(int a=1;a<=h->GetBinContent(i,j);a++)
	     hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j), h->GetBinContent(i,j));
	      //hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
	  //    if(i==200)
	//	      cout<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<"\t"<<h->GetBinError(i,j)<<endl;
	     // hnew->
	//if(i<=h->GetNbinsX()/2&&i>=h->GetNbinsX()/2-1)
	    }
	  }
	//cout<<"h"<<"\t"<<h->GetEntries()<<endl;
	//cout<<"h"<<"\t"<<h->GetBinContent(200,250)<<endl;
	//or(j =1; j<=hnew->GetNbinsY();j++)
	//	if( hnew-> GetBinContent(38,j)!=0)
	//cout<<"h_new"<<"\t"<<hnew->GetXaxis()->GetBinCenter(38)<<"\t"<<""<<endl;
	//cout<<"h_new"<<"\t"<<hnew->GetEntries()<<endl;
	//
	//double stats[7];
	//hnew->GetStats(stats);
	//for(i=0;i<7;i++)
	//	cout<<"star"<<stats[7]<<endl;
	  TH2F* h_return = (TH2F*)hnew->Clone("");
	  delete hnew;

	  // return histo
	  return h_return;
	}




	//__________________________________________
	//
	// Put info for fit plots
	// g=gain sl=slot index
	//__________________________________________
	void drawFitInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
	  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

	  return;

	}


	//____________________________________________
	//
	// Put info for run by run plots
	// g=gain, rn = run number index
	//____________________________________________
	void drawRunInfo(int g, int rn){
	  
	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

	  return;

	}

	//_________________________________________
	//
	// Put info for slot by slot plots
	// g=gain index, sl=slot index
	//_________________________________________
	void drawSlInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
	  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
	  
	  return;

	}
	//_________________________________________
	//
	// Put info for subdetector summary plots
	// g=gain index, d=subdetector index
	//_________________________________________
	void drawSubDetInfo(int g, int d){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
	  
	  return;

	}

	//_________________________________________
	//
	// Put info for subdetector summary plots
	// ent=entries, m=mean, r=rms
	//_________________________________________
	void drawStats(int ent, double m, double rms){

	  myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
	  myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
	  myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
	 
	  return;

	}

	//_________________________________________
	//
	// Put info for 3 fits on plot
	// p0, p4, p0
	//_________________________________________
	void drawStatBox(double par[7],std::string var){
	  myText(0.55,0.89,kGreen-3,Form("%s < -0.5",var.c_str()),0.6);
	  myText(0.55,0.86,kGreen-3,Form("p0: %.3f",par[0]),0.6);
	  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
	  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
	  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
	  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
	  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
	  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
	  myText(0.83,0.89,kGreen-3,Form("0.5 < %s",var.c_str()),0.6);
	  myText(0.83,0.86,kGreen-3,Form("p0: %.3f",par[6]),0.6); 
	  return;
	}
