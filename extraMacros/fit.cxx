//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooChi2Var.h"
#include "RooDataSet.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooCBShape.h"
#include "RooExponential.h"
 #include "RooDataSet.h"
#include "RooDataHist.h"
#include "TMinuit.h"

#include "TCanvas.h"
#include "RooFFTConvPdf.h"
//#include "TDataSet.h"
#include "TVirtualFFT.h"
#include "RooPlot.h"
#include "TAxis.h"
#include  "RooDecay.h"
using namespace RooFit ;
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include <TMath.h>
#include <TF1NormSum.h>
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"
#include "Math/DistFunc.h"
// functions
void doCellTimePlot();
void drawFitInfo(int gain, int sl);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1, int order=0,int gain=0);
TH1F* fitGaus_only(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1);
TH1F* fitGaus_all(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1, int g=0);
TH1F* fitNormSum(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1, int order=0,int gain=0);
TCanvas *fitroofit(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1, int order=0,int gain=0,int d=0);
TCanvas *fitroofit_all(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1, int order=0,int g=0);
TCanvas *fitroofit_together(TH1F* h_cell,  double pos_y=0.74, int color=1,int gain=0,int d=0);
TH1F* fitfn[3];

//TH1F* fitGaus_TOGETHER(TH1F* h_cell, double* mean, double* rms, double pos_y=0.74, int color=1, int g=0);
int weird=0;
// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string gain[2]       = {"High","Medium"};
const std::string peakname[3]   = {"5ns", "-5ns", "10ns"};
double rms[4];
double int_ent[4];
double mean[4];
//double lowedge[4] 		= {4.5,-7,9,-15};
//double highedge[4] 		= {7, -4.4, 14,15} ;
double lowedge[4] 		= {3,-7.5,9,-3};
double highedge[4] 		= {7.5, -3.5, 14,3} ;
const int passN=1;
bool docenter=1, dopeak=0;
// histograms
TH1F *h_cell_t_pass[passN-1][2][5];  // EMBA/C, EMECA/C, all
// Configuration
const bool saveEPS = true;
const bool debug   = false;
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;
int array_order =0;
double paramH[3][7];
double param[3][6];
double paramM[3][3];
ofstream text_file0("peak1.txt");
ofstream text_file1("peak2.txt");
ofstream text_file2("peak3.txt");

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void fit(int pn=0, int mode=0){
	SetAtlasStyle();    
#ifdef __CINT__
	gROOT->LoadMacro("../utils/AtlasLabels.C");
	gROOT->LoadMacro("../utils/AtlasStyle.C");  
#endif
	doCellTimePlot();
}




//_____________________________________________
//
// Plot avg Cell time 
// by subdetector/slot
//_____________________________________________
void doCellTimePlot(){
  std::cout << "\nMaking plots for average Cell Timing\n\n";
  for(int k=0; k<passN; k++)
	  for( int j=0; j<2; j++)
		  for( int i=0; i<5; i++)
			  h_cell_t_pass[k][j][i] = new TH1F(Form("h_cell_t_pass%d%d%d",k,j,i),Form("Cell Time %s; Time [ns]; Events / 0.02 ns",subDet[i].c_str()),1000,-25,25);

	// Open the data file
	TFile *file;
	// Loop over gains
	for( int g=0; g<2; g++){  
		for(int pass=0; pass<passN; pass++){
			// Loop over slots
			switch(pass){
				case 0: file= TFile::Open("../files/pass8_iovs_actual_plot_mu.root ","READ");    
//				case 0: file= TFile::Open("../files/pass8_iovs_actual_plot_mu.root","READ");    

			}
			// Make names for the histograms to open
			for(int sl=0; sl<22; sl++){
				std::string tname = Form("h_cellTimeAll%s%d",gain[g].c_str(),sl);
				TH1F *f_t = (TH1F*)file->Get(tname.c_str());

				if( !f_t->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				// Summary plots
				h_cell_t_pass[pass][g][4]->Add(f_t);
				if( sl < 4 || sl == 8 ){ 
					h_cell_t_pass[pass][g][0]->Add(f_t);
				}
				else if ( (sl > 3 && sl < 8) || sl == 9 ) {
					h_cell_t_pass[pass][g][1]->Add(f_t);
				}
				else if ( sl > 9 && sl < 16 ){
					h_cell_t_pass[pass][g][2]->Add(f_t);
				}
				else if ( sl > 15 ){
					h_cell_t_pass[pass][g][3]->Add(f_t);
				}
			}

		   file->Close();
		   h_cell_t_pass[pass][g][0]->Add( h_cell_t_pass[pass][g][0+1]);
		   h_cell_t_pass[pass][g][2]->Add( h_cell_t_pass[pass][g][2+1]);
		}//loop over pass
		for(int d=0; d<5; d=d+2){
			// separate peaks
			TH1F *h[3];
			for(int peak=0;peak<3; peak++){
			h[peak]= (TH1F*)h_cell_t_pass[passN-1][g][d]->Clone();
			h[peak]->GetXaxis()->SetRangeUser(lowedge[peak],highedge[peak]); 
			int_ent[peak]=h[peak]->Integral(h[peak]->FindBin(lowedge[peak]), h[peak]->FindBin(highedge[peak]));
			mean[peak]= h[peak]->GetMean(); 
			rms[peak]= h[peak]->GetRMS(); 
			cout<<"peak"<<"\t"<<mean[peak]<<"\t"<<rms[peak]<<"\t"<<int_ent[peak]<<endl;

			if(dopeak){
				TCanvas *ggg;
				//ggg= fitroofit( h[peak], mean[peak], rms[peak],.72, 1,peak,g,d);

				ggg = new TCanvas("c_gg","ggg",800,700);


				if(peak==2&&g==1){ 
				h[peak]->Draw("E");
				}
				else{
					ggg= fitroofit( h[peak], mean[peak], rms[peak],.72, 1,peak,g,d);
					
					//h[peak]=fitNormSum(h[peak], mean[peak], rms[peak],.72, 1,peak,g);//double mean, double rms, double pos_y, int color, int order, int g	
			    //     h[peak]=fitGaus(h[peak], mean[peak], rms[peak],.72, 1,peak,g);//double mean, double rms, double pos_y, int color, int order, int g	
				}
				myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",mean[peak]),0.6);
				myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",rms[peak]),0.6);
				ATLASLabel(0.2, 0.88,pInternal);
				if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
				else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
				else if( d == 4 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
				myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
				myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.6);
				myText(0.22, 0.72,kBlack,Form("Pass 7 (W#rightarrow e#nu)"),0.6); 
				myText(0.22, 0.68,kBlack,Form("Peak ~5ns"),0.6); 
				h[peak]->GetYaxis()->SetRangeUser(0.5,h[peak]->GetBinContent(h[peak]->GetMaximumBin())/0.8);
			//		gg1->SetLogy(1);
				if( d== 0)ggg->SaveAs(Form("~/WWW/peak/peak%d_Log_1st_pass7_EMB_Celltime_%s.png",peak,gain[g].c_str())); 
				else if( d==2)ggg->SaveAs(Form("~/WWW/peak/peak%d_Log_1st_pass7_EMEC_Celltime_%s.png",peak,gain[g].c_str())); 
				else if ( d==4)ggg->SaveAs(Form("~/WWW/peak/peak%d_Log_1st_pass7_All_Celltime_%s.png",peak,gain[g].c_str()));
				delete ggg;
			}//loop over peak
			}
			//	for(int i=0; i<3; i++)
		//	for(int j=0; j<6; j++)
		//		cout<<"findme!!!!!!!!!!!!!"<<g<<"\t"<<d<<"\t"<<param[i][j]<<endl;
		        if(docenter){
			TCanvas *gg = new TCanvas(Form("c_gg%d_%d",g,d),"gg",800,700);
			h_cell_t_pass[passN-1][g][d]->Draw();
			rms[3]  = h_cell_t_pass[passN-1][g][d]->GetRMS();
			mean[3] = h_cell_t_pass[passN-1][g][d]->GetMean();
			h_cell_t_pass[passN-1][g][d]->GetYaxis()->SetRangeUser(0.5, h_cell_t_pass[passN-1][g][d]->GetBinContent( h_cell_t_pass[passN-1][g][d]->GetMaximumBin())/0.8);
			h_cell_t_pass[passN-1][g][d]->GetXaxis()->SetRangeUser(-15,15); 
			int_ent[3]=h_cell_t_pass[passN-1][g][d]-> GetEntries();
		//	h_cell_t_pass[passN-1][g][d]->Draw();
		//	int entries = h_cell_t_pass[passN-1][g][d]->GetEntries();
		//`	myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
	  		h_cell_t_pass[passN-1][g][d] = fitGaus_all( h_cell_t_pass[passN-1][g][d], mean[3], rms[3],.72 ,1, g);
		  //      gg= fitroofit_all( h_cell_t_pass[passN-1][g][d], mean[3], rms[3],.72 ,1,3, g);
	//	        gg= fitroofit_together( h_cell_t_pass[passN-1][g][d],.72 ,1, g,d);
			ATLASLabel(0.2, 0.88,pInternal);
		//	myText(0.73, 0.80,kBlack,Form("Mean: %.3f ns",mean[3]),0.6);
		//	myText(0.73, 0.77,kBlack,Form("RMS: %.3f ns",rms[3]),0.6);
			if(d == 0 )myText(0.22, 0.68,kBlack,Form("EMB"),0.7);
			else if( d == 2 )myText(0.22, 0.68,kBlack,Form("EMEC"),0.7);
			else if( d == 4 )myText(0.22, 0.68,kBlack,Form("EMB+EMEC"),0.7);
			myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.6);
			myText(0.22, 0.72,kBlack,Form("Pass 8 (W#rightarrow e#nu)"),0.6);
			if(weird==1){
				h_cell_t_pass[passN-1][g][d]->GetYaxis()->SetRangeUser(0.5,1000);
				if(g==1)
					h_cell_t_pass[passN-1][g][d]->GetYaxis()->SetRangeUser(0.5,200); 
				if( d== 0)gg->SaveAs(Form("full_pass0_EMB_Celltime_%s.png",gain[g].c_str())); 
				else if( d==2)gg->SaveAs(Form("full_pass0_EMEC_Celltime_%s.png",gain[g].c_str())); 
				else if ( d==4)gg->SaveAs(Form("full_pass0_All_Celltime_%s.png",gain[g].c_str()));
			}else{
				gg->SetLogy(1);
				if( d== 0)gg->SaveAs(Form("~/WWW/peak/full_Log_1st_pass7_EMB_Celltime_%s.png",gain[g].c_str())); 
				else if( d==2)gg->SaveAs(Form("~/WWW/peak/full_Log_1st_pass7_EMEC_Celltime_%s.png",gain[g].c_str())); 
				else if ( d==4)gg->SaveAs(Form("~/WWW/peak/full_Log_1st_pass7_All_Celltime_%s.png",gain[g].c_str()));
			}
			delete gg;
			}
		}//loop over d
	} // end loop over gains

	return;
}
TCanvas *fitroofit(TH1F* h_cell, double mean, double rms, double pos_y, int color, int order,int g,int d)
{
	gStyle->SetOptFit(1112); 
	TCanvas *ggg = new TCanvas(Form("c_gg1%d_%d_%d",g,d, order),"ggg",800,700);
//	cout<<"order"<<"\t"<<order<<"\t"<<d<<"\t"<<rms<<endl;
	h_cell->Draw();
	int nsig0;
	if(order==3)
	 nsig0 =  h_cell-> Integral(h_cell->FindBin(-2),h_cell->FindBin(2));
	else
	  nsig0=	h_cell-> Integral(h_cell->FindBin(mean-rms),h_cell->FindBin(mean+rms));//h_cell->Integral(h_cell->FindBin(lowedge[order]+1.5),h_cell->FindBin(highedge[order]));
	int nbkg0 = h_cell->Integral(h_cell->FindBin(lowedge[order]),h_cell->FindBin(highedge[order]))-nsig0;
//	int nbkg0 = h_cell->Integral(h_cell->FindBin(lowedge[order]), h_cell->FindBin(lowedge[order]+1.5));
	// --- Observable ---
	RooRealVar x("mes","time (ns)",lowedge[order],highedge[order]) ;
	// --- Build Gaussian signal PDF ---
	RooRealVar sigmean("#mu","b^{#pm} mass",mean,mean-rms,mean+rms) ;
	RooRealVar sigmean1("#mu","b^{#pm} mass",mean,mean-rms,mean+rms) ;
	RooRealVar sigwidth("#sigma","B^{#pm} width",rms,0,2*rms) ; 
	RooRealVar sigwidth1("#sigma_L","B^{#pm} width",rms,0,2*rms) ; 
	RooRealVar sigwidth2("#sigma_R","B^{#pm} width",rms,0,2*rms) ; 
	RooGaussian gauss("gauss","gaussian PDF",x,sigmean,sigwidth) ;
	RooGaussian gauss1("gauss","gaussian PDF",x,sigmean,sigwidth1) ;
	RooGaussian gauss2("gauss","gaussian PDF",x,sigmean1,sigwidth2) ;
	RooRealVar sig1frac("sig1frac","fraction of component 1 in signal",0.8,0.,1.) ;
	RooAddPdf sig("sig","Signal",RooArgList(gauss1,gauss2),sig1frac) ;
	RooBreitWigner bw("bw","bwpdf",x,sigmean,sigwidth);
	RooBifurGauss bifurgaus("bifurgaus", "bg",x, sigmean,sigwidth1,sigwidth2);
	RooLandau landau("ld","lddf",x,sigmean,sigwidth);

	RooVoigtian voigt("voigt","voigtpdf",x, sigmean, sigwidth1, sigwidth2);
	// RooRealVar m("m", "Dependent", 2.2,4.6);
	RooRealVar alpha("alpha", "Alpha", 5,-100,100);
	RooRealVar n("n", "Order", 6,-100,100);

	// Build gaussian p.d.f in terms of x,mean and sigma
	RooCBShape cbshape("cbshape","Crystal Ball", x, sigmean,sigwidth,alpha,n);  
	//
	// --- Build Argus background PDF ---
	//RooRealVar ("argpar","argus shape parameter",-20.0,-100.,-1.) ; 
	RooRealVar c("c","C",1,-1000,1000);
	RooExponential bkg("exp","Background",x,c);
	
	 RooRealVar a0("a0","a0",1) ;
	 RooRealVar a1("a1","a1",0,-1,1) ;
	 RooRealVar a2("a2","a2",1,0,10) ;
	 RooRealVar a3("a3","a3",1,0,10) ;
	 RooPolynomial p2("p2","p2",x,RooArgList(a0,a1,a2,a3),0) ;

//	RooRealVar a0("a0","a0",0.5,-10000.,10000.) ;
//	RooRealVar a1("a1","a1",0.2,-10000.,10000.) ;
//	RooChebychev bkg1("bkg1","Background 1",x,RooArgSet(a0,a1)); 

	//RooRealVar argpar("argpar","argus shape parameter",-20.0,-100.,-1.) ;
       	//RooArgusBG argus("argus","Argus PDF",x,RooConst(5.291),argpar) ;

	// --- Construct signal+background PDF ---
	RooRealVar nsig("nsig","#signal events",nsig0,0.,100000) ;
	RooRealVar nbkg("nbkg","#background events",nbkg0,0.,100000) ;
//	RooAddPdf sum("sig", "g", RooArgList(sig), RooArgList(nsig));
	//	RooAddPdf sum("sum","g+a",RooArgList(bifurgaus,bkg),RooArgList(nsig,nbkg));
	RooAddPdf sum("sum","g+a",RooArgList(gauss, bkg),RooArgList(nsig,nbkg)); 
		//RooAddPdf sum("sum","g+a",RooArgList(cbshape,bkg),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(bw,bkg1),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(landau,bkg),RooArgList(nsig,nbkg)) ;
	// --- Generate a toyMC sample from composite PDF ---
	RooDataHist data("data", "dataset", x, h_cell);
	// --- Perform extended ML fit of composite PDF to toy data ---
	RooPlot* mesframe = x.frame() ;
//	RooDataHist* dh=data.binnedClone() ;

	
	sum.fitTo(data );
	RooChi2Var chi2_lowstat("chi2_lowstat","chi2",sum,data) ;
	//double signalchi = sum.createChi2(data)->getVal();
	cout << chi2_lowstat.getVal() << endl ;
//	cout<<g<<"\t"<<d<<"\t"<<order<<endl;
//	cout<<"lala"<<nsig.getValV()<<"$\\pm$"<<nsig.getError()<<endl;  
	switch(order){
		case 0:
	std::cout.precision(0);
	text_file0<<g<<"\t"<<d<<"\t"<<order<<endl;
	text_file0<<std::fixed << std::setprecision(2)<<nsig.getValV()<<"$\\pm$"<<nsig.getError()<<endl;  
	text_file0<<"\\hline"<<endl;
	std::cout.precision(2);
	text_file0<<std::fixed << std::setprecision(2)<<sigmean.getValV()<<"$\\pm$"<<sigmean.getError()<<endl;
	text_file0<<"\\hline"<<endl;
	text_file0<<std::fixed << std::setprecision(2)<<sigwidth.getValV()<<"$\\pm$"<<sigwidth.getError()<<endl;  
//	text_file0<<"\\hline"<<endl;
//	text_file0<<std::fixed << std::setprecision(2)<<sigwidth1.getValV()<<"$\\pm$"<<sigwidth1.getError()<<endl;  
	break;
		case 1:

	std::cout.precision(0);
	text_file1<<g<<"\t"<<d<<"\t"<<order<<endl;
	text_file1<<std::fixed << std::setprecision(2)<<nsig.getValV()<<"$\\pm$"<<nsig.getError()<<endl;  
	text_file1<<"\t"<<endl; 
	std::cout.precision(2);
	text_file1<<std::fixed << std::setprecision(2)<<sigmean.getValV()<<"$\\pm$"<<sigmean.getError()<<endl;
	text_file1<<"\t"<<endl; 
	text_file1<<std::fixed << std::setprecision(2)<<sigwidth.getValV()<<"$\\pm$"<<sigwidth.getError()<<endl;  
//	text_file1<<"\t"<<endl; 
//	text_file1<<std::fixed << std::setprecision(2)<<sigwidth1.getValV()<<"$\\pm$"<<sigwidth1.getError()<<endl;  
	break;
		case 2:
	std::cout.precision(0);
	text_file2<<g<<"\t"<<d<<"\t"<<order<<endl;
	text_file2<<std::fixed << std::setprecision(2)<<nsig.getValV()<<"$\\pm$"<<nsig.getError()<<endl;  
	text_file2<<"\t"<<endl; 
	std::cout.precision(2);
	text_file2<<std::fixed << std::setprecision(2)<<sigmean.getValV()<<"$\\pm$"<<sigmean.getError()<<endl;
	text_file2<<"\t"<<endl; 
	text_file2<<std::fixed << std::setprecision(2)<<sigwidth.getValV()<<"$\\pm$"<<sigwidth.getError()<<endl;  
//	text_file2<<"\t"<<endl; 
//	text_file2<<std::fixed << std::setprecision(2)<<sigwidth1.getValV()<<"$\\pm$"<<sigwidth1.getError()<<endl;  
	break;
	}
	//cout<<sum.GetChisquare()/sum.GetNDF()<<endl;
//	result->floatParsFinal().printMultiline(text_file, 1111);
	data.plotOn(mesframe, Name("data")) ;
	sum.plotOn(mesframe, Name("model")) ; 
	sum.plotOn(mesframe,Components(bkg),LineStyle(kDashed));
//	param[order][0]=bh

	//double signalchi = sum.createChi2(data)->getVal();
//	double signalchi = 
//	RooAbsReal signalchi("chi","chi",mesframe->chiSquare("model","data",3) );      
	float  signalchi = mesframe->chiSquare("model","data",3);

	  cout << "lala"<<signalchi << endl ;


	//if(order!=2||g!=1)
	
	sum.paramOn(mesframe,Parameters( RooArgSet(sigmean,sigwidth,nsig, nbkg)), Format("NELU", AutoPrecision(2)), Layout(0.7, 0.95, 0.75) );
	mesframe->getAttText()->SetTextSize(0.03) ; 
//	TPaveLabel *t1(0.7,0.2,0.9,0.28, Form("#chi^{2} = %f", signalchi));
	TPaveText *box= new TPaveText(0.73, 0.835, 0.95, 0.875,"BRNDC");
	box->SetFillColor(10);
	box->SetBorderSize(1);
	box->SetTextAlign(12);
	box->SetTextSize(0.03F);
	box->SetFillStyle(1001);
	box->SetFillColor(10);
	TText *text = 0;
	Char_t buf[30];
	sprintf( buf,  "#chi^{2}/ndf = %f", signalchi);
	text = box->AddText( buf );

	mesframe->addObject(box);

	mesframe->Draw();
	////	t1.Draw("same");

	//	ggg->SetLogy();
	ggg->Modified();
	ggg->Update();

	return ggg;


}
TCanvas *fitroofit_all(TH1F* h_cell, double mean, double rms, double pos_y, int color, int order,int g)
{
	gStyle->SetOptFit(1); 
	TCanvas *ggg = new TCanvas(Form("c_gg1%d_%d",g, order),"ggg",800,700);
//	cout<<"order"<<"\t"<<order<<"\t"<<d<<"\t"<<rms<<endl;
	h_cell->Draw();
	int nsig0;
	if(order==3)
	 nsig0 =  h_cell-> Integral(h_cell->FindBin(-2),h_cell->FindBin(2));
	else
	  nsig0=	h_cell-> Integral(h_cell->FindBin(mean-rms),h_cell->FindBin(mean+rms));//h_cell->Integral(h_cell->FindBin(lowedge[order]+1.5),h_cell->FindBin(highedge[order]));
	int nbkg0 = h_cell->Integral(h_cell->FindBin(lowedge[order]),h_cell->FindBin(highedge[order]))-nsig0;
//	int nbkg0 = h_cell->Integral(h_cell->FindBin(lowedge[order]), h_cell->FindBin(lowedge[order]+1.5));
	// --- Observable ---
	RooRealVar x("mes","time (ns)",-15,15) ;
	// --- Build Gaussian signal PDF ---
	RooRealVar sigmean("#mu","b^{#pm} mass",mean,mean-rms,mean+rms) ;
	RooRealVar sigmean1("#mu","b^{#pm} mass",mean,mean-rms,mean+rms) ;
	RooRealVar sigwidth("#sigma","B^{#pm} width",rms,0,2*rms) ; 
	RooRealVar sigwidth1("#sigma_L","B^{#pm} width",rms,0,2*rms) ; 
	RooRealVar sigwidth2("#sigma_R","B^{#pm} width",rms,0,2*rms) ; 
	RooGaussian gauss("gauss","gaussian PDF",x,sigmean,sigwidth) ;
	//RooRealVar ("argpar","argus shape parameter",-20.0,-100.,-1.) ; 
	RooRealVar c("c","C",1,-1000,1000);
	RooExponential bkg("exp","Background",x,c);

	// --- Construct signal+background PDF ---
	RooRealVar nsig("nsig","#signal events",nsig0,0.,1e9) ;
	RooRealVar nbkg("nbkg","#background events",nbkg0,0.,1e7) ;
//	RooAddPdf sum("sig", "g", RooArgList(sig), RooArgList(nsig));
	//	RooAddPdf sum("sum","g+a",RooArgList(bifurgaus,bkg),RooArgList(nsig,nbkg));
	//RooAddPdf sum("sum","g+a",RooArgList(gauss, bkg),RooArgList(nsig,nbkg)); 
		//RooAddPdf sum("sum","g+a",RooArgList(cbshape,bkg),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(bw,bkg1),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(landau,bkg),RooArgList(nsig,nbkg)) ;
	// --- Generate a toyMC sample from composite PDF ---
	RooDataHist data("data", "dataset", x, h_cell);
	// --- Perform extended ML fit of composite PDF to toy data ---
	RooPlot* mesframe = x.frame() ;
//	RooDataHist* dh=data.binnedClone() ;

	
	gauss.fitTo(data,Range(mean-5*rms,mean+5*rms) );
//td::cout.precision(2);
//	cout<<g<<"\t"<<d<<"\t"<<order<<endl;
//	cout<<"lala"<<nsig.getValV()<<"$\\pm$"<<nsig.getError()<<endl;  
	data.plotOn(mesframe, Name("data")) ;
	gauss.plotOn(mesframe, Name("model")) ; 
	gauss.plotOn(mesframe,Components(bkg),LineStyle(kDashed));

	//double signalchi = sum.createChi2(data)->getVal();
//	double signalchi = 
//	RooAbsReal signalchi("chi","chi",mesframe->chiSquare("model","data",3) );      
	float  signalchi = mesframe->chiSquare("model","data",3);

	//if(order!=2||g!=1)
	
	gauss.paramOn(mesframe,Parameters( RooArgSet(sigmean,sigwidth)), Format("NELU", 2), Layout(0.7, 0.95, 0.75) );
	mesframe->getAttText()->SetTextSize(0.03) ; 
//	TPaveLabel *t1(0.7,0.2,0.9,0.28, Form("#chi^{2} = %f", signalchi));
	TPaveText *box= new TPaveText(0.73, 0.835, 0.95, 0.875,"BRNDC");
	box->SetFillColor(10);
	box->SetBorderSize(1);
	box->SetTextAlign(12);
	box->SetTextSize(0.03F);
	box->SetFillStyle(1001);
	box->SetFillColor(10);
	TText *text = 0;
	Char_t buf[30];
	sprintf( buf,  "#chi^{2}/ndf = %f", signalchi);
	text = box->AddText( buf );

	mesframe->addObject(box);
	mesframe-> SetMinimum(1);
	mesframe->Draw();
	////	t1.Draw("same");

	//	ggg->SetLogy();
	ggg->Modified();
	ggg->Update();

	return ggg;


}




TCanvas *fitroofit_together(TH1F* h_cell, double pos_y, int color,int g,int d)
{
	TCanvas *ggg = new TCanvas(Form("c_gg1%d_%d",g,d),"ggg",800,700);
//	cout<<"order"<<"\t"<<order<<"\t"<<d<<"\t"<<rms<<endl;
//	h_cell->Scale(1/h_cell-> GetEntries());
	h_cell->Draw();
	for(int i;i<4;i++)
		cout<<"la"<<mean[i]<<"\t"<<rms[i]<<"\t"<<int_ent[i]<<endl;
	
	int nbkg0 = h_cell->Integral(h_cell->FindBin(-2), h_cell->FindBin(2));
	// --- Observable ---
	RooRealVar x("mes","time (ns)",-2,2) ;

	// --- Build Gaussian signal PDF ---
//	RooRealVar sigmean0("#mu0","b^{#pm} mass",mean[0],mean[0]-rms[0],mean[0]+rms[0]) ;
//	RooRealVar sigmean1("#mu1","b^{#pm} mass",mean[1],mean[1]-rms[1],mean[1]+rms[1]) ;
//	RooRealVar sigmean2("#mu2","b^{#pm} mass",mean[2],mean[2]-rms[2],mean[2]+rms[2]) ;
//	RooRealVar sigmean("#mu","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
//	RooRealVar sigwidth0("#sigma_L0","B^{#pm} width",rms[0]*0.5,0,rms[0]) ; 
//	RooRealVar sigwidth01("#sigma_R0","B^{#pm} width",rms[0]*0.5,0,rms[0]) ; 
//	RooRealVar sigwidth1("#sigma_L1","B^{#pm} width",rms[1]*0.5,0,2*rms[1]) ; 
//	RooRealVar sigwidth11("#sigma_R1","B^{#pm} width",rms[1]*0.5,0,2*rms[1]) ; 
//	RooRealVar sigwidth2("#sigma_L2","B^{#pm} width",rms[2]*0.5,0,2*rms[2]) ; 
//	RooRealVar sigwidth21("#sigma_R2","B^{#pm} width",rms[2]*0.5,0,2*rms[2]) ; 
//	RooRealVar sigwidth("#sigma_R2","B^{#pm} width",rms[3]*0.5,0,2*rms[3]) ; 
	RooRealVar sigmean0("#mu0","b^{#pm} mass",5.58767e+00  ,mean[0]-rms[0],mean[0]+rms[0]) ;
	RooRealVar sigmean1("#mu1","b^{#pm} mass",-5.2,mean[1]-rms[1],mean[1]+rms[1]) ;
	RooRealVar sigmean2("#mu2","b^{#pm} mass",11.15,mean[2]-rms[2],mean[2]+rms[2]) ;
	RooRealVar sigmean("#mu","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
	RooRealVar sigwidth0("#sigma_L0","B^{#pm} width",4.32338e-01 ,0,2*rms[0]) ; 
	RooRealVar sigwidth01("#sigma_R0","B^{#pm} width",3.18366e-01,0,2*rms[0]) ; 
	RooRealVar sigwidth1("#sigma_L1","B^{#pm} width",3.43672e-01,0,2*rms[1]) ; 
	RooRealVar sigwidth11("#sigma_R1","B^{#pm} width",3.72548e-01 ,0,2*rms[1]) ; 
	RooRealVar sigwidth2("#sigma_L2","B^{#pm} width",0.5,0,3*rms[2]) ; 
	RooRealVar sigwidth21("#sigma_R2","B^{#pm} width",.554,0,3*rms[2]) ; 
	RooRealVar sigwidth("#sigma_R2","B^{#pm} width",rms[3]*0.5,0,2*rms[3]) ; 

	//guassian different width two side
	RooBifurGauss bifurgaus("bifurgaus", "bg",x, sigmean0,sigwidth0,sigwidth01);
	RooBifurGauss bifurgaus1("bifurgaus", "bg",x, sigmean1,sigwidth1,sigwidth11);
	RooBifurGauss bifurgaus2("bifurgaus", "bg",x, sigmean2,sigwidth2,sigwidth21);
	RooLandau landau("ld","lddf",x,sigmean,sigwidth);
	//double gaussian
	RooRealVar meanL("#mu_L","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
	RooRealVar meanR("#mu_R","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
	RooRealVar sigmaL("#sigma_L_bi","B^{#pm} width",rms[3],0,2*rms[3]) ; 
	RooRealVar sigmaR("#sigma_R_bi","B^{#pm} width",rms[3],0,2*rms[3]) ; 
	RooGaussian gauss1("gauss1","gaussian PDF",x, meanL,sigmaL) ;
	RooGaussian gauss2("gauss2","gaussian PDF",x, meanR,sigmaR) ;
	RooRealVar sig1frac("sig1frac","fraction of component 1 in signal",0.,0.,1.) ;
	RooAddPdf bigaus("sig","Signal",RooArgList(gauss1,gauss2),sig1frac) ;
	
	RooRealVar bifurgaus3_sigmaL("#sigma_L3","B^{#pm} width",rms[3],0,2*rms[3]) ; 
	RooRealVar bifurgaus3_sigmaR("#sigma_R3","B^{#pm} width",rms[3],0,2*rms[3]) ; 
	RooBifurGauss bifurgaus3("bifurgaus3","voigtpdf",x, sigmean, bifurgaus3_sigmaL, bifurgaus3_sigmaR);

	//BW
	RooRealVar bwMean("#mu_W","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
	RooRealVar bwWidth("#sigma_W","B^{#pm} width",rms[3],0,100*rms[3]) ; 
	RooBreitWigner bw("bw","bwpdf",x,bwMean,bwWidth);
	//Voigtian convolution of BW and gaussian
	RooRealVar voigtmean("#mu_v","b^{#pm} mass",mean[3],mean[3]-rms[3],mean[3]+rms[3]) ;
	RooRealVar voigtwidth("#sigma_v","B^{#pm} width",rms[3],0,100*rms[3]) ; 
	RooRealVar voigtsigma("Width_v","B^{#pm} width",rms[3],0,100*rms[3]) ; 
	RooVoigtian voigt("voigt","voigtpdf",x, voigtmean, voigtwidth, voigtsigma);
	//
	//
	//
	RooRealVar alpha("alpha", "Alpha", 5,-100,100);
	RooRealVar n("n", "Order", 6,-100,100);
	RooCBShape cbshape("cbshape","Crystal Ball", x, sigmean0,sigwidth0,alpha,n);  
	RooTruthModel tm("tm","truth model",x);	
	RooDecay decay_tm("decay_tm","decay", x,sigwidth,tm,RooDecay::DoubleSided) ;
	//
	// --- Build Argus background PDF ---
	//RooRealVar ("argpar","argus shape parameter",-20.0,-100.,-1.) ; 
	RooRealVar c("c","C",1,-1000,1000);
	RooRealVar c1("c1","C",1,-1000,1000);
	RooRealVar c2("c2","C",1,-1000,1000);
	RooRealVar c3("c3","C",1,-1000,1000);
	RooExponential bkg("exp","Background",x,c);
	RooExponential bkg1("exp","Background",x,c1);
	RooExponential bkg2("exp","Background",x,c2);
	RooExponential bkg3("exp","Background",x,c3);

//	RooRealVar a0("a0","a0",0.5,-10000.,10000.) ;
//	RooRealVar a1("a1","a1",0.2,-10000.,10000.) ;
//	RooChebychev bkg4("bkg4","Background 1",x,RooArgSet(a0,a1)); 

	//RooRealVar argpar("argpar","argus shape parameter",-20.0,-100.,-1.) ;
       	//RooArgusBG argus("argus","Argus PDF",x,RooConst(5.291),argpar) ;

	// --- Construct signal+background PDF ---
//	RooRealVar nsig("nsig","#signal events",nsig0,0.,100000000) ;
	RooRealVar nbkg("nbkg","#background events",1,0.,1000) ;
	RooRealVar entr0("entr0","+5     peak", 2070,1500,3e3);
	RooRealVar entr1("entr1","-5     peak", 1.68729e+03  ,1000,2e3);
	RooRealVar entr2("entr2","10   r peak", 300,0,1e3);
	RooRealVar entr3("entr3","center peak", 1e8,0,int_ent[3]);
	RooRealVar f1("f1","10   r peak",0,0, nbkg0 );
	RooRealVar f2("f2","10   r peak",0,0, nbkg0);
	RooRealVar f3("f3","10   r peak",0,0, nbkg0);
	RooAddPdf peak0("peak0","g+a",RooArgList(bifurgaus, bkg),RooArgList(entr0,nbkg)); 
	RooAddPdf peak1("peak1","g+a",RooArgList(bifurgaus1, bkg1),RooArgList(entr1,nbkg)); 
	RooAddPdf peak2("peak2","g+a",RooArgList(bifurgaus2, bkg2),RooArgList(entr2,nbkg)); 
	

	
	RooAddPdf sum("sum","g+a",RooArgList(   bifurgaus3),RooArgList(f2)); 
//	 RooFFTConvPdf sum("lxg","bi_bw ",x, gauss1, bw) ;


	 RooRealVar a0("a0","a0",1) ;
	 RooRealVar a1("a1","a1",0,-1,1) ;
	 RooRealVar a2("a2","a2",1,0,10) ;
	 RooRealVar a3("a3","a3",1,0,10) ;
	 RooPolynomial p2("p2","p2",x,RooArgList(a0,a1,a2),0) ;
	 //	RooAddPdf sum("sig", "g", RooArgList(sig), RooArgList(nsig));
	//	RooAddPdf sum("sum","g+a",RooArgList(bifurgaus,bkg),RooArgList(nsig,nbkg));
		//RooAddPdf sum("sum","g+a",RooArgList(cbshape,bkg),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(bw,bkg1),RooArgList(nsig,nbkg)) ;
	//RooAddPdf sum("sum","g+a",RooArgList(landau,bkg),RooArgList(nsig,nbkg)) ;
	// --- Generate a toyMC sample from composite PDF ---
	RooDataHist data("data", "dataset", x, h_cell);
	// --- Perform extended ML fit of composite PDF to toy data ---
	RooPlot* mesframe = x.frame() ;
//	peak0.fitTo(data,Range(lowedge[0],highedge[0]));
//	peak1.fitTo(data,Range(lowedge[1],highedge[1]));
//	peak2.fitTo(data,Range(lowedge[2],highedge[2]));
//	RooAddPdf sum("sum","g+a",RooArgList(bifurgaus,bifurgaus1,bifurgaus2,peak3),RooArgList(entr0,entr1,entr2,entr3)); 
//	 RooAddPdf sum("sum","g+a",RooArgList(peak3),RooArgList(f1));


	data.plotOn(mesframe) ;
//	peak0.plotOn(mesframe);
//	peak1.plotOn(mesframe);/	peak2.plotOn(mesframe);
        sum.fitTo(data,"r") ;
	 RooChi2Var chi2("chi2","chi2",sum, data);

//
//	r.Print();
	args = RooArgSet(meanL,sigmaL , bwMean,bwWidth);
//	sum.paramOn(mesframe,Parameters(args), Format("NELU", AutoPrecision(2)), Layout(0.7, 0.95, 0.75) );
//	mesframe->getAttText()->SetTextSize(0.03) ; 
//	sum.plotOn(mesframe) ; 
//	sum.plotOn(mesframe,Components(),LineStyle(kDashed));
//	sig.fitTo(data) ;
	sum.paramOn(mesframe,Parameters(args), Format("NELU", AutoPrecision(2)), Layout(0.7, 0.95, 0.75) );
	sum.plotOn(mesframe) ; 
	RooDataHist newdata("new", "datanew", x);
//	sum.plotOn(mesframe,Components(bigaus)) ; 
//	sum.plotOn(mesframe,Components(bkg4),LineStyle(kDashed));

	mesframe->Draw();
	mesframe->SetMinimum(1);
//	mesframe-> SetMaximum(1000);

	ggg->SetLogy();
	ggg->Modified();
	ggg->Update();

	return ggg;


}


TH1F* fitNormSum(TH1F* h_cell, double mean, double rms, double pos_y, int color, int order,int gain)
{
	gStyle->SetOptFit(1112); 
	
   const int nsig = h_cell->Integral(h_cell->FindBin(lowedge[order]+1.5),h_cell->FindBin(highedge[order]));
   const int nbkg =  h_cell->Integral(h_cell->FindBin(lowedge[order]), h_cell->FindBin(lowedge[order]+1.5));
  cout<<"la"<<lowedge[order]<<"\t"<<highedge[order]<<"\t"<<nsig<<"\t"<<nbkg<<endl;
   Int_t NEvents = nsig+nbkg;
   Int_t NBins   = 1e3;
   TF1 *f_cb    = new TF1("MyCrystalBall","gaus",lowedge[order],highedge[order]);
   TF1 *f_exp   = new TF1("MyExponential","expo",lowedge[order],highedge[order]);
 
 //  f_exp-> SetParameters(1.,-0.3);
   f_cb -> SetParameter(1,mean);
   f_cb-> SetParameter(2,rms);
  //  f_cb->SetParameters(1, 0, 1, 2, 0.5);
   // CONSTRUCTION OF THE TF1NORMSUM OBJECT ........................................
   // 1) :
   TF1NormSum *fnorm_exp_cb = new TF1NormSum(f_cb,f_exp,nsig,nbkg);
   // 4) :
   TF1   * f_sum = new TF1("fsum", *fnorm_exp_cb, lowedge[order],highedge[order], fnorm_exp_cb->GetNpar());
   f_sum->Draw();
   // III.:
   f_sum->SetParameters( fnorm_exp_cb->GetParameters().data() );
   f_sum->SetParName(1,"NBackground");
   f_sum->SetParName(0,"NSignal");
   for (int i = 2; i < f_sum->GetNpar(); ++i)
	   f_sum->SetParName(i,fnorm_exp_cb->GetParName(i) );
   auto result = h_cell -> Fit("fsum","SQ");
   result->Print();
   h_cell -> Draw();
   gStyle->SetOptStat(0);
   // add parameters
//   auto t1 = new TLatex(-2.5, 300000, TString::Format("%s = %8.0f #pm %4.0f", "NSignal",f_sum->GetParameter(0), f_sum->GetParError(0) ) );
//   auto t2 = new TLatex(-2.5, 270000, TString::Format("%s = %8.0f #pm %4.0f", "Nbackgr",f_sum->GetParameter(1), f_sum->GetParError(1) ) );
//   t1->Draw();
//   t2->Draw();
   return h_cell;
}



//_____________________________________________
//
// Make a gaussian fit for sub range,
// Show dotted line for full range,
// put fit params
//_____________________________________________
TH1F* fitGaus_all(TH1F* h_cell, double mean, double rms, double pos_y, int color , int g ){

	TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
	h_cell->Draw("E");
	gStyle->SetOptFit(1);

	// guess starting point for fit
	fg->SetParameter(1,mean);
	fg->SetParameter(2,rms);

	// put limits on the paramters
	fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
	fg->SetParLimits(2,0,4*rms);

	// Draw the fit in subrange
	fg->SetLineColor(kBlue);
	h_cell->Fit("fg","BR");
	double chi_old = 1.1e5;
	double chi_new = 1e5;
//	while(chi_old-chi_new>1e-7){
//		//      for(int j=0;j<100;j++){ 
//		chi_old = chi_new;
//		cout<<"chi_old"<<chi_old<<endl;

//		chi_new = fg->GetChisquare()/fg->GetNDF();
//		cout<<"chi_new"<<chi_new<<endl;
//		fg->SetParameters(fg->GetParameters());  
//		h_cell->Fit("fg","R");
//	}

	//fg->Draw("same");

	double pos_x = 0.76;
	//myText(pos_x-0.15, pos_y,kBlue,Form("Peak 0ns: #mu: %.3f ns",fg->GetParameter(1)),0.8);
	//myText(pos_x, pos_y-0.03,kBlue,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.8);


	// Make the fit extend and draw as dotted line
	TF1 *f2 = new TF1("f2","gaus",-15,15);
	f2->SetParameters(fg->GetParameters());
	f2->SetLineColor(kBlue);
	f2->SetLineStyle(7); //dashed line
	f2->Draw("same");
 		
//	for(int peak=0;peak<3;peak++){
//
//		if(g==1&&peak==2) continue;	
//		TF1* f3;
//		if(g==0){
//			f3 = new TF1("f3","gaus(0)+pol2(3)",lowedge[peak],highedge[peak]);
//			f3->SetParameters(param[peak]);
//		}
//		if(g==1){
//			f3 = new TF1("f3","gaus",lowedge[peak],highedge[peak]);
//			f3->SetParameters(param[peak][0], param[peak][1],param[peak][2]);
//		}
//
//		f3->SetLineColor(kGreen-peak*6);
//		f3->SetLineStyle(7); //dashed line
//		f3->Draw("same"); 
//		myText(pos_x-0.068, pos_y-0.07*(peak+1),kGreen-peak*6,Form("%s: #mu: %.3f ns",peakname[peak].c_str(),f3->GetParameter(1)),0.8);
//		myText(pos_x, pos_y-0.10-peak*0.07,kGreen-peak*6,Form("#sigma: %.3f ns",f3->GetParameter(2)),0.8);
//	}
	// Put parameters on the plot
	return h_cell;

	}
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y, int color, int order, int g){

//	gStyle->SetOptFit(0);

	// Gaus fit (limited range around peak)
	TF1* fg;

	if(g==0){
	//	TF1 *fSignal = new TF1("fSignal","gaus",mean-5*rms, mean+5*rms);
	//	TF1 *fBackground = new TF1("fBackground","pol2",mean-5*rms, mean+5*rms);
	//	fg->SetParNames("Strength","Mean","Sigma","Back1","Back2","Back3");

		fg = new TF1("fg","gaus(0)+expo(3)",lowedge[order], highedge[order]);
	}
	if(g==1){
	//	fg = new TF1("fg","gaus",mean-5*rms, mean+5*rms);
		fg = new TF1("fg","gaus",lowedge[order], highedge[order]);
		fg->SetParNames("Strength","Mean","Sigma");
	}
	// guess starting point for fit
	fg->SetParameter(1,mean);
	fg->SetParameter(2,rms);

	// put limits on the paramters
	fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
	fg->SetParLimits(2,0,4*rms);

	// Draw the fit in subrange
	if(color==1)
		fg->SetLineColor(kRed);
	else 
		fg->SetLineColor(kYellow);
	h_cell->Draw("E");
	h_cell->Fit("fg","BR");
	double chi_old = 1.1e5;
	double chi_new = 1e5;
	while(chi_old-chi_new>1e-6){
		//      for(int j=0;j<100;j++){ 
		chi_old = chi_new;
		cout<<"chi_old"<<chi_old<<endl;

		chi_new = fg->GetChisquare()/fg->GetNDF();
		cout<<"chi_new"<<chi_new<<endl;

		fg->SetParameters(fg->GetParameters());  
		h_cell->Fit("fg","R");
	}
      // Make the fit extend and draw as dotted line
      //TF1 *f2 = new TF1("f2","gaus",-15,15);
      
	TF1 *f2 = new TF1("f2","gaus",lowedge[order], highedge[order]);
	f2->SetParameters(fg->GetParameters());
	if(color==1)
		f2->SetLineColor(kRed);
	else 
		f2->SetLineColor(kYellow);
	f2->SetLineStyle(7); //dashed line
	f2->Draw("same");
//	if(g==0)
	fg->GetParameters( param[order]);
	ofstream out;
	//out.Open("out.txt");

	out<<  f2-> Integral(lowedge[order], highedge[order])<<endl;

//if(g==1)
//	fg->GetParameteprs( param[order]);
	Double_t param[6];
	// Put parameters on the plot
	double pos_x = 0.73;
//	double entries=fg->Integral(lowedge[order],highedge[order]);
//	double entries_error=
	myText(pos_x, pos_y,color,Form("#mu: %.3f ns",fg->GetParameter(1)),0.8);
	myText(pos_x, pos_y-0.04,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.8);
//	myText(pos_x, pos_y-0.08,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.8);

	if(g==0)
	myText(pos_x-0.05, pos_y-0.08,color,Form("bkg: %.1f+%.1fx+%.1fx^{2} ns",fg->GetParameter(3),fg->GetParameter(4),fg->GetParameter(5)),0.5);


	return h_cell;

	}
