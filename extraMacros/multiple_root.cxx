//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// functions
void doMassPlot();
void doFEBTimePlot();
void list_files(char *dir, char *ext);
void doCellTimePlot();
void doAngularPlot();
void doEFracPlot();
void doETimePlot();
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
void drawFitInfo(int gain, int sl);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1,int pass=0);


// for calculating correcitons at each pass
double febTime_0[2][47+17][620]; // [2] gain [47] runs [620] febs for pass 0
double ftTime_0[2][47+17][104]; // [2] gain [47] runs [620] ft for pass 0
double febTime_1[2][620];     // [2] gain [620] febs for pass 1
double chTime[2][79360];      // [2] gain [79360] channels for pass 2
double enFit[2][22][6];       // [2] gain [22] slots [6] energy fit params
double dphiFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[2][22][2];       // [2] gain [22] slots [2] df1 fit params
double f3Fit[2][22][2];       // [2] gain [22] slots [2] df3 fit params
int Color[11] ={921,1, 800,397, 600,416,880, 432, 821, 632,839};//, "kGreen", "kRed", "kViolet"};

//string Color[6] ={kgray" "kBlack", "kOrange",""kYellow","kBlue", "kGreen", "kViolet","kcyan","kSpring","kRed", "kTeal"};

// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","High","Low"};
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
std::vector< int > runNumberList;
//const std::string Correction[8] = {"Pass 0","Pass 1: FT","Pass 2: FEB", "Pass 3: Channel","Pass 4: Energy","Pass 5: Angular","Pass 6: f1/f3","Pass 7: 2nd Channel"};  
const int passN=8;

//const std::string Correction[passN] = {"Pass 1: After FT", "Pass 3: After Channel", "Pass 3 separate iovs"};//,"Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 2", "Pass 3: After Channel", "Pass 4: After Energy","Pass 3 separate iovs","Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs", "Pass 5: after Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: After 2nd Channel", "Pass 7: separate iovs"};
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 5: After Angular","Pass 6: After f1/f3", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"}/
///Efrac
//const std::string Correction[passN] = { "Pass 3: After Channel", "Pass 3 separate iovs", "Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};
//const std::string Correction[passN] = { "Pass 1: After FT", "Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs"};
const std::string Correction[passN] = {"Pass 0", "Pass 1: After FT","Pass 2: After Feb", "Pass 3 separate iovs", "Pass 4 separate iovs","Pass 5: separate iovs",  "Pass 6: separate iovs", "Pass 7: separate iovs in Pass 2"};
///Angular
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 4 separate iovs","Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};


// histogramsi
TH1F *h_cell_t_pass[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_feb_run[2][5][2]; // EMBA/C, EMECA/C, all
TH2F *h_phi_t[passN][5];   // EMBA/C, EMECA/C, all
TH2F *h_dphi_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_eta_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_deta_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_f1_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_f3_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_e_t_pass[passN][2][5];     // EMBA/C, EMECA/C, all
TH2F *hp_pass[passN][2][3];

std::vector<std::string> lista;
// Configuration
const int  NRUNS   = 150; // 47 for IOVconst int  NRUNS2  = 17; // 17 for IOV2 const 
bool saveEPS = false;
const int  NRUNS2  = 0; // 17 for IOV2
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void multiple_root(int pn=0, int mode=0){

#ifdef __CINT__
  gROOT->LoadMacro("../utils/AtlasLabels.C");
  gROOT->LoadMacro("../utils/AtlasStyle.C");
#endif
  SetAtlasStyle();  
  // Set Run List
  setRunList();
  // Make plots
  //doMassPlot();
 // doCellTimePlot();
  setTRange(-0.03, 0.1);
  doFEBTimePlot();
//  setTRange(-0.6,1.5);   
//  doETimePlot();
//  setTRange(-0.5, 1);
//
//  doAngularPlot();

  setTRange(-.25, .8);
//  doEFracPlot();
   return;    
}

void doFEBTimePlot(){
	TFile *file;
	TH1F* h = new TH1F("h", "h", 150, 0,150);
	list_files("/data/users/jchen/WTiming/WTiming_root/CalibrationFramework/ArcondCalib/submit_all_pass3_iovs/RootOutputs/", ".root");
	double mean;
	TH1F *f1 = new TH1F("f1", "f1", 50, 0, 100);
	TH1F *f2 = new TH1F("f2", "f2", 50, 0, 100);
	TH1F *f_t = new TH1F("f_t", "f_t", 50, 0, 100);  ;
	TString fname;
	double error;
	int a=0;
		//	file=TFile::Open
	for(vector<string>::iterator it = lista.begin(); it != lista.end(); it++){ 
	        fname= *it;
	//	cout<<*it<<endl;
		file= TFile::Open(fname,"READ"); 
	//	cout<<"file open"<<endl;

		f_t = (TH1F*)file->Get("h_maxEcellEnergyHigh");
	        mean = f_t->GetMean();
		error = f_t->GetMeanError();
	///	cout<<"file open"<<endl;
	//	cout<<mean<<endl;
		if(a<65){
			f1->Add(f_t);
		//	cout<<"lalal"<<endl;
		}
		else{
			f2->Add(f_t);
		}

		h->SetBinContent(a, mean);
		h->SetBinError(a, error);
//		if(mean>16.5)
		cout<<mean<<"\t"<<runNumberList.at(a)<<endl;
		a++;
        }	
	TCanvas *c3 = new TCanvas("test","test",800,700);   
	h->Draw("E1");
	for( int bin=0; bin<NRUNS; bin++){
		//	for( int bin=60; bin<NRUNS; bin++){
		int s_run =  runNumberList[bin] ;
		h->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
	}
	h->GetXaxis()->LabelsOption("v");
//	h->GetXaxis()->SetRangeUser(60,67);
	h->GetYaxis()->SetRangeUser(16,17);
	h->GetXaxis()->SetLabelSize(0.03);
	h->GetXaxis()->SetTitle("Run Number");
	myText(0.65, 0.86,kBlack,Form("High Gain"),0.6);
	h->GetYaxis()->SetTitle("Average MaxcellEnergy");
	c3->SaveAs("testHigh.png");
	delete c3;
	TCanvas *c2 = new TCanvas("test","test",800,700); 
	f1->Draw();
	f2->SetLineColor(kRed);
	f2->Scale(f1->GetEntries()/f2->GetEntries());
	int e1= f1->GetEntries();
	int e2= f2->GetEntries();
	f2->Draw("same");
	myText(0.65, 0.86,kBlack,Form("High Gain"),0.6);
	myText(0.65, 0.83,kBlack,Form("Before:Entries: %d",e1),0.6);
	myText(0.65, 0.8,kRed,Form("After:Entries: %d",e2),0.6);
		
	f1->GetXaxis()->SetTitle("Max Cell Energy [GeV]");
	cout<<e1<<"\t"<<e2<<endl;

	c2->SetLogy();
	 c2->SaveAs("energy_jumpHigh.png");   
	
	//	cout<<*it<<endl;
	
	
}
void list_files(char *dirname,char *ext)
{

   TSystemDirectory  dire(dirname, dirname);

   TList *files = dire.GetListOfFiles();
   TIter next(files);
   TSystemFile *file;
   TString fname;
   while((file = (TSystemFile*)next())) {
         fname = file->GetName();
	 fname =dirname+fname;
         if(file->IsDirectory()) continue;
         lista.push_back(fname.Data());
      }
}
/*
			//	case 7: file= TFile::Open("../files/pass6_iovs.root","READ");

			}
				int old=0;
			for( int rn=0; rn<NRUNS; rn++){

			for(int sl=0; sl<22; sl++){
				// Make names for the histograms to open
				std::string tname = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl);
				TH1F *f_t = (TH1F*)file->Get(tname.c_str());

				if( !f_t->GetEntries() ) {
					std::cout << "  >> Slot index: " << sl << " has no data for " 
						<< gain[g] << " gain, no plotswill be made!\n";
					continue;
				}

				// Summary plots
				h_e_t_pass[pass][g][4]->Add(f_t);
				std::string tname = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl);
				for( int ft=0; ft<114; ft++){
					// Summary plots
					hp_pass[pass][g][2]->Fill(rn, f_t->GetBinContent(ft+1));
					if( ft < 64){ // emb
						hp_pass[pass][g][0]->Fill(rn, f_t->GetBinContent(ft+1));
					}
					else{
						hp_pass[pass][g][1]->Fill(rn, f_t->GetBinContent(ft+1));
					}
				}
			}

		}//loop over pass
		for(int d=0;d<3;d++){
			TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
			TProfile *ff_e[passN];
			for(int pass=0; pass<passN; pass++){

				int entries = hp_pass[0][g][d]->GetEntries();
				ff_e[pass] = hp_pass[pass][g][d]->ProfileX(Form("ff_e%d%d%d",pass,d,g),-1,-1,"o");
				cout<<pass<<"\t"<<d<<"\t"<<g<<"\t"<< hp_pass[pass][g][d]->GetEntries()<<endl;
				if(pass==0)
					ff_e[pass]->Draw();
				for( int bin=0; bin<NRUNS; bin++){
			//	for( int bin=60; bin<NRUNS; bin++){
					int s_run =  runNumberList[bin] ;
					ff_e[pass]->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
				}
				ff_e[pass]->Draw("same");
				//	ff_e[pass]->SetErrorOption("g");
			//	ff_e[pass]->GetYaxis()->SetRangeUser(tmin,tmax);
				ff_e[pass]->GetYaxis()->SetRangeUser(tmin,tmax);
			//	ff_e[pass]->GetXaxis()->SetRangeUser(0,NRUNS);  
				ff_e[pass]->GetXaxis()->SetRangeUser(50,100);  
				ff_e[pass]->GetXaxis()->LabelsOption("v");
				ff_e[pass]->SetLineColor(Color[pass]);
				ff_e[pass]->SetMarkerColor(Color[pass]);
				ff_e[pass]->GetXaxis()->LabelsOption("v");
				ff_e[pass]->GetXaxis()->SetLabelSize(0.03);
				ff_e[pass]->GetXaxis()->SetTitle("Run Number");
				ff_e[pass]->GetYaxis()->SetTitle("Time [ns]");
				if(pass==0)
					ff_e[pass]->Draw();
				ff_e[pass]->Draw("same");
				if(pass==0){
					ATLASLabel(0.2, 0.88,pInternal);
					//	myText(0.73, 0.83,kBlack,Form("Entries: %d",entries),0.6);
					if(d == 0 )myText(0.73, 0.88,kBlack,Form("EMB"),0.7);
					else if( d == 1 )myText(0.73, 0.88,kBlack,Form("EMEC"),0.7);
					else if( d == 2 )myText(0.73, 0.88,kBlack,Form("EMB+EMEC"),0.7);
					myText(0.20, 0.81,kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
					myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
					myText(0.22, 0.72-pass*0.03,Color[pass],Form("%s (W#rightarrow e#nu)",Correction[pass].c_str()),0.5);
					TLine *l = new TLine(0,0,NRUNS,0);
					l->SetLineColor(kRed);
					l->SetLineWidth(1);
					l->Draw("same");
					TLine *l1 = new TLine(16,-0.015,16,0.02);
					l1->SetLineColor(kRed);
					l1->SetLineWidth(1);
					l1->Draw("same");
					TLine *l2 = new TLine(30,-0.015,30,0.02);
					l2->SetLineColor(kRed);
					l2->SetLineWidth(1);
					l2->Draw("same");
					TLine *l3 = new TLine(78,-0.015,78,0.02);
					l3->SetLineColor(kRed);
					l3->SetLineWidth(1);
					l3->Draw("same");
					TLine *l4 = new TLine(119,-0.015,119,0.02);
					l4->SetLineColor(kRed);
					l4->SetLineWidth(1);
					l4->Draw("same");
				}
				else
					myText(0.22, 0.72-pass*0.03,Color[pass],Form("%s", Correction[pass].c_str()),0.5);

			}
			if( d== 0)c3->SaveAs(Form("pass0_pass7_EMB_ft_%s.png",gain[g].c_str()));
			else if( d==1)c3->SaveAs(Form("pass0_pass7_EMEC_ft_%s.png",gain[g].c_str()));
			else if ( d==2)c3->SaveAs(Form("pass0_pass7_All_ft_%s.png",gain[g].c_str()));
			//delete ff_e[passN];
			delete c3;
		}//loop over d
	}//loop over gain
	file->Close();

	return;
}

*/
	//_____________________________________________
	//
	// Read list of run numbers from config file
	//_____________________________________________
	void setRunList(){
	  
	  // Run number config file
	  ifstream f1("../config/RunNumberList.txt");
	  int tempRunNum;

	  // Store each run into local vector
	  while( f1 >> tempRunNum ){
	      runNumberList.push_back(tempRunNum);
	  }
	  
	  f1.close();

	  std::cout << "  > Added " << runNumberList.size() << " runs successfully from "
		    << runNumberList[0] << " to " << runNumberList.back() << "\n\n";
	  return;
	}


	//_____________________________________________
	//
	// Set tmin/tmax for plotting
	//_____________________________________________
	void setTRange(double t_l, double t_h){
	  
	  tmin = t_l;
	  tmax = t_h;
	  
	  return;
	}

	TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y, int color, int pass ){
	  
	  // Hide fit stats
	  gStyle->SetOptFit(0);
	 
	  // Gaus fit (limited range around peak)
	  TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
	  //fg->SetParameter(0,0); // not sure if this is necessary
	  
	  // guess starting point for fit
	  fg->SetParameter(1,mean);
	  fg->SetParameter(2,rms);
	  
	  // put limits on the paramters
	  fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
	  fg->SetParLimits(2,0.5*rms,2*rms);
	  
	  // Draw the fit in subrange
	  fg->SetLineColor(kRed);
	  h_cell->Fit("fg","BR");
	  fg->Draw("same");
	  
	  // Make the fit extend and draw as dotted line
	  TF1 *f2 = new TF1("f2","gaus",-25,25);
	  f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
	  f2->SetLineColor(kRed);
	  f2->SetLineStyle(7); //dashed line
	  f2->Draw("same");
	  
	  // Put parameters on the plot
	  double pos_x = 0.78;
	  myText(pos_x, pos_y-0.06*pass,color,Form("#mu: %.3f ns",fg->GetParameter(1)),0.6);
	  myText(pos_x, pos_y-0.06*pass-0.03,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.6);

	  //FIXME printing info
	  //std::cout << "    Mu: " << fg->GetParameter(1) << "; Sigma: "<<fg->GetParameter(2) <<std::endl;
	  return h_cell;

	}


	// Rebin the 2D histogram so that each bin
	// has at least entPerBin entries
	// firstBin is the first bin you want to consider
	//_____________________________________________
	TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

	  // hold the bin edge information
	  std::vector< double > xbins;
	  // Combine all bins below first bin
	  // Cut at 5 GeV so this bin is empty
	  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
	  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

	  // keep track of last bin with at least entPerBin
	  int lastFullIndex = firstBin; // for energy 5
	  // Get the xaxis
	  TAxis *axis = h->GetXaxis();

	  // Loop over bins in xaxis
	  // start after 5GeV bin
	  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {
	    // Get entries in this bin, and width
	    int y = h->Integral(i,i);
	    double w = axis->GetBinWidth(i);

	    // If not enough entries, need to combine bins
	    if (y <= entPerBin){
	      // Find integral from last combined bin
	      double integral = h->Integral(lastFullIndex+1, i);
	      if (integral <= entPerBin ) continue;
	      // if above threshold, mark as new bin
	      lastFullIndex = i;
	      xbins.push_back( axis->GetBinLowEdge(i) + w);
	    }
	    else{
	      // above threshold, mark as bin
	      lastFullIndex = i;
	      xbins.push_back( axis->GetBinLowEdge(i) + w );
	    }

	  }

	  // put bin edges into an array
	  xbins.push_back( axis->GetXmax() );
	  size_t s = xbins.size();
	  double *xbinsFinal = &xbins[0];
	cout<<"s"<<s<<endl;
	  // create new histo with new bin edges
	  TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
	  hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());

	  hnew->Sumw2();
	 // cout<<
	  // fill new histo with old values
	  for( int i=1; i<=h->GetNbinsX(); i++){
	    for( int j=1; j<=h->GetNbinsY(); j++){
		//    for(int a=1;a<=h->GetBinContent(i,j);a++)
	     hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j), h->GetBinContent(i,j));
	      //hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
	  //    if(i==200)
	//	      cout<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<"\t"<<h->GetBinError(i,j)<<endl;
	     // hnew->
	//if(i<=h->GetNbinsX()/2&&i>=h->GetNbinsX()/2-1)
	    }
	  }
	//cout<<"h"<<"\t"<<h->GetEntries()<<endl;
	//cout<<"h"<<"\t"<<h->GetBinContent(200,250)<<endl;
	//or(j =1; j<=hnew->GetNbinsY();j++)
	//	if( hnew-> GetBinContent(38,j)!=0)
	//cout<<"h_new"<<"\t"<<hnew->GetXaxis()->GetBinCenter(38)<<"\t"<<""<<endl;
	//cout<<"h_new"<<"\t"<<hnew->GetEntries()<<endl;
	//
	//double stats[7];
	//hnew->GetStats(stats);
	//for(i=0;i<7;i++)
	//	cout<<"star"<<stats[7]<<endl;
	  TH2F* h_return = (TH2F*)hnew->Clone("");
	  delete hnew;

	  // return histo
	  return h_return;
	}




	//__________________________________________
	//
	// Put info for fit plots
	// g=gain sl=slot index
	//__________________________________________
	void drawFitInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
	  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

	  return;

	}


	//____________________________________________
	//
	// Put info for run by run plots
	// g=gain, rn = run number index
	//____________________________________________
	void drawRunInfo(int g, int rn){
	  
	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

	  return;

	}

	//_________________________________________
	//
	// Put info for slot by slot plots
	// g=gain index, sl=slot index
	//_________________________________________
	void drawSlInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
	  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
	  
	  return;

	}
	//_________________________________________
	//
	// Put info for subdetector summary plots
	// g=gain index, d=subdetector index
	//_________________________________________
	void drawSubDetInfo(int g, int d){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
	  
	  return;

	}

	//_________________________________________
	//
	// Put info for subdetector summary plots
	// ent=entries, m=mean, r=rms
	//_________________________________________
	void drawStats(int ent, double m, double rms){

	  myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
	  myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
	  myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
	 
	  return;

	}

	//_________________________________________
	//
	// Put info for 3 fits on plot
	// p0, p4, p0
	//_________________________________________
	void drawStatBox(double par[7],std::string var){
	  myText(0.55,0.89,kGreen-3,Form("%s < -0.5",var.c_str()),0.6);
	  myText(0.55,0.86,kGreen-3,Form("p0: %.3f",par[0]),0.6);
	  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
	  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
	  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
	  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
	  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
	  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
	  myText(0.83,0.89,kGreen-3,Form("0.5 < %s",var.c_str()),0.6);
	  myText(0.83,0.86,kGreen-3,Form("p0: %.3f",par[6]),0.6); 
	  return;
	}
