//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>

// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

// functions
void dodepthPlot();
void domuPlot();
void energy_slot_checkPlot();
void doFEBTimePlot();
void dosliceslot_gainfit();
void dofindchannel_gain();
void dosliceslot_large_channel();
void dochannelPlot();
void doCellTimePlot();
void doAngularPlot();
void doEFracPlot();
void doETimePlot();
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
void drawFitInfo(int gain, int sl);
void drawChannelInfo(int g, int ch);
void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStatBox(double par[7], std::string var);
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y=0.74, int color=1,int pass=0);


// for calculating correcitons at each pass
double febTime_0[2][47+17][620]; // [2] gain [47] runs [620] febs for pass 0
double ftTime_0[2][47+17][104]; // [2] gain [47] runs [620] ft for pass 0
double febTime_1[2][620];     // [2] gain [620] febs for pass 1
double chTime[2][79360];      // [2] gain [79360] channels for pass 2
double enFit[2][22][6];       // [2] gain [22] slots [6] energy fit params
double dphiFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[2][22][7];     // [2] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[2][22][2];       // [2] gain [22] slots [2] df1 fit params
double f3Fit[2][22][2];       // [2] gain [22] slots [2] df3 fit params
int Color[11] ={921,1, 800,397, 600,416,880, 432, 821, 632,839};//, "kGreen", "kRed", "kViolet"};
const int Nch=12;
//string Color[6] ={kgray" "kBlack", "kOrange",""kYellow","kBlue", "kGreen", "kViolet","kcyan","kSpring","kRed", "kTeal"};

// strings for plotting
char Intern[15]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","Medium","Low"};
const std::map<int,int> bad_slot = {{12,0},{13,1},{14,2},{15,3},{19,4},{20,5},{21,6}};    //   = {12,13,14,15,19,20,21};
const int Slot[7] = {12,13,14,15,19,20,21};

const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
std::vector< int > runNumberList;
std::vector< int > channelList;
std::vector< string >onlineid;
//const std::string Correction[8] = {"Pass 0","Pass 1: FT","Pass 2: FEB", "Pass 3: Channel","Pass 4: Energy","Pass 5: Angular","Pass 6: f1/f3","Pass 7: 2nd Channel"};  
const int passN=8;

//const std::string Correction[passN] = {"Pass 1: After FT", "Pass 3: After Channel", "Pass 3 separate iovs"};//,"Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 2", "Pass 3: After Channel", "Pass 4: After Energy","Pass 3 separate iovs","Pass 4 separate iovs"};
//const std::string Correction[passN] = {"Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs", "Pass 5: after Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: After 2nd Channel", "Pass 7: separate iovs"};
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 5: After Angular","Pass 6: After f1/f3", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"}/
///Efrac
const std::string Correction[passN] = { "Pass 3: After Channel", "Pass 3 separate iovs", "Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};
//const std::string Correction[passN] = { "Pass 1: After FT", "Pass 3: After Channel","Pass 3 separate iovs", "Pass 4: After energy","Pass 4 separate iovs","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs"};
//const std::string Correction[passN] = {"Pass 0", "Pass 1: After FT","Pass 2: After Feb", "Pass 3 separate iovs", "Pass 4 separate iovs","Pass 5: separate iovs",  "Pass 6: separate iovs", "Pass 7: separate iovs in Pass 2"};
///Angular
//const std::string Correction[passN] = { "Pass 4: After energy","Pass 4 separate iovs","Pass 5: After Angular","Pass 5: separate iovs", "Pass 6: After f1/f3", "Pass 6: separate iovs", "Pass 7: after 2nd Channel(all together)", "Pass 7: separate iovs in Pass6", "Pass 7: separate iovs in Pass 2"};


// histogramsi
TH1F *h_cell_t_pass[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_feb_run[2][5][2]; // EMBA/C, EMECA/C, all
TH2F *h_phi_t[passN][5];   // EMBA/C, EMECA/C, all
TH2F *h_dphi_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_eta_t[2][5];   // EMBA/C, EMECA/C, all
TH2F *h_deta_t[passN][2][5];  // EMBA/C, EMECA/C, all
TH2F *h_f1_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_f3_t[passN][2][5];    // EMBA/C, EMECA/C, all
TH2F *h_e_t_pass[passN][2][5];     // EMBA/C, EMECA/C, all
TH2F *hp_pass[passN][2][3];

// Configuration
const int  NRUNS   = 150; // 47 for IOVconst int  NRUNS2  = 17; // 17 for IOV2 const 
bool saveEPS = false;
const int  NRUNS2  = 0; // 17 for IOV2
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;

//______________________________________
// Change the default passNumber here     
std::string sPassNumber = "pass0";
int passNumber = 0;
//______________________________________



//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void split_slot(int pn=0, int mode=0){

#ifdef __CINT__
  gROOT->LoadMacro("../utils/AtlasLabels.C");
  gROOT->LoadMacro("../utils/AtlasStyle.C");
#endif
  SetAtlasStyle();  
  // Set Run List
  setRunList();
  // Make plots
 //dofindchannel_gain();
 
  setTRange(-0.4,0.8);
  //passNumber=7;
  dosliceslot_large_channel();

  
}
  

void  dofindchannel_gain(){
	ofstream out[7];
	double energy_bondary[7];
	int t_gain, run, ch, slot,wgt;
	auto it=bad_slot.begin();
	int j;
	double energy, mu, t_time, eta,phi;

	TFile *file_hist= TFile::Open("../files/recheck_2016_all/pass3_iovs.root","READ");//break;    
	// Loop over slots
	for(int sl=0; sl<22; sl++){
		// Make names for the histograms to open
			if((sl!=12&&sl!=13&&sl!=14&&sl!=15&&sl!=19&&sl!=20&&sl!=21)) continue;
		std::string tname = Form("h_AvgEnergyTime_%s_%d",gain[1].c_str(),sl);
		TH2F *f_t = (TH2F*)file_hist->Get(tname.c_str());
		it = bad_slot.find(sl);
		j=it->second;

		//find the Energy boundary between H/M gain
		if(j==0||j==1||j==4){
			cout<<sl<<"\t"<<f_t-> GetEntries()/5000<<"\t"<<f_t->GetXaxis()->GetBinCenter(f_t->FindFirstBinAbove(f_t-> GetEntries()/5000))<<endl;
			energy_bondary[j]=f_t->GetXaxis()->GetBinCenter(f_t->FindFirstBinAbove(f_t-> GetEntries()/5000));
		}
		else{
			cout<<sl<<"\t"<<f_t-> GetEntries()/10000<<"\t"<<f_t->GetXaxis()-> GetBinLowEdge (f_t->FindFirstBinAbove(0))<<endl;
			energy_bondary[j]=f_t->GetXaxis()-> GetBinLowEdge (f_t->FindFirstBinAbove(0));

		}
	}

	TH1F *h_ch[7];
	double channel_cut[7]={200,200, 20, 20,100,10, 50};

	for(int s=0;s<7;s++){
		out[s].open(Form("./txt_folder/large_energy_channel%d.dat",s));
		//				for(int p=0; p<2; p++){
		h_ch[s]=new TH1F(Form("h_ch_%d",s),Form("h_en_t%d",s), 79360,-0.5,79359.5 );  
		//			h_en_t[g][s][p] = new TH2F(Form("h_en_t%d%d%d",g,s,p),Form("h_en_t%d%d%d",g,s,p),500,0,100,1000,-25,25);
		//		}}}
		}
TFile *file   = TFile::Open("/data/users/jchen/WTiming/off_LAr_Timing/WTiming/files/pass3_iovs_bothmu.root");
TTree *tree =(TTree*)file->Get("tree");
tree->SetBranchAddress("gain", &t_gain);
tree->SetBranchAddress("time", &t_time);
tree->SetBranchAddress("channel", &ch);
//	tree->SetBranchAddress("mu", &mu);
tree->SetBranchAddress("slot", &slot);
tree->SetBranchAddress("run", &run);
tree->SetBranchAddress("wgt", &wgt);
tree->SetBranchAddress("energy", &energy);

int num[7]={0};
Long64_t nentries = tree->GetEntries();
for (Long64_t i=0;i<nentries;i++) {	
	tree->GetEntry(i); 
	if((slot!=12&&slot!=13&&slot!=14&&slot!=15&&slot!=19&&slot!=20&&slot!=21)||t_gain==2||t_gain==1)
		continue;
	it = bad_slot.find(slot);
	j=it->second;
	if(energy>energy_bondary[j])
		h_ch[j]->Fill(ch, wgt);
	num[j]++;

}
int slot_n;

double minX=0,maxX=0;
double enFit[6];

for(int s=0;s<7;s++){
	slot_n=Slot[s];
	TCanvas *cw4 = new TCanvas("c_ma1","c_mw1",800,700);
	minX=h_ch[s]->GetXaxis()->GetBinLowEdge(h_ch[s]->FindFirstBinAbove(0));
	maxX=h_ch[s]->GetXaxis()->GetBinLowEdge(h_ch[s]->FindLastBinAbove(0)+1);
	h_ch[s]-> GetXaxis()->SetRangeUser(minX,maxX); 
	h_ch[s]->Draw();
	int nbins = h_ch[s]->GetNbinsX();
	double dummy_evn =  h_ch[s]->GetSumOfWeights();

	//string output_datei_data(run_name);
	//output_datei_data=output_datei_data+ output_data;

	//const char *output_datei_data_l = output_datei_data.c_str();

	//ofstream filetotcross100(output_datei_data_l);
	for (int i=1;i<=nbins;i++)
	{
		if(h_ch[s]->GetBinContent(i) > channel_cut[s])
			out[s] << h_ch[s]->GetBinCenter(i) << endl;
	}
	out[s].close();


	myText(0.72, 0.80,kBlack,Form("entries: %d",num[s] ),0.6);
	ATLASLabel(0.2,0.88,pInternal);
	myText(0.22, 0.76,kRed,Form("%s Gain",gain[0].c_str()),0.8);  
	myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
	cw4->SaveAs(Form("~/WWW/bad_slot/channel_slot_en_time%d.png",slot_n));
	delete cw4;
}
return;
myText(0.22,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 



}
void dosliceslot_large_channel(){
	gStyle->SetOptFit(1);
	TFile *file;
	// Open files
	file   = TFile::Open("/data/users/jchen/WTiming/off_LAr_Timing/WTiming/files/pass3_iovs_bothmu.root");
	TH2F *h_en_t[7][3]; 
	TH2F *h_deta_t[7][3]; 
	TH2F *h_dphi_t[7][3]; 
	TH1F *h_eta[7][3];

	ifstream in[7];
	std::vector< int > large_ch[7];
	for(int s=0;s<7;s++){
		in[s].open(Form("./txt_folder/large_energy_channel%d.dat",s)); 
		int tempchannel;
		while( in[s] >> tempchannel ){
			large_ch[s].push_back(tempchannel);
		}
		for(int p=0;p<3;p++){
			h_en_t[s][p] = new TH2F(Form("h_en_t%d%d",s,p),Form("h_en_t%d%d",s,p),1000,0,80,1000,-25,25);
			h_deta_t[s][p] = new TH2F(Form("h_deta_t%d%d",s,p),Form("h_deta_t%d%d",s,p),500,-.6,.6,1000,-25,25);
			h_dphi_t[s][p] = new TH2F(Form("h_dphi_t%d%d",s,p),Form("h_dphi_t%d%d",s,p),500,-2,2,1000,-25,25);
			h_eta[s][p] = new TH1F(Form("h_eta%d%d",s,p),Form("h_en%d%d",s,p),1000,-4,4);
		}
	}
	//	for(auto it = large_ch[0].begin(); it !=large_ch[0].end(); ++it) {
	//		/* std::cout << *it; ... */
	//		cout<<*it<<endl;
	//	}
	int j,ch,t_gain,pos_ch,slot,wgt;
	double energy,eta, mu, t_time, deta,dphi;
	auto it=bad_slot.begin();
	TTree *tree =(TTree*)file->Get("tree");

	tree->SetBranchAddress("gain", &t_gain);
	tree->SetBranchAddress("time", &t_time);
	tree->SetBranchAddress("channel", &ch);
	tree->SetBranchAddress("slot", &slot);
	tree->SetBranchAddress("caloCluster_eta", &eta);
	tree->SetBranchAddress("deta", &deta);
	tree->SetBranchAddress("dphi", &dphi);
	//	tree->SetBranchAddress("run", &run);
	tree->SetBranchAddress("wgt", &wgt);
	tree->SetBranchAddress("energy", &energy);

	int num[7][3]={0};
	Long64_t nentries = tree->GetEntries();
	for (Long64_t i=0;i<nentries;i++) {	

		tree->GetEntry(i); 
		if((slot!=12&&slot!=13&&slot!=14&&slot!=15&&slot!=19&&slot!=20&&slot!=21)||t_gain==2||t_gain==1)
			continue;
		it = bad_slot.find(slot);
		j=it->second;
		pos_ch=std::find( large_ch[j].begin(), large_ch[j].end(), ch)-large_ch[j].begin();
		if(pos_ch<large_ch[j].size()){
			h_en_t[j][0]->Fill(energy,t_time,wgt);
			h_deta_t[j][0]->Fill(deta,t_time,wgt);
			h_dphi_t[j][0]->Fill(dphi,t_time,wgt);
			h_eta[j][0]->Fill(eta,wgt);
			num[j][0]++;
		//	cout<<"lala"<<ch<<endl;
		}
		else{
			h_en_t[j][1]->Fill(energy,t_time,wgt);   
			h_deta_t[j][1]->Fill(deta,t_time,wgt);
			h_dphi_t[j][1]->Fill(dphi,t_time,wgt);
			h_eta[j][1]->Fill(eta,wgt);
			num[j][1]++;
		}
		h_en_t[j][2]->Fill(energy,t_time,wgt);
		h_deta_t[j][2]->Fill(deta,t_time,wgt);
		h_dphi_t[j][2]->Fill(dphi,t_time,wgt);
		h_eta[j][2]->Fill(eta,wgt);
		num[j][2]++;
	}
	int slot_n;
	string subslot[3]={"Large energy channels","Normal channels", "All channels"};
	int nFrac=100;
	double minX[7][3];
	double maxX[7][3];
	double enFit[7][3][6];
	///CJ:
	// Initialize the en corrections
	for( int g = 0; g < 3; g++){
		for( int e = 0; e < 7; e++){
			for( int p = 0; p < 7; p++){
				enFit[e][g][p] = 0;
			}
			minX[e][g] = -99;
			maxX[e][g] = -99;
		}
	}
	ofstream f2;
	f2.open("AvgEnSlFit.dat");
	TProfile *profile2;
	for(int s=0;s<7;s++){
		slot_n=Slot[s];
		for(int p=0;p<3;p++){
			TCanvas *cw1 = new TCanvas("c_ma1","c_mw1",800,700);
			profile2=h_deta_t[s][p]->ProfileX();
			profile2->Draw();
			profile2->GetYaxis()->SetRangeUser(tmin,tmax);
		//	profile2->SetErrorOption("g");
			profile2->Draw();
			profile2->GetXaxis()->SetTitle("#delta#eta");
			profile2->GetYaxis()->SetTitle("time(ns)");
			minX[s][p]=profile2->GetXaxis()->GetBinCenter(h_deta_t[s][p]->FindFirstBinAbove(0)+1);
			maxX[s][p]=profile2->GetXaxis()->GetBinCenter(h_deta_t[s][p]->FindLastBinAbove(0)-1);
			//	profile2->GetYaxis()->SetRangeUser(tmin,tmax);
		//	TF1 *f1 = new TF1("f1", "pol5", minX[s][p], maxX[s][p]);
	//		//   tpf->GetXaxis()->SetRangeUser(5,300);
//			int fit_rpf = profile2->Fit("f1", "RQ");
//
//			f1->SetLineColor(kRed);
//			f1->Draw("sames");
////			for(int k=0; k<6; k++){
//				enFit[s][p][k] = f1->GetParameter(k);
//				//	cout<<enFit[g][sl][p]<<endl;
//			}
			myText(0.22,0.89,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7);
			myText(0.22, 0.81, kBlack, Form("%s",subslot[p].c_str()), 0.7);
			myText(0.22, 0.85, kBlack, Form("Entries: %d",num[s][p] ), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[0].c_str()),0.8);
			cw1->SaveAs(Form("~/WWW/bad_slot/profile_deta_time%d_%d.png",p,s));
			TCanvas *cw2 = new TCanvas("c_ma1","c_mw1",800,700);
			profile2=h_dphi_t[s][p]->ProfileX();
			profile2->Draw();
			profile2->GetYaxis()->SetRangeUser(tmin,tmax);
		//	profile2->SetErrorOption("g");
			profile2->Draw();
			profile2->GetXaxis()->SetTitle("#delta#phi");
			profile2->GetYaxis()->SetTitle("time(ns)");
			minX[s][p]=profile2->GetXaxis()->GetBinCenter(h_dphi_t[s][p]->FindFirstBinAbove(0)+1);
			maxX[s][p]=profile2->GetXaxis()->GetBinCenter(h_dphi_t[s][p]->FindLastBinAbove(0)-1);
			//	profile2->GetYaxis()->SetRangeUser(tmin,tmax);
	//		TF1 *f1 = new TF1("f1", "pol5", minX[s][p], maxX[s][p]);
	//		//   tpf->GetXaxis()->SetRangeUser(5,300);
	//		int fit_rpf = profile2->Fit("f1", "RQ");

	//		f1->SetLineColor(kRed);
	//		f1->Draw("sames");
	//		for(int k=0; k<6; k++){
	//			enFit[s][p][k] = f1->GetParameter(k);
	//			//	cout<<enFit[g][sl][p]<<endl;
	//		}
			myText(0.22,0.89,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7);
			myText(0.22, 0.81, kBlack, Form("%s",subslot[p].c_str()), 0.7);
			myText(0.22, 0.85, kBlack, Form("Entries: %d",num[s][p] ), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[0].c_str()),0.8);
			cw2->SaveAs(Form("~/WWW/bad_slot/profile_dphi_time%d_%d.png",p,s));
			TCanvas *cw4 = new TCanvas("c_ma1","c_mw1",800,700);
			if(s==0||s==1||s==4){
				h_en_t[s][p] = rebin( h_en_t[s][p], h_en_t[s][p]->GetEntries()/nFrac, h_en_t[s][p]->GetXaxis()->FindBin(0.)-1);
			}
			else{
				if(p==1){
					h_en_t[s][p] = rebin( h_en_t[s][p], h_en_t[s][p]->GetEntries()/15, h_en_t[s][p]->GetXaxis()->FindBin(0.)-1);
				}
				else{

					h_en_t[s][p] = rebin( h_en_t[s][p], h_en_t[s][p]->GetEntries()/15 , h_en_t[s][p]->GetXaxis()->FindBin(0.)-1);

				}
			}

			profile2=h_en_t[s][p]->ProfileX();
			profile2->Draw();
			profile2->GetYaxis()->SetRangeUser(tmin,tmax);
			profile2->SetErrorOption("g");
			profile2->Draw();
			profile2->GetXaxis()->SetTitle("Energy(GeV)");
			profile2->GetYaxis()->SetTitle("time(ns)");
			minX[s][p]=profile2->GetXaxis()->GetBinCenter(h_en_t[s][p]->FindFirstBinAbove(0)+1);
			maxX[s][p]=profile2->GetXaxis()->GetBinCenter(h_en_t[s][p]->FindLastBinAbove(0)-1);
			//	profile2->GetYaxis()->SetRangeUser(tmin,tmax);
			TF1 *f1 = new TF1("f1", "pol5", minX[s][p], maxX[s][p]);
			//   tpf->GetXaxis()->SetRangeUser(5,300);
			int fit_rpf = profile2->Fit("f1", "RQ");

			f1->SetLineColor(kRed);
			f1->Draw("sames");
			for(int k=0; k<6; k++){
				enFit[s][p][k] = f1->GetParameter(k);
				//	cout<<enFit[g][sl][p]<<endl;
			}
			myText(0.22,0.89,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7);
			myText(0.22, 0.81, kBlack, Form("%s",subslot[p].c_str()), 0.7);
			myText(0.22, 0.85, kBlack, Form("Entries: %d",num[s][p] ), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[0].c_str()),0.8);
			cw4->SaveAs(Form("~/WWW/bad_slot/profile_en_time%d_%d.png",p,s));

			TCanvas *cw = new TCanvas("c_m","c_mw",800,700);
			h_eta[s][p]->Draw();
			h_eta[s][p]->Draw();
			h_eta[s][p]->GetXaxis()->SetTitle("eta");
			h_eta[s][p]->GetYaxis()->SetTitle("Entries");
			h_eta[s][p]-> GetXaxis()-> SetRangeUser(h_eta[s][p]->GetXaxis()->GetBinLowEdge(h_eta[s][p]->FindFirstBinAbove(0)),h_eta[s][p]->GetXaxis()->GetBinLowEdge(h_eta[s][p]->FindLastBinAbove(0)+1));

			myText(0.22,0.89,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7);
			myText(0.22, 0.81, kBlack, Form("%s",subslot[p].c_str()), 0.7);
			myText(0.22, 0.85, kBlack, Form("Entries: %d",num[s][p] ), 0.7);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[0].c_str()),0.8);
			cw->SaveAs(Form("~/WWW/bad_slot/channel_eta%d_%d.png",p,s));

			
		}
	}
	for(int sl = 0; sl < 7; sl++){
		for(int p  = 0; p  < 3; p ++){
			f2 << sl << " " << p << " "<< minX[sl][p] << " " << maxX[sl][p];
			///Cj
			for(int k = 0; k < 6; k++){
				f2 << " " << enFit[sl][p][k];
			}
			f2 << std::endl;
		}
	}

	return;
}
/*
void bad_checkPlot(){
	TFile *file;
	// Open files
	file   = TFile::Open("/data/users/jchen/WTiming/WTiming_root/CalibrationFramework/ArcondCalib/submit_pass3_trees/RootOutputs/pass3.root");

	ofstream out[2][7];



	//	out[g]	("channel_position_energy.txt");
	TH2F *h_ch[7];

	for( int g=0; g<2; g++){ 
		for(int s=0;s<7;s++){
			out[g][s].open(Form("phi_edge_ch%d%d", s,g));

			h_en_t[g][s] = new TH2F(Form("h_en_t%d%d",g,s),Form("h_en_t%d%d",g,s),200,0,600,1000,-25,25);
			h_ch_t[g][s] = new TH2F(Form("h_ch_t%d%d",g,s),Form("%d%d",g,s),79360,-0.5,79359.5, 1000,-25,25);
			h_ch_eta[g][s] = new TH2F(Form("h_ch_eta%d%d",g,s),Form("h_ch_eta%d%d",g,s),79360,-0.5,79359.5, 1000,-5,5);
			h_ch_phi[g][s] = new TH2F(Form("h_ch_phi%d%d",g,s),Form("h_ch_phi%d%d",g,s),79360,-0.5,79359.5, 1000,-5,5);
			h_ch_en[g][s] =  new TH2F(Form("h_ch_en%d%d",g,s), Form("h_ch_en%d%d",g,s),79360,-0.5,79359.5, 200,0,120);
			h_eta_en[g][s] = new TH2F(Form("h_eta_en%d%d",g,s),Form("h_eta_en%d%d",g,s),1000,-5,-5,200,0,150);
			h_phi_en[g][s] = new TH2F(Form("h_phi_en%d%d",g,s),Form("h_phi_en%d%d",g,s),1000,-5,-5,200,0,150);
		}}

	TTree *tree =(TTree*)file->Get("tree");
	int max_ch[2][7]={{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}}, min_ch[2][7]={{79360,79360,79360,79360,79360,79360,79360},{79360,79360,79360,79360,79360,79360,79360}};	
	int t_gain, run, ch, slot,wgt;
	int num[2]={0,0};
	auto it=bad_slot.begin();
	int j;
	double energy, mu, t_time, eta,phi;
	tree->SetBranchAddress("gain", &t_gain);
	tree->SetBranchAddress("time", &t_time);
	tree->SetBranchAddress("channel", &ch);
	tree->SetBranchAddress("mu", &mu);
	tree->SetBranchAddress("slot", &slot);
	tree->SetBranchAddress("run", &run);
	tree->SetBranchAddress("wgt", &wgt);
	tree->SetBranchAddress("energy", &energy);
	tree->SetBranchAddress("caloCluster_eta", &eta);
	tree->SetBranchAddress("caloCluster_phi", &phi);
			

	Long64_t nentries = tree->GetEntries();
	for (Long64_t i=0;i<nentries;i++) {	
		tree->GetEntry(i); 
		if((slot!=12&&slot!=13&&slot!=14&&slot!=15&&slot!=19&&slot!=20&&slot!=21)||t_gain==2)
		continue;
		it = bad_slot.find(slot);
		j=it->second;
	//	h_mu[t_gain]->Fill(run,mu,wgt);
//		cout<<"lalla"<<"\t"<<t_time<<"\t"<<slot<<"\t"<<ch<<endl;
//		if(ch>max_ch[t_gain][j])
//			max_ch[t_gain][j]=ch;
//		if(ch<min_ch[t_gain][j])
//			min_ch[t_gain][j]=ch;
			//cout<<t_gain<<"\t"<<min_ch[t_gain][j]<<"\t"<<max_ch[t_gain][j]<<endl;
	//	out[t_gain][j]<<ch<<"\t"<<eta<<"\t"<<phi<<"\t"<<energy<<"\t"<<t_time<<endl;
			

	       

	//	if(slot==13&&ch<52500&&ch>52000){
	//	h_ch_t[t_gain][j]->fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->fill(ch, energy, wgt);
	//	max_ch[t_gain][j]=52500;
	//	min_ch[t_gain][j]=52000;
	//	}
	//	else if(slot==15&&ch>57200&&ch<57350){
	//	h_ch_t[t_gain][j]->Fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->Fill(ch, energy, wgt);
	//	max_ch[t_gain][j]=57350;
	//	min_ch[t_gain][j]=57200;
	//	}
	     //   h_ch_eta[t_gain][j]->Fill(ch,eta,wgt); 
	     //   h_ch_phi[t_gain][j]->Fill(ch,phi,wgt); 
	      h_en_t[t_gain][j]->Fill(energy,t_time, wgt);
	//	h_ch_t[t_gain][j]->Fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->Fill(ch, energy, wgt);
	//	h_eta_en[t_gain][j]->Fill(eta, energy, wgt);
//		h_phi_en[t_gain][j]->Fill(phi, energy, wgt);

	}	
	TProfile *profile;
	TProfile *profile1;
	TProfile *profile2;
	TProfile *profile3;
	int slot_n;
	for( int g=0; g<2; g++){ 
		for(int s=0;s<7;s++){
		//	if(s!=1&&s!=3) continue;
			slot_n=Slot[s];
		//	cout<<g<<"\t"<<s<<"\t"<<min_ch[g][s]<<"\t"<<max_ch[g][s]<<endl;
		/*
			TCanvas *cw = new TCanvas("c_ma","c_mw",800,700);
			profile1=h_ch_t[g][s]->ProfileX();
			profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			profile1->Draw("E"); 
			profile1->GetXaxis()->SetTitle("channel");
			profile1->GetYaxis()->SetTitle("time(ns)");
			profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw->SaveAs(Form("~/WWW/bad_slot/ch_time%s_%d.png",gain[g].c_str(),slot_n));
			delete cw;

			TCanvas *cw1 = new TCanvas("c_ma","c_mw",800,700);
		//	profile1=h_ch_eta[g][s]->ProfileX();
		//	profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		//	profile1->Draw("E"); 
		//	profile1->GetXaxis()->SetTitle("channel");
		//	profile1->GetYaxis()->SetTitle("eta");

		//	profile1-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
			h_ch_eta[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			h_ch_eta[g][s]->Draw(); 
			h_ch_eta[g][s]->GetXaxis()->SetTitle("channel");
			h_ch_eta[g][s]->GetYaxis()->SetTitle("eta");

			h_ch_eta[g][s]-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
		//	profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
		//	profile1-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
	
			
	       //         profile1-> GetYaxis()-> SetRangeUser(2,2.5);
		//	if(slot_n>16)
	          //      profile1-> GetYaxis()-> SetRangeUser(-2.5,-2);

			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw1->SaveAs(Form("~/WWW/bad_slot/ch_eta%s_%d.png",gain[g].c_str(),slot_n));
			delete cw1;

			TCanvas *cw2 = new TCanvas("c_ma","c_mw",800,700);
			//profile1=h_ch_phi[g][s]->ProfileX();
			//profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			//profile1->Draw("E"); 
			//profile1->GetXaxis()->SetTitle("channel");
			//profile1->GetYaxis()->SetTitle("phi");
			h_ch_phi[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			h_ch_phi[g][s]->Draw(); 
			h_ch_phi[g][s]->GetXaxis()->SetTitle("channel");
			h_ch_phi[g][s]->GetYaxis()->SetTitle("phi");
			//		profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw2->SaveAs(Form("~/WWW/bad_slot/ch_phi%s_%d.png",gain[g].c_str(),slot_n));
			delete cw2;

			TCanvas *cw3 = new TCanvas("c_ma3","c_mw3",800,700);
			profile3=h_ch_en[g][s]->ProfileX();
			profile3->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			profile3->Draw("E"); 
			profile3->GetXaxis()->SetTitle("channel");
			profile3->GetYaxis()->SetTitle("Energy");
			if(g==0)
				profile3->GetYaxis()->SetRangeUser(15,60);   
	//		h_ch_en[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
	//		h_ch_en[g][s]->Draw(); 
	//		h_ch_en[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_en[g][s]->GetYaxis()->SetTitle("Energy");
		//	if(g==0)
		//		h_ch_en[g][s]->GetYaxis()->SetRangeUser(15,60);   


	//		profile3->GetYaxis()->SetRangeUser(-1,1);
	//		for( int bin=0; bin<profile3->GetNbinsX(); bin++){
	//			profile3->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
	//		}
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
		//	if(g==0)
		//		myText(0.72, 0.83,kBlack,Form("Entries: %d",num[0] ),0.6);
		//	else
		//		myText(0.72, 0.83,kBlack,Form("Entries: %d",num[1] ),0.6);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw3->SaveAs(Form("~/WWW/bad_slot/ch_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw3;
*/
	/*
			TCanvas *cw4 = new TCanvas("c_ma1","c_mw1",800,700);
			h_en_t[g][s]->Draw(); 
			h_en_t[g][s]->GetXaxis()->SetTitle("Energy(GeV)");
			h_en_t[g][s]->GetYaxis()->SetTitle("time(ns)");
			h_en_t[g][s]->GetYaxis()->SetRangeUser(15,15);
	
			if(g==0)
				h_en_t[g][s]->GetXaxis()->SetRangeUser(0,150);   
		//	profile2=h_en_t[g][s]->ProfileX();
		//	profile2->Draw("E"); 
		//	profile2->GetXaxis()->SetTitle("Energy(GeV)");
		//	profile2->GetYaxis()->SetTitle("time(ns)");
		//	profile2->GetYaxis()->SetRangeUser(-0.6,1.5);
	
		//	if(g==0)
		//		profile2->GetXaxis()->SetRangeUser(0,100);   
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw4->SaveAs(Form("~/WWW/bad_slot/en_time%s_%d.png",gain[g].c_str(),slot_n));
				/*
				TCanvas *cw5 = new TCanvas("c_ma1","c_mw1",800,700);
			//		h_eta_en[g][s]->Draw(); 

			//                h_eta_en[g][s]->GetYaxis()->SetTitle("Energy(GeV)"); 
			//		h_eta_en[g][s]->GetXaxis()->SetTitle("eta"); 

			profile2=h_eta_en[g][s]->ProfileX();
			profile2->Draw("E"); 
			profile2->GetYaxis()->SetTitle("Energy(GeV)");
			profile2->GetXaxis()->SetTitle("eta");
			int nbins = profile2->GetNbinsX();
			double dummy_evn =  profile2->GetSumOfWeights();

			//string output_datei_data(run_name);
			//output_datei_data=output_datei_data+ output_data;

			//const char *output_datei_data_l = output_datei_data.c_str();

			//ofstream filetotcross100(output_datei_data_l);
			for (int i=1;i<=nbins;i++)
			{
				out[g][s] << profile2->GetBinCenter(i) << " " << profile2->GetBinContent(i) <<"  " << dummy_evn << endl;
			}
			out[g][s].close();


			if(g==0)
				profile2->GetYaxis()->SetRangeUser(10,80);   

			profile2-> GetXaxis()-> SetRangeUser(h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0)),h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1));
			//			out[g][s]<<h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0))<<"\t"<<h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1)<<endl;
			//h_eta_en[g][s]-> GetXaxis()-> SetRangeUser(h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0)),h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1));
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw5->SaveAs(Form("~/WWW/bad_slot/eta_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw5;
			TCanvas *cw6 = new TCanvas("c_ma1","c_mw1",800,700);
		//	h_phi_en[g][s]->Draw(); 
		//	h_phi_en[g][s]->GetYaxis()->SetTitle("Energy(GeV)");
		//	h_phi_en[g][s]->GetXaxis()->SetTitle("phi     ");
		//	h_phi_en[g][s]->GetYaxis()->SetRangeUser(15,15);

	
		//	if(g==0)
		//		h_en_t[g][s]->GetXaxis()->SetRangeUser(0,150);   
			profile2=h_phi_en[g][s]->ProfileX();
			profile2->Draw("E"); 
			profile2->GetYaxis()->SetTitle("Energy(GeV)");
			profile2->GetXaxis()->SetTitle("Phi");
			int nbins = profile2->GetNbinsX();
			double dummy_evn =  profile2->GetSumOfWeights();
			for (int n=1;n<=nbins;n++)
			{
				out[g][s] << profile2->GetBinCenter(n) << " " << profile2->GetBinContent(n) <<"  " << dummy_evn << endl;
			}
			out[g][s].close();
			if(g==0)
				profile2->GetYaxis()->SetRangeUser(10,80);   
			profile2-> GetXaxis()-> SetRangeUser(h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindFirstBinAbove(0)),h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindLastBinAbove(0)+1));
			//h_phi_en[g][s]-> GetXaxis()-> SetRangeUser(h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindFirstBinAbove(0)),h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindLastBinAbove(0)+1));
			//	ATLASLabel(0.2,0.88,pInternal);
			//	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw6->SaveAs(Form("~/WWW/bad_slot/phi_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw6;
				*/
				/*
//	TCanvas *cw_all = new TCanvas("c_massw","c_massw",800,700);
//	profile=h_mu[g]->ProfileX();
////	h->Draw();
//	profile->Draw("e"); 
//	profile->GetXaxis()->SetTitle("Run Number");
////	profile->GetXaxis()->SetRangeUser(50,80);
//	profile->GetYaxis()->SetRangeUser(0,50);
//	profile->GetYaxis()->SetTitle("Actual Interactions Per Crossing");
//	for( int bin=0; bin<NRUNS; bin++){
//		//	for( int bin=60; bin<NRUNS; bin++){
//		int s_run =  runNumberList[bin] ;
//		profile->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
//	}
//
//	ATLASLabel(0.2,0.88,pInternal);
//	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
//	cout<<"out"<<endl;
//	myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
//	if(g==0)
//	myText(0.72, 0.83,kBlack,Form("Entries: %d",num[0] ),0.6);
//	else
//	myText(0.72, 0.83,kBlack,Form("Entries: %d",num[1] ),0.6);
//
//	myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
//	cw_all->SaveAs(Form("run_mu%s.png",gain[g].c_str()));
//
//	delete cw_all;
		}
		}
	return;
}
*/
//___________________
// 
// Make mass plots
//_____________________
/*
void energy_slot_checkPlot(){
	TFile *file;
	// Open files
	file   = TFile::Open("/data/users/jchen/WTiming/WTiming_root/CalibrationFramework/ArcondCalib/submit_pass3_trees/RootOutputs/pass3.root");

	ofstream out[2][7];



	//	out[g]	("channel_position_energy.txt");
	TH2F *h_ch_t[2][7];
	TH2F *h_en_t[2][7];
	TH2F *h_ch_en[2][7];
	TH2F *h_ch_eta[2][7];
	TH2F *h_ch_phi[2][7];
	TH2F *h_eta_en[2][7];
	TH2F *h_phi_en[2][7];

	for( int g=0; g<2; g++){ 
		for(int s=0;s<7;s++){
			out[g][s].open(Form("phi_edge_ch%d%d", s,g));

			h_en_t[g][s] = new TH2F(Form("h_en_t%d%d",g,s),Form("h_en_t%d%d",g,s),1000,5,350,1000,-2,2);
			h_ch_t[g][s] = new TH2F(Form("h_ch_t%d%d",g,s),Form("%d%d",g,s),79360,-0.5,79359.5, 1000,-25,25);
			h_ch_eta[g][s] = new TH2F(Form("h_ch_eta%d%d",g,s),Form("h_ch_eta%d%d",g,s),79360,-0.5,79359.5, 1000,-5,5);
			h_ch_phi[g][s] = new TH2F(Form("h_ch_phi%d%d",g,s),Form("h_ch_phi%d%d",g,s),79360,-0.5,79359.5, 1000,-5,5);
			h_ch_en[g][s] =  new TH2F(Form("h_ch_en%d%d",g,s), Form("h_ch_en%d%d",g,s),79360,-0.5,79359.5, 200,0,120);
			h_eta_en[g][s] = new TH2F(Form("h_eta_en%d%d",g,s),Form("h_eta_en%d%d",g,s),1000,-5,-5,200,0,150);
			h_phi_en[g][s] = new TH2F(Form("h_phi_en%d%d",g,s),Form("h_phi_en%d%d",g,s),1000,-5,-5,200,0,150);
		}}

	TTree *tree =(TTree*)file->Get("tree");
	int max_ch[2][7]={{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}}, min_ch[2][7]={{79360,79360,79360,79360,79360,79360,79360},{79360,79360,79360,79360,79360,79360,79360}};	
	int t_gain, run, ch, slot,wgt;
	int num[2]={0,0};
	auto it=bad_slot.begin();
	//	out[g]	("channel_position_energy.txt");
	TH2F *h_en_t[2][7][2];

	ofstream fitfile[2];
	for( int g=0; g<2; g++){ 
		fitfile[g].open(Form("AvgEnSlFit_%s.dat",gain[g].c_str()));
		for(int s=0;s<7;s++){
			for(int p=0; p<2; p++){
				//	out[g][s].open(Form("large_energy_ch%d%d", s,g), std::ifstream::in);
				h_en_t[g][s][p] = new TH2F(Form("h_en_t%d%d%d",g,s,p),Form("h_en_t%d%d%d",g,s,p),500,0,100,1000,-25,25);
			}}}

	TTree *tree =(TTree*)file->Get("tree");
	int t_gain, run, ch, slot,wgt;
	auto it=bad_slot.begin();
	int j;
	double energy, mu, t_time, eta,phi;
	tree->SetBranchAddress("gain", &t_gain);
	tree->SetBranchAddress("time", &t_time);
	tree->SetBranchAddress("channel", &ch);
	tree->SetBranchAddress("mu", &mu);
	tree->SetBranchAddress("slot", &slot);
	tree->SetBranchAddress("run", &run);
	tree->SetBranchAddress("wgt", &wgt);
	tree->SetBranchAddress("energy", &energy);
	tree->SetBranchAddress("caloCluster_eta", &eta);
	tree->SetBranchAddress("caloCluster_phi", &phi);

//	cout<<sub_r[1][0]<<endl;
	Long64_t nentries = tree->GetEntries();
	for (Long64_t i=0;i<nentries;i++) {	
		tree->GetEntry(i); 
		if((slot!=12&&slot!=13&&slot!=14&&slot!=15&&slot!=19&&slot!=20&&slot!=21)||t_gain==2||t_gain==1)
			//	if((slot!=13&&slot!=15)||t_gain==2)
			continue;
		it = bad_slot.find(slot);
		j=it->second;
		if(j==0||j==1||j==4){
			if(eta<=sub_r[j][0])
				h_en_t[t_gain][j][0]->Fill(energy,t_time, wgt);
			else
				h_en_t[t_gain][j][1]->Fill(energy,t_time, wgt);
		}
		else{
			if(eta<=sub_r[j][0]||(eta>sub_r[j][1]&&eta<=sub_r[j][2])||eta>sub_r[j][3])
				h_en_t[t_gain][j][0]->Fill(energy,t_time, wgt);  
			else
				h_en_t[t_gain][j][1]->Fill(energy,t_time, wgt);  
		}
	}
	int j;
	double energy, mu, t_time, eta,phi;
	tree->SetBranchAddress("gain", &t_gain);
	tree->SetBranchAddress("time", &t_time);
	tree->SetBranchAddress("channel", &ch);
	tree->SetBranchAddress("mu", &mu);
	tree->SetBranchAddress("slot", &slot);
	tree->SetBranchAddress("run", &run);
	tree->SetBranchAddress("wgt", &wgt);
	tree->SetBranchAddress("energy", &energy);
	tree->SetBranchAddress("caloCluster_eta", &eta);
	tree->SetBranchAddress("caloCluster_phi", &phi);
			

	Long64_t nentries = tree->GetEntries();
	for (Long64_t i=0;i<nentries;i++) {	
		tree->GetEntry(i); 
		if((slot!=12&&slot!=13&&slot!=14&&slot!=15&&slot!=19&&slot!=20&&slot!=21)||t_gain==2)
		continue;
		it = bad_slot.find(slot);
		j=it->second;
	//	h_mu[t_gain]->Fill(run,mu,wgt);
//		cout<<"lalla"<<"\t"<<t_time<<"\t"<<slot<<"\t"<<ch<<endl;
//		if(ch>max_ch[t_gain][j])
//			max_ch[t_gain][j]=ch;
//		if(ch<min_ch[t_gain][j])
//			min_ch[t_gain][j]=ch;
			//cout<<t_gain<<"\t"<<min_ch[t_gain][j]<<"\t"<<max_ch[t_gain][j]<<endl;
	//	out[t_gain][j]<<ch<<"\t"<<eta<<"\t"<<phi<<"\t"<<energy<<"\t"<<t_time<<endl;
			

	       

	//	if(slot==13&&ch<52500&&ch>52000){
	//	h_ch_t[t_gain][j]->fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->fill(ch, energy, wgt);
	//	max_ch[t_gain][j]=52500;
	//	min_ch[t_gain][j]=52000;
	//	}
	//	else if(slot==15&&ch>57200&&ch<57350){
	//	h_ch_t[t_gain][j]->Fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->Fill(ch, energy, wgt);
	//	max_ch[t_gain][j]=57350;
	//	min_ch[t_gain][j]=57200;
	//	}
	     //   h_ch_eta[t_gain][j]->Fill(ch,eta,wgt); 
	     //   h_ch_phi[t_gain][j]->Fill(ch,phi,wgt); 
	      h_en_t[t_gain][j]->Fill(energy,t_time, wgt);
	//	h_ch_t[t_gain][j]->Fill(ch, t_time, wgt);
	//	h_ch_en[t_gain][j]->Fill(ch, energy, wgt);
	//	h_eta_en[t_gain][j]->Fill(eta, energy, wgt);
//		h_phi_en[t_gain][j]->Fill(phi, energy, wgt);

	      

	}	
	for(int k=0;k<7;k++){
	
	
	h_en_t[0][k]->Add(h_en_t[1][k]);
	}
	TProfile *profile;
	TProfile *profile1;
	TProfile *profile2;
	TProfile *profile3;
	int slot_n;
	for( int g=0; g<1; g++){ 
		for(int s=0;s<7;s++){
		//	if(s!=1&&s!=3) continue;
			slot_n=Slot[s];
		//	cout<<g<<"\t"<<s<<"\t"<<min_ch[g][s]<<"\t"<<max_ch[g][s]<<endl;
			TCanvas *cw = new TCanvas("c_ma","c_mw",800,700);
			profile1=h_ch_t[g][s]->ProfileX();
			profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			profile1->Draw("E"); 
			profile1->GetXaxis()->SetTitle("channel");
			profile1->GetYaxis()->SetTitle("time(ns)");
			profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw->SaveAs(Form("~/WWW/bad_slot/ch_time%s_%d.png",gain[g].c_str(),slot_n));
			delete cw;

			TCanvas *cw1 = new TCanvas("c_ma","c_mw",800,700);
		//	profile1=h_ch_eta[g][s]->ProfileX();
		//	profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		//	profile1->Draw("E"); 
		//	profile1->GetXaxis()->SetTitle("channel");
		//	profile1->GetYaxis()->SetTitle("eta");

		//	profile1-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
			h_ch_eta[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			h_ch_eta[g][s]->Draw(); 
			h_ch_eta[g][s]->GetXaxis()->SetTitle("channel");
			h_ch_eta[g][s]->GetYaxis()->SetTitle("eta");

			h_ch_eta[g][s]-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
		//	profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
		//	profile1-> GetYaxis()-> SetRangeUser(h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindFirstBinAbove(0)),h_ch_eta[g][s]->GetYaxis()->GetBinLowEdge(h_ch_eta[g][s]->FindLastBinAbove(0)+1));
	
			
	       //         profile1-> GetYaxis()-> SetRangeUser(2,2.5);
		//	if(slot_n>16)
	          //      profile1-> GetYaxis()-> SetRangeUser(-2.5,-2);

			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw1->SaveAs(Form("~/WWW/bad_slot/ch_eta%s_%d.png",gain[g].c_str(),slot_n));
			delete cw1;

			TCanvas *cw2 = new TCanvas("c_ma","c_mw",800,700);
			//profile1=h_ch_phi[g][s]->ProfileX();
			//profile1->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			//profile1->Draw("E"); 
			//profile1->GetXaxis()->SetTitle("channel");
			//profile1->GetYaxis()->SetTitle("phi");
			h_ch_phi[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			h_ch_phi[g][s]->Draw(); 
			h_ch_phi[g][s]->GetXaxis()->SetTitle("channel");
			h_ch_phi[g][s]->GetYaxis()->SetTitle("phi");
			//		profile1->GetYaxis()->SetRangeUser(-0.2,0.2);
	//		h_ch_t[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
		
	//		h_ch_t[g][s]->Draw(); 
	//		h_ch_t[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_t[g][s]->GetYaxis()->SetTitle("time(ns)");
		//	h_ch_t[g][s]->GetYaxis()->SetRangeUser(-2,2);
		//	profile1->GetYaxis()->SetRangeUser(-1,1);
		//	for( int bin=0; bin<profile1->GetNbinsX(); bin++){
		//		profile1->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
		//	}
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw2->SaveAs(Form("~/WWW/bad_slot/ch_phi%s_%d.png",gain[g].c_str(),slot_n));
			delete cw2;

			TCanvas *cw3 = new TCanvas("c_ma3","c_mw3",800,700);
			profile3=h_ch_en[g][s]->ProfileX();
			profile3->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
			profile3->Draw("E"); 
			profile3->GetXaxis()->SetTitle("channel");
			profile3->GetYaxis()->SetTitle("Energy");
			if(g==0)
				profile3->GetYaxis()->SetRangeUser(15,60);   
	//		h_ch_en[g][s]->GetXaxis()->SetRangeUser(min_ch[g][s],max_ch[g][s]);
	//		h_ch_en[g][s]->Draw(); 
	//		h_ch_en[g][s]->GetXaxis()->SetTitle("channel");
	//		h_ch_en[g][s]->GetYaxis()->SetTitle("Energy");
		//	if(g==0)
		//		h_ch_en[g][s]->GetYaxis()->SetRangeUser(15,60);   


	//		profile3->GetYaxis()->SetRangeUser(-1,1);
	//		for( int bin=0; bin<profile3->GetNbinsX(); bin++){
	//			profile3->GetXaxis()->SetBinLabel(bin+1, Form("%d",min_ch[g]+bin) );
	//		}
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
		//	if(g==0)
		//		myText(0.72, 0.83,kBlack,Form("Entries: %d",num[0] ),0.6);
		//	else
		//		myText(0.72, 0.83,kBlack,Form("Entries: %d",num[1] ),0.6);
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw3->SaveAs(Form("~/WWW/bad_slot/ch_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw3;
			TCanvas *cw4 = new TCanvas("c_ma1","c_mw1",800,700);
			h_en_t[g][s]->Draw("COLZ"); 
			h_en_t[g][s]->GetXaxis()->SetTitle("Energy(GeV)");
			h_en_t[g][s]->GetYaxis()->SetTitle("time(ns)");
			h_en_t[g][s]->GetYaxis()->SetRangeUser(7,7);
	
			h_en_t[g][s]-> GetXaxis()-> SetRangeUser(h_en_t[g][s]->GetYaxis()->GetBinLowEdge(h_en_t[g][s]->FindFirstBinAbove(0)),h_en_t[g][s]->GetYaxis()->GetBinLowEdge(h_en_t[g][s]->FindLastBinAbove(0)+1));
			if(g==0)
				h_en_t[g][s]->GetXaxis()->SetRangeUser(0,70);   
			if(g==1)
				h_en_t[g][s]->GetXaxis()->SetRangeUser(0,320);   
		//	profile2=h_en_t[g][s]->ProfileX();
		//	profile2->Draw("E"); 
		//	profile2->GetXaxis()->SetTitle("Energy(GeV)");
		//	profile2->GetYaxis()->SetTitle("time(ns)");
		//	profile2->GetYaxis()->SetRangeUser(-0.6,1.5);
	
		//	if(g==0)
		//		profile2->GetXaxis()->SetRangeUser(0,100);   
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw4->SaveAs(Form("~/WWW/bad_slot/2d_en_timeH_M_%d.png",slot_n));
				TCanvas *cw5 = new TCanvas("c_ma1","c_mw1",800,700);
			//		h_eta_en[g][s]->Draw(); 

			//                h_eta_en[g][s]->GetYaxis()->SetTitle("Energy(GeV)"); 
			//		h_eta_en[g][s]->GetXaxis()->SetTitle("eta"); 

			profile2=h_eta_en[g][s]->ProfileX();
			profile2->Draw("E"); 
			profile2->GetYaxis()->SetTitle("Energy(GeV)");
			profile2->GetXaxis()->SetTitle("eta");
			int nbins = profile2->GetNbinsX();
			double dummy_evn =  profile2->GetSumOfWeights();

			//string output_datei_data(run_name);
			//output_datei_data=output_datei_data+ output_data;

			//const char *output_datei_data_l = output_datei_data.c_str();

			//ofstream filetotcross100(output_datei_data_l);
			for (int i=1;i<=nbins;i++)
			{
				out[g][s] << profile2->GetBinCenter(i) << " " << profile2->GetBinContent(i) <<"  " << dummy_evn << endl;
			}
			out[g][s].close();


			if(g==0)
				profile2->GetYaxis()->SetRangeUser(10,80);   

			profile2-> GetXaxis()-> SetRangeUser(h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0)),h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1));
			//			out[g][s]<<h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0))<<"\t"<<h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1)<<endl;
			//h_eta_en[g][s]-> GetXaxis()-> SetRangeUser(h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindFirstBinAbove(0)),h_eta_en[g][s]->GetXaxis()->GetBinLowEdge(h_eta_en[g][s]->FindLastBinAbove(0)+1));
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw5->SaveAs(Form("~/WWW/bad_slot/eta_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw5;
			TCanvas *cw6 = new TCanvas("c_ma1","c_mw1",800,700);
		//	h_phi_en[g][s]->Draw(); 
		//	h_phi_en[g][s]->GetYaxis()->SetTitle("Energy(GeV)");
		//	h_phi_en[g][s]->GetXaxis()->SetTitle("phi     ");
		//	h_phi_en[g][s]->GetYaxis()->SetRangeUser(15,15);

	
		//	if(g==0)
		//		h_en_t[g][s]->GetXaxis()->SetRangeUser(0,150);   
			profile2=h_phi_en[g][s]->ProfileX();
			profile2->Draw("E"); 
			profile2->GetYaxis()->SetTitle("Energy(GeV)");
			profile2->GetXaxis()->SetTitle("Phi");
			int nbins = profile2->GetNbinsX();
			double dummy_evn =  profile2->GetSumOfWeights();
			for (int n=1;n<=nbins;n++)
			{
				out[g][s] << profile2->GetBinCenter(n) << " " << profile2->GetBinContent(n) <<"  " << dummy_evn << endl;
			}
			out[g][s].close();
			if(g==0)
				profile2->GetYaxis()->SetRangeUser(10,80);   
			profile2-> GetXaxis()-> SetRangeUser(h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindFirstBinAbove(0)),h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindLastBinAbove(0)+1));
			//h_phi_en[g][s]-> GetXaxis()-> SetRangeUser(h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindFirstBinAbove(0)),h_phi_en[g][s]->GetXaxis()->GetBinLowEdge(h_phi_en[g][s]->FindLastBinAbove(0)+1));
			//	ATLASLabel(0.2,0.88,pInternal);
			//	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
			myText(0.6,0.83,kBlack,Form("%s",slotNames[slot_n].c_str()),0.7); 
			myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			cw6->SaveAs(Form("~/WWW/bad_slot/phi_en%s_%d.png",gain[g].c_str(),slot_n));
			delete cw6;
//	TCanvas *cw_all = new TCanvas("c_massw","c_massw",800,700);
//	profile=h_mu[g]->ProfileX();
////	h->Draw();
//	profile->Draw("e"); 
//	profile->GetXaxis()->SetTitle("Run Number");
////	profile->GetXaxis()->SetRangeUser(50,80);
//	profile->GetYaxis()->SetRangeUser(0,50);
//	profile->GetYaxis()->SetTitle("Actual Interactions Per Crossing");
//	for( int bin=0; bin<NRUNS; bin++){
//		//	for( int bin=60; bin<NRUNS; bin++){
//		int s_run =  runNumberList[bin] ;
//		profile->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
//	}
//
//	ATLASLabel(0.2,0.88,pInternal);
//	myText(0.20, 0.81, kBlack, Form("#int L = 33.2 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
//	cout<<"out"<<endl;
//	myText(0.22, 0.73, kBlack, Form("W#rightarrow e#nu"), 0.7);
//	if(g==0)
//	myText(0.72, 0.83,kBlack,Form("Entries: %d",num[0] ),0.6);
//	else
//	myText(0.72, 0.83,kBlack,Form("Entries: %d",num[1] ),0.6);
//
//	myText(0.22, 0.76,kRed,Form("%s Gain",gain[g].c_str()),0.8);
//	cw_all->SaveAs(Form("run_mu%s.png",gain[g].c_str()));
//
//	delete cw_all;
	return;
}
*/
//_____________________________________________
//
// Read list of run numbers from config file
//_____________________________________________
void setRunList(){

	// Run number config file
	ifstream f1("../config/RunNumberList.txt");
	int tempRunNum;

	// Store each run into local vector
	while( f1 >> tempRunNum ){
		runNumberList.push_back(tempRunNum);
	}

	f1.close();
ifstream f2("nearby_channel");  
  int i=0, tempch;
  string tempid;
  while( f2 >>tempch>>tempid){
          channelList.push_back(tempch);
          onlineid.push_back(tempid);
          cout<<channelList[i]<<"\t"<<onlineid[i]<<endl;
	  i++;
  }
  f2.close();

	std::cout << "  > Added " << runNumberList.size() << " runs successfully from "
		<< runNumberList[0] << " to " << runNumberList.back() << "\n\n";
	return;
}


//_____________________________________________
//
// Set tmin/tmax for plotting
//_____________________________________________
void setTRange(double t_l, double t_h){

	tmin = t_l;
	tmax = t_h;

	return;
}

TH1F* fitGaus(TH1F* h_cell, double mean, double rms, double pos_y, int color, int pass ){

	// Hide fit stats
	gStyle->SetOptFit(0);

	// Gaus fit (limited range around peak)
	TF1 *fg = new TF1("fg","gaus",mean-5*rms, mean+5*rms);
	//fg->SetParameter(0,0); // not sure if this is necessary

	// guess starting point for fit
	fg->SetParameter(1,mean);
	fg->SetParameter(2,rms);

	// put limits on the paramters
	fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
	fg->SetParLimits(2,0.5*rms,4*rms);

	// Draw the fit in subrange
	fg->SetLineColor(kRed);
	h_cell->Fit("fg","BR");
	double chi_old = 1.1e5;
	double chi_new = 1e5;
	while(chi_old-chi_new>1e-7){
		//      for(int j=0;j<100;j++){ 
		chi_old = chi_new;
		cout<<"chi_old"<<chi_old<<endl;

		chi_new = fg->GetChisquare()/fg->GetNDF();
		cout<<"chi_new"<<chi_new<<endl;
		fg->SetParameters(fg->GetParameters());  
		h_cell->Fit("fg","R");
	}
	fg->Draw("same");

	// Make the fit extend and draw as dotted line
	TF1 *f2 = new TF1("f2","gaus",-25,25);
	f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
	f2->SetLineColor(kRed);
	f2->SetLineStyle(7); //dashed line
	f2->Draw("same");

	// Put parameters on the plot
	double pos_x = 0.78;
	myText(pos_x, pos_y-0.06*pass,color,Form("#mu: %.3f ns",fg->GetParameter(1)),0.6);
	myText(pos_x, pos_y-0.06*pass-0.03,color,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.6);

	//FIXME printing info
	//std::cout << "    Mu: " << fg->GetParameter(1) << "; Sigma: "<<fg->GetParameter(2) <<std::endl;
	return h_cell;

	}


	// Rebin the 2D histogram so that each bin
	// has at least entPerBin entries
	// firstBin is the first bin you want to consider
	//_____________________________________________
	TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

		// hold the bin edge information
		std::vector< double > xbins;
		// Combine all bins below first bin
		// Cut at 5 GeV so this bin is empty
		xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
		xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

		// keep track of last bin with at least entPerBin
		int lastFullIndex = firstBin; // for energy 5
		// Get the xaxis
		TAxis *axis = h->GetXaxis();

		// Loop over bins in xaxis
		// start after 5GeV bin
		for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {
			// Get entries in this bin, and width
			int y = h->Integral(i,i);
			double w = axis->GetBinWidth(i);

			// If not enough entries, need to combine bins
			if (y <= entPerBin){
				// Find integral from last combined bin
				double integral = h->Integral(lastFullIndex+1, i);
				if (integral <= entPerBin ) continue;
				// if above threshold, mark as new bin
				lastFullIndex = i;
				xbins.push_back( axis->GetBinLowEdge(i) + w);
			}
			else{
				// above threshold, mark as bin
				lastFullIndex = i;
				xbins.push_back( axis->GetBinLowEdge(i) + w );
			}

		}

		// put bin edges into an array
		xbins.push_back( axis->GetXmax() );
		size_t s = xbins.size();
		double *xbinsFinal = &xbins[0];
		cout<<"s"<<s<<endl;
		// create new histo with new bin edges
		TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
		hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());

		hnew->Sumw2();
		// cout<<
		// fill new histo with old values
		for( int i=1; i<=h->GetNbinsX(); i++){
			for( int j=1; j<=h->GetNbinsY(); j++){
				//    for(int a=1;a<=h->GetBinContent(i,j);a++)
				hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j), h->GetBinContent(i,j));
				//hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
				//    if(i==200)
				//	      cout<<h->GetXaxis()->GetBinCenter(i)<<"\t"<<h->GetYaxis()->GetBinCenter(j)<<"\t"<<h->GetBinContent(i,j)<<"\t"<<h->GetBinError(i,j)<<endl;
				// hnew->
				//if(i<=h->GetNbinsX()/2&&i>=h->GetNbinsX()/2-1)
			}
		}
		//cout<<"h"<<"\t"<<h->GetEntries()<<endl;
		//cout<<"h"<<"\t"<<h->GetBinContent(200,250)<<endl;
		//or(j =1; j<=hnew->GetNbinsY();j++)
		//	if( hnew-> GetBinContent(38,j)!=0)
		//cout<<"h_new"<<"\t"<<hnew->GetXaxis()->GetBinCenter(38)<<"\t"<<""<<endl;
		//cout<<"h_new"<<"\t"<<hnew->GetEntries()<<endl;
		//
		//double stats[7];
		//hnew->GetStats(stats);
		//for(i=0;i<7;i++)
		//	cout<<"star"<<stats[7]<<endl;
		TH2F* h_return = (TH2F*)hnew->Clone("");
		delete hnew;

		// return histo
		return h_return;
	}




	//__________________________________________
	//
	// Put info for fit plots
	// g=gain sl=slot index
	//__________________________________________
	void drawFitInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
	  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

	  return;

	}


	//____________________________________________
	//
	// Put info for run by run plots
	// g=gain, rn = run number index
	//____________________________________________
	void drawRunInfo(int g, int rn){
	  
	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

	  return;

	}

	//_________________________________________
	//
	// Put info for slot by slot plots
	// g=gain index, sl=slot index
	//_________________________________________
	void drawSlInfo(int g, int sl){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
	  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
	  
	  return;

	}
	//_________________________________________
	//
	// Put info for subdetector summary plots
	// g=gain index, d=subdetector index
	//_________________________________________
	void drawSubDetInfo(int g, int d){

	  ATLASLabel(0.2,0.88,pInternal);
	  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
	  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
	  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
	  
	  return;

	}

	//_________________________________________
	//
	// Put info for subdetector summary plots
	// ent=entries, m=mean, r=rms
	//_________________________________________
	void drawStats(int ent, double m, double rms){

	  myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
	  myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
	  myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
	 
	  return;

	}
void drawChannelInfo(int g, int ch){
  
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.58,0.88,kBlack,Form("Channel %d", channelList[ch]),1.0);
  myText(0.57,0.82,kBlack,Form("Online Id %s", onlineid[ch].c_str()),1.0);  
  return;

}

	//_________________________________________
	//
	// Put info for 3 fits on plot
	// p0, p4, p0
	//_________________________________________
	void drawStatBox(double par[7],std::string var){
	  myText(0.55,0.89,kGreen-3,Form("%s < -0.5",var.c_str()),0.6);
	  myText(0.55,0.86,kGreen-3,Form("p0: %.3f",par[0]),0.6);
	  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
	  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
	  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
	  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
	  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
	  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
	  myText(0.83,0.89,kGreen-3,Form("0.5 < %s",var.c_str()),0.6);
	  myText(0.83,0.86,kGreen-3,Form("p0: %.3f",par[6]),0.6); 
	  return;
	}
