//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________

#ifndef __RUNPASSCALC_H_INCLUDED__
#define __RUNPASSCALC_H_INCLUDED__

// standard includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
// ATLAS style includes
#include "../utils/AtlasLabels.C"
#include "../utils/AtlasStyle.C"
// Root includes
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "TRandom.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveStats.h"
#include "TColor.h"
#include "TStyle.h"

//______________
// Functions

// in pass_help.cxx
void pass0();
void pass1();
void pass2();
void pass3();
void pass4();
void pass5();
void pass6();
void pass7();
void pass8();
void pass9();
void pass10();
// in low_help.cxx
void low_offset();
void low_zres();
void low_pass4();
void lowFEBTimePlot();
void lowCellTimePlot();
void lowAngularPlot();
void lowEFracPlot();
void lowETimePlot();
// in plot_help.cxx
void doFEBTimePlot();
void doCellTimePlot();
void doAngularPlot();
void doEFracPlot();
void doETimePlot();
void doChannelPlot();
// in runPassCalc.cxx
void setPass(int pn, int mode);
void setRunList();
void setTRange(double t_l, double t_h);
// in draw_help.cxx
void drawChannelInfo(int gain, int ch);
void drawFitInfo(int gain, int sl);

void drawRunInfo(int gain, int rn);
void drawSlInfo(int gain, int sl);
void drawSubDetInfo(int gain, int detec);
void drawStats(int entries, double mean, double rms);
void drawStats_std(int entries, double mean, double rms, double meanerror);
void drawStatBox(double par[7], std::string var);
// in fit_help.cxx
TH2F* rebin(TH2F* h, int entPerBin, int firstBin);
TH1F* fitGaus(TH1F* h_cell, double mean, double rms);
TH1D* fitGaus_TH1D(TH1D* h_cell, double mean, double rms);
// in z_help.cxx
void plotZeet1t2( TH2F* h_2D, TH1F** h_1D );
void plotZeeResolution( TH2F* h_en_t_sl_a[2][11], TH2F* h_en_t_sl_c[2][11]);
void makeResolutionPlot(TH2F *h_hi, TH2F *h_med, std::string sname, std::string lname, std::string dname);
void makeResolutionPlot(TH2F *h_lo, std::string sname, std::string lname, std::string dname);

const int  NRUNS   = 150;
//________________
// Configuration
// TODO Need to set number of runs for your IOV
// 47 for IOV1, 17 for IOV2
const int  Nch = 1;
const bool saveEPS = true; 
const bool debug   = false;
bool   doPlot      = true;
bool   doCorr      = true;
double tmin        = -5.;
double tmax        = 5.;

// Change the default passNumber here
std::string sPassNumber = "pass0";
int passNumber = 0;


//______________
// Arrays

// for calculating correcitons at each pass
double ftTime_0[3][NRUNS][114];  // [3] gain [47] runs [114] ft for pass 0
double febTime_1[3][620];     // [3] gain [620] febs for pass 1
double chTime[3][79360];      // [3] gain [79360] channels for pass 2
double enFit[3][22][7];       // [3] gain [22] slots [6] energy fit params
double dphiFit[3][22][7];     // [3] gain [22] slots [7] p0,p4,p0 dphi fit params
double detaFit[3][22][7];     // [3] gain [22] slots [7] p0,p4,p0 deta fit params
double f1Fit[3][22][2];       // [3] gain [22] slots [2] df1 fit params
double f3Fit[3][22][2];       // [3] gain [22] slots [2] df3 fit params


//_____________________
// Strings for plotting

char Intern[10]                 = "Internal";
//char Intern[15]                 = "Preliminary";
char * pInternal                = Intern;
const std::string gain[3]       = {"High","Medium","Low"};
const std::string subDet[5]     = {"EMBA","EMBC","EMECA","EMECC","All"};
const std::string slotNames[22] = {"EMBA Slot 11","EMBA Slot 12","EMBA Slot 13","EMBA Slot 14",
                                   "EMBC Slot 11","EMBC Slot 12","EMBC Slot 13","EMBC Slot 14",
                                   "EMBA Slot 10","EMBC Slot 10","EMECA Slot 10","EMECA Slot 11",
                                   "EMECA Slot 12","EMECA Slot 13","EMECA Slot 14","EMECA Slot 15",
                                   "EMECC Slot 10","EMECC Slot 11","EMECC Slot 12","EMECC Slot 13",
                                   "EMECC Slot 14","EMECC Slot 15"};
// To retreive the correct plots when combining A/C for same slot
const int slotIndexASide[11]         = {0, 1, 2, 3, 8, 10, 11, 12, 13, 14, 15};
const int slotIndexCSide[11]         = {4, 5, 6, 7, 9, 16, 17, 18, 19, 20, 21};
const std::string slotIndexNames[11] = {"EMB Slot 11",  "EMB Slot 12",  "EMB Slot 13",
                                        "EMB Slot 14",  "EMB Slot 10",  "EMEC Slot 10",
                                        "EMEC Slot 11", "EMEC Slot 12", "EMEC Slot 13",
                                        "EMEC Slot 14", "EMEC Slot 15"};

int channel[Nch];
int onlineid[Nch];
int runNumberList[NRUNS];

//________________
// Histograms
//!!!
TH1D *h_channel_t[2][Nch];
TH2F *h_channel_run[2][Nch];
//!!!
TH1F *h_cell_t[3][5];  //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_feb_run[3][5]; //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_phi_t[3][5];   //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_dphi_t[3][5];  //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_eta_t[3][5];   //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_deta_t[3][5];  //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_f1_t[3][5];    //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_f3_t[3][5];    //[3]gains, [5] EMBA/C, EMECA/C, all
TH2F *h_e_t[3][5];     //[3]gains, [5] EMBA/C, EMECA/C, all
TH1D *h_feb_t[2];
TH2F *h_feb_single_run[2];

#endif // __RUNPASSCALC_H_INCLUDED__
