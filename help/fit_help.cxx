//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"


//_____________________________________________
//
// Make a gaussian fit for sub range,
// Show dotted line for full range,
// put fit params
//_____________________________________________
TH1D* fitGaus_TH1D(TH1D* h_cell, double mean, double rms){
  
  // Hide fit stats
  gStyle->SetOptFit(0);
 
  // Gaus fit (limited range around peak)
  TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
  //fg->SetParameter(0,0); // not sure if this is necessary
  
  // guess starting point for fit
  fg->SetParameter(1,mean);
  fg->SetParameter(2,rms);
  
  // put limits on the paramters
  fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
  fg->SetParLimits(2,0.5*rms,2*rms);
  
  // Draw the fit in subrange
  fg->SetLineColor(kRed);
  h_cell->Fit("fg","BR");
  fg->Draw("same");
  
  // Make the fit extend and draw as dotted line
  TF1 *f2 = new TF1("f2","gaus",-25,25);
  f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
  f2->SetLineColor(kRed);
  f2->SetLineStyle(7); //dashed line
  f2->Draw("same");
    
  // Put parameters on the plot
  double pos_x = 0.73;
  //FIXME y value was .74, .71
    myText(pos_x, 0.69,kBlack,Form("#mu: %.3f ns",fg->GetParameter(1)),0.6); // FIXME .6
    myText(pos_x, 0.65,kBlack,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.6);

  // Print out values if you want
  //std::cout << " >>    Mu: " << fg->GetParameter(1) << " +/- " << fg->GetParError(1) << std::endl
  //          << " >> Sigma: " << fg->GetParameter(2) << " +/- " << fg->GetParError(2) << std::endl;

  return h_cell;

}
TH1F* fitGaus(TH1F* h_cell, double mean, double rms){
  
  // Hide fit stats
  gStyle->SetOptFit(0);
 
  // Gaus fit (limited range around peak)
  TF1 *fg = new TF1("fg","gaus",mean-2*rms, mean+2*rms);
  //fg->SetParameter(0,0); // not sure if this is necessary
  
  // guess starting point for fit
  fg->SetParameter(1,mean);
  fg->SetParameter(2,rms);
  
  // put limits on the paramters
  fg->SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
  fg->SetParLimits(2,0.5*rms,2*rms);
  
  // Draw the fit in subrange
  fg->SetLineColor(kRed);
  //
  h_cell->Fit("fg","BR");
 ///CJ///
 //change to narrower range in Xasix 
  h_cell->GetXaxis()->SetRangeUser(-25,25);
  fg->Draw("same");
  
  // Make the fit extend and draw as dotted line
  TF1 *f2 = new TF1("f2","gaus",-15,15);
  f2->SetParameters(fg->GetParameter(0), fg->GetParameter(1), fg->GetParameter(2));
  f2->SetLineColor(kRed);
  f2->SetLineStyle(7); //dashed line
  f2->Draw("same");
    
  // Put parameters on the plot
  double pos_x = 0.73;
  //FIXME y value was .74, .71
  myText(pos_x, 0.72,kBlack,Form("#mu: %.3f ns",fg->GetParameter(1)),0.8); // FIXME .6
  myText(pos_x, 0.68,kBlack,Form("#sigma: %.3f ns",fg->GetParameter(2)),0.8);

  // Print out values if you want
  //std::cout << " >>    Mu: " << fg->GetParameter(1) << " +/- " << fg->GetParError(1) << std::endl
  //          << " >> Sigma: " << fg->GetParameter(2) << " +/- " << fg->GetParError(2) << std::endl;

  return h_cell;

}


//_____________________________________________
//
// Rebin the 2D histogram so that each bin
// has at least entPerBin entries
// firstBin is the first bin you want to consider
//_____________________________________________
TH2F* rebin(TH2F *h, int entPerBin, int firstBin) {

  // hold the bin edge information
  std::vector< double > xbins;
  // Combine all bins below first bin
  // Cut at 5 GeV so this bin is empty
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); // for energy this is 6

  // keep track of last bin with at least entPerBin
  int lastFullIndex = firstBin; // for energy 5

  // Get the xaxis
  TAxis *axis = h->GetXaxis();

  // Loop over bins in xaxis
  // start after 5GeV bin
  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {

    // Get entries in this bin, and width
    int y = h->Integral(i,i);
    double w = axis->GetBinWidth(i);

    // If not enough entries, need to combine bins
    if (y <= entPerBin){
      // Find integral from last combined bin
      double integral = h->Integral(lastFullIndex+1, i);
      if (integral <= entPerBin ) continue;
      // if above threshold, mark as new bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w);
    }
    else{
      // above threshold, mark as bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w );
    }

  }

  // put bin edges into an array
  xbins.push_back( axis->GetXmax() );
  size_t s = xbins.size();
  double *xbinsFinal = &xbins[0];

  // create new histo with new bin edges
  TH2F* hnew = new TH2F(Form("hnew_%s",h->GetTitle()),h->GetTitle(),s-1, xbinsFinal, h->GetNbinsY(), -5, 5);
  hnew->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle());

  // fill new histo with old values
  for( int i=1; i<=h->GetNbinsX(); i++){
    for( int j=1; j<=h->GetNbinsY(); j++){
        for(int k=1;k<=h->GetBinContent(i,j);k++)
    hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j));
//      hnew->Fill(h->GetXaxis()->GetBinCenter(i), h->GetYaxis()->GetBinCenter(j),h->GetBinContent(i,j));
    }
  }
  
  TH2F* h_return = (TH2F*)hnew->Clone("");
  delete hnew;

  // return histo
  return h_return;
}

