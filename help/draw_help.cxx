//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"


//__________________________________________
//
// Put info for fit plots
// g=gain sl=slot index
//__________________________________________
void drawFitInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s",slotNames[sl].c_str()),0.7);
  myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.75,kBlack,Form("Pass %d",passNumber),0.6);

  return;

}


//____________________________________________
//
// Put info for run by run plots
// g=gain, rn = run number index
//____________________________________________
void drawRunInfo(int g, int rn){
  
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("Run %d",runNumberList[rn]),0.6);

  return;

}
//_________________________________________
//
// Put info for slot by slot plots
// g=gain index, sl=slot index
//_________________________________________
void drawChannelInfo(int g, int ch){
  
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str() ),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.58,0.88,kBlack,Form("Channel %d", channel[ch]),1.0);
  myText(0.57,0.82,kBlack,Form("Online Id %d", onlineid[ch]),1.0);  
  return;

}

//_________________________________________
//
// Put info for slot by slot plots
// g=gain index, sl=slot index
//_________________________________________
void drawSlInfo(int g, int sl){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
  myText(0.73,0.88,kBlack,Form("%s", slotNames[sl].c_str()), 0.7);
  
  return;

}
//_________________________________________
//
// Put info for subdetector summary plots
// g=gain index, d=subdetector index
//_________________________________________
void drawSubDetInfo(int g, int d){

  ATLASLabel(0.2,0.88,pInternal);
  myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
  myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
  myText(0.73,0.88,kBlack,Form("%s",subDet[d].c_str()),0.7);
  
  return;

}

//_________________________________________
//
// Put info for subdetector summary plots
// ent=entries, m=mean, r=rms
//_________________________________________
void drawStats(int ent, double m, double rms){
//  myText(0.73,0.77,kBlack,Form("Entries: %d",ent),0.6);
//  myText(0.73,0.71,kBlack,Form("Mean: %.3f ns",m),0.6);
//  myText(0.73,0.67,kBlack,Form("Rms: %.3f ns",rms),0.6);
   myText(0.73,0.83,kBlack,Form("Entries: %d",ent),0.6);
   myText(0.73,0.80,kBlack,Form("Mean: %.3f ns",m),0.6);
   myText(0.73,0.77,kBlack,Form("Rms: %.3f ns",rms),0.6);
  return;

}

void drawStats_std(int ent, double m, double rms, double meanerror){

  myText(0.73,0.77,kBlack,Form("Entries: %d",ent),0.6);
//  myText(0.73,0.71,kBlack,Form("Mean: %.3f ns",m),0.6);
  myText(0.73,0.73,kBlack,Form("Rms: %.3f ns",rms),0.6);
//  myText(0.73,0.63,kBlack,Form("Sigma: %.3f ns",meanerror),0.6);  
  return;

}
//_________________________________________
//
// Put info for 3 fits on plot
// p0, p4, p0
//_________________________________________
void drawStatBox(double par[7],std::string var){
  myText(0.55,0.89,kBlue,Form("%s < -0.5",var.c_str()),0.6);
  myText(0.55,0.86,kBlue,Form("p0: %.3f",par[0]),0.6);
  myText(0.68,0.89,kRed,Form("%s#in[-0.5,0.5]",var.c_str()),0.6);
  myText(0.68,0.86,kRed,Form("p0: %.3f",par[1]),0.6);
  myText(0.68,0.83,kRed,Form("p1: %.3f",par[2]),0.6);
  myText(0.68,0.80,kRed,Form("p2: %.3f",par[3]),0.6);
  myText(0.68,0.77,kRed,Form("p3: %.3f",par[4]),0.6);
  myText(0.68,0.74,kRed,Form("p4: %.3f",par[5]),0.6);
  myText(0.83,0.89,kBlue,Form("0.5 < %s",var.c_str()),0.6);
  myText(0.83,0.86,kBlue,Form("p0: %.3f",par[6]),0.6); 
  return;
}
