//_________________________________
//
// Author : Ryne Carbone
// Date   : April 2016
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"


//
// Returns canvas with hi/med timem resolution on it
//______________________________________________________
void makeResolutionPlot(TH2F *h_hi, TH2F *h_med, std::string sname, std::string lname, std::string dname){

  // Make canvas to return
  TCanvas c1( Form("c_%s",sname.data()), Form("%s",lname.data()), 800, 700);
  
  //FIXME find first bin to cut on!
  
  // rebin histogram
  h_hi  = rebin( h_hi,  h_hi->GetEntries()/20, 2); // roughly ~1/20 entries per bin, 5GeV cut so first 2 bins empty
  h_med = rebin( h_med, h_med->GetEntries()/10, 2);

  // Find fit range
  double enLow[2];
  double enHigh[2];
  double lowBin[2];
  double highBin[2];
  lowBin[0]  = h_hi->GetXaxis()->FindBin(6.);
  enLow[0]   = h_hi->GetXaxis()->GetBinCenter( lowBin[0] );
  highBin[0] = h_hi->GetXaxis()->FindBin(249.) - 1;
  enHigh[0]  = h_hi->GetXaxis()->GetBinCenter( highBin[0] );
  lowBin[1]  = h_med->GetXaxis()->FindBin(6.) + 1;
  enLow[1]   = h_med->GetXaxis()->GetBinCenter( lowBin[1] );
  highBin[1] = h_med->GetXaxis()->FindBin(249.);
  enHigh[1]  = h_med->GetXaxis()->GetBinCenter( highBin[1] );

  // Find resolution per energy bin
  TObjArray aSlices;
  TObjArray bSlices;
  h_hi->FitSlicesY( 0, lowBin[0], highBin[0], 2, "QNR", &aSlices); // 0: gaus
  h_med->FitSlicesY(0, lowBin[1], highBin[1], 2, "QNR", &bSlices); 

  // Make resolution histogram (copy binning structure)
  TH1F h_res[2];
  h_res[0]  = TH1F(Form("1d_h_%s",sname.data()),
                   Form("1d_h_%s",lname.data()),
                   h_hi->GetNbinsX(),
                   h_hi->GetXaxis()->GetXbins()->GetArray() );
  h_res[1]  = TH1F(Form("1d_m_%s",sname.data()),
                   Form("1d_m_%s",lname.data()),
                   h_med->GetNbinsX(),
                   h_med->GetXaxis()->GetXbins()->GetArray() );
  h_res[0] = (TH1F&)*(aSlices[2]); // [2] is sigma of gaussian fit
  h_res[1] = (TH1F&)*(bSlices[2]);

  // Fit the resolution plot to assumed form
  TF1 f1("f1","sqrt( sq([0]/x)+sq([1]))", enLow[0], enHigh[0]);
  TF1 f2("f2","sqrt( sq([0]/x)+sq([1]))", enLow[1], enHigh[1]);
  f1.SetLineColor(kRed);
  f2.SetLineColor(kRed);
  h_res[0].Fit("f1","QBR");
  h_res[1].Fit("f2","QBR+");

  // Make plot
  h_res[0].SetTitle(lname.data());
  h_res[0].Draw();
  h_res[1].SetLineColor(kBlue);
  h_res[1].SetMarkerColor(kBlue);
  h_res[1].Draw("same");
  h_res[0].GetXaxis()->SetTitle("Energy [GeV]");
  h_res[0].GetYaxis()->SetTitle("Time Resolution [ns]");
  h_res[0].GetXaxis()->SetRangeUser(4,250.);
  h_res[0].GetYaxis()->SetRangeUser(0.0,0.6);
  c1.SetLogx(1);

  // Put info on plot
  //myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.82, kBlack, Form("%s",dname.data()), .7);
  myText(0.40, 0.74, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"), 0.8);
  myText(0.73, 0.78, kBlack, Form("Fit Results:"), .7 );
  myText(0.67, 0.74, kBlack, Form("High"), 0.7);
  myText(0.73, 0.74, kRed,   Form("p_{0}: %.3f", f1.GetParameter(0)), .7 );
  myText(0.73, 0.70, kRed,   Form("p_{1}: %.3f", f1.GetParameter(1)), .7 );
  myText(0.63, 0.66, kBlue,  Form("Medium"), 0.7);
  myText(0.73, 0.66, kRed,   Form("p_{0}: %.3f", f2.GetParameter(0)), .7 );
  myText(0.73, 0.62, kRed,   Form("p_{1}: %.3f", f2.GetParameter(1)), .7 );

  c1.SaveAs(Form("plots/res/Res_hi_med%s.png",dname.c_str()));

}


//
// Returns canvas with low timem resolution on it
//______________________________________________________
void makeResolutionPlot(TH2F *h_lo, std::string sname, std::string lname, std::string dname){

  // Make canvas to return
  TCanvas c1( Form("c_%s",sname.data()), Form("%s",lname.data()), 800, 700);
  
  // rebin histogram
  h_lo  = rebin( h_lo,  h_lo->GetEntries()/10, 5); // roughly ~1/20 entries per bin, 5GeV cut so first 5 bins empty

  // Find fit range
  double enLow;
  double enHigh;
  double lowBin;
  double highBin;
  lowBin  = h_lo->GetXaxis()->FindBin(6.) + 1;
  enLow   = h_lo->GetXaxis()->GetBinCenter( lowBin );
  highBin = h_lo->GetXaxis()->FindBin(499.) ;
  enHigh  = h_lo->GetXaxis()->GetBinCenter( highBin );
  
  // Find resolution per energy bin
  TObjArray aSlices;
  h_lo->FitSlicesY( 0, lowBin, highBin, 2, "QNR", &aSlices); // 0: gaus

  // Make resolution histogram (copy binning structure)
  TH1F h_res;
  h_res = TH1F(Form("1d_h_%s",sname.data()),
               Form("1d_h_%s",lname.data()),
               h_lo->GetNbinsX(),
               h_lo->GetXaxis()->GetXbins()->GetArray() );
  h_res = (TH1F&)*(aSlices[2]); // [2] is sigma of gaussian fit

  // Fit the resolution plot to assumed form
  TF1 f1("f1","sqrt( sq([0]/x)+sq([1]))", enLow, enHigh);
  f1.SetLineColor(kRed);
  h_res.Fit("f1","QBR");

  // Make plot
  h_res.SetTitle(lname.data());
  h_res.Draw();
  h_res.GetXaxis()->SetTitle("Energy [GeV]");
  h_res.GetYaxis()->SetTitle("Time Resolution [ns]");
  h_res.GetXaxis()->SetRangeUser(5.,500.);
  //h_res.GetYaxis()->SetRangeUser(0.0,0.6);
  //c1.SetLogx(1);

  // Put info on plot
  //myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  ATLASLabel(0.2,0.88,pInternal);
  myText(0.20, 0.82, kBlack, Form("%s",dname.data()), .7);
  myText(0.40, 0.74, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"), 0.8);
  myText(0.73, 0.78, kBlack, Form("Fit Results:"), .7 );
  myText(0.67, 0.74, kBlack, Form("Low"), 0.7);
  myText(0.73, 0.74, kRed,   Form("p_{0}: %.3f", f1.GetParameter(0)), .7 );
  myText(0.73, 0.70, kRed,   Form("p_{1}: %.3f", f1.GetParameter(1)), .7 );

  std::cout << " > p0: " << f1.GetParameter(0) << " +/- " << f1.GetParError(0) 
            << ";  p1: " << f1.GetParameter(1) << " +/- " << f1.GetParError(1) << std::endl;

  c1.SaveAs( Form("plots/res/Res_lo_%s.png",dname.c_str()) );
  c1.SaveAs( Form("plots/res/Res_lo_%s.eps",dname.c_str()) );

}
