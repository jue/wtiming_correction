//_________________________________
//
// Author : Ryne Carbone
// Date   : March 2016
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"

// TODO FIXME
// This is a work in progress, in 2015 we attempted
// to do Low gain analysis but weren't able to go through
// all 7 passes. If in the future there is enough Low gain stats,
// try to integrate low gain into the normal code, instead of this
// separate code

//
// Make res plots for low gain, zee
//_________________________________
void low_zres(){
  
  std::cout<< "Making Resolution plot for Low Gain Pass 5\n" <<std::endl;

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open("files/Low_iov1/pass5.root","READ");
  file2 = TFile::Open("files/Low_iov2/pass5.root","READ");
  std::cout<< "Opened root file pass5.root successfully\n\n";

  std::string det[3] = { "EMB", "EMEC", "All" }; 

  TH2F* h_e_t[3]; // summary for emb, emec, all
  
  for( int d=0; d<3; d++){
    h_e_t[d] = new TH2F( Form("h_low_res_%s",det[d].c_str()), Form("h_low_res_%s",det[d].c_str()), 500, 0., 500., 500, -5., 5.);   
  }

  // open all histograms and add them to summary
  for( int g=2; g<3; g++){
    for( int in = 0; in < 11; in++){
      std::string nameA = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexASide[in]);
      std::string nameC = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexCSide[in]);
      TH2F* tempA  = (TH2F*)file1->Get(nameA.c_str());
      TH2F* tempC  = (TH2F*)file1->Get(nameC.c_str());
      TH2F* temp2A = (TH2F*)file2->Get(nameA.c_str());
      TH2F* temp2C = (TH2F*)file2->Get(nameC.c_str());
      tempA->Add(tempC);
      temp2A->Add(temp2C);
      tempA->Add(temp2A);
      h_e_t[2]->Add(tempA);
      if( in < 5 ) 
        h_e_t[0]->Add(tempA);
      else 
        h_e_t[1]->Add(tempA);
    }
  }

  for( int d=0; d<3; d++){
    std::cout << det[d] << "\n  > Entries: " << h_e_t[d]->GetEntries() << std::endl; 
    makeResolutionPlot( h_e_t[d], Form("res_low_%s",det[d].c_str()), Form("Low Gain Time Resolution %s", det[d].c_str()), det[d].c_str());
  }

  file1->Close();
  file2->Close();

}

//_____________________________________________
//
// Pass4: find the average low gain time correction
//        per deta/dphi by gain
//________________________________________________
void low_pass4(){

  std::cout<< "Finding Corrections for Low Gain Pass 4\n" <<std::endl;

  // Store fit range
  double pminX[11];
  double pmaxX[11];
  double eminX[11];
  double emaxX[11];
  double dphiFit[11][7];
  double detaFit[11][7];

  // Initialize the dphi/deta corrections
  for( int s = 0; s < 11; s++){
    for( int p = 0; p < 7; p++){
      dphiFit[s][p] = 0;
      detaFit[s][p] = 0;
    }
    pminX[s] = -99;
    pmaxX[s] = -99;
    eminX[s] = -99;
    emaxX[s] = -99;
  }
  

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open("files/Low_iov1/pass4.root","READ");
  file2 = TFile::Open("files/Low_iov2/pass4.root","READ");
  std::cout<< "Opened root file pass4.root successfully\n\n";

  // Open the corrections file
  ofstream f4("config/AvgdPhidEtaSlFitL.dat");
  
  // Loop over gains
  for( int g=2; g<3; g++){
    
    // Loop over slots
    for( int sl = 0; sl < 11; sl++){

      // Make names for the histograms to open and profile plots
      std::string pname   = Form("Prof Avg dPhi Time %s Slot%d",gain[g].c_str(),slotIndexASide[sl] );
      std::string ename   = Form("Prof Avg dEta Time %s Slot%d",gain[g].c_str(),slotIndexASide[sl] );
      std::string hpname  = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),slotIndexASide[sl] );
      std::string hpnamec = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),slotIndexCSide[sl] );
      std::string hename  = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),slotIndexASide[sl] );
      std::string henamec = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),slotIndexCSide[sl] );

      // Open the histogram, continue if empty
      TH2F *f_dp  = (TH2F*)file1->Get(hpname.c_str());
      TH2F *f_dpc = (TH2F*)file1->Get(hpnamec.c_str());
      TH2F *f_de  = (TH2F*)file1->Get(hename.c_str());
      TH2F *f_dec = (TH2F*)file1->Get(henamec.c_str());
      // iov 2
      TH2F *f_dp2  = (TH2F*)file2->Get(hpname.c_str());
      TH2F *f_dp2c = (TH2F*)file2->Get(hpnamec.c_str());
      TH2F *f_de2  = (TH2F*)file2->Get(hename.c_str());
      TH2F *f_de2c = (TH2F*)file2->Get(henamec.c_str());
      
      f_dp->Add(f_dpc);
      f_dp->Add(f_dp2);
      f_dp->Add(f_dp2c);
      f_de->Add(f_dec);
      f_de->Add(f_de2);
      f_de->Add(f_de2c);

      if( !f_dp->GetEntries() ) {
        std::cout << "  >> Histogram has no data for gain " << gain[g] << " slot " << sl
          << ", corrections will be zero!\n";
        continue;
      }
      // Make a profile plot to find avg time for each slot
      TProfile *tpp  = f_dp->ProfileX(pname.c_str(),-1,-1,"o");
      TProfile *tpe  = f_de->ProfileX(ename.c_str(),-1,-1,"o");
      tpp->GetYaxis()->SetRangeUser(tmin,tmax);
      tpe->GetYaxis()->SetRangeUser(tmin,tmax);
      tpp->GetYaxis()->SetTitle("Time [ns]");
      tpe->GetYaxis()->SetTitle("Time [ns]");

      // low gain rebin:
      if( sl == 0 ){
        tpp->Rebin(10);
        tpe->Rebin(5);
      }else if( sl == 5 || sl == 6 || sl == 10){
        tpp->Rebin(16);
        tpe->Rebin(5);
      }
      else if( sl == 1){
        tpp->Rebin(16);
        tpe->Rebin(10);
      }
      else{
        tpp->Rebin(8);
        tpe->Rebin(4);
      }
      
      // Find the Max in x direction, and set dPhi fit range
      for (int b=1; b<= tpp->GetXaxis()->GetNbins(); b++){ // bins in dphi
        if( tpp->GetBinEntries(b) > 0 ){
          if( pminX[sl] == -99 ) pminX[sl] = tpp->GetBinCenter(b);
          pmaxX[sl] = tpp->GetBinCenter(b);
        }
      }
      // Find the Max in x direction, and set dEta fit range
      for (int b=1; b<= tpe->GetXaxis()->GetNbins(); b++){ // bins in deta
        if( tpe->GetBinEntries(b) > 0 ){
          if( eminX[sl] == -99 ) eminX[sl] = tpe->GetBinCenter(b);
          emaxX[sl] = tpe->GetBinCenter(b);
        }
      }
      if( debug ){
        std::cout << "Gain: " << g << "; Slot: " << sl << std::endl;
        std::cout << "  pminX: " << pminX[sl] << ";  pmaxX: " << pmaxX[sl] <<std::endl;
        std::cout << "  eminX: " << eminX[sl] << ";  emaxX: " << emaxX[sl] <<std::endl;
      }
      // 3 function fit!
      TF1 *fp_low = new TF1("fp_low", "pol0", pminX[sl], -0.5);
      TF1 *fe_low = new TF1("fe_low", "pol0", eminX[sl], -0.5);
      TF1 *fp_mid = new TF1("fp_mid", "pol4", -0.5, 0.5);
      TF1 *fe_mid = new TF1("fe_mid", "pol4", -0.5, 0.5);
      TF1 *fp_hi  = new TF1("fp_hi", "pol0", 0.5, pmaxX[sl]);
      TF1 *fe_hi  = new TF1("fe_hi", "pol0", 0.5, emaxX[sl]);
      // store paramters for fits
      double par_p[7];
      double par_e[7];
      // apply the fit
      tpp->Fit("fp_low","QR");
      tpe->Fit("fe_low","QR");
      int fit_rp = tpp->Fit("fp_mid","QR+");
      int fit_re = tpe->Fit("fe_mid","QR+");
      tpp->Fit("fp_hi","QR+");
      tpe->Fit("fe_hi","QR+");
      // retrieve fit params
      fp_low->GetParameters(&par_p[0]);
      fe_low->GetParameters(&par_e[0]);
      fp_mid->GetParameters(&par_p[1]);
      fe_mid->GetParameters(&par_e[1]);
      fp_hi->GetParameters(&par_p[6]);
      fe_hi->GetParameters(&par_e[6]);
      // set fit colors
      fp_low->SetLineColor(kBlue);
      fe_low->SetLineColor(kBlue);
      fp_mid->SetLineColor(kRed);
      fe_mid->SetLineColor(kRed);
      fp_hi->SetLineColor(kBlue);
      fe_hi->SetLineColor(kBlue);

      // If fit unsuccessful
      if( fit_rp != 0 )
        std::cout << gain[g].c_str() << " gain " << slotIndexNames[sl].c_str() << std::endl
          << "  dphi fit unsuccessful, result status: " << fit_rp << std::endl;
      if( fit_re != 0 )
        std::cout << gain[g].c_str() << " gain " << slotIndexNames[sl].c_str() << std::endl
          << "  deta fit unsuccessful, result status: " << fit_re << std::endl;

      // Store Fit Params
      for(int p=0; p<7; p++){
        dphiFit[sl][p] = par_p[p];
        detaFit[sl][p] = par_e[p];
      }

      // Make the dPhi plot
      TCanvas *cp = new TCanvas(Form("cdphi%d%d",g,sl),Form("cdphi%d%d",g,sl), 800, 700);
      tpp->Draw();
      fp_low->Draw("same");
      fp_mid->Draw("same");
      fp_hi->Draw("same");
      drawStatBox( par_p , "#delta#phi");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s",slotIndexNames[sl].c_str()),0.7);
      myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.75,kBlack,Form("Pass %d",4),0.6);
      if(saveEPS) cp->SaveAs(Form("plots/pass4/angular/Sl%dcelldPhi_%s.eps",sl,gain[g].c_str()));
      cp->SaveAs(Form("plots/pass4/angular/Sl%dcelldPhi_%s.png",sl,gain[g].c_str()));
      delete cp;

      // Make the dEta plot
      TCanvas *ce = new TCanvas(Form("cdeta%d%d",g,sl),Form("cdeta%d%d",g,sl), 800, 700);
      tpe->Draw();
      fe_low->Draw("same");
      fe_mid->Draw("same");
      fe_hi->Draw("same");
      drawStatBox( par_e , "#delta#eta");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s",slotIndexNames[sl].c_str()),0.7);
      myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.75,kBlack,Form("Pass %d",4),0.6);
      if(saveEPS) ce->SaveAs(Form("plots/pass4/angular/Sl%dcelldEta_%s.eps",sl,gain[g].c_str()));
      ce->SaveAs(Form("plots/pass4/angular/Sl%dcelldEta_%s.png",sl,gain[g].c_str()));
      delete ce;

    }// end loop over slots
  }// end loop over gains

  // Store the avg times:
  // [g][sl][0] p0 const fit, [g][sl[1-5] p0-p4 fit, [g][sl][6] p0 const fit
  for(int sl = 0; sl < 11; sl++){
    f4 << sl << " " << dphiFit[sl][0]
             << " " << dphiFit[sl][6]
             << " " << detaFit[sl][0]
             << " " << detaFit[sl][6];
    for(int p = 0; p < 10; p++){
      if ( p<5){
        f4 << " " << dphiFit[sl][p+1];
      } else{
        f4 << " " << detaFit[sl][p+1-5];
      }
    }
    f4 << std::endl;
  }

  std::cout << "Stored Run Corrections to AvgdPhidEtaSlFit.dat successfully\n\n";

  f4.close();
  file1->Close();
  file2->Close();
  std::cout << "Files successfully closed\n\n";

  return;

}



//_____________________________________________
//
// (After pass3): find overall offset
//________________________________________________
void low_offset(){
  
  std::cout<< "Finding Average Offset for Low Gain\n" <<std::endl;

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open("files/Low_iov1/pass4.root","READ");
  file2 = TFile::Open("files/Low_iov2/pass4.root","READ");
  
  std::cout<< "Opened root file pass3.root for IOV1 & IOV2 successfully\n\n";

  TH1F *h_CellTime[3]; // [0] emb, [1] emec, [2] total
  std::string det[3] = {"EMB", "EMEC", "All"};

  for( int i=0; i<3; i++){
    h_CellTime[i] = new TH1F( Form("h_avgCellTime%d",i), 
                                 Form("Cell Time %s; Cell Time [ns];",det[i].c_str()), 
                                 1000, -25, 25);
  }

  // Open the corrections file
  ofstream f_out("config/AvgTimeL.dat");

  // Loop over slots
  for( int sl=0; sl<22; sl++){
  
    // Make names for the histograms to open and profile plots
    std::string hname = Form("h_cellTimeAllLow%d", sl);

    // Open the histogram, continue if empty
    TH1F *f_ct  = (TH1F*)file1->Get(hname.c_str());
    TH1F *f_ct2 = (TH1F*)file2->Get(hname.c_str());
    f_ct->Add(f_ct2);
    if( !f_ct->GetEntries() ) {
      std::cout << "  >> Histogram has no data for Low gain slot " 
                << sl << ", corrections will be zero!\n";
      continue;
    }

    // Fill summary plots
    h_CellTime[2]->Add(f_ct);
    if( sl <= 9 )
      h_CellTime[0]->Add(f_ct);
    else
      h_CellTime[1]->Add(f_ct);
    
  } // end loop over slots

  // Find avg time and make plot
  double corr[3];

  for ( int i=0; i<3; i++){
  
    std::cout << "Making plot for : " << det[i].c_str() << std::endl;
    TCanvas *c_ct = new TCanvas(Form("ct%d",i),Form("ct%d",i), 800, 700);

    int entries = h_CellTime[i]->GetEntries();
    double mean = h_CellTime[i]->GetMean();
    double rms  = h_CellTime[i]->GetRMS();
    
    // Rebin
    if( i == 0 )
      h_CellTime[i]->Rebin(8);
    else
      h_CellTime[i]->Rebin(4);

    // Plot
    h_CellTime[i]->Draw();
    h_CellTime[i] = fitGaus( h_CellTime[i], mean, rms);
    drawStats( entries, mean, rms);
   
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.2,0.83,kBlack,Form("Low Gain"),0.6);
    myText(0.2,0.79,kBlack,Form("Pass 3"),0.6);
    myText(0.73,0.88,kBlack,Form("%s",det[i].c_str()),0.7);

    corr[i] = mean;
   
    // Save plot
    c_ct->SaveAs(Form("plots/Low/Low_CellTime_%s.png",det[i].c_str()));
    // Make log plot
    c_ct->SetLogy(1);
    double maxY = h_CellTime[i]->GetBinContent( h_CellTime[i]->GetMaximumBin() );
    h_CellTime[i]->GetYaxis()->SetRangeUser(0.5, maxY/0.08);
    c_ct->SaveAs(Form("plots/Low/Log_Low_CellTime_%s.png",det[i].c_str()));
    
    delete c_ct;

  }

  // Store the avg times: // this is just the mean, mu from fit isn't saved...
  //for(int i = 0; i < 3; i++){
  //  f_out << det[i].c_str() << " " << corr[i] << std::endl;
  //}

  std::cout << "Stored Run Corrections to AvgTimeL.dat successfully\n\n";

  f_out.close();
  file1->Close();
  file2->Close();
  
  std::cout << "Files successfully closed\n\n";

  return;
}


//_____________________________________________
//
// Make plots for avg time by FEB
// and by Run Number per FEB for Low gain
//_____________________________________________
void lowFEBTimePlot(){

  std::cout << "\n\nMaking plots for average Low gain FEB Timing\n\n";
  
  // [0] emb, [1] emec, [2] all
  TH2F *h_feb_run[3];

  // Initialize the histograms
  for( int i=0; i<3; i++){
      h_feb_run[i] = new TH2F( Form("h_feb_run%d",i), Form("h_feb_run%d; Run Number; Avg FEB Time [ns]",i), NRUNS+17,0,NRUNS+17, 500,-5,5);
  }
  
  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open(Form("files/Low_iov1/%s.root",sPassNumber.c_str()),"READ");
  file2 = TFile::Open(Form("files/Low_iov2/%s.root",sPassNumber.c_str()),"READ");

  // Loop over gains (only low gain)
  for( int g=2; g<3; g++){

    // Loop over the runs
    for( int rn=0; rn<NRUNS+17; rn++){

      // Make names for the histograms to open and profile plots
      int ind = rn;
      if( rn  > 46 ) ind = rn-47;
      std::string pname = Form("Prof run %s%d",gain[g].c_str(),ind);
      std::string hname = Form("h_RunAvgFebTime_%s%d",gain[g].c_str(),ind);

      // Open the histogram, continue if empty
      TH2F *f_rn;
      if( rn < 47)
        f_rn = (TH2F*)file1->Get(hname.c_str());
      else 
        f_rn = (TH2F*)file2->Get(hname.c_str());

      if( !f_rn->GetEntries() ) {
        std::cout << "  >> Run index: " << rn << " has no data for "
                  << gain[g] << " gain, no data to be plotted!\n";
        continue;
      }

      // Make a profile plot to find avg time for each feb
      TProfile *tpf   = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
      tpf->GetYaxis()->SetRangeUser(tmin, tmax);
      tpf->GetYaxis()->SetTitle("Time [ns]");
      
      // Find nbins to loop over
      int nbins =tpf->GetXaxis()->GetNbins();
      // For each feb, put in right partition
      for(int j=0; j<nbins; j++){
        // bins start at 1, but feb numb start at 0
        double i_time = tpf->GetBinContent(j+1);
        if( i_time != 0){
          // rn is the run number index, j is the feb index, g is gain
          h_feb_run[2]->Fill(rn,i_time);
          if( j < 320)
            h_feb_run[0]->Fill(rn, i_time);
          else
            h_feb_run[1]->Fill(rn, i_time);
        }
      }

      // FEB by run num
      TCanvas *c1 = new TCanvas(Form("c%d%d",g,rn),Form("c%d%d",g,rn), 800, 700);
      tpf->Draw();

      // Put info on plot
      drawRunInfo(g, rn);

      // Save plots
      if( saveEPS )c1->SaveAs(Form("plots/%s/Run%dFebCorrections_%s.eps",sPassNumber.c_str(),rn,gain[g].c_str()));
      c1->SaveAs(Form("plots/%s/Run%dFebCorrections_%s.png",sPassNumber.c_str(),rn,gain[g].c_str()));
      delete c1;

      //Make 1D histo also showing FEB time
      TH1F *hh = new TH1F(Form("1D%d%d",g,rn),Form("1D%d%d",g,rn),500,-5.,5.);
      for(int k=1; k<=tpf->GetXaxis()->GetNbins(); k++){
        if( tpf->GetBinEntries(k)) // if entries for feb, fill histogram
          hh->Fill( tpf->GetBinContent(k) );
      }
      TCanvas *cc = new TCanvas(Form("cc%d%d",g,rn),Form("cc%d%d",g,rn),800,700);
      hh->Draw();
      //hh->Rebin(5);
      hh->GetXaxis()->SetTitle("FEB Time [ns]");
      hh->GetYaxis()->SetTitle("FEBs / 0.02 ns");
      double mean = hh->GetMean();
      double rms  = hh->GetRMS();
      int entries = hh->GetEntries();
      // Put info on plot
      drawRunInfo(g, rn);
      drawStats(entries, mean, rms);
      cc->SetLogy(1);
      // Save plots
      if( saveEPS )cc->SaveAs(Form("plots/%s/1DRun%dFebCorrections_%s.eps",sPassNumber.c_str(),rn,gain[g].c_str()));
      cc->SaveAs(Form("plots/%s/1DRun%dFebCorrections_%s.png",sPassNumber.c_str(),rn,gain[g].c_str()));
      cc->SetLogy(0);
      delete cc;

    }// end loop over runs
    
    std::string det[3] = {"EMB", "EMEC","All"};

    // Run by run plots for subdetector/slot
    for( int sd = 0; sd < 3; sd++){

      // Make profile plot
      TCanvas *c4 = new TCanvas(Form("c4%d",sd),Form("c4%d",sd), 800, 700);
      TProfile *tp  = h_feb_run[sd]->ProfileX(Form("Profile Run%d",sd),-1,-1,"o");

      // Write run number as bin label
      for( int bin=0; bin<NRUNS+17; bin++){
        int s_run =  runNumberList[bin] ;
        tp->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
      }
      tp->GetXaxis()->SetRangeUser(0,NRUNS+17);
      tp->GetXaxis()->LabelsOption("v");
      tp->GetXaxis()->SetLabelSize(0.03);
      tp->GetYaxis()->SetRangeUser(tmin, tmax);
      tp->GetYaxis()->SetTitle("Time [ns]");
      tp->Draw();

      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[sd].c_str()),0.7);

      // Save plots
      if( saveEPS )c4->SaveAs(Form("plots/%s/%s_runTime_%s.eps",sPassNumber.c_str(),det[sd].c_str(),gain[g].c_str()));
      c4->SaveAs(Form("plots/%s/%s_runTime_%s.png",sPassNumber.c_str(),det[sd].c_str(),gain[g].c_str()));
      delete c4;
    }

    // FEB time plots for all runs combined
    std::string pname = Form("Prof Avg Feb Time %s",gain[g].c_str() );
    std::string hname = Form("h_AvgFebTime_%s",gain[g].c_str() );

    // Open the histogram, continue if empty
    TH2F *f_rn  = (TH2F*)file1->Get(hname.c_str());
    TH2F *f_rn2 = (TH2F*)file2->Get(hname.c_str());
    if( !f_rn->GetEntries() ) {
      std::cout << "  >> Histogram has no data for gain "
                << gain[g] << ", no plots will be made!\n";
      continue;
    }
    // add both iovs
    f_rn->Add(f_rn2);

    // Make a profile plot to find avg time for each feb
    TProfile *tf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
    tf->GetYaxis()->SetRangeUser(tmin,tmax);
    tf->GetYaxis()->SetTitle("Time [ns]");

    // Make the plot
    TCanvas *c5 = new TCanvas(Form("c5%d",g),"c1", 800, 700);
    tf->Draw();
    // Put info on plot
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
    myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
    char allRuns[10] = "All Runs";
    char* pallRuns = allRuns;
    myText(0.73,0.88,kBlack,pallRuns,0.7);

    // Save the plots
    if( saveEPS )c5->SaveAs(Form("plots/%s/AvgFebCorrections_%s.eps",sPassNumber.c_str(),gain[g].c_str()));
    c5->SaveAs(Form("plots/%s/AvgFebCorrections_%s.png",sPassNumber.c_str(),gain[g].c_str()));
    delete c5;

    //Make 1D histo also showing FEB time
    TH1F *hh = new TH1F(Form("1Df%d",g),Form("1Df%d",g),500,-5.,5.);
    for(int k=1; k<=tf->GetXaxis()->GetNbins(); k++){
      if( tf->GetBinEntries(k) ) // if data for feb, fill histogram
        hh->Fill( tf->GetBinContent(k) );
    }
    TCanvas *cc = new TCanvas(Form("cc%d",g),Form("cc%d",g),800,700);
    hh->Draw();
    //hh->Rebin(5);
    hh->GetXaxis()->SetTitle("FEB Time [ns]");
    hh->GetYaxis()->SetTitle("FEBs / 0.02 ns");
    double mean = hh->GetMean();
    double rms  = hh->GetRMS();
    int entries = hh->GetEntries();
    // Put info on plot
    ATLASLabel(0.2,0.88,pInternal);
    myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
    myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
    drawStats(entries, mean, rms);
    cc->SetLogy(1);
    // Save plots
    if( saveEPS )cc->SaveAs(Form("plots/%s/1DAvgFebCorrections_%s.eps",sPassNumber.c_str(),gain[g].c_str()));
    cc->SaveAs(Form("plots/%s/1DAvgFebCorrections_%s.png",sPassNumber.c_str(),gain[g].c_str()));
    cc->SetLogy(0);
    delete cc;

  }// end loop over gains

  file1->Close();
  file2->Close();
  return;
}


//_____________________________________________
//
// Plot avg Cell time for Low gain
// by subdetector/slot
//_____________________________________________
void lowCellTimePlot(){

  std::cout << "\nMaking plots for Low Gain average Cell Timing\n\n";
  std::string det[3] = {"EMB","EMEC","All"};

  TH1F *h_cell_t[3];

  // Initialize the histograms
  for( int i=0; i<3; i++){
    h_cell_t[i] = new TH1F(Form("h_cell_t%d",i),Form("Cell Time %s; Time [ns]; Events / 0.02 ns",det[i].c_str()),1000,-25,25);
  }

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open(Form("files/Low_iov1/%s.root",sPassNumber.c_str()),"READ");
  file2 = TFile::Open(Form("files/Low_iov2/%s.root",sPassNumber.c_str()),"READ");

  // Loop over gains
  for( int g=2; g<3; g++){
    // Loop over slots
    for(int sl=0; sl<11; sl++){

      // Make names for the histograms to open
      std::string aname = Form("h_cellTimeAll%s%d", gain[g].c_str(), slotIndexASide[sl]);
      std::string cname = Form("h_cellTimeAll%s%d", gain[g].c_str(), slotIndexCSide[sl]);
      TH1F *f_t  = (TH1F*)file1->Get(aname.c_str());
      TH1F *f_t2  = (TH1F*)file2->Get(aname.c_str());
      TH1F *f_tc = (TH1F*)file1->Get(cname.c_str());
      TH1F *f_tc2 = (TH1F*)file2->Get(cname.c_str());
      f_t->Add(f_tc);
      f_t2->Add(f_tc2);
      f_t->Add(f_t2);

      if( !f_t->GetEntries() ) {
        std::cout << "  >> Slot index: " << slotIndexNames[sl] << " has no data for "
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }

      // Summary plots
      h_cell_t[2]->Add(f_t);
      if( sl < 5 )
        h_cell_t[0]->Add(f_t);
      else
        h_cell_t[1]->Add(f_t);

      // Make each slot plot
      TCanvas *c2 = new TCanvas(Form("c%d_%d",g,sl),Form("c%d_%d",g,sl),800,700);
      f_t->Draw();
      double rms  = f_t->GetRMS();
      double mean = f_t->GetMean();
      int entries = f_t->GetEntries();
      if( sl < 5 )
        f_t->Rebin(8);
      else 
        f_t->Rebin(4);
      f_t = fitGaus( f_t, mean, rms);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7); 
      drawStats(entries, mean, rms);
      if( saveEPS )c2->SaveAs(Form("plots/%s/cellTime/Sl%dCelltime_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      c2->SaveAs(Form("plots/%s/cellTime/Sl%dCelltime_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));

      // Make Log plots too
      double maxY = f_t->GetBinContent( f_t->GetMaximumBin() );
      if(maxY == 0 )
        f_t->GetYaxis()->SetRangeUser(0.5, 10);
      else
        f_t->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
      c2->SetLogy();
      if( saveEPS )c2->SaveAs(Form("plots/%s/cellTime/Log_Sl%dCelltime_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      c2->SaveAs(Form("plots/%s/cellTime/Log_Sl%dCelltime_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      c2->SetLogy(0);
      delete c2;

    } // end loop over slots

    // Make summary plots
    for( int d=0; d<3; d++){
      TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      h_cell_t[d]->Draw();
      double rms  = h_cell_t[d]->GetRMS();
      double mean = h_cell_t[d]->GetMean();
      int entries = h_cell_t[d]->GetEntries();
      if( d == 0 || d == 2){
        h_cell_t[d]->Rebin(8);
        h_cell_t[d]->GetYaxis()->SetTitle("Events / 0.16 ns");
      }
      else{
        h_cell_t[d]->Rebin(5);
        h_cell_t[d]->GetYaxis()->SetTitle("Events / 0.10 ns");
      }
      std::cout << "CELL TIME FIT PARAMS det: " << det[d].c_str() << std::endl; // FIXME
      h_cell_t[d] = fitGaus( h_cell_t[d], mean, rms );
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83, kBlack,Form("Z#rightarrow ee"),0.6);
      myText(0.2,0.79,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      //myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      drawStats(entries, mean, rms);
      if( saveEPS )c3->SaveAs(Form("plots/%s/cellTime/%s_Celltime_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      c3->SaveAs(Form("plots/%s/cellTime/%s_Celltime_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));

      // Make Log Plots too
      double maxY = h_cell_t[d]->GetBinContent( h_cell_t[d]->GetMaximumBin() );
      h_cell_t[d]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
      c3->SetLogy();
      if( saveEPS )c3->SaveAs(Form("plots/%s/cellTime/Log_%s_Celltime_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      c3->SaveAs(Form("plots/%s/cellTime/Log_%s_Celltime_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      c3->SetLogy(0);
      delete c3;

    }

  } // end loop over gains

  file1->Close();
  file2->Close();
  return;
}


//_____________________________________________
//
// Plot avg time vs Phi/Eta for low gain
// by subdetector/slot
//_____________________________________________
void lowAngularPlot(){

  std::cout << "\nMaking plots for Low Gain angular cell position timing\n\n";

  TH2F *h_phi_t[3];
  TH2F *h_eta_t[3];
  TH2F *h_dphi_t[3];
  TH2F *h_deta_t[3];
  
  std::string det[3] = {"EMB","EMEC","All"};

  // Initialize the histograms
  for( int i=0; i<3; i++){
    h_phi_t[i]  = new TH2F(Form("h_phi_t%d",i),Form("Cell Time %s; #phi; Time [ns]",det[i].c_str()),32,-3.1416,3.1416,500,-5,5);
    h_eta_t[i]  = new TH2F(Form("h_eta_t%d",i),Form("Cell Time %s; #eta; Time [ns]",det[i].c_str()),125,-2.5,2.5,500,-5,5);
    h_dphi_t[i] = new TH2F(Form("h_dphi_t%d",i),Form("Cell Time %s; #delta#phi[0.0245 units]; Time [ns]",det[i].c_str()),400,-2,2,500,-5,5);
    h_deta_t[i] = new TH2F(Form("h_deta_t%d",i),Form("Cell Time %s; #delta#eta[0.025 units]; Time [ns]", det[i].c_str()),100,-1,1,500,-5,5);
  }

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open(Form("files/Low_iov1/%s.root",sPassNumber.c_str()),"READ");
  file2 = TFile::Open(Form("files/Low_iov2/%s.root",sPassNumber.c_str()),"READ");

  // Loop over gains
  for( int g=2; g<3; g++){
    // Loop over slots
    for(int sl=0; sl<11; sl++){

      // Make names for the histograms to open
      std::string pnamea  = Form("h_cellTimeVsPhi%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string pnamec  = Form("h_cellTimeVsPhi%s%d",gain[g].c_str(),slotIndexCSide[sl]);
      std::string enamea  = Form("h_cellTimeVsEta%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string enamec  = Form("h_cellTimeVsEta%s%d",gain[g].c_str(),slotIndexCSide[sl]);
      std::string dpnamea = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string dpnamec = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),slotIndexCSide[sl]);
      std::string denamea = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string denamec = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),slotIndexCSide[sl]);

      // iov1
      TH2F *t_p   = (TH2F*)file1->Get(pnamea.c_str());
      TH2F *t_pc  = (TH2F*)file1->Get(pnamec.c_str());
      TH2F *t_e   = (TH2F*)file1->Get(enamea.c_str());
      TH2F *t_ec  = (TH2F*)file1->Get(enamec.c_str());
      TH2F *t_dp  = (TH2F*)file1->Get(dpnamea.c_str());
      TH2F *t_dpc = (TH2F*)file1->Get(dpnamec.c_str());
      TH2F *t_de  = (TH2F*)file1->Get(denamea.c_str());
      TH2F *t_dec = (TH2F*)file1->Get(denamec.c_str());
      // iov 2
      TH2F *t_p2   = (TH2F*)file2->Get(pnamea.c_str());
      TH2F *t_p2c  = (TH2F*)file2->Get(pnamec.c_str());
      TH2F *t_e2   = (TH2F*)file2->Get(enamea.c_str());
      TH2F *t_e2c  = (TH2F*)file2->Get(enamec.c_str());
      TH2F *t_dp2  = (TH2F*)file2->Get(dpnamea.c_str());
      TH2F *t_dp2c = (TH2F*)file2->Get(dpnamec.c_str());
      TH2F *t_de2  = (TH2F*)file2->Get(denamea.c_str());
      TH2F *t_de2c = (TH2F*)file2->Get(denamec.c_str());

      t_p->Add(t_pc);
      t_p->Add(t_p2);
      t_p->Add(t_p2c);
      t_e->Add(t_ec);
      t_e->Add(t_e2);
      t_e->Add(t_e2c);
      t_dp->Add(t_dpc);
      t_dp->Add(t_dp2);
      t_dp->Add(t_dp2c);
      t_de->Add(t_dec);
      t_de->Add(t_de2);
      t_de->Add(t_de2c);

      if( !t_dp->GetEntries() ) {
        std::cout << "  >> Slot index: " << slotIndexNames[sl] << " has no data for "
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }

      // Get Profile Plots
      TProfile *f_p   = t_p->ProfileX(Form("f_p%d%d",g,sl),-1,-1,"o");
      TProfile *f_e   = t_e->ProfileX(Form("f_e%d%d",g,sl),-1,-1,"o");
      TProfile *f_dp  = t_dp->ProfileX(Form("f_dp%d%d",g,sl),-1,-1,"o");
      TProfile *f_de  = t_de->ProfileX(Form("f_de%d%d",g,sl),-1,-1,"o");
      f_p->GetYaxis()->SetRangeUser(tmin,tmax);
      f_e->GetYaxis()->SetRangeUser(tmin,tmax);
      f_dp->GetYaxis()->SetRangeUser(tmin,tmax);
      f_de->GetYaxis()->SetRangeUser(tmin,tmax);
      f_p->GetYaxis()->SetTitle("Time [ns]");
      f_e->GetYaxis()->SetTitle("Time [ns]");
      f_dp->GetYaxis()->SetTitle("Time [ns]");
      f_de->GetYaxis()->SetTitle("Time [ns]");
      // Summary plots
      h_phi_t[2]->Add(t_p);
      h_eta_t[2]->Add(t_e);
      h_dphi_t[2]->Add(t_dp);
      h_deta_t[2]->Add(t_de);
      if( sl < 5 ){
        h_phi_t[0]->Add(t_p);
        h_eta_t[0]->Add(t_e);
        h_dphi_t[0]->Add(t_dp);
        h_deta_t[0]->Add(t_de);
      }else{
        h_phi_t[1]->Add(t_p);
        h_eta_t[1]->Add(t_e);
        h_dphi_t[1]->Add(t_dp);
        h_deta_t[1]->Add(t_de);
      }

      // Make each slot plot
      // Phi
      TCanvas *cp = new TCanvas(Form("cp%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
      f_p->Draw();
      f_p->Rebin(2);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )cp->SaveAs(Form("plots/%s/angular/Sl%dcellPhi_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      cp->SaveAs(Form("plots/%s/angular/Sl%dcellPhi_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cp;
      // Eta
      TCanvas *ce = new TCanvas(Form("ce%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
      f_e->Draw();
      f_e->Rebin(2);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )ce->SaveAs(Form("plots/%s/angular/Sl%dcellEta_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      ce->SaveAs(Form("plots/%s/angular/Sl%dcellEta_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete ce;
      // dPhi
      TCanvas *cdp = new TCanvas(Form("cdp%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
      f_dp->Draw();
      f_dp->Rebin(8);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )cdp->SaveAs(Form("plots/%s/angular/Sl%dcelldPhi_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      // for pass4 we save the fit plots instead
      cdp->SaveAs(Form("plots/%s/angular/Sl%dcelldPhi_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cdp;
      // dEta
      TCanvas *cde = new TCanvas(Form("cde%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
      f_de->Draw();
      f_de->Rebin(4);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )cde->SaveAs(Form("plots/%s/angular/Sl%dcelldEta_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      // for pass4 we save the fit plots instead
      cde->SaveAs(Form("plots/%s/angular/Sl%dcelldEta_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cde;

    }// end loop over slots

    // Make summary plots
    for( int d=0; d<3; d++){
      // Phi
      TCanvas *cp = new TCanvas(Form("cp_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      TProfile *ff_p = h_phi_t[d]->ProfileX(Form("ff_p%d%d",g,d),-1,-1,"o");
      ff_p->Draw();
      ff_p->Rebin(2);
      ff_p->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_p->GetYaxis()->SetTitle("Time [ns]");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )cp->SaveAs(Form("plots/%s/angular/%s_cellPhi_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cp->SaveAs(Form("plots/%s/angular/%s_cellPhi_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cp;
      // Eta
      TCanvas *ce = new TCanvas(Form("ce_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      TProfile *ff_e = h_eta_t[d]->ProfileX(Form("ff_e%d%d",g,d),-1,-1,"o");
      ff_e->Draw();
      ff_e->Rebin(2);
      ff_e->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_e->GetYaxis()->SetTitle("Time [ns]");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )ce->SaveAs(Form("plots/%s/angular/%s_cellEta_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      ce->SaveAs(Form("plots/%s/angular/%s_cellEta_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete ce;
      // dPhi
      TCanvas *cdp = new TCanvas(Form("cdp_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      TProfile *ff_dp = h_dphi_t[d]->ProfileX(Form("ff_dp%d%d",g,d),-1,-1,"o");
      ff_dp->Draw();
      ff_dp->Rebin(8);
      ff_dp->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_dp->GetYaxis()->SetTitle("Time [ns]");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )cdp->SaveAs(Form("plots/%s/angular/%s_celldPhi_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cdp->SaveAs(Form("plots/%s/angular/%s_celldPhi_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cdp;
      // dEta
      TCanvas *cde = new TCanvas(Form("cde_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
      TProfile *ff_de = h_deta_t[d]->ProfileX(Form("ff_de%d%d",g,d),-1,-1,"o");
      ff_de->Draw();
      ff_de->Rebin(4);
      ff_de->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_de->GetYaxis()->SetTitle("Time [ns]");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )cde->SaveAs(Form("plots/%s/angular/%s_celldEta_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cde->SaveAs(Form("plots/%s/angular/%s_celldEta_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cde;
    }

  }// end loop over gains

  file1->Close();
  file2->Close();
  return;
}



//_____________________________________________
//
// Plot avg time vs f1/f3 for low gain
// by subdetector/slot
//_____________________________________________
void lowEFracPlot(){

  std::cout << "\nMaking plots for Low Gain fractional energy deposit timing\n\n";

  TH2F * h_f1_t[3];
  TH2F * h_f3_t[3];
  
  std::string det[3] = {"EMB","EMEC","All"};

  // Initialize the histograms
  for( int i=0; i<3; i++){
      h_f1_t[i]  = new TH2F(Form("h_f1_t%d",i),Form("Cell Time f1%s%d; f1; Time [ns]",det[i].c_str(),i),400,-0.05,0.6,500,-5,5); // 220, -0.10, 1.
      h_f3_t[i]  = new TH2F(Form("h_f3_t%d",i),Form("Cell Time f3%s%d; f3; Time [ns]",det[i].c_str(),i),400,-0.05,0.2,500,-5,5);
  }

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open(Form("files/Low_iov1/%s.root",sPassNumber.c_str()),"READ");
  file2 = TFile::Open(Form("files/Low_iov2/%s.root",sPassNumber.c_str()),"READ");

  // Loop over gains
  for( int g=2; g<3; g++){
    // Loop over slots
    for(int sl=0; sl<11; sl++){

      // Make names for the histograms to open
      std::string f1namea  = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string f1namec  = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),slotIndexCSide[sl]);
      std::string f3namea  = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string f3namec  = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),slotIndexCSide[sl]);
      // iov1
      TH2F *t_f1   = (TH2F*)file1->Get(f1namea.c_str());
      TH2F *t_f1c  = (TH2F*)file1->Get(f1namec.c_str());
      TH2F *t_f3   = (TH2F*)file1->Get(f3namea.c_str());
      TH2F *t_f3c  = (TH2F*)file1->Get(f3namec.c_str());
      // iov2
      TH2F *t_f12   = (TH2F*)file2->Get(f1namea.c_str());
      TH2F *t_f12c  = (TH2F*)file2->Get(f1namec.c_str());
      TH2F *t_f32   = (TH2F*)file2->Get(f3namea.c_str());
      TH2F *t_f32c  = (TH2F*)file2->Get(f3namec.c_str());

      t_f1->Add(t_f1c);
      t_f1->Add(t_f12);
      t_f1->Add(t_f12c);
      t_f3->Add(t_f3c);
      t_f3->Add(t_f32);
      t_f3->Add(t_f32c);

      if( !t_f1->GetEntries() ) {
        std::cout << "  >> Slot index: " << sl << " has no data for "
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }

      // Summary plots
      h_f1_t[2]->Add(t_f1);
      h_f3_t[2]->Add(t_f3);
      if( sl < 5 ){
        h_f1_t[0]->Add(t_f1);
        h_f3_t[0]->Add(t_f3);
      } else {
        h_f1_t[1]->Add(t_f1);
        h_f3_t[1]->Add(t_f3);
      }
     
      // Rebin
      int binMinf1 = t_f1->GetEntries()/20;
      int binMinf3 = t_f3->GetEntries()/20;
      t_f1 = rebin( t_f1, binMinf1, t_f1->GetXaxis()->FindBin(0.) -1 );
      t_f3 = rebin( t_f3, binMinf3, t_f3->GetXaxis()->FindBin(0.) -1 );
      t_f1->SetName( Form("%s%d%d",t_f1->GetName(),g,sl));
      t_f3->SetName( Form("%s%d%d",t_f3->GetName(),g,sl));

      // Get Profile Plots
      TProfile *f_f1   = t_f1->ProfileX(Form("f_f1%d%d",g,sl),-1,-1,"o");
      TProfile *f_f3   = t_f3->ProfileX(Form("f_f3%d%d",g,sl),-1,-1,"o");
      f_f1->GetYaxis()->SetRangeUser(tmin,tmax);
      f_f3->GetYaxis()->SetRangeUser(tmin,tmax);
      f_f1->GetYaxis()->SetTitle("Time [ns]");
      f_f3->GetYaxis()->SetTitle("Time [ns]");
      f_f1->GetXaxis()->SetTitle("f1");
      f_f3->GetXaxis()->SetTitle("f3");

      // Make each slot plot
      // f1
      TCanvas *cf1 = new TCanvas(Form("cf1%d_%d",g,sl),Form("cf1%d_%d",g,sl),800,700);
      f_f1->Draw();
      f_f1->GetXaxis()->SetRangeUser( f_f1->GetXaxis()->GetBinLowEdge( f_f1->GetXaxis()->FindBin(0.)) ,.6);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )cf1->SaveAs(Form("plots/%s/efrac/Sl%dcellf1_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      // Store fits for pass5
      cf1->SaveAs(Form("plots/%s/efrac/Sl%dcellf1_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cf1;
      // f3
      TCanvas *cf3 = new TCanvas(Form("cf3%d_%d",g,sl),Form("cf3%d_%d",g,sl),800,700);
      f_f3->Draw();
      //f_f3->GetXaxis()->SetRangeUser( f_f3->GetXaxis()->GetBinLowEdge( f_f3->GetXaxis()->FindBin(0.)),.2);
      f_f3->GetXaxis()->SetRangeUser( 0.0,.2);
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      cf3->SetLogx(1);
      if( saveEPS )cf3->SaveAs(Form("plots/%s/efrac/Sl%dcellf3_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      // Store fits for pass5
      cf3->SaveAs(Form("plots/%s/efrac/Sl%dcellf3_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cf3;

    }// end loop over slots

    // Make summary plots
    for( int d=0; d<3; d++){
      // Rebin
      int binMinf1 = h_f1_t[d]->GetEntries()/20;
      int binMinf3 = h_f3_t[d]->GetEntries()/20;
      h_f1_t[d] = rebin( h_f1_t[d], binMinf1, h_f1_t[d]->GetXaxis()->FindBin(0.) -1 );
      h_f3_t[d] = rebin( h_f3_t[d], binMinf3, h_f3_t[d]->GetXaxis()->FindBin(0.) -1 );

      // f1
      TCanvas *cf1 = new TCanvas(Form("cf1_%d_%d",g,d),Form("cf1_%d_%d",g,d),800,700);
      TProfile *ff_f1 = h_f1_t[d]->ProfileX(Form("ff_f1%d%d",g,d),-1,-1,"o");
      ff_f1->Draw();
      ff_f1->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_f1->GetXaxis()->SetRangeUser( ff_f1->GetXaxis()->GetBinLowEdge( ff_f1->GetXaxis()->FindBin(0.)), .60);
      ff_f1->GetYaxis()->SetTitle("Time [ns]");
      ff_f1->GetXaxis()->SetTitle("f1");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )cf1->SaveAs(Form("plots/%s/efrac/%s_cellf1_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cf1->SaveAs(Form("plots/%s/efrac/%s_cellf1_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cf1;
      // f3
      TCanvas *cf3 = new TCanvas(Form("cf3_%d_%d",g,d),Form("cf3_%d_%d",g,d),800,700);
      TProfile *ff_f3 = h_f3_t[d]->ProfileX(Form("ff_f3%d%d",g,d),-1,-1,"o");
      ff_f3->Draw();
      ff_f3->GetYaxis()->SetRangeUser(tmin,tmax);
      //ff_f3->GetXaxis()->SetRangeUser( ff_f3->GetXaxis()->GetBinLowEdge( ff_f3->GetXaxis()->FindBin(0.)),.2);
      ff_f3->GetXaxis()->SetRangeUser( 0.,.2);
      ff_f3->GetYaxis()->SetTitle("Time [ns]");
      ff_f3->GetXaxis()->SetTitle("f3");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      cf3->SetLogx(1);
      if( saveEPS )cf3->SaveAs(Form("plots/%s/efrac/%s_cellf3_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cf3->SaveAs(Form("plots/%s/efrac/%s_cellf3_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cf3;
    }

  }// end loop over gains

  file1->Close();
  file2->Close();
  return;
}


//_____________________________________________
//
// Plot avg time vs energy for low gain
// by subdetector/slot
//_____________________________________________
void lowETimePlot(){

  std::cout << "\nMaking plots for Low Gain average energy timing\n\n";

  TH2F* h_e_t[3];
  std::string det[3] = {"EMB", "EMEC", "All"};

  // Initialize the histograms
  for( int i=0; i<3; i++){
      h_e_t[i]  = new TH2F(Form("h_e_t%d",i),Form("Cell Time Energy %s%d; Energy [GeV]; Time [ns]",det[i].c_str(),i),500,0,500.,500,-5,5);
  }

  // Open the data file
  TFile *file1;
  TFile *file2;
  file1 = TFile::Open(Form("files/Low_iov1/%s.root",sPassNumber.c_str()),"READ");
  file2 = TFile::Open(Form("files/Low_iov2/%s.root",sPassNumber.c_str()),"READ");

  // Loop over gains
  for( int g=2; g<3; g++){
    // Loop over slots
    for(int sl=0; sl<11; sl++){

      // Make names for the histograms to open
      std::string enamea  = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),slotIndexASide[sl]);
      std::string enamec  = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),slotIndexCSide[sl]);

      TH2F *t_e    = (TH2F*)file1->Get(enamea.c_str());
      TH2F *t_ec   = (TH2F*)file1->Get(enamec.c_str());
      TH2F *t_e2   = (TH2F*)file2->Get(enamea.c_str());
      TH2F *t_e2c  = (TH2F*)file2->Get(enamec.c_str());
      
      t_e->Add(t_ec);
      t_e->Add(t_e2);
      t_e->Add(t_e2c);

      if( !t_e->GetEntries() ) {
        std::cout << "  >> Slot index: " << sl << " has no data for "
                  << gain[g] << " gain, no plots will be made!\n";
        continue;
      }

      // Summary plots
      h_e_t[2]->Add(t_e);
      if( sl < 5 )
        h_e_t[0]->Add(t_e);
      else
        h_e_t[1]->Add(t_e);
     
      // Rebin FIXME have to adjust to make data look reasonable
      int binMin = t_e->GetEntries()/20; // 2 for lowgain
      t_e = rebin( t_e, binMin, 5);
      t_e->SetName( Form("%s%d%d",t_e->GetName(),g,sl));

      // Get Profile Plots
      TProfile *f_e   = t_e->ProfileX(Form("f_e%d%d",g,sl),-1,-1,"o");
      f_e->GetYaxis()->SetRangeUser(tmin,tmax);
      f_e->GetYaxis()->SetTitle("Time [ns]");
      f_e->GetXaxis()->SetTitle("Energy [GeV]");

      // Make each slot plot
      // e
      TCanvas *cce = new TCanvas(Form("cce%d_%d",g,sl),Form("cce%d_%d",g,sl),800,700);
      f_e->Draw();
      f_e->GetXaxis()->SetRangeUser( 5., 500); //FIXME
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      if( saveEPS )cce->SaveAs(Form("plots/%s/energy/Sl%dcelle_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      // Save the fit version instead for pass3
      cce->SaveAs(Form("plots/%s/energy/Sl%dcelle_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      delete cce;

      //Make 1D histo also showing energy time
      TH1F *he = new TH1F(Form("1De_%d%d",g,sl),Form("1De_%d%d",g,sl),250,-5.,5.);
      for(int k=1; k<=f_e->GetXaxis()->GetNbins(); k++){
        if( f_e->GetBinEntries(k)) // if entries for energy, fill histogram
          he->Fill( f_e->GetBinContent(k) );
      }
      TCanvas *ce = new TCanvas(Form("ce%d%d",g,sl),Form("ce%d%d",g,sl),800,700);
      he->Draw();
      he->Rebin(5);
      he->GetXaxis()->SetTitle("Time [ns]");
      he->GetYaxis()->SetTitle("1 GeV bins / 0.2 ns"); // no rebin 0.04ns
      double mean = he->GetMean();
      double rms  = he->GetRMS();
      int entries = he->GetEntries();
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain", gain[g].c_str()), 0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d", passNumber), 0.6);
      myText(0.73,0.88,kBlack,Form("%s", slotIndexNames[sl].c_str()), 0.7);
      drawStats(entries, mean, rms);
      ce->SetLogy(1);
      // Save plots
      if( saveEPS )ce->SaveAs(Form("plots/%s/energy/1DSl%dcelle_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
      ce->SaveAs(Form("plots/%s/energy/1DSl%dcelle_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
      ce->SetLogy(0);
      delete ce;

    }// end loop over slots
    // Make summary plots
    for( int d=0; d<3; d++){

      // Rebin FIXME have to adjust to make data look reasonable
      int binMin = h_e_t[d]->GetEntries()/20; // low gain 5
      h_e_t[d] = rebin( h_e_t[d], binMin, 5);

      // e
      TCanvas *cce = new TCanvas(Form("ccce_%d_%d",g,d),Form("ce_%d_%d",g,d),800,700);
      TProfile *ff_e = h_e_t[d]->ProfileX(Form("ff_e%d%d",g,d),-1,-1,"o");
      ff_e->Draw();
      ff_e->GetYaxis()->SetRangeUser(tmin,tmax);
      ff_e->GetXaxis()->SetRangeUser(5.,500);
      ff_e->GetYaxis()->SetTitle("Time [ns]");
      ff_e->GetXaxis()->SetTitle("Energy [GeV]");
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      if( saveEPS )cce->SaveAs(Form("plots/%s/energy/%s_celle_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      cce->SaveAs(Form("plots/%s/energy/%s_celle_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      delete cce;

      //Make 1D histo also showing energy time
      TH1F *he = new TH1F(Form("1De%d%d",g,d),Form("1De%d%d",g,d),250,-5.,5.);
      for(int k=1; k<=ff_e->GetXaxis()->GetNbins(); k++){
        if( ff_e->GetBinEntries(k)) // if entries for energy, fill histogram
          he->Fill( ff_e->GetBinContent(k) );
      }
      TCanvas *ce = new TCanvas(Form("ccce%d%d",g,d),Form("ce%d%d",g,d),800,700);
      he->Draw();
      he->Rebin(5);
      he->GetXaxis()->SetTitle("Time [ns]");
      he->GetYaxis()->SetTitle("1 GeV bins / 0.2 ns"); // no rebin .04ns
      double mean = he->GetMean();
      double rms  = he->GetRMS();
      int entries = he->GetEntries();
      // Put info on plot
      ATLASLabel(0.2,0.88,pInternal);
      myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
      myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
      myText(0.73,0.88,kBlack,Form("%s",det[d].c_str()),0.7);
      drawStats(entries, mean, rms);
      ce->SetLogy(1);
      // Save plots
      if( saveEPS )ce->SaveAs(Form("plots/%s/energy/1D%s_celle_%s.eps",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      ce->SaveAs(Form("plots/%s/energy/1D%s_celle_%s.png",sPassNumber.c_str(),det[d].c_str(),gain[g].c_str()));
      ce->SetLogy(0);
      delete ce;

    }

  }// end loop over gains

  file1->Close();
  file2->Close();
  return;
}

