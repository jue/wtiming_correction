//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"


//________________________________________
//
// Plot leading/subleading electron plots
// for Zee sample
//_________________________________________
void plotZeet1t2( TH2F* h_2D, TH1F** h_1D ){

	// Make string to read plots for t1/t2
	std::string hname[5] = {"h_t1plust2Zee","h_t1minust2Zee",
		"h_t1Zee","h_t2Zee","h_t1vst2Zee"};

	for( int h=0; h<5; h++){
		TCanvas * c;
		// 2D histo correlation leading/subleading Z electron times
		if( h == 4 ){
			c = new TCanvas( Form("c_%s", (hname[h]).c_str()), Form("c_%s", (hname[h]).c_str()), 900, 750 );
			// set style
			c->SetRightMargin(0.17);
			gStyle->SetPalette(1,0);
			// draw
			h_2D->Draw("colz");
			// print info on plot
			double corr = h_2D->GetCorrelationFactor(1,2);
			int ent = h_2D->GetEntries();
			double meanX = h_2D->GetMean(1);
			double meanY = h_2D->GetMean(2);
			double rmsX  = h_2D->GetRMS(1);
			double rmsY  = h_2D->GetRMS(2);
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.83, kBlack, Form("Z#rightarrow ee"), 0.7);
			myText(0.20, 0.79, kBlack, Form("Corr(x,y) =%.3f", corr), 0.6);
			myText(0.62,0.88,kBlack,Form("Entries: %d",ent),0.6);
			myText(0.62,0.85,kBlack,Form("Mean X: %.3f ns",meanX),0.6);
			myText(0.62,0.82,kBlack,Form("RMS X: %.3f ns",rmsX),0.6);
			myText(0.62,0.79,kBlack,Form("Mean Y: %.3f ns",meanY),0.6);
			myText(0.62,0.76,kBlack,Form("RMS Y: %.3f ns",rmsY),0.6);
		}
		// 1D histo with gaussian fit
		else{
			c = new TCanvas( Form("c_%s", (hname[h]).c_str()), Form("c_%s", (hname[h]).c_str()), 800, 700);
			// set style
			h_1D[h]->SetMinimum(0.5);
			h_1D[h]->GetYaxis()->SetTitle("Events / 0.02 ns");
			if( h == 3 )
				h_1D[h]->GetXaxis()->SetTitle("t_{2} [ns]");
			// draw
			h_1D[h]->Draw();
			// put info on plot
			double mean = h_1D[h]->GetMean();
			double rms  = h_1D[h]->GetRMS();
			int ent     = h_1D[h]->GetEntries();
			h_1D[h] = fitGaus( h_1D[h], mean, rms);
			drawStats(ent, mean, rms);
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
			myText(0.22, 0.77, kBlack, Form("Z#rightarrow ee"), 0.7);  
			// Save Log version too
			c->SetLogy(1);
			if( saveEPS ) c->SaveAs(Form("plots/Zplots/%s_log.eps", (hname[h]).c_str()));
			c->SaveAs(Form("plots/Zplots/%s_log.png", (hname[h]).c_str()));
			c->SetLogy(0);
		}
		if( saveEPS ) c->SaveAs(Form("plots/Zplots/%s.eps", (hname[h]).c_str()));
		c->SaveAs(Form("plots/Zplots/%s.png", (hname[h]).c_str()));
		delete c;
	}

	return; 
}
/*
void plotZeeResolution( TH2F* t_e_A[2][11], TH2F* t_e_C[2][11]){

	// For making total plots
	TH2F * h_EMB[2]; 
	TH1F * h_EMB_res[2];
	TH2F * h_EMEC[2]; 
	TH1F * h_EMEC_res[2];
	TF1 * f3[4];
	double p0[4];
	double p1[4];
	ofstream fitH("fitH.dat");  
	ofstream fitM("fitM.dat");
	// loop over gains
	for( int g = 0; g < 2; g++){

		h_EMB[g] = new TH2F(Form("h_EMEC%s",gain[g].c_str()),Form("h_EMEC%s",gain[g].c_str()),350, 0, 350, 500, -5, 5);
		h_EMEC[g]= new TH2F(Form("h_EMB%s",gain[g].c_str()),Form("h_EMB%s",gain[g].c_str()),350, 0, 350, 500, -5, 5);

		// loop over slots (combine A&C)
		for( int sl = 0; sl < 11; sl++){
			h_EMB[g]-> GetXaxis()-> SetRangeUser(0,350);
			h_EMEC[g]-> GetXaxis()-> SetRangeUser(0,350);

			// Add A/C side histograms
			t_e_A[g][sl]->Add(t_e_C[g][sl]); 
			t_e_A[g][sl]->GetXaxis()-> SetRangeUser(0,350); 
			if( sl < 5){
				h_EMB[g]->Add( t_e_A[g][sl] );

				cout<<"lalala"<<slotIndexNames[sl].c_str()<<endl;
			}
			//	else if(sl>4&&sl<9){
			else if(sl<10){
				h_EMEC[g]->Add( t_e_A[g][sl] );
				cout<<"lalala"<<slotIndexNames[sl].c_str()<<endl;
			}

			// Make sure histogram is not empty
			if( !t_e_A[g][sl]->GetEntries() ) {
				std::cout << "  >> Slot: " << slotIndexNames[sl] << " has no data for " 
					<< gain[g] << " gain, no plots will be made!\n";
				continue;
			}
			// Variable Rebin the 2D histogram with threshold of entries per bin
			int binMin = ( g == 0 ? 100 : 75 );
			t_e_A[g][sl] = rebin( t_e_A[g][sl], t_e_A[g][sl]-> GetEntries()/binMin, 5);  
			t_e_A[g][sl]->SetName(Form("%s_%d",t_e_A[g][sl]->GetName(),g));
			cout<<"llala"<<g<<endl;
			// Find RMS of gaussian fit for each slice in energy
			TObjArray aSlices;
			binMin = ( g == 0 ? 20 : 10 );
			// 0->gaus, 0,-1 -> fit all bins, 20->bin threshold, QNR plot options, aslices->store fit histo
			t_e_A[g][sl]->FitSlicesY(0, 0, -1, binMin, "QNR", &aSlices); 
			TH1F * h_res = new TH1F( Form("hres%d%d", g, sl), Form("hres%d%d", g, sl), 
					t_e_A[g][sl]->GetNbinsX(), t_e_A[g][sl]->GetXaxis()->GetXbins()->GetArray());
			h_res = (TH1F*)aSlices[2]; // [2] is the sigma of gaussian fit for the slice

			// Make plot for each slot
			TCanvas *c_res = new TCanvas( Form("c_res%s%d", gain[g].c_str(), sl), Form("c_res%s%d", gain[g].c_str(), sl), 800, 700);
			h_res->GetXaxis()->SetRangeUser(5, h_res->GetXaxis()->GetBinLowEdge(h_res->GetXaxis()->FindBin(300)));
			h_res->Draw();

			// Find fit range
			double enLow  = -99;
			double enHigh = -99;
			int nBins = h_res->GetNbinsX();
			for (int b=1; b <= nBins; b++){ // bins in en
				if( h_res->GetBinContent(b) > 0 ){
					if( enLow == -99 ) enLow = h_res->GetBinCenter(b+(1-g));
					enHigh = h_res->GetBinCenter(b-g*2); 
				}
			}

			// Cut off ranges (40 for Hi/ 28 for MED)

			if(g == 0 && enHigh >40 ){
				enHigh = 40;
				if(sl==8)
					enHigh=30;
			}
			if(g == 1 && enLow < 30 ){
				if( sl < 2 && enLow < 28)
					enLow = 28;
				else
					enLow = 30;
			}
			if( debug ) {
				std::cout << "Index:  " << sl << ";  Slot: "  << slotIndexNames[sl] 
					<< "; low:  " << enLow << "; high: " << enHigh << std::endl;
			}

			// Declare fit
			TF1 *f1 = new TF1( "f1", "sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
			f1->SetLineColor(kRed);
			h_res->Fit("f1","QNR");
			f1->Draw("same");
			double pp0 = f1->GetParameter(0);
			double pp1 = f1->GetParameter(1);

			// Put text on plot
			h_res->GetYaxis()->SetTitle("Time Resolution [ns]");
			//	if( sl < 9 || g == 1 )
			h_res->GetYaxis()->SetRangeUser(0.2, 0.7);
			h_res->GetYaxis()->SetTitle("Time Resolution [ns]");
			h_res->GetXaxis()->SetTitle("Energy [GeV]");
			//	h_res->GetYaxis()->SetRangeUser(0.20, .60-0.25*g);
			//	h_res->GetXaxis()->SetRangeUser(5, 250);
			//	c_res->SetLogx(1);
			ATLASLabel(0.2,0.88,pInternal);
			myText(0.20, 0.83, kBlack, Form("Z#rightarrow ee"), 0.7);  
			myText(0.20, 0.79, kRed, Form("%s Gain", gain[g].c_str()), 0.6);
			myText(0.73, 0.88, kBlack, Form("%s", slotIndexNames[sl].c_str()), 0.7);
			myText(0.73, 0.84, kRed, Form("p_{0}: %.3f", pp0), 0.6);
			myText(0.73, 0.81, kRed, Form("p_{1}: %.3f", pp1), 0.6);
			std::cout << std::fixed;
			std::cout.precision(0); 
			if(g==0)
				fitH<<sl<<"\t"<<std::fixed << std::setprecision(0)<<pp0*1000<<" "<<pp1*1000<<endl;
			else if(g==1)
				fitM<<sl<<"\t"<<std::fixed << std::setprecision(0)<<pp0*1000<<" "<<pp1*1000<<endl;

			if( saveEPS) c_res->SaveAs(Form("~/WWW/tr_energy/h_eResolution_sl%d_%s.eps", sl, gain[g].c_str()));
			c_res->SaveAs(Form("~/WWW/tr_energy/h_eResolution_sl%d_%s.png", sl, gain[g].c_str()));
			delete c_res;
		} // end loop over slots


		// Subdetector summary resolution
		// EMB
		int binMin = (g == 0 ?  100 :30); // 25000,4000
		h_EMB[g] ->Sumw2();
		h_EMB[g] = rebin( h_EMB[g], h_EMB[g]->GetEntries()/binMin, 5); 
		//	int binMin = (g == 0 ?  40000 :2000 ); // 25000,4000
		//	h_EMB[g] = rebin( h_EMB[g], binMin, 5); 
		h_EMB[g]->SetName(Form("%s_%d",h_EMB[g]->GetName(),g));
		binMin = ( g == 0 ? 20 : 40 ); // 10, 29
		double enLow  = -99;
		double enHigh = -99;
		// Find fit range
		int nBins = h_EMB[g]->GetNbinsX();
		std::cout << " NBins:  " << nBins << std::endl;
		for (int b=1; b <= nBins; b++){ // bins in en
			if( h_EMB[g]->Integral(b,b) > binMin ){
				if( enLow == -99 ) enLow = h_EMB[g]->GetXaxis()->GetBinCenter(b);
				//	enHigh = h_EMB[g]->GetXaxis()->GetBinCenter(b);
				//`	if(g==1)
				enHigh = h_EMB[g]->GetXaxis()->GetBinCenter(b-g);
				std::cout << "Bin: " << b << " ; Bin Center: " << h_EMB[g]->GetXaxis()->GetBinCenter(b) << "; Bin Integral: " << h_EMB[g]->Integral(b,b) << std::endl;
			}
		}
		// FIXME originally 27
		if(g == 0 && enHigh > 33 )

			enHigh = 33;
		if(g == 1 && enLow < 30 )
			enLow = 30;
		int lowBin = h_EMB[g]->GetXaxis()->FindBin(enLow);
		int highBin = h_EMB[g]->GetXaxis()->FindBin(enHigh);
		std::cout << "low bin :  " << lowBin << "; high bin: " << highBin << std::endl;
		TObjArray bSlices;
		h_EMB[g]->FitSlicesY(0, lowBin, highBin, binMin, "QNR", &bSlices); // bins was 0, -1 
		TH1F *h_res = new TH1F(Form("hhres%d",g),Form("hhres%d",g), h_EMB[g]->GetNbinsX(), h_EMB[g]->GetXaxis()->GetXbins()->GetArray());
		h_res->Sumw2();
		h_res = (TH1F*)bSlices[2]; // [2] is the sigma of gaussian fit for the slice
		// Declare fit
		TF1 *f2 = new TF1("f2","sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
		h_res->Fit( "f2","QNR");
		//f1[g]->SetLineColor(kRed);
		p0[g] = f2->GetParameter(0);
		p1[g] = f2->GetParameter(1);
		std::cout << " p0 :  " << p0[g] << " p1: " << p1[g] << std::endl;
		std::cout << " enL :  " << enLow << " enHigh: " << enHigh << std::endl;
		h_EMB_res[g] = (TH1F*)h_res->Clone("");
		f3[g] = (TF1*)f2->Clone("");
		delete f2;
		delete h_res;
		// EMEC
		binMin = (g == 0 ?  100 : 30); // 25000,4000

		h_EMEC[g] ->Sumw2();
		h_EMEC[g] = rebin( h_EMEC[g], h_EMEC[g]->GetEntries()/binMin, 5); 
		//	binMin = (g == 0 ?  10000 : 10000 ); // 25000,4000
		//	h_EMEC[g] = rebin( h_EMEC[g], binMin, 5); 
		h_EMEC[g]->SetName(Form("%s_%d",h_EMEC[g]->GetName(),g));
		binMin = ( g == 0 ? 20 : 40 ); // 10, 29
		enLow  = -99;
		enHigh = -99;
		// Find fit range
		nBins = h_EMEC[g]->GetNbinsX();
		std::cout << " NBins:  " << nBins << std::endl;
		for (int b=1; b <= nBins; b++){ // bins in en
			if( h_EMEC[g]->Integral(b,b) > binMin ){
				if( enLow == -99 ) enLow = h_EMEC[g]->GetXaxis()->GetBinCenter(b);
				enHigh = h_EMEC[g]->GetXaxis()->GetBinCenter(b-g);
				std::cout << "Bin: " << b << " ; Bin Center: " << h_EMEC[g]->GetXaxis()->GetBinCenter(b) << "; Bin Integral: " << h_EMEC[g]->Integral(b,b) << std::endl;
			}
		}
		// FIXME originally 35
		if(g == 0 && enHigh > 30 )
			enHigh = 30;
		if(g == 1 && enLow < 30 )
			enLow =30;
		lowBin = h_EMEC[g]->GetXaxis()->FindBin(enLow);
		highBin = h_EMEC[g]->GetXaxis()->FindBin(enHigh);
		std::cout << "low bin :  " << lowBin << "; high bin: " << highBin << std::endl;
		TObjArray cSlices;
		h_EMEC[g]->FitSlicesY(0, lowBin, highBin, binMin, "QNR", &cSlices); // bins was 0, -1 
		TH1F *h_res2 = new TH1F(Form("hhres2%d",g),Form("hhres2%d",g), h_EMEC[g]->GetNbinsX(), h_EMEC[g]->GetXaxis()->GetXbins()->GetArray());
		h_res2 ->Sumw2();
		h_res2 = (TH1F*)cSlices[2]; // [2] is the sigma of gaussian fit for the slice
		// Declare fit
		//	if( g == 1) enHigh = 250;
		TF1 *f22 = new TF1("f22","sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
		h_res2->Fit( "f22","QNR");
		//f1[g]->SetLineColor(kRed);
		p0[g+2] = f22->GetParameter(0);
		p1[g+2] = f22->GetParameter(1);
		std::cout << " p0 :  " << p0[g+2] << " p1: " << p1[g+2] << std::endl;
		std::cout << " enL :  " << enLow << " enHigh: " << enHigh << std::endl;
		h_EMEC_res[g] = (TH1F*)h_res2->Clone("");
		f3[g+2] = (TF1*)f22->Clone("");
		delete f22;
		delete h_res;

		} // end loop over gains
		// EMB
		TCanvas *cc = new TCanvas( Form("cc_sum"), Form("cc_sum"), 800,700);
		h_EMB_res[1]->SetLineColor(kBlue);
		h_EMB_res[1]->SetMarkerColor(kBlue);
		std::cout << "params: " << p0[0] << " " << p1[0] << "  " << p0[1] << " " << p1[1] << std::endl;
		h_EMB_res[1]->GetYaxis()->SetTitle("Time Resolution [ns]");
		h_EMB_res[1]->GetXaxis()->SetTitle("Energy [GeV]");
		h_EMB_res[1]->GetYaxis()->SetRangeUser(0.20, .60);
		h_EMB_res[1]->GetXaxis()->SetRangeUser(0, h_EMB_res[1]->GetXaxis()->GetBinLowEdge(h_EMB_res[1]->GetXaxis()->FindBin(300)));
		//	h_EMB_res[1]-> SetErrorOption("g"); 
		//
		h_EMB_res[1]->Draw("P");
		h_EMB_res[0]->Draw("same");
		f3[0]->SetLineColor(kRed);
		f3[0]->Draw("same");
		f3[1]->SetLineColor(kRed);
		f3[1]->Draw("same");
		//	cc->SetLogx(1);
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.73, 0.88, kBlack, Form("Z#rightarrow ee"), 0.6);  
		//myText(0.73, 0.84, kBlack, Form("EMB"), 0.7); // FIXME
		myText(0.73, 0.84, kBlack, Form("EMB "), 0.7);
		myText(0.73, 0.81, kBlack, Form("0.0 < |#eta| < 1.4"),0.6);
		//  myText(0.32, 0.71, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.75);
		myText(0.60, 0.75, kRed, Form("Fit Results: #sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.7);
		myText(0.67, 0.71-0.02, kBlack, Form("High"), 0.6);
		myText(0.73, 0.71-0.02, kBlack, Form("p_{0}: %.3f", p0[0]), 0.6);
		myText(0.73, 0.68-0.02, kBlack, Form("p_{1}: %.3f", p1[0]), 0.6);
		myText(0.70-0.02, 0.65-0.03, kBlack,Form("#chi^{2}/ndf : %.4f", f3[0]->GetChisquare()/f3[0]->GetNDF()),0.6); 
		myText(0.63, 0.65-0.07, kBlue, Form("Medium"), 0.6);
		myText(0.73, 0.65-0.07, kBlue, Form("p_{0}: %.3f", p0[1]), 0.6);
		myText(0.73, 0.62-0.07, kBlue, Form("p_{1}: %.3f", p1[1]), 0.6);
		myText(0.70-0.02,0.51,kBlue,Form("#chi^{2}/ndf : %.4f", f3[1]->GetChisquare()/f3[1]->GetNDF()),0.6); 
		if( saveEPS) cc->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMB.eps"));
		cc->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMB.png"));
		cc->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMB.pdf"));
		delete cc;

		// EMEC
		TCanvas *cd = new TCanvas( Form("cd_sum"), Form("cd_sum"), 800,700);
		h_EMEC_res[1]->SetLineColor(kBlue);
		h_EMEC_res[1]->SetMarkerColor(kBlue);

		std::cout << "params: " << p0[2] << " " << p1[2] << "  " << p0[3] << " " << p1[3] << std::endl;
		h_EMEC_res[1]->GetYaxis()->SetTitle("Time Resolution [ns]");
		h_EMEC_res[1]->GetXaxis()->SetTitle("Energy [GeV]");
		h_EMEC_res[1]->GetYaxis()->SetRangeUser(0.20, .5);
		h_EMEC_res[1]->GetXaxis()->SetRangeUser(0, h_EMEC_res[1]->GetXaxis()->GetBinLowEdge(h_EMEC_res[1]->GetXaxis()->FindBin(300)));
		//	h_EMEC_res[1]-> SetErrorOption("g"); 
		h_EMEC_res[1]->Draw("p");
		h_EMEC_res[0]->Draw("same");
		f3[2]->SetLineColor(kRed);
		f3[2]->Draw("same");
		f3[3]->SetLineColor(kRed);
		f3[3]->Draw("same");
		//	cd->SetLogx(1);
		ATLASLabel(0.2,0.88,pInternal);
		myText(0.20, 0.81, kBlack, Form("#int L = 33.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
		myText(0.73, 0.88, kBlack, Form("Z#rightarrow ee"), 0.6);  
		//myText(0.73, 0.84, kBlack, Form("EMB"), 0.7); // FIXME
		myText(0.73, 0.84, kBlack, Form("EMEC "), 0.7);
		myText(0.73, 0.81, kBlack, Form("1.5 < |#eta| < 2.4"),0.6);
		//  myText(0.32, 0.71, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.75);
		myText(0.60, 0.75, kRed, Form("Fit Results: #sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.7);
		myText(0.67, 0.71-0.02, kBlack, Form("High"), 0.6);
		myText(0.73, 0.71-0.02, kBlack, Form("p_{0}: %.3f", p0[2]), 0.6);
		myText(0.73, 0.68-0.02, kBlack, Form("p_{1}: %.3f", p1[2]), 0.6);
		myText(0.70-0.02, 0.65-0.03, kBlack,Form("#chi^{2}/ndf : %.4f", f3[2]->GetChisquare()/f3[2]->GetNDF()),0.6); 
		myText(0.63, 0.65-0.07, kBlue, Form("Medium"), 0.6);
		myText(0.73, 0.65-0.07, kBlue, Form("p_{0}: %.3f", p0[3]), 0.6);
		myText(0.73, 0.62-0.07, kBlue, Form("p_{1}: %.3f", p1[3]), 0.6);
		myText(0.70-0.02,0.51,kBlue,Form("#chi^{2}/ndf : %.4f", f3[3]->GetChisquare()/f3[3]->GetNDF()),0.6); 
		if( saveEPS) cd->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMEC.eps"));
		cd->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMEC.png"));
		cd->SaveAs(Form("~/WWW/tr_energy/h_eResolution_EMEC.pdf"));
		delete cd;
		return;

	}
	*/
//_____________________________________________________________
//
// Make Time resolution plots for Zee events by slot
// Also summary plots for each subdetector
//_____________________________________________________________
   void plotZeeResolution( TH2F* t_e_A[2][11], TH2F* t_e_C[2][11]){
   // For making total plots
   TH2F * h_EMB[2]; 
   TH1F * h_EMB_res[2];
   TH2F * h_EMEC[2]; 
   TH1F * h_EMEC_res[2];
   TF1 * f3[4];
   double p0[4];
   double p1[4];

// loop over gains
for( int g = 0; g < 2; g++){

h_EMB[g] = new TH2F(Form("h_EMEC%s",gain[g].c_str()),Form("h_EMEC%s",gain[g].c_str()),250, 0, 250, 500, -5, 5);
h_EMEC[g]= new TH2F(Form("h_EMB%s",gain[g].c_str()),Form("h_EMB%s",gain[g].c_str()),250, 0, 250, 500, -5, 5);

// loop over slots (combine A&C)
for( int sl = 0; sl < 11; sl++){

// Add A/C side histograms
t_e_A[g][sl]->Add(t_e_C[g][sl]); 
// Summary FIXME
   if( sl < 5)
   h_EMB[g]->Add( t_e_A[g][sl] );
   else if(sl>=5&&sl<11) 
   h_EMEC[g]->Add( t_e_A[g][sl] );

// Make sure histogram is not empty
if( !t_e_A[g][sl]->GetEntries() ) {
std::cout << "  >> Slot: " << slotIndexNames[sl] << " has no data for " 
<< gain[g] << " gain, no plots will be made!\n";
continue;
}

// Variable Rebin the 2D histogram with threshold of entries per bin
int binMin = ( g == 0 ? 150 : 200 );
if(g==0&&sl==8)
	binMin=100;
if(sl==10){
	t_e_A[g][sl]->Add(t_e_A[g][9]);
	t_e_A[g][sl]->Add(t_e_A[g][8]);
}
t_e_A[g][sl] = rebin( t_e_A[g][sl], t_e_A[g][sl]->GetEntries()/binMin, 5);  
t_e_A[g][sl]->SetName(Form("%s_%d",t_e_A[g][sl]->GetName(),g));

// Find RMS of gaussian fit for each slice in energy
TObjArray aSlices;
binMin = ( g == 0 ? 10 : 20 );
// 0->gaus, 0,-1 -> fit all bins, 20->bin threshold, QNR plot options, aslices->store fit histo
t_e_A[g][sl]->FitSlicesY(0, 0, -1, binMin, "QNR", &aSlices); 
TH1F * h_res = new TH1F( Form("hres%d%d", g, sl), Form("hres%d%d", g, sl), 
t_e_A[g][sl]->GetNbinsX(), t_e_A[g][sl]->GetXaxis()->GetXbins()->GetArray());
h_res = (TH1F*)aSlices[2]; // [2] is the sigma of gaussian fit for the slice

// Make plot for each slot
TCanvas *c_res = new TCanvas( Form("c_res%s%d", gain[g].c_str(), sl), Form("c_res%s%d", gain[g].c_str(), sl), 800, 700);
h_res->Draw();

// Find fit range
double enLow  = -99;
double enHigh = -99;
int nBins = h_res->GetNbinsX();
for (int b=1; b <= nBins; b++){ // bins in en
if( h_res->GetBinContent(b) > 0 ){
	
if( enLow == -99 ){
	if(g==0&&(sl==7))
       	enLow = h_res->GetBinCenter(b+1);
	else if(g==0&&(sl==8))
		enLow = h_res->GetBinCenter(b+1);              
	else if(g==0&&sl>=9)
		enLow = h_res->GetBinCenter(b+1);  
	else
    enLow = h_res->GetBinCenter(b);    
}
enHigh = h_res->GetBinCenter(b); 
}
}

// Cut off ranges (40 for Hi/ 28 for MED)
if(g == 0 && enHigh > 40 )
enHigh = 40;
if(g == 0 && enHigh > 40 )
enHigh = 40;
if(g == 1 && enLow < 40 ){
if( sl < 2 && enLow < 28)
enLow = 28;
else
enLow = 40;
}
if( debug ) {
std::cout << "Index:  " << sl << ";  Slot: "  << slotIndexNames[sl] 
<< "; low:  " << enLow << "; high: " << enHigh << std::endl;
}

// Declare fit
TF1 *f1 = new TF1( "f1", "sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
f1->SetLineColor(kRed);
h_res->Fit("f1","QNR");
f1->Draw("same");
double pp0 = f1->GetParameter(0);
double pp1 = f1->GetParameter(1);

// Put text on plot
h_res->GetYaxis()->SetTitle("Time Resolution [ns]");
if( sl < 9 || g == 1 )
h_res->GetYaxis()->SetRangeUser(0.2, 0.7);
if(g==0&&sl==8)
h_res->GetYaxis()->SetRangeUser(0.2, 0.4);

h_res->GetXaxis()->SetRangeUser(5, 250);
c_res->SetLogx(1);
ATLASLabel(0.2,0.88,pInternal);
myText(0.20, 0.83, kBlack, Form("Z#rightarrow ee"), 0.7);  
myText(0.20, 0.79, kBlack, Form("%s Gain", gain[g].c_str()), 0.6);
myText(0.73, 0.88, kBlack, Form("%s", slotIndexNames[sl].c_str()), 0.7);
myText(0.73, 0.84, kRed, Form("p_{0}: %.3f", pp0), 0.6);
myText(0.73, 0.81, kRed, Form("p_{1}: %.3f", pp1), 0.6);
if( saveEPS) c_res->SaveAs(Form("plots/Zplots/h_eResolution_sl%d_%s.eps", sl, gain[g].c_str()));
c_res->SaveAs(Form("plots/Zplots/h_eResolution_sl%d_%s.png", sl, gain[g].c_str()));
delete c_res;

} // end loop over slots

// Subdetector summary resolution
// EMB
int binMin = (g == 0 ?  30000 : 2800 ); // 25000,4000
h_EMB[g] = rebin( h_EMB[g], binMin, 5); 
h_EMB[g]->SetName(Form("%s_%d",h_EMB[g]->GetName(),g));
binMin = ( g == 0 ? 40 : 40 ); // 10, 29
double enLow  = -99;
double enHigh = -99;
// Find fit range
int nBins = h_EMB[g]->GetNbinsX();
std::cout << " NBins:  " << nBins << std::endl;
for (int b=1; b <= nBins; b++){ // bins in en
	if( h_EMB[g]->Integral(b,b) > binMin ){
		if( enLow == -99 ) enLow = h_EMB[g]->GetXaxis()->GetBinCenter(b);
		enHigh = h_EMB[g]->GetXaxis()->GetBinCenter(b);
		std::cout << "Bin: " << b << " ; Bin Center: " << h_EMB[g]->GetXaxis()->GetBinCenter(b) << "; Bin Integral: " << h_EMB[g]->Integral(b,b) << std::endl;
	}
}
// FIXME originally 27
if(g == 0 && enHigh > 23.5 )
	enHigh = 23.5;
if(g == 1 && enLow < 28.5 )
	enLow = 28.5;
	int lowBin = h_EMB[g]->GetXaxis()->FindBin(enLow);
	int highBin = h_EMB[g]->GetXaxis()->FindBin(enHigh);
	std::cout << "low bin :  " << lowBin << "; high bin: " << highBin << std::endl;
	TObjArray bSlices;
	h_EMB[g]->FitSlicesY(0, lowBin, highBin, binMin, "QNR", &bSlices); // bins was 0, -1 
	TH1F *h_res = new TH1F(Form("hhres%d",g),Form("hhres%d",g), h_EMB[g]->GetNbinsX(), h_EMB[g]->GetXaxis()->GetXbins()->GetArray());
	h_res = (TH1F*)bSlices[2]; // [2] is the sigma of gaussian fit for the slice
	// Declare fit
	TF1 *f2 = new TF1("f2","sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
	h_res->Fit( "f2","QNR");
	//f1[g]->SetLineColor(kRed);
	p0[g] = f2->GetParameter(0);
	p1[g] = f2->GetParameter(1);
	std::cout << " p0 :  " << p0[g] << " p1: " << p1[g] << std::endl;
	std::cout << " enL :  " << enLow << " enHigh: " << enHigh << std::endl;
	h_EMB_res[g] = (TH1F*)h_res->Clone("");
	f3[g] = (TF1*)f2->Clone("");
	delete f2;
	delete h_res;
	// EMEC
	binMin = (g == 0 ?  10000 : 6000 ); // 25000,4000
	h_EMEC[g] = rebin( h_EMEC[g], binMin, 5); 
	h_EMEC[g]->SetName(Form("%s_%d",h_EMEC[g]->GetName(),g));
	binMin = ( g == 0 ? 40 : 40 ); // 10, 29
	enLow  = -99;
	enHigh = -99;
	// Find fit range
	nBins = h_EMEC[g]->GetNbinsX();
	std::cout << " NBins:  " << nBins << std::endl;
	for (int b=1; b <= nBins; b++){ // bins in en
		if( h_EMEC[g]->Integral(b,b) > binMin ){
			if( enLow == -99 ) enLow = h_EMEC[g]->GetXaxis()->GetBinCenter(b);
			enHigh = h_EMEC[g]->GetXaxis()->GetBinCenter(b);
			std::cout << "Bin: " << b << " ; Bin Center: " << h_EMEC[g]->GetXaxis()->GetBinCenter(b) << "; Bin Integral: " << h_EMEC[g]->Integral(b,b) << std::endl;
		}
	}
// FIXME originally 35
if(g == 0 && enHigh > 35 )
	enHigh = 35;
if(g == 1 && enLow < 39 )
	enLow =39;
	lowBin = h_EMEC[g]->GetXaxis()->FindBin(enLow);
	highBin = h_EMEC[g]->GetXaxis()->FindBin(enHigh);
	std::cout << "low bin :  " << lowBin << "; high bin: " << highBin << std::endl;
	TObjArray cSlices;
	h_EMEC[g]->FitSlicesY(0, lowBin, highBin, binMin, "QNR", &cSlices); // bins was 0, -1 
	TH1F *h_res2 = new TH1F(Form("hhres2%d",g),Form("hhres2%d",g), h_EMEC[g]->GetNbinsX(), h_EMEC[g]->GetXaxis()->GetXbins()->GetArray());
	h_res2 = (TH1F*)cSlices[2]; // [2] is the sigma of gaussian fit for the slice
	// Declare fit
	if( g == 1) enHigh = 250;
	TF1 *f22 = new TF1("f22","sqrt( sq([0]/x) + sq([1]))", enLow, enHigh);
	h_res2->Fit( "f22","QNR");
	//f1[g]->SetLineColor(kRed);
	p0[g+2] = f22->GetParameter(0);
	p1[g+2] = f22->GetParameter(1);
	std::cout << " p0 :  " << p0[g+2] << " p1: " << p1[g+2] << std::endl;
	std::cout << " enL :  " << enLow << " enHigh: " << enHigh << std::endl;
	h_EMEC_res[g] = (TH1F*)h_res2->Clone("");
	f3[g+2] = (TF1*)f22->Clone("");
	delete f22;
	delete h_res;

	} // end loop over gains
// EMB
TCanvas *cc = new TCanvas( Form("cc_sum"), Form("cc_sum"), 800,700);
h_EMB_res[0]->Draw();
h_EMB_res[1]->SetLineColor(kBlue);
h_EMB_res[1]->SetMarkerColor(kBlue);
h_EMB_res[1]->Draw("same");
f3[0]->SetLineColor(kRed);
f3[0]->Draw("same");
f3[1]->SetLineColor(kRed);
f3[1]->Draw("same");
std::cout << "params: " << p0[0] << " " << p1[0] << "  " << p0[1] << " " << p1[1] << std::endl;
h_EMB_res[0]->GetYaxis()->SetTitle("Time Resolution [ns]");
h_EMB_res[0]->GetXaxis()->SetTitle("Energy [GeV]");
h_EMB_res[0]->GetYaxis()->SetRangeUser(0.15, .55);
h_EMB_res[0]->GetXaxis()->SetRangeUser(4, 250);
cc->SetLogx(1);
ATLASLabel(0.2,0.88,pInternal);
myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
myText(0.73, 0.88, kBlack, Form("Z#rightarrow ee data"), 0.6);  
//myText(0.73, 0.84, kBlack, Form("EMB"), 0.7); // FIXME
myText(0.73, 0.84, kBlack, Form("EMB"), 0.7);
myText(0.73, 0.81, kBlack, Form("0 < |#eta| < 1.4"),0.6);
myText(0.32, 0.71, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.75);
myText(0.73, 0.75, kBlack, Form("Fit Results:"),0.7);
myText(0.67, 0.71, kBlack, Form("High"), 0.6);
myText(0.73, 0.71, kRed, Form("p_{0}: %.3f", p0[0]), 0.6);
myText(0.73, 0.68, kRed, Form("p_{1}: %.3f", p1[0]), 0.6);
myText(0.63, 0.65, kBlue, Form("Medium"), 0.6);
myText(0.73, 0.65, kRed, Form("p_{0}: %.3f", p0[1]), 0.6);
myText(0.73, 0.62, kRed, Form("p_{1}: %.3f", p1[1]), 0.6);
if( saveEPS) cc->SaveAs(Form("plots/Zplots/h_eResolution_EMB.eps"));
cc->SaveAs(Form("plots/Zplots/h_eResolution_EMB.png"));

cc->SaveAs(Form("plots/Zplots/h_eResolution_EMB.pdf"));
delete cc;

// EMEC
TCanvas *cd = new TCanvas( Form("cd_sum"), Form("cd_sum"), 800,700);
h_EMEC_res[0]->Draw();
h_EMEC_res[1]->SetLineColor(kBlue);
h_EMEC_res[1]->SetMarkerColor(kBlue);
h_EMEC_res[1]->Draw("same");
f3[2]->SetLineColor(kRed);
f3[2]->Draw("same");
f3[3]->SetLineColor(kRed);
f3[3]->Draw("same");
std::cout << "params: " << p0[2] << " " << p1[2] << "  " << p0[3] << " " << p1[3] << std::endl;
h_EMEC_res[0]->GetYaxis()->SetTitle("Time Resolution [ns]");
h_EMEC_res[0]->GetXaxis()->SetTitle("Energy [GeV]");
h_EMEC_res[0]->GetYaxis()->SetRangeUser(0.15, .50);
//h_EMEC_res[0]->GetYaxis()->SetRangeUser(0.15, .45);
h_EMEC_res[0]->GetXaxis()->SetRangeUser(4, 250);
cd->SetLogx(1);
ATLASLabel(0.2,0.88,pInternal);
myText(0.20, 0.81, kBlack, Form("#int L = 3.3 fb^{-1}, #sqrt{s}=13 TeV"), 0.7);
myText(0.73, 0.88, kBlack, Form("Z#rightarrow ee data"), 0.6);  
//myText(0.73, 0.84, kBlack, Form("EMEC"), 0.7); // FIXME
myText(0.73, 0.84, kBlack, Form("EMEC"), 0.7);
myText(0.73, 0.81, kBlack, Form("1.5 < |#eta| < 2.4"),0.6);
myText(0.32, 0.71, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"),0.75);
myText(0.73, 0.75, kBlack, Form("Fit Results:"),0.7);
myText(0.67, 0.71, kBlack, Form("High"), 0.6);
myText(0.73, 0.71, kRed, Form("p_{0}: %.3f", p0[2]), 0.6);
myText(0.73, 0.68, kRed, Form("p_{1}: %.3f", p1[2]), 0.6);
myText(0.63, 0.65, kBlue, Form("Medium"), 0.6);
myText(0.73, 0.65, kRed, Form("p_{0}: %.3f", p0[3]), 0.6);
myText(0.73, 0.62, kRed, Form("p_{1}: %.3f", p1[3]), 0.6);
if( saveEPS) cd->SaveAs(Form("plots/Zplots/h_eResolution_EMEC.eps"));
cd->SaveAs(Form("plots/Zplots/h_eResolution_EMEC.png"));
cd->SaveAs(Form("plots/Zplots/h_eResolution_EMEC.pdf"));
delete cd;
return;

}

