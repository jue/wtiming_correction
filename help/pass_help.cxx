//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"

///*
//_____________________________________________
//
// Make plots for resolution with Zee data 
//_____________________________________________
void pass10(){

	std::cout << "\nMaking plots for Z Timing (Pass 8)\n\n";

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass10.root","READ"); // for IOV1/2 by itself
	//file = TFile::Open("files/combinedZ/pass8_comb.root","READ"); // for combined data


	//-- Make t1 and t2 plots--//

	// Make string to read plots for t1/t2
	std::string hname[5] = {"h_t1plust2Zee","h_t1minust2Zee",
		"h_t1Zee","h_t2Zee","h_t1vst2Zee"};
	// Make histograms to hold copies
	TH2F* h_2D; 
	TH1F* h_1D[4];

	// Read histos from file
	for( int h=0; h<5; h++){
		if( h==4 ) h_2D    = (TH2F*)file->Get(hname[h].c_str());
		else       h_1D[h] = (TH1F*)file->Get(hname[h].c_str());
	}

	// Make the leading/subleading electron plots
	plotZeet1t2( h_2D, h_1D);
	TH2F* tempa;
	TH2F* tempc;

	//-- Make time resolution plots --//
	TH2F* h_en_t_sl_a[2][11];
	TH2F* h_en_t_sl_c[2][11];
	for( int g=0; g<2; g++){
		for( int in = 0; in < 11; in++){
			std::string nameA = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexASide[in]);
			std::string nameC = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexCSide[in]);
			h_en_t_sl_a[g][in] = (TH2F*)file->Get(nameA.c_str());
			h_en_t_sl_c[g][in] = (TH2F*)file->Get(nameC.c_str());
			if(g==0){
				switch(in){
					case 7:
						tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),22));
						h_en_t_sl_a[g][in]->Add(tempa);
						cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
						break;
					case 8:
						tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),23));
						tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),26));
						h_en_t_sl_a[g][in]->Add(tempa);
						h_en_t_sl_c[g][in]->Add(tempc);
						cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
						break;  
					case 9:
						tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),24));
						tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),27));
						h_en_t_sl_a[g][in]->Add(tempa);
						h_en_t_sl_c[g][in]->Add(tempc);
						cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
						break;  
					case 10:
						tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),25));
						tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),28));
						h_en_t_sl_a[g][in]->Add(tempa);
						h_en_t_sl_c[g][in]->Add(tempc);
						cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
				}


			}
		}
	}
	plotZeeResolution( h_en_t_sl_a, h_en_t_sl_c);
	return;

	file->Close();
	return;
}

/*

   void pass8(){

   std::cout << "\nMaking plots for Z Timing (Pass 8)\n\n";

// Open the data file
TFile *file;
// file = TFile::Open("files/pass8.root","READ"); // for IOV1/2 by itself
file = TFile::Open("files/pass10_new_dis_front.root","READ"); // for combined data



//-- Make time resolution plots --//
TH2F* h_en_t_sl_a[2][11];
TH2F* h_en_t_sl_c[2][11];
TH2F* tempa;
TH2F* tempc;
for( int g=0; g<2; g++){
for( int in = 0; in < 11; in++){
std::string nameA = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexASide[in]);
std::string nameC = Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(), slotIndexCSide[in]);
h_en_t_sl_a[g][in] = (TH2F*)file->Get(nameA.c_str());
h_en_t_sl_c[g][in] = (TH2F*)file->Get(nameC.c_str());
if(g==0){
switch(in){
case 7:
tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),22));
h_en_t_sl_a[g][in]->Add(tempa);
cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
break;
case 8:
tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),23));
tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),26));
h_en_t_sl_a[g][in]->Add(tempa);
h_en_t_sl_c[g][in]->Add(tempc);
cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
break;  
case 9:
tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),24));
tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),27));
h_en_t_sl_a[g][in]->Add(tempa);
h_en_t_sl_c[g][in]->Add(tempc);
cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
break;  
case 10:
tempa = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),25));
tempc = (TH2F*)file->Get(Form("h_AvgEnergyTime_%s_%d", gain[g].c_str(),28));
h_en_t_sl_a[g][in]->Add(tempa);
h_en_t_sl_c[g][in]->Add(tempc);
cout<<"add subslot"<<in<<"\t"<<slotIndexASide[in]<<endl;
}


}
}
plotZeeResolution( h_en_t_sl_a, h_en_t_sl_c);
return;

file->Close();
return;
}
}
*/
//_____________________________________________
//
// Pass8: find the average FT time
// by run and gain.
//________________________________________________
void pass8(){

	std::cout<< "Finding Corrections for Pass 0\n" <<std::endl;

	// Initialize the feb corrections
	for( int i = 0; i < 3; i++){
		for( int j = 0; j < NRUNS; j++){
			for( int k = 0; k < 114; k++){
				ftTime_0[i][j][k] = 0;
			}
		}
	} 

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass8.root","READ");
	std::cout<< "Opened root file pass8.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/runByRunFTOffsetsH_2nd.dat");
	ofstream f3("config/runByRunFTOffsetsM_2nd.dat");
	//ofstream f4("config/runByRunFTOffsetsL_2nd.dat");

	// Loop over the gains
	for (int g = 0; g < 2; g++){

		// Loop over all the runs
		for (int rn = 0; rn < NRUNS; rn++){

			// Make names for the histograms to open and profile plots
			std::string pname = Form("Prof run %s%d",gain[g].c_str(),rn);
			std::string hname = Form("h_RunAvgFTTime_%s%d",gain[g].c_str(),rn);

			// Open the histogram, continue if empty 
			TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
			if( !f_rn->GetEntries() ) {
				std::cout << "  >> Run index: " << rn << " has no data for " 
					<< gain[g] << " gain, corrections will be zero!\n";
				continue;
			}
			// Make a profile plot to find avg time for each feb
			TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");

			// Find nbins to loop over
			int nbins =tpf->GetXaxis()->GetNbins();

			// For each feb, store correction
			for(int j=0; j<nbins; j++){
				// bins start at 1, but feb numb start at 0
				double i_time = tpf->GetBinContent(j+1); 
				if( i_time != 0){
					// rn is the run number index, j is the feb index, g is gain
					ftTime_0[g][rn][j] = i_time;
				}
			}

		} // end loop over runs

	} // end loop over gains

	// Store the avg times:
	for(int ft = 0; ft < 114; ft++){
		f2 << ft;
		f3 << ft;
		//   f4 << ft;
		for(int rn = 0; rn < NRUNS; rn++){
			f2 << " " << ftTime_0[0][rn][ft];
			f3 << " " << ftTime_0[1][rn][ft];
			//   f4 << " " << ftTime_0[2][rn][ft];
		}
		f2 << "\n";
		f3 << "\n";
		//	    f4 << "\n";
	}
	std::cout << "Stored Run Corrections to runByRunFTOffsets.dat successfully\n\n";

	f2.close();
	f3.close();
	//	  f4.close();
	file->Close(); 

	std::cout << "Files successfully closed\n\n";

	return;
}

//_____________________________________________
//
// Pass7: find the average Channel time
//        by gain
//________________________________________________
void pass7(){
	std::cout<< "Finding Corrections for Pass 6\n" <<std::endl;

	// Initialize the feb corrections
	for( int j = 0; j < 79360; j++){
		for( int g = 0; g < 2; g++){
			chTime[g][j] = 0;
		}
	}

	// Open the data file
	TFile *file;
	std::cout<<"lala"<<endl;
	file = TFile::Open("files/pass7.root","READ");
	std::cout<< "Opened root file pass7.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/AvgChOffsetsp6H.dat");
	ofstream f3("config/AvgChOffsetsp6M.dat");


	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Make names for the histograms to open and profile plots
		std::string pname = Form("Prof Avg Ch Time %s",gain[g].c_str() );
		std::string hname = Form("h_AvgChannelTime_%s",gain[g].c_str() );

		// Open the histogram, continue if empty
		TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
		if( !f_rn->GetEntries() ) {
			std::cout << "  >> Histogram has no data for gain " 
				<< gain[g] << ", corrections will be zero!\n";
			continue;
		}

		// Make a profile plot to find avg time for each feb
		TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
		tpf->GetYaxis()->SetRangeUser(-5,5);
		tpf->GetYaxis()->SetTitle("Avg Time [ns]");

		// Find nbins to loop over
		int nbins =tpf->GetXaxis()->GetNbins();

		// For non-empty febs, store correction
		for(int j=0; j<nbins; j++){
			// bins start at 1, but ch numb start at 0
			double i_time = tpf->GetBinContent(j+1);
			if( i_time != 0){
				//  j is the feb index, g is the gain index
				chTime[g][j] = i_time;
			}
		}

		// Make the plo
		TCanvas *c1 = new TCanvas("c1","c1", 800, 700);
		tpf->Draw();
		if(saveEPS) c1->SaveAs(Form("plots/pass7/AvgChCorrections_%s.eps",gain[g].c_str()));
		c1->SaveAs(Form("plots/pass7/AvgChCorrections_%s.png",gain[g].c_str()));
		delete c1;

	} // end loop over gains

	// Store the avg times:
	for(int ch = 0; ch < 79360; ch++){
		f2 << ch << " " << chTime[0][ch] << std::endl;
		f3 << ch << " " << chTime[1][ch] << std::endl;
	}

	std::cout << "Stored Run Corrections to AvgChOffsetsp6.dat successfully\n\n";

	f2.close();
	f3.close();
	file->Close();
	std::cout << "Files successfully closed\n\n";

	return;


}


//_____________________________________________
//
// Pass6: find the average time correction vs bunch train position
//        per slot  by gain
//________________________________________________
void pass6(){

	std::cout<< "Finding Corrections for Pass 9\n" <<std::endl;

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass6.root","READ");
	std::cout<< "Opened root file pass6.root successfully\n\n";


	ofstream f[2];
	int num[3][23]={0};
	// Loop over the gains
	for (int g = 0; g < 2; g++){
		f[g].open(Form("config/AvgBunchOffsets%d.dat", g));

		// Loop over all the runs
		for (int sl= 0; sl < 22; sl++){

			// Make names for the histograms to open and profile plots
			std::string pname = Form("Prof run %s%d",gain[g].c_str(),sl);
			std::string hname = Form("h_timeVsbunchpos%s%d",gain[g].c_str(),sl);

			// Open the histogram, continue if empty 
			TH2F *f_bunch = (TH2F*)file->Get(hname.c_str());
			if( !f_bunch->GetEntries() ) {
				std::cout << "  >> Run index: " <<sl << " has no data for " 
					<< gain[g] << " gain, corrections will be zero!\n";
				continue;
			}
			//  MMake a profile plot to find avg time for each feb
			TCanvas *cw2 = new TCanvas("c_ma1","c_mw1",800,700);
			TProfile *tpf  = f_bunch->ProfileX(pname.c_str(),-1,-1,"o");

			tpf->Draw("e"); 
			//	tpf->SetErrorOption("g"); 	
			tpf->GetXaxis()->SetTitle("Distance from beginning of train [ns]");
			tpf->GetYaxis()->SetTitle("time(ns)");
			if(sl==14||sl==15||sl==20||sl==21){
				tpf->GetYaxis()->SetRangeUser(-0.1,0.1);  
			}
			else{
				tpf->GetYaxis()->SetRangeUser(-0.05,0.05);
			}


			num[g][sl] = f_bunch->GetEntries();
			num[g][22] +=f_bunch->GetEntries();        
			tpf-> GetXaxis()-> SetRangeUser(f_bunch->GetXaxis()->GetBinLowEdge(f_bunch->FindFirstBinAbove(0)),f_bunch->GetXaxis()->GetBinLowEdge(f_bunch->FindLastBinAbove(0)+1));
			myText(0.72,0.79,kRed,Form("%s Gain",gain[g].c_str()),0.8);
			myText(0.72,0.86,kBlack,Form("%s",slotNames[sl].c_str()),0.7); 
			myText(0.72, 0.83,kBlack,Form("Entries: %d",num[g][sl] ),0.6);
			cw2->SaveAs(Form("plots/pass6/distance_time%d_%s.png",sl,gain[g].c_str()));
			delete cw2;
			// Find nbins to loop over
			int nbins =tpf->GetXaxis()->GetNbins();
			f[g]<<sl;

			// For each feb, store correction
			for(int j=0; j<nbins; j++){
				// bins start at 1, but feb numb start at 0

				double i_time = tpf->GetBinContent(j+1); 
				//   if( i_time != 0){
				// bunch is the run number index, j is the feb index, g is gain
				// ftTime_0[g][bunch][j] = i_time;
				f[g]<<" "<<i_time;
				//  cout<<i_time<<endl;
				//	}
			}

			f[g]<<endl; 
		} // end loop over sl
		cout<<num[g][22]<<endl;
		f[g].close();
	} // end loop over gains

	std::cout << "Stored Run Corrections to BunchOffsets.dat successfully\n\n";

	file->Close(); 

	std::cout << "Files successfully closed\n\n";

	return;
}


//_____________________________________________
//
// Pass5: find the average time correction
//        per f1/f3 by gain
//________________________________________________
void pass5(){

	std::cout<< "Finding Corrections for Pass 5\n" <<std::endl;

	// Show fit stats
	gStyle->SetOptFit(1111);

	// Store fit range
	double f1minX[2][22];
	double f1maxX[2][22];
	double f3minX[2][22];
	double f3maxX[2][22];

	// Initialize the en frac corrections
	for( int g = 0; g < 2; g++){
		for( int s = 0; s < 22; s++){
			for( int p = 0; p < 2; p++){
				f1Fit[g][s][p] = 0;
				f3Fit[g][s][p] = 0;
			}
			f1minX[g][s] = -99;
			f1maxX[g][s] = -99;
			f3minX[g][s] = -99;
			f3maxX[g][s] = -99;
		}
	}

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass5.root","READ");
	std::cout<< "Opened root file pass5.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/Avgf1f3SlFitH.dat");
	ofstream f3("config/Avgf1f3SlFitM.dat");

	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Loop over slots
		for( int sl = 0; sl < 22; sl++){

			// Make names for the histograms to open and profile plots
			std::string f1name = Form("Prof Avg f1 Time %s Slot%d",gain[g].c_str(),sl );
			std::string f3name = Form("Prof Avg f3 Time %s Slot%d",gain[g].c_str(),sl );
			std::string h1name = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),sl );
			std::string h3name = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),sl );

			// Open the histogram, continue if empty
			TH2F *f_f1 = (TH2F*)file->Get(h1name.c_str());
			TH2F *f_f3 = (TH2F*)file->Get(h3name.c_str());
			if( !f_f1->GetEntries() ) {
				std::cout << "  >> Histogram has no data for gain " << gain[g] << " slot " << sl 
					<< ", corrections will be zero!\n";
				continue;
			}
			// Rebin after 0!
			int binMinf1 = f_f1->GetEntries()/100;      
			int binMinf3 = f_f3->GetEntries()/100;
			f_f1 = rebin( f_f1, binMinf1, f_f1->GetXaxis()->FindBin(0.) -1 );
			f_f3 = rebin( f_f3, binMinf3, f_f3->GetXaxis()->FindBin(0.) -1 );
			f_f1->SetName( Form("%s%d%d",f_f1->GetName(),g,sl));   
			f_f3->SetName( Form("%s%d%d",f_f3->GetName(),g,sl)); 

			// Make a profile plot to find avg time for each slot
			TProfile *tp1  = f_f1->ProfileX(f1name.c_str(),-1,-1, "o");
			TProfile *tp3  = f_f3->ProfileX(f3name.c_str(),-1,-1, "o");
			tp1->GetXaxis()->SetTitle("f1");
			tp3->GetXaxis()->SetTitle("f3");
			tp1->GetYaxis()->SetRangeUser(tmin,tmax);
			tp3->GetYaxis()->SetRangeUser(tmin,tmax);
			tp1->GetYaxis()->SetTitle("Time [ns]");
			tp3->GetYaxis()->SetTitle("Time [ns]");

			// Set Fit Range
			f1minX[g][sl] = tp1->GetXaxis()->GetBinCenter(tp1->GetXaxis()->FindBin(0.)+2);
			f1maxX[g][sl] = tp1->GetXaxis()->GetBinCenter(tp1->GetXaxis()->FindBin(0.59)-1);
			f3minX[g][sl] = tp3->GetXaxis()->GetBinCenter(tp3->GetXaxis()->FindBin(0.)+1);
			///CJ///
			if(g==0&&(sl==15||sl==20||sl==21))
				f3maxX[g][sl] = tp3->GetXaxis()->GetBinCenter(tp3->GetXaxis()->FindBin(0.04)-2);
			else
				f3maxX[g][sl] = tp3->GetXaxis()->GetBinCenter(tp3->GetXaxis()->FindBin(0.19)-1);
			if( debug){
				std::cout << "Gain: " << g << "; Slot: " << sl <<std::endl;
				std::cout << "  f1minX: " << f1minX[g][sl] << ";  f1maxX: " << f1maxX[g][sl] <<std::endl;
				std::cout << "  f3minX: " << f3minX[g][sl] << ";  f3maxX: " << f3maxX[g][sl] <<std::endl;
			}   
			// Make a fit
			TF1 *ff1 = new TF1("ff1", "pol1", f1minX[g][sl], f1maxX[g][sl]);
			TF1 *ff3 = new TF1("ff3", "pol1", f3minX[g][sl], f3maxX[g][sl]);
			int fit_r1;
			// EMEC sl 14/15 don't have f1 data!
			if( sl == 14 || sl == 15 || sl == 20 || sl == 21 ){
				ff1->FixParameter(0,0.); // forcing no fit
				ff1->FixParameter(1,0.); // forcing no fit
				fit_r1 = -1; // foreced not to fit
			}else{
				fit_r1 = tp1->Fit("ff1","RQ"); // F "S" gives TFitResultPtr
			}
			int fit_r3 = tp3->Fit("ff3","RQ");
			ff1->SetLineColor(kRed);
			ff3->SetLineColor(kRed);
			// If fit unsuccessful
			if( fit_r1 != 0 )
				std::cout << gain[g].c_str() << " gain " << slotNames[sl].c_str() << std::endl
					<< "  f1 fit unsuccessful, result status: "  << fit_r1 << std::endl;
			if( fit_r3 != 0)
				std::cout << gain[g].c_str() << " gain " << slotNames[sl].c_str() << std::endl
					<< "  f3 fit unsuccessful, result status: "  << fit_r3 << std::endl;

			// Store Fit Params
			for(int p=0; p<2; p++){
				f1Fit[g][sl][p] = ff1->GetParameter(p);
				f3Fit[g][sl][p] = ff3->GetParameter(p);
			}

			// Make the f1 plot
			TCanvas *cf1 = new TCanvas(Form("cdf1%d%d",g,sl),Form("cdf1%d%d",g,sl), 800, 700);
			tp1->Draw();
			tp1->GetXaxis()->SetRangeUser( tp1->GetXaxis()->GetBinLowEdge( tp1->GetXaxis()->FindBin(0.)),.6);
			tp1->SetErrorOption("g"); 
			tp1->Draw();
			//     myText(0.83,0.71,kBlue,Form("#chi^{2}/ndf : %.3f", ff1->GetChisquare()/ff1->GetNDF()),0.6); 
			ff1->Draw("sames");
			drawFitInfo(g, sl);
			if(saveEPS) cf1->SaveAs(Form("/pass5/pass5/efrac/Sl%dcellf1_%s.eps",sl,gain[g].c_str()));
			cf1->SaveAs(Form("/pass5/pass5/efrac/Sl%dcellf1_%s.png",sl,gain[g].c_str()));
			delete cf1;

			// Make the f3 plot
			TCanvas *cf3 = new TCanvas(Form("cdf3%d%d",g,sl),Form("cdf3%d%d",g,sl), 800, 700);
			tp3->Draw();
			tp3->GetXaxis()->SetRangeUser( tp3->GetXaxis()->GetBinLowEdge( tp3->GetXaxis()->FindBin(0.)+1),0.1);
			tp3->SetErrorOption("g"); 
			tp3->Draw();
			//      myText(0.83,0.71,kBlue,Form("#chi^{2}/ndf : %.3f", ff3->GetChisquare()/ff3->GetNDF()),0.6); 
			ff3->Draw("sames");
			cf3->SetLogx(1);
			drawFitInfo(g, sl);
			if(saveEPS) cf3->SaveAs(Form("pass5/pass5/efrac/Sl%dcellf3_%s.eps",sl,gain[g].c_str()));
			cf3->SaveAs(Form("pass5/pass5/efrac/Sl%dcellf3_%s.png",sl,gain[g].c_str()));
			delete cf3;

		}// end loop over slots

	} // end loop over gain

	// Store the avg times:
	for(int sl = 0; sl < 22; sl++){
		f2 << sl << " " << f1minX[0][sl] << " " << f1maxX[0][sl]
			<< " " << f3minX[0][sl] << " " << f3maxX[0][sl];
		f3 << sl << " " << f1minX[1][sl] << " " << f1maxX[1][sl]
			<< " " << f3minX[1][sl] << " " << f3maxX[1][sl];
		for(int p = 0; p < 4; p++){
			if ( p<2){
				f2 << " " << f1Fit[0][sl][p];
				f3 << " " << f1Fit[1][sl][p];
			} else{
				f2 << " " << f3Fit[0][sl][p-2];
				f3 << " " << f3Fit[1][sl][p-2];
			}
		}
		f2 << std::endl;
		f3 << std::endl;
	}

	std::cout << "Stored Run Corrections to Avgf1f3SlFit.dat successfully\n\n";

	f2.close();
	f3.close();
	file->Close();
	std::cout << "Files successfully closed\n\n";

	return;

}





//_____________________________________________
//
// Pass4: find the average time correction
//        per deta/dphi by gain
//________________________________________________
void pass4(){

	std::cout<< "Finding Corrections for Pass 4\n" <<std::endl;

	// Store fit range
	double pminX[3][22];
	double pmaxX[3][22];
	double eminX[3][22];
	double emaxX[3][22];

	// Initialize the dphi/deta corrections
	for( int g = 0; g < 3; g++){
		for( int s = 0; s < 22; s++){
			for( int p = 0; p < 7; p++){
				dphiFit[g][s][p] = 0;
				detaFit[g][s][p] = 0;
			}
			pminX[g][s] = -99;
			pmaxX[g][s] = -99;
			eminX[g][s] = -99;
			emaxX[g][s] = -99;
		}
	}

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass4.root","READ");
	std::cout<< "Opened root file pass4.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/AvgdPhidEtaSlFitH.dat");
	ofstream f3("config/AvgdPhidEtaSlFitM.dat");
	ofstream f4("config/AvgdPhidEtaSlFitL.dat");

	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Loop over slots
		for( int sl = 0; sl < 22; sl++){

			// Make names for the histograms to open and profile plots
			std::string pname = Form("Prof Avg dPhi Time %s Slot%d",gain[g].c_str(),sl );
			std::string ename = Form("Prof Avg dEta Time %s Slot%d",gain[g].c_str(),sl );
			std::string hpname = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),sl );
			std::string hename = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),sl );

			// Open the histogram, continue if empty
			TH2F *f_dp = (TH2F*)file->Get(hpname.c_str());
			TH2F *f_de = (TH2F*)file->Get(hename.c_str());
			if( !f_dp->GetEntries() ) {
				std::cout << "  >> Histogram has no data for gain " << gain[g] << " slot " << sl 
					<< ", corrections will be zero!\n";
				continue;
			}

			// Make a profile plot to find avg time for each slot
			TProfile *tpp  = f_dp->ProfileX(pname.c_str(),-1,-1,"o");
			TProfile *tpe  = f_de->ProfileX(ename.c_str(),-1,-1,"o");
			tpp->GetYaxis()->SetRangeUser(tmin,tmax);
			tpe->GetYaxis()->SetRangeUser(tmin,tmax);
			tpp->GetYaxis()->SetTitle("Time [ns]");
			tpe->GetYaxis()->SetTitle("Time [ns]");


			// low gain rebin:
			if( g == 2){
				tpp->Rebin(10);
				tpe->Rebin(5);
			}else{
				// Rebin dphi
				tpp->Rebin(2);
			}
			// Find the Max in x direction, and set dPhi fit range
			for (int b=1; b<= 400; b++){ // bins in dphi
				if( tpp->GetBinEntries(b) > 0 ){
					if( pminX[g][sl] == -99 ) pminX[g][sl] = tpp->GetBinCenter(b);
					pmaxX[g][sl] = tpp->GetBinCenter(b); 
				}
			}
			// Find the Max in x direction, and set dEta fit range
			for (int b=1; b<= 100; b++){ // bins in deta
				if( tpe->GetBinEntries(b) > 0 ){
					if( eminX[g][sl] == -99 ) eminX[g][sl] = tpe->GetBinCenter(b);
					emaxX[g][sl] = tpe->GetBinCenter(b); 
				}
			}
			if( debug ){
				std::cout << "Gain: " << g << "; Slot: " << sl << std::endl;
				std::cout << "  pminX: " << pminX[g][sl] << ";  pmaxX: " << pmaxX[g][sl] <<std::endl;
				std::cout << "  eminX: " << eminX[g][sl] << ";  emaxX: " << emaxX[g][sl] <<std::endl;
			}
			// 3 function fit!
			TF1 *fp_low = new TF1("fp_low", "pol0", pminX[g][sl], -0.5);
			TF1 *fe_low = new TF1("fe_low", "pol0", eminX[g][sl], -0.5);
			TF1 *fp_mid = new TF1("fp_mid", "pol4", -0.5, 0.5);
			TF1 *fe_mid = new TF1("fe_mid", "pol4", -0.5, 0.5);
			TF1 *fp_hi  = new TF1("fp_hi", "pol0", 0.5, pmaxX[g][sl]);
			TF1 *fe_hi  = new TF1("fe_hi", "pol0", 0.5, emaxX[g][sl]);
			// store paramters for fits
			double par_p[7];
			double par_e[7];
			// apply the fit
			tpp->Fit("fp_low","QR");
			tpe->Fit("fe_low","QR");
			int fit_rp = tpp->Fit("fp_mid","QR+");
			int fit_re = tpe->Fit("fe_mid","QR+");
			tpp->Fit("fp_hi","QR+");
			tpe->Fit("fe_hi","QR+");
			// retrieve fit params
			fp_low->GetParameters(&par_p[0]);
			fe_low->GetParameters(&par_e[0]);
			fp_mid->GetParameters(&par_p[1]);
			fe_mid->GetParameters(&par_e[1]);
			fp_hi->GetParameters(&par_p[6]);
			fe_hi->GetParameters(&par_e[6]);
			// set fit colors
			fp_low->SetLineColor(kBlue);
			fe_low->SetLineColor(kBlue);
			fp_mid->SetLineColor(kRed);
			fe_mid->SetLineColor(kRed);
			fp_hi->SetLineColor(kBlue);
			fe_hi->SetLineColor(kBlue);

			// If fit unsuccessful
			if( fit_rp != 0 )
				std::cout << gain[g].c_str() << " gain " << slotNames[sl].c_str() << std::endl
					<< "  dphi fit unsuccessful, result status: " << fit_rp << std::endl;
			if( fit_re != 0 )
				std::cout << gain[g].c_str() << " gain " << slotNames[sl].c_str() << std::endl
					<< "  deta fit unsuccessful, result status: " << fit_re << std::endl;

			// Store Fit Params
			for(int p=0; p<7; p++){
				dphiFit[g][sl][p] = par_p[p];
				detaFit[g][sl][p] = par_e[p];
			}

			// Make the dPhi plot
			TCanvas *cp = new TCanvas(Form("cdphi%d%d",g,sl),Form("cdphi%d%d",g,sl), 800, 700);
			tpp->Draw();
			fp_low->Draw("same");
			fp_mid->Draw("same");
			fp_hi->Draw("same");
			drawStatBox( par_p , "#delta#phi");
			//   myText(0.83,0.71,kBlue,Form("#chi^{2}/ndf : %.3f", f1->GetChisquare()/f1->GetNDF()),0.6); 
			drawFitInfo(g, sl);
			if(saveEPS) cp->SaveAs(Form("plots/pass4/angular/Sl%dcelldPhi_%s.eps",sl,gain[g].c_str()));
			cp->SaveAs(Form("plots/pass4/angular/Sl%dcelldPhi_%s.png",sl,gain[g].c_str()));
			delete cp;

			// Make the dEta plot
			TCanvas *ce = new TCanvas(Form("cdeta%d%d",g,sl),Form("cdeta%d%d",g,sl), 800, 700);
			tpe->GetXaxis()->SetRangeUser(-0.6,0.6);
			tpe->Draw();
			fe_low->Draw("same");
			fe_mid->Draw("same");
			fe_hi->Draw("same");
			drawStatBox( par_e , "#delta#eta");
			drawFitInfo(g, sl);
			if(saveEPS) ce->SaveAs(Form("plots/pass4/angular/Sl%dcelldEta_%s.eps",sl,gain[g].c_str()));
			ce->SaveAs(Form("plots/pass4/angular/Sl%dcelldEta_%s.png",sl,gain[g].c_str()));
			delete ce;

		}// end loop over slots

	} // end loop over gain

	// Store the avg times:
	// [g][sl][0] p0 const fit, [g][sl[1-5] p0-p4 fit, [g][sl][6] p0 const fit
	for(int sl = 0; sl < 22; sl++){
		f2 << sl << " " << dphiFit[0][sl][0] 
			<< " " << dphiFit[0][sl][6] 
			<< " " << detaFit[0][sl][0] 
			<< " " << detaFit[0][sl][6];
		f3 << sl << " " << dphiFit[1][sl][0]
			<< " " << dphiFit[1][sl][6]
			<< " " << detaFit[1][sl][0]
			<< " " << detaFit[1][sl][6];
		f4 << sl << " " << dphiFit[2][sl][0]
			<< " " << dphiFit[2][sl][6]
			<< " " << detaFit[2][sl][0]
			<< " " << detaFit[2][sl][6];
		for(int p = 0; p < 10; p++){
			if ( p<5){
				f2 << " " << dphiFit[0][sl][p+1];
				f3 << " " << dphiFit[1][sl][p+1];
				f4 << " " << dphiFit[2][sl][p+1];
			} else{
				f2 << " " << detaFit[0][sl][p+1-5];
				f3 << " " << detaFit[1][sl][p+1-5];
				f4 << " " << detaFit[2][sl][p+1-5];
			}
		}
		f2 << std::endl;
		f3 << std::endl;
		f4 << std::endl;
	}

	std::cout << "Stored Run Corrections to AvgdPhidEtaSlFit.dat successfully\n\n";

	f2.close();
	f3.close();
	f4.close();
	file->Close();
	std::cout << "Files successfully closed\n\n";

	return;

}



//_____________________________________________
//
// Pass3: find the average time correction
//        per 1GeV energy bin by gain
//
// Info: Will have to play with rebinning/fit
//       ranges to get them reasonable!
//________________________________________________
void pass3(){

	// Show fit stats
	gStyle->SetOptFit(0);

	std::cout<< "Finding Corrections for Pass 3\n" <<std::endl;

	// store range of fits
	double minX[3][22+7];
	double maxX[3][22+7];
	///CJ:
	// Initialize the en corrections
	for( int g = 0; g < 3; g++){
		for( int s = 0; s < 22+7; s++){
			for( int e = 0; e < 6; e++){
				enFit[g][s][e] = 0;
			}
			minX[g][s] = -99;
			maxX[g][s] = -99;
		}
	}

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass3.root","READ");
	std::cout<< "Opened root file pass3.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/AvgEnSlFitH.dat");
	ofstream f3("config/AvgEnSlFitM.dat");
	ofstream f4("config/AvgEnSlFitL.dat");

	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Loop over slots
		for( int sl = 0; sl < 22+7; sl++){
			if(g==1&&sl>21) continue;

			// Make names for the histograms to open and profile plots
			std::string pname = Form("Prof Avg En Time %s Slot%d",gain[g].c_str(),sl );
			std::string hname = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl );

			// Open the histogram, continue if empty
			TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
			if( !f_rn->GetEntries() ) {
				std::cout << "  >> Histogram has no data for gain " << gain[g] << " slot " << sl 
					<< ", corrections will be zero!\n";
				continue;
			}

			// Rebin FIXME have to adjust to make data look reasonable
			int binMin = f_rn->GetEntries() / 100 ; // each bin has at least 1/100th total entries
			if( g == 1 ) binMin = f_rn->GetEntries() /  150;  // 1/150 for medium gain


			if( g == 2 ) binMin = 2; // low gain 5 is the bin min
			//  f_rn->GetXaxis()->SetRangeUser(5,400); 
			f_rn = rebin( f_rn, binMin, 5); 


			// Make a profile plot to find avg time per energy
			TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
			tpf->GetYaxis()->SetRangeUser(tmin,tmax);
			tpf->GetYaxis()->SetTitle("Time [ns]");
			minX[g][sl]=tpf->GetXaxis()->GetBinCenter(f_rn->FindFirstBinAbove(0)+g);
			if((sl==14||sl==15||sl==20||sl==21||sl==24||sl==28 )&&g==0)
				minX[g][sl]=tpf->GetXaxis()->GetBinCenter(f_rn->FindFirstBinAbove(0)+1);
			//if(sl!=19&&sl!=13&&sl!=26)
			//maxX[g][sl]=tpf->GetXaxis()->GetBinLowEdge(f_rn->FindLastBinAbove(0)-g);
			//else

			maxX[g][sl]=tpf->GetXaxis()->GetBinCenter(f_rn->FindLastBinAbove(0)-g-1);
			//tpf->GetXaxis()->SetRangeUser(5,400);
			// Find the Max in x direction, and set fit range
			/*
			   for (int b=1; b<= tpf->GetNbinsX(); b++){ // number of rebinned energy bins
			   if( tpf->GetBinEntries(b) > 1 ){  // at least this many entries to be part of fit

			   if( minX[g][sl] == -99 ) minX[g][sl] = tpf-> GetBinCenter(b+g);
			   if((sl>=13&&sl<=15)||(sl>=19&&sl<=21)||sl==24||sl==25||sl==28)
			   minX[g][sl] =tpf-> GetXaxis()-> GetBinUpEdge(b);

			   if( debug ) std::cout << "Bin :" << b << "; entries: " << tpf->GetBinEntries(b) << std::endl; // FIXME
			   maxX[g][sl] = tpf-> GetBinCenter(b-g*2); 
			   }
			   }*/
			//	      if( g == 1 ){
			//		int fBin = tpf->GetXaxis()->FindBin(5.5);
			//		minX[g][sl] = tpf->GetXaxis()->GetBinLowEdge(fBin+2);
			//		int lBin = tpf->GetXaxis()->FindBin(400);
			//		maxX[g][sl] = tpf->GetXaxis()->GetBinLowEdge(lBin-2);
			//		///CJ///exclude the 2 highest bin except for 
			//		if( sl == 19|| sl == 20 || sl == 21){ // sl == 1 
			//		  // for EMB(a/c) sl 11,12 exclude highest bin for MED, low entries usually
			//		  maxX[g][sl] = tpf->GetXaxis()->GetBinCenter( tpf->GetXaxis()->FindBin(400) - 1 ); 
			//		}
			//	      }
			if( g == 2){
				int fBin = tpf->GetXaxis()->FindBin(5.5);
				minX[g][sl] = tpf->GetXaxis()->GetBinLowEdge(fBin+1);
			}
			if( debug ){
				std::cout << "Gain: " << g << "; Slot: " << slotNames[sl] <<std::endl;
				std::cout << "  minX: " << minX[g][sl] << ";  maxX: " << maxX[g][sl] <<std::endl;
			}
			// Make a fit
			// /////CJ 
			TF1 *f1 = new TF1("f1", "pol5", minX[g][sl], maxX[g][sl]);
			//   tpf->GetXaxis()->SetRangeUser(5,300);
			int fit_rpf = tpf->Fit("f1", "RQ");

			f1->SetLineColor(kRed);
			// If fit unsuccessful
			if( fit_rpf != 0 )
				std::cout << gain[g].c_str() << " gain " << slotNames[sl].c_str() << std::endl
					<< "  Energy fit unsuccessful, result status: " << fit_rpf << std::endl;
			// Store Fit Params
			for(int p=0; p<6; p++){
				enFit[g][sl][p] = f1->GetParameter(p);
				//	cout<<enFit[g][sl][p]<<endl;
			}
			// Make the plot including fit
			TCanvas *c1 = new TCanvas(Form("c1%d%d",g,sl),Form("c1%d%d",g,sl), 800, 700);
			//cout<<tpf->GetXaxis()->
			tpf->Draw();
			tpf->SetErrorOption("g"); 
			tpf->Draw();
			myText(0.83,0.89,kBlue,Form("p0: %.3f",enFit[g][sl][0]),0.6);
			myText(0.83,0.86,kBlue,Form("p1: %.3f",enFit[g][sl][1]),0.6); 
			myText(0.83,0.83,kBlue,Form("p2: %.3f",enFit[g][sl][2]),0.6); 
			myText(0.83,0.80,kBlue,Form("p3: %.3f",enFit[g][sl][3]),0.6); 
			myText(0.83,0.77,kBlue,Form("p4: %.3f",enFit[g][sl][4]),0.6); 
			myText(0.83,0.74,kBlue,Form("p5: %.3f",enFit[g][sl][5]),0.6); 
			myText(0.81,0.71,kBlue,Form("#chi^{2}/ndf : %.3f", f1->GetChisquare()/f1->GetNDF()),0.6); 

			///CJ///
			//   tpf->GetXaxis()->SetRangeUser(5,300); 
			tpf->GetXaxis()->SetRangeUser(5,tpf->GetXaxis()->GetBinLowEdge(tpf->GetXaxis()->FindBin(300.)));


			//  tpf->GetYaxis()->SetRangeUser(-1,1);
			//	     tpf->GetXaxis()->SetRangeUser(5,300);
			//   tpf->GetXaxis()->SetLimits(5,300); 
			f1->Draw("sames");
			//  c1->SetLogx(1);
			drawFitInfo(g, sl);
			// Save plot
			if(saveEPS) c1->SaveAs(Form("plots/pass3/energy/Sl%dcelle_%s.eps",sl,gain[g].c_str()));
			c1->SaveAs(Form("plots/pass3/energy/Sl%dcelle_%s.png",sl,gain[g].c_str()));
			delete c1;

		}// end loop over slots

	} // end loop over gain

	// Store the avg times:
	for(int sl = 0; sl < 22+7; sl++){
		f2 << sl << " " << minX[0][sl] << " " << maxX[0][sl];
		f3 << sl << " " << minX[1][sl] << " " << maxX[1][sl];
		f4 << sl << " " << minX[2][sl] << " " << maxX[2][sl];
		///Cj
		for(int p = 0; p < 6; p++){
			f2 << " " << enFit[0][sl][p];
			f3 << " " << enFit[1][sl][p];
			f4 << " " << enFit[2][sl][p];
		}
		f2 << std::endl;
		f3 << std::endl;
		f4 << std::endl;
	}

	std::cout << "Stored Run Corrections to AvgEnSlFit.dat successfully\n\n";

	f2.close();
	f3.close();
	f4.close();
	file->Close();

	std::cout << "Files successfully closed\n\n";

	return;

}


//_____________________________________________
//
// Pass2: find the average Channel time
//        by gain
//________________________________________________
void pass2(){

	std::cout<< "Finding Corrections for Pass 2\n" <<std::endl;

	// Initialize the feb corrections
	for( int g = 0; g < 2; g++){
		for( int j = 0; j < 79360; j++){
			chTime[g][j] = 0;
		}
	}

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass2.root","READ");
	std::cout<< "Opened root file pass2.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/AvgChOffsetsH.dat");
	ofstream f3("config/AvgChOffsetsM.dat");

	///CJ///
	//	  ofstream f4("config/AvgChOffsetsL.dat");
	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Make names for the histograms to open and profile plots
		std::string pname = Form("Prof Avg Ch Time %s",gain[g].c_str() );
		std::string hname = Form("h_AvgChannelTime_%s",gain[g].c_str() );

		// Open the histogram, continue if empty
		TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
		if( !f_rn->GetEntries() ) {
			std::cout << "  >> Histogram has no data for gain " 
				<< gain[g] << ", corrections will be zero!\n";
			continue;
		}

		// Make a profile plot to find avg time for each feb
		TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");

		// Find nbins to loop over
		int nbins =tpf->GetXaxis()->GetNbins();

		// For non-empty febs, store correction
		for(int j=0; j<nbins; j++){
			// bins start at 1, but ch numb start at 0
			double i_time = tpf->GetBinContent(j+1);
			if( i_time != 0){
				//  j is the ch index, g is the gain index
				chTime[g][j] = i_time;
			}
		}
	} // end loop over gains

	// Store the avg times:
	for(int ch = 0; ch < 79360; ch++){
		f2 << ch << " " << chTime[0][ch] << std::endl;
		f3 << ch << " " << chTime[1][ch] << std::endl;
		//	    f4 << ch << " " << chTime[2][ch] << std::endl;
	}

	std::cout << "Stored Run Corrections to AvgChOffsets.dat successfully\n\n";

	f2.close();
	f3.close();
	//	  f4.close();
	file->Close();

	std::cout << "Files successfully closed\n\n";

	return;


}



//_____________________________________________
//
// Pass1: find the average FEB time
// by gain
//________________________________________________
void pass1(){

	std::cout<< "Finding Corrections for Pass 1\n" <<std::endl;

	// Initialize the feb corrections
	for( int g = 0; g < 3; g++){
		for( int j = 0; j < 620; j++){
			febTime_1[g][j] = 0;
		}
	}

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass1.root","READ");
	std::cout<< "Opened root file pass1.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/AvgFebOffsetsH.dat");
	ofstream f3("config/AvgFebOffsetsM.dat");
	//	  ofstream f4("config/AvgFebOffsetsL.dat");

	// Loop over gains
	for( int g = 0; g < 2; g++){

		// Make names for the histograms to open and profile plots
		std::string pname = Form("Prof Avg Feb Time %s",gain[g].c_str() );
		std::string hname = Form("h_AvgFebTime_%s",gain[g].c_str() );

		// Open the histogram, continue if empty
		TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
		if( !f_rn->GetEntries() ) {
			std::cout << "  >> Histogram has no data for gain " 
				<< gain[g] << ", corrections will be zero!\n";
			continue;
		}

		// Make a profile plot to find avg time for each feb
		TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");

		// Find nbins to loop over
		int nbins =tpf->GetXaxis()->GetNbins();

		// For non-empty febs, store correction
		for(int j=0; j<nbins; j++){
			// bins start at 1, but feb numb start at 0
			double i_time = tpf->GetBinContent(j+1);
			if( i_time != 0){
				//  j is the feb index, g is the gain index
				febTime_1[g][j] = i_time;
			}
		}
	} // end loop over gain

	// Store the avg times:
	for(int feb = 0; feb < 620; feb++){
		f2 << feb << " " << febTime_1[0][feb] << std::endl;
		f3 << feb << " " << febTime_1[1][feb] << std::endl;
		//	    f4 << feb << " " << febTime_1[2][feb] << std::endl;
	}

	std::cout << "Stored Run Corrections to AvgFebOffsets.dat successfully\n\n";

	f2.close();
	f3.close();
	//	  f4.close();
	file->Close();

	std::cout << "Files successfully closed\n\n";

	return;
}
//_____________________________________________
//
// Pass0: find the average FEB time
// by run and gain.
//________________________________________________
void pass0(){

	std::cout<< "Finding Corrections for Pass 0\n" <<std::endl;

	// Initialize the feb corrections
	for( int i = 0; i < 3; i++){
		for( int j = 0; j < NRUNS; j++){
			for( int k = 0; k < 114; k++){
				ftTime_0[i][j][k] = 0;
			}
		}
	} 

	// Open the data file
	TFile *file;
	file = TFile::Open("files/pass0.root","READ");
	std::cout<< "Opened root file pass0.root successfully\n\n";

	// Open the corrections file
	ofstream f2("config/runByRunFTOffsetsH.dat");
	ofstream f3("config/runByRunFTOffsetsM.dat");
	//ofstream f4("config/runByRunFTOffsetsL_2nd.dat");

	// Loop over the gains
	for (int g = 0; g < 2; g++){

		// Loop over all the runs
		for (int rn = 0; rn < NRUNS; rn++){

			// Make names for the histograms to open and profile plots
			std::string pname = Form("Prof run %s%d",gain[g].c_str(),rn);
			std::string hname = Form("h_RunAvgFTTime_%s%d",gain[g].c_str(),rn);

			// Open the histogram, continue if empty 
			TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
			if( !f_rn->GetEntries() ) {
				std::cout << "  >> Run index: " << rn << " has no data for " 
					<< gain[g] << " gain, corrections will be zero!\n";
				continue;
			}
			// Make a profile plot to find avg time for each feb
			TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");

			// Find nbins to loop over
			int nbins =tpf->GetXaxis()->GetNbins();

			// For each feb, store correction
			for(int j=0; j<nbins; j++){
				// bins start at 1, but feb numb start at 0
				double i_time = tpf->GetBinContent(j+1); 
				if( i_time != 0){
					// rn is the run number index, j is the feb index, g is gain
					ftTime_0[g][rn][j] = i_time;
				}
			}

		} // end loop over runs

	} // end loop over gains

	// Store the avg times:
	for(int ft = 0; ft < 114; ft++){
		f2 << ft;
		f3 << ft;
		//   f4 << ft;
		for(int rn = 0; rn < NRUNS; rn++){
			f2 << " " << ftTime_0[0][rn][ft];
			f3 << " " << ftTime_0[1][rn][ft];
			//   f4 << " " << ftTime_0[2][rn][ft];
		}
		f2 << "\n";
		f3 << "\n";
		//	    f4 << "\n";
	}
	std::cout << "Stored Run Corrections to runByRunFTOffsets.dat successfully\n\n";

	f2.close();
	f3.close();
	//	  f4.close();
	file->Close(); 

	std::cout << "Files successfully closed\n\n";

	return;
}

