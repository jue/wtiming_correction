//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "runPassCalc.h"



void doChannelPlot(){
	std::cout << "\n\nMaking plots for time_runnumber channel\n\n";
	for(int i=0; i<2; i++){    
		for(int j=0; j<Nch; j++){    
			h_channel_t[i][j] = new TH1D(Form("h_channel_t%d%d",j,i),Form("Channel; Time [ns]; Events / 0.02 ns"),50,-10,10);
			h_channel_run[i][j] = new TH2F( Form("h_channel_run%d%d", j, i), Form("h_channel_run%d%d; Run Number; Avg Cell Time[ns]",j,i), NRUNS,0, NRUNS, 500, -10, 10); 
		}
	}
	// Open the data file
	TFile *file;
	file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ");
	for( int g=0; g<2; g++){   
		for( int rn=0; rn<NRUNS; rn++){ 
			//for( int rn=0; rn<4; rn++){ 
			std::string pname = Form("Prof run %s%d",gain[g].c_str(),rn);     
			std::string hname = Form("h_RunChannelTime_%s%d",gain[g].c_str(),rn);   
			TH2F *f_rn = (TH2F*)file->Get(hname.c_str());     
			if( !f_rn->GetEntries() ) { 
				std::cout << "  >> Run index: " << rn << " has no data for "
					<< gain[g] << " gain, no data to be plotted!\n";
				continue;
			}

			TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
			tpf->GetYaxis()->SetRangeUser(tmin, tmax);
			tpf->GetYaxis()->SetTitle("Time [ns]");
			//  cout<<tpf->GetBin(0)<<endl;
			for(int ch=0; ch<Nch; ch++){
				double i_time = tpf->GetBinContent(ch+1);    
				//  if(ch==18)
				//cout<<ch<<"\t"<<channel[ch]<<"\t"<<rn<<"\t"<<runNumberList[rn]<<"\t"<<i_time<<endl;
				if(i_time!=0)  {    h_channel_run[g][ch]->Fill(rn,i_time);}
			}
		}
		for(int ch=0; ch<Nch; ch++){    
			//cout<<"t_coo"<<"\t"<<ch<<"\t"<<rn<<"\t"<<i_time<<endl;
			TCanvas *c4 = new TCanvas(Form("c4%d%d",g,ch),Form("c4%d%d",g,ch), 800, 700); 
			TProfile *tp  = h_channel_run[g][ch]->ProfileX(Form("Profile Run%d%d",g,ch),-1,-1,"o"); 
			for( int bin=0; bin<NRUNS; bin++){
				//for( int bin=0; bin<4; bin++){
				int s_run =  runNumberList[bin] ;
				h_channel_run[g][ch]->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
			}
			h_channel_run[g][ch]->GetXaxis()->SetRangeUser(0,NRUNS);
			//h_channel_run[g][ch]->GetXaxis()->SetRangeUser(0,4);
			h_channel_run[g][ch]->GetXaxis()->LabelsOption("v");
			h_channel_run[g][ch]->GetXaxis()->SetLabelSize(0.03);
			h_channel_run[g][ch]->GetYaxis()->SetRangeUser(tmin, tmax);
			h_channel_run[g][ch]->GetYaxis()->SetTitle("Time [ns]");
			h_channel_run[g][ch]->Draw();


			TLine *l1 = new TLine(16,-10,16,10);
			l1->SetLineColor(kRed);
			l1->SetLineWidth(1);
			l1->Draw("same");
			TLine *l2 = new TLine(30,-10,30,10);
			l2->SetLineColor(kRed);
			l2->SetLineWidth(1);
			l2->Draw("same");
			TLine *l3 = new TLine(78,-10,78,10);
			l3->SetLineColor(kRed);
			l3->SetLineWidth(1);
			l3->Draw("same");
			TLine *l4 = new TLine(119,-10,119,10);
			l4->SetLineColor(kRed);
			l4->SetLineWidth(1);
			l4->Draw("same");
			drawChannelInfo(g, ch);

			if( saveEPS )c4->SaveAs(Form("plots_channel/%d_runTime_%s.eps",ch,gain[g].c_str()));
			c4->SaveAs(Form("plots_channel/%d_runTime_%s.png",ch,gain[g].c_str()));
			delete c4;


			std::string pname  = Form("h_AvgChannelTime_%s",gain[g].c_str());
			TH2F *t_p  = (TH2F*)file->Get(pname.c_str());
			if( !t_p->GetEntries() ) { 
				std::cout << "  >> Channel has no data for " 
					<< gain[g] << " gain, no plots will be made!\n";
				continue;
			}
			h_channel_t[g][ch]  = t_p->ProjectionY(Form("f_p%d",g),ch+1,ch+1,"o");
			TCanvas *cp = new TCanvas(Form("cp%d",g),Form("cp%d",g),800,700);

			h_channel_t[g][ch]->GetXaxis()->SetRangeUser(tmin, tmax);
			h_channel_t[g][ch]->Draw();
			drawChannelInfo(g, ch);
			// Make Log plots too
			if( saveEPS )cp->SaveAs(Form("plots_channel/channel%dCelltime_%s.eps",ch,gain[g].c_str()));
			cp->SaveAs(Form("plots_channel/channel%dCelltime_%s.png",ch,gain[g].c_str()));

			double maxY = h_channel_t[g][ch]->GetBinContent( h_channel_t[g][ch]->GetMaximumBin() );
			if(maxY == 0 ) 
				h_channel_t[g][ch]->GetYaxis()->SetRangeUser(0.5, 10);
			else
				h_channel_t[g][ch]->GetYaxis()->SetRangeUser(0.5,maxY/0.8);

			cp->SetLogy();
			if( saveEPS )cp->SaveAs(Form("plots_channel/Log_channel%dCelltime_%s.eps",ch,gain[g].c_str()));
			cp->SaveAs(Form("plots_channel/Log_channel%dCelltime_%s.png", ch,gain[g].c_str()));
			}
		}

		}
		//_____________________________________________
		//
		// Make plots for avg time by FEB
		// and by Run Number per FEB
		//_____________________________________________
		void doFEBTimePlot(){

			std::cout << "\n\nMaking plots for average FEB Timing\n\n";

			// Initialize the histograms
			for( int i=0; i<2; i++){
				for( int j=0; j<5; j++){
					h_feb_run[i][j] = new TH2F( Form("h_feb_run%d%d",j,i), Form("h_feb_run%d%d; Run Number; Avg FEB Time [ns]",j,i), NRUNS,0,NRUNS, 500,-5,5);  
				}
			}

			// Open the data file
			TFile *file;
			file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ");

			// Loop over gains
			for( int g=0; g<2; g++){

				// Loop over the runs
				for( int rn=0; rn<NRUNS; rn++){

					// Make names for the histograms to open and profile plots
					std::string pname = Form("Prof run %s%d",gain[g].c_str(),rn);
					std::string hname = Form("h_RunAvgFebTime_%s%d",gain[g].c_str(),rn);

					// Open the histogram, continue if empty 
					TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
			//cout<<gain[g]<<"\t"<<rn<<"\t"<<f_rn->GetEntries()<<endl;
			//		if( !f_rn->GetEntries() ) {
			//			std::cout << "  >> Run index: " << rn << " has no data for " 
			//				<< gain[g] << " gain, no data to be plotted!\n";
			//			continue;
			//		}

					// Make a profile plot to find avg time for each feb
			cout<<"check1"<<rn<<endl;
					TProfile *tpf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
					tpf->GetYaxis()->SetRangeUser(tmin, tmax);
			cout<<"check1"<<rn<<endl;
					tpf->GetYaxis()->SetTitle("Time [ns]");

			cout<<"check1"<<rn<<endl;
					// Find nbins to loop over
					int nbins =tpf->GetXaxis()->GetNbins();

					// For each feb, put in right partition
					for(int j=0; j<nbins; j++){
						// bins start at 1, but feb numb start at 0
						double i_time = tpf->GetBinContent(j+1); 
						if( i_time != 0){
							// rn is the run number index, j is the feb index, g is gain
							h_feb_run[g][4]->Fill(rn,i_time);
							if( j < 128 || (j >= 256 && j < 288)) 
								h_feb_run[g][0]->Fill(rn, i_time);
							else if( (j >= 128 && j <256) || (j>=288 && j <320) ) 
								h_feb_run[g][1]->Fill(rn, i_time);
							else if( j >= 320 && j < 470 ) 
								h_feb_run[g][2]->Fill(rn, i_time);
							else if( j>= 470 ) 
								h_feb_run[g][3]->Fill(rn, i_time);
						}
					}
			cout<<"check2"<<endl;

					// FEB by run num
					TCanvas *c1 = new TCanvas(Form("c%d%d",g,rn),Form("c%d%d",g,rn), 800, 700); 
					tpf->Draw();

					// Put info on plot
					drawRunInfo(g, rn);

					// Save plots
					if( saveEPS )c1->SaveAs(Form("plots/%s/Run%dFebCorrections_%s.eps",sPassNumber.c_str(),rn,gain[g].c_str()));
					c1->SaveAs(Form("plots/%s/Run%dFebCorrections_%s.png",sPassNumber.c_str(),rn,gain[g].c_str()));
					delete c1;

					//Make 1D histo also showing FEB time
					TH1F *hh = new TH1F(Form("1D%d%d",g,rn),Form("1D%d%d",g,rn),500,-5.,5.);
					for(int k=1; k<=tpf->GetXaxis()->GetNbins(); k++){
						if( tpf->GetBinEntries(k)) // if entries for feb, fill histogram
							hh->Fill( tpf->GetBinContent(k) );
					}
					TCanvas *cc = new TCanvas(Form("cc%d%d",g,rn),Form("cc%d%d",g,rn),800,700);
					hh->Draw();
					//hh->Rebin(5);
					hh->GetXaxis()->SetTitle("FEB Time [ns]");
					hh->GetYaxis()->SetTitle("FEBs / 0.02 ns");
					double mean = hh->GetMean();
					double rms  = hh->GetRMS();
					int entries = hh->GetEntries();
					// Put info on plot
					drawRunInfo(g, rn);
					drawStats(entries, mean, rms);
					cc->SetLogy(1);
					// Save plots
					if( saveEPS )cc->SaveAs(Form("plots/%s/1DRun%dFebCorrections_%s.eps",sPassNumber.c_str(),rn,gain[g].c_str()));
					cc->SaveAs(Form("plots/%s/1DRun%dFebCorrections_%s.png",sPassNumber.c_str(),rn,gain[g].c_str()));
					cc->SetLogy(0);
					delete cc;
			cout<<"check3"<<endl;

				}// end loop over runs

				// Run by run plots for subdetector/slot
				for( int sd = 0; sd < 5; sd++){

					// Make profile plot
					TCanvas *c4 = new TCanvas(Form("c4%d%d",g,sd),Form("c4%d%d",g,sd), 800, 700); 
					TProfile *tp  = h_feb_run[g][sd]->ProfileX(Form("Profile Run%d%d",g,sd),-1,-1,"o");

					// Write run number as bin label
					for( int bin=0; bin<NRUNS; bin++){
						// FIXME! right now runs 279345 and 279685 are on GRL
						int s_run =  runNumberList[bin] ;
						tp->GetXaxis()->SetBinLabel(bin+1, Form("%d",s_run) );
					}
					tp->GetXaxis()->SetRangeUser(0,NRUNS);
					tp->GetXaxis()->LabelsOption("v");
					tp->GetXaxis()->SetLabelSize(0.03);
					tp->GetYaxis()->SetRangeUser(tmin, tmax);
					tp->GetYaxis()->SetTitle("Time [ns]");
					tp->Draw();

					// Put info on plot
					drawSubDetInfo(g, sd);

					// Save plots
					if( saveEPS )c4->SaveAs(Form("plots/%s/%s_runTime_%s.eps",sPassNumber.c_str(),subDet[sd].c_str(),gain[g].c_str()));
					c4->SaveAs(Form("plots/%s/%s_runTime_%s.png",sPassNumber.c_str(),subDet[sd].c_str(),gain[g].c_str()));
					delete c4;
				}

				// FEB time plots for all runs combined
				std::string pname = Form("Prof Avg Feb Time %s",gain[g].c_str() );
				std::string hname = Form("h_AvgFebTime_%s",gain[g].c_str() );

				// Open the histogram, continue if empty
				TH2F *f_rn = (TH2F*)file->Get(hname.c_str());
				if( !f_rn->GetEntries() ) {
					std::cout << "  >> Histogram has no data for gain " 
						<< gain[g] << ", no plots will be made!\n";
					continue;
				}

				// Make a profile plot to find avg time for each feb
				TProfile *tf  = f_rn->ProfileX(pname.c_str(),-1,-1,"o");
				tf->GetYaxis()->SetRangeUser(tmin,tmax);
				tf->GetYaxis()->SetTitle("Time [ns]");

				// Make the plot
				TCanvas *c5 = new TCanvas(Form("c5%d",g),"c1", 800, 700);
				tf->Draw();
				// Put info on plot
				ATLASLabel(0.2,0.88,pInternal);
				myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
				myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
				char allRuns[10] = "All Runs";
				char* pallRuns = allRuns;
				myText(0.73,0.88,kBlack,pallRuns,0.7);

				// Save the plots
				if( saveEPS )c5->SaveAs(Form("plots/%s/AvgFebCorrections_%s.eps",sPassNumber.c_str(),gain[g].c_str()));
				c5->SaveAs(Form("plots/%s/AvgFebCorrections_%s.png",sPassNumber.c_str(),gain[g].c_str()));
				delete c5;

				//Make 1D histo also showing FEB time
				TH1F *hh = new TH1F(Form("1Df%d",g),Form("1Df%d",g),500,-5.,5.);
				for(int k=1; k<=tf->GetXaxis()->GetNbins(); k++){
					if( tf->GetBinEntries(k) ) // if data for feb, fill histogram
						hh->Fill( tf->GetBinContent(k) );
				}
				TCanvas *cc = new TCanvas(Form("cc%d",g),Form("cc%d",g),800,700);
				hh->Draw();
				//hh->Rebin(5);
				hh->GetXaxis()->SetTitle("FEB Time [ns]");
				hh->GetYaxis()->SetTitle("FEBs / 0.02 ns");
				double mean = hh->GetMean();
				double rms  = hh->GetRMS();
				int entries = hh->GetEntries();
				// Put info on plot
				ATLASLabel(0.2,0.88,pInternal);
				myText(0.2,0.83,kBlack,Form("%s Gain",gain[g].c_str()),0.6);
				myText(0.2,0.79,kBlack,Form("Pass %d",passNumber),0.6);
				drawStats(entries, mean, rms);
				cc->SetLogy(1);
				// Save plots
				if( saveEPS )cc->SaveAs(Form("plots/%s/1DAvgFebCorrections_%s.eps",sPassNumber.c_str(),gain[g].c_str()));
				cc->SaveAs(Form("plots/%s/1DAvgFebCorrections_%s.png",sPassNumber.c_str(),gain[g].c_str()));
				cc->SetLogy(0);
				delete cc;

			}// end loop over gains

			file->Close();
			return;
		}



		//_____________________________________________
		//
		// Plot avg Cell time 
		// by subdetector/slot
		//_____________________________________________
		void doCellTimePlot(){

			std::cout << "\nMaking plots for average Cell Timing\n\n";

			// Initialize the histograms
			for( int i=0; i<3; i++){
				for( int j=0; j<5; j++){
					h_cell_t[i][j] = new TH1F(Form("h_cell_t%d%d",j,i),Form("Cell Time %s; Time [ns]; Events / 0.02 ns",subDet[j].c_str()),1000,-25,25);
				}
			}

			// Open the data file
			TFile *file;
			file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ"); 

			// Loop over gains
			for( int g=0; g<2; g++){  
				// Loop over slots
				for(int sl=0; sl<22; sl++){

					// Make names for the histograms to open
					std::string tname = Form("h_cellTimeAll%s%d",gain[g].c_str(),sl);
					TH1F *f_t = (TH1F*)file->Get(tname.c_str());

					if( !f_t->GetEntries() ) {
						std::cout << "  >> Slot index: " << sl << " has no data for " 
							<< gain[g] << " gain, no plots will be made!\n";
						continue;
					}

					// Summary plots
					h_cell_t[g][4]->Add(f_t);
					if( sl < 4 || sl == 8 ) 
						h_cell_t[g][0]->Add(f_t);
					else if ( (sl > 3 && sl < 8) || sl == 9 ) 
						h_cell_t[g][1]->Add(f_t);
					else if ( sl > 9 && sl < 16 )
						h_cell_t[g][2]->Add(f_t);
					else if ( sl > 15 )
						h_cell_t[g][3]->Add(f_t);


					// Make each slot plot
					TCanvas *c2 = new TCanvas(Form("c%d_%d",g,sl),Form("c%d_%d",g,sl),800,700);
					f_t->Draw();
					double rms  = f_t->GetRMS();
					double mean = f_t->GetMean();
					int entries = f_t->GetEntries();
					f_t = fitGaus( f_t, mean, rms);
					drawSlInfo(g,sl);
					drawStats(entries, mean, rms);
					if( saveEPS )c2->SaveAs(Form("plots/%s/cellTime/Sl%dCelltime_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					c2->SaveAs(Form("plots/%s/cellTime/Sl%dCelltime_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));

					// Make Log plots too
					double maxY = f_t->GetBinContent( f_t->GetMaximumBin() );
					if(maxY == 0 )
						f_t->GetYaxis()->SetRangeUser(0.5, 10);
					else
						f_t->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
					c2->SetLogy();
					if( saveEPS )c2->SaveAs(Form("plots/%s/cellTime/Log_Sl%dCelltime_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					c2->SaveAs(Form("plots/%s/cellTime/Log_Sl%dCelltime_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					c2->SetLogy(0);
					delete c2;

				} // end loop over slots

				// Make summary plots
				for( int d=0; d<5; d++){
					TCanvas *c3 = new TCanvas(Form("c_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
					h_cell_t[g][d]->Draw();
					double rms  = h_cell_t[g][d]->GetRMS();
					double mean = h_cell_t[g][d]->GetMean();
					int entries = h_cell_t[g][d]->GetEntries();
					h_cell_t[g][d] = fitGaus( h_cell_t[g][d], mean, rms );
					drawSubDetInfo(g, d);
					drawStats(entries, mean, rms);
					if( saveEPS )c3->SaveAs(Form("plots/%s/cellTime/%s_Celltime_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					c3->SaveAs(Form("plots/%s/cellTime/%s_Celltime_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));

					// Make Log Plots too
					double maxY = h_cell_t[g][d]->GetBinContent( h_cell_t[g][d]->GetMaximumBin() );
					h_cell_t[g][d]->GetYaxis()->SetRangeUser(0.5,maxY/0.08);
					c3->SetLogy();
					if( saveEPS )c3->SaveAs(Form("plots/%s/cellTime/Log_%s_Celltime_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					c3->SaveAs(Form("plots/%s/cellTime/Log_%s_Celltime_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					c3->SetLogy(0);
					delete c3;

				}

			} // end loop over gains

			file->Close();
			return;
		}



		//_____________________________________________
		//
		// Plot avg time vs Phi/Eta 
		// by subdetector/slot
		//_____________________________________________
		void doAngularPlot(){

			std::cout << "\nMaking plots for angular cell position timing\n\n";

			// Initialize the histograms
			for( int i=0; i<2; i++){
				for( int j=0; j<5; j++){
					h_phi_t[i][j]  = new TH2F(Form("h_phi_t%d%d",j,i),Form("Cell Time %s; #phi; Time [ns]",subDet[j].c_str()),32,-3.1416,3.1416,500,-5,5);
					h_eta_t[i][j]  = new TH2F(Form("h_eta_t%d%d",j,i),Form("Cell Time %s; #eta; Time [ns]",subDet[j].c_str()),125,-2.5,2.5,500,-5,5);
					h_dphi_t[i][j] = new TH2F(Form("h_dphi_t%d%d",j,i),Form("Cell Time %s; #delta#phi[0.0245 units]; Time [ns]",subDet[j].c_str()),400,-2,2,500,-5,5);
					h_deta_t[i][j] = new TH2F(Form("h_deta_t%d%d",j,i),Form("Cell Time %s; #delta#eta[0.025 units]; Time [ns]",subDet[j].c_str()),100,-1,1,500,-5,5);
				}
			}

			// Open the data file
			TFile *file;
			file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ");
			//file = TFile::Open("hist-test.root","READ");

			// Loop over gains
			for( int g=0; g<2; g++){  
				// Loop over slots
				for(int sl=0; sl<22; sl++){

					// Make names for the histograms to open
					std::string pname  = Form("h_cellTimeVsPhi%s%d",gain[g].c_str(),sl);
					std::string ename  = Form("h_cellTimeVsEta%s%d",gain[g].c_str(),sl);
					std::string dpname = Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),sl);
					std::string dename = Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),sl);

					TH2F *t_p  = (TH2F*)file->Get(pname.c_str());
					TH2F *t_e  = (TH2F*)file->Get(ename.c_str());
					TH2F *t_dp = (TH2F*)file->Get(dpname.c_str());
					TH2F *t_de = (TH2F*)file->Get(dename.c_str());

					if( !t_dp->GetEntries() ) {
						std::cout << "  >> Slot index: " << sl << " has no data for " 
							<< gain[g] << " gain, no plots will be made!\n";
						continue;
					}

					// Get Profile Plots 
					TProfile *f_p   = t_p->ProfileX(Form("f_p%d%d",g,sl),-1,-1,"o");
					TProfile *f_e   = t_e->ProfileX(Form("f_e%d%d",g,sl),-1,-1,"o");
					TProfile *f_dp  = t_dp->ProfileX(Form("f_dp%d%d",g,sl),-1,-1,"o");
					TProfile *f_de  = t_de->ProfileX(Form("f_de%d%d",g,sl),-1,-1,"o");
					f_p->GetYaxis()->SetRangeUser(tmin,tmax);
					f_e->GetYaxis()->SetRangeUser(tmin,tmax);
					f_dp->GetYaxis()->SetRangeUser(tmin,tmax);
					f_de->GetYaxis()->SetRangeUser(tmin,tmax);
					f_p->GetYaxis()->SetTitle("Time [ns]");
					f_e->GetYaxis()->SetTitle("Time [ns]");
					f_dp->GetYaxis()->SetTitle("Time [ns]");
					f_de->GetYaxis()->SetTitle("Time [ns]");

					// Summary plots
					h_phi_t[g][4]->Add(t_p);
					h_eta_t[g][4]->Add(t_e);
					h_dphi_t[g][4]->Add(t_dp);
					h_deta_t[g][4]->Add(t_de);
					if( sl < 4 || sl == 8 ){ 
						h_phi_t[g][0]->Add(t_p);
						h_eta_t[g][0]->Add(t_e);
						h_dphi_t[g][0]->Add(t_dp);
						h_deta_t[g][0]->Add(t_de);
					}
					else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
						h_phi_t[g][1]->Add(t_p);
						h_eta_t[g][1]->Add(t_e);
						h_dphi_t[g][1]->Add(t_dp);
						h_deta_t[g][1]->Add(t_de);
					}
					else if ( sl > 9 && sl < 16 ){
						h_phi_t[g][2]->Add(t_p);
						h_eta_t[g][2]->Add(t_e);
						h_dphi_t[g][2]->Add(t_dp);
						h_deta_t[g][2]->Add(t_de);
					}
					else if ( sl > 15 ){
						h_phi_t[g][3]->Add(t_p);
						h_eta_t[g][3]->Add(t_e);
						h_dphi_t[g][3]->Add(t_dp);
						h_deta_t[g][3]->Add(t_de);
					}

					// Make each slot plot
					// Phi
					TCanvas *cp = new TCanvas(Form("cp%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
					f_p->Draw();
					drawSlInfo(g, sl);
					if( saveEPS )cp->SaveAs(Form("plots/%s/angular/Sl%dcellPhi_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					cp->SaveAs(Form("plots/%s/angular/Sl%dcellPhi_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cp;
					// Eta
					TCanvas *ce = new TCanvas(Form("ce%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
					f_e->Draw();
					drawSlInfo(g, sl);
					if( saveEPS )ce->SaveAs(Form("plots/%s/angular/Sl%dcellEta_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					ce->SaveAs(Form("plots/%s/angular/Sl%dcellEta_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete ce;
					// dPhi
					TCanvas *cdp = new TCanvas(Form("cdp%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
					f_dp->Draw();
					drawSlInfo(g, sl);
					if( saveEPS )cdp->SaveAs(Form("plots/%s/angular/Sl%dcelldPhi_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					// for pass4 we save the fit plots instead
					if( passNumber != 4) cdp->SaveAs(Form("plots/%s/angular/Sl%dcelldPhi_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cdp;
					// dEta
					TCanvas *cde = new TCanvas(Form("cde%d_%d",g,sl),Form("cp%d_%d",g,sl),800,700);
					f_de->Draw();
					drawSlInfo(g, sl);
					if( saveEPS )cde->SaveAs(Form("plots/%s/angular/Sl%dcelldEta_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					// for pass4 we save the fit plots instead
					if(passNumber != 4)cde->SaveAs(Form("plots/%s/angular/Sl%dcelldEta_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cde;

				}// end loop over slots

				// Make summary plots
				for( int d=0; d<5; d++){
					// Phi
					TCanvas *cp = new TCanvas(Form("cp_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
					TProfile *ff_p = h_phi_t[g][d]->ProfileX(Form("ff_p%d%d",g,d),-1,-1,"o");
					ff_p->Draw();
					ff_p->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_p->GetYaxis()->SetTitle("Time [ns]");
					drawSubDetInfo(g, d);
					if( saveEPS )cp->SaveAs(Form("plots/%s/angular/%s_cellPhi_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cp->SaveAs(Form("plots/%s/angular/%s_cellPhi_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cp;
					// Eta
					TCanvas *ce = new TCanvas(Form("ce_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
					TProfile *ff_e = h_eta_t[g][d]->ProfileX(Form("ff_e%d%d",g,d),-1,-1,"o");
					ff_e->Draw();
					ff_e->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_e->GetYaxis()->SetTitle("Time [ns]");
					drawSubDetInfo(g, d);
					if( saveEPS )ce->SaveAs(Form("plots/%s/angular/%s_cellEta_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					ce->SaveAs(Form("plots/%s/angular/%s_cellEta_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete ce;
					// dPhi
					TCanvas *cdp = new TCanvas(Form("cdp_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
					TProfile *ff_dp = h_dphi_t[g][d]->ProfileX(Form("ff_dp%d%d",g,d),-1,-1,"o");
					ff_dp->Draw();
					ff_dp->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_dp->GetYaxis()->SetTitle("Time [ns]");
					drawSubDetInfo(g, d);
					if( saveEPS )cdp->SaveAs(Form("plots/%s/angular/%s_celldPhi_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cdp->SaveAs(Form("plots/%s/angular/%s_celldPhi_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cdp;
					// dEta
					TCanvas *cde = new TCanvas(Form("cde_%d_%d",g,d),Form("c_%d_%d",g,d),800,700);
					TProfile *ff_de = h_deta_t[g][d]->ProfileX(Form("ff_de%d%d",g,d),-1,-1,"o");
					ff_de->Draw();
					ff_de->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_de->GetYaxis()->SetTitle("Time [ns]");
					drawSubDetInfo(g, d);
					if( saveEPS )cde->SaveAs(Form("plots/%s/angular/%s_celldEta_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cde->SaveAs(Form("plots/%s/angular/%s_celldEta_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cde;
				}

			}// end loop over gains

			file->Close();
			return;
		}



		//_____________________________________________
		//
		// Plot avg time vs f1/f3 
		// by subdetector/slot
		//_____________________________________________
		void doEFracPlot(){

			std::cout << "\nMaking plots for fractional energy deposit timing\n\n";

			// Initialize the histograms
			for( int i=0; i<3; i++){
				for( int j=0; j<5; j++){
					h_f1_t[i][j]  = new TH2F(Form("h_f1_t%d%d",j,i),Form("Cell Time f1%s%d; f1; Time [ns]",subDet[j].c_str(),i),400,-0.05,0.6,500,-5,5); // 220, -0.10, 1.
					h_f3_t[i][j]  = new TH2F(Form("h_f3_t%d%d",j,i),Form("Cell Time f3%s%d; f3; Time [ns]",subDet[j].c_str(),i),400,-0.05,0.2,500,-5,5);
				}
			}

			// Open the data file
			TFile *file;
			file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ");

			// Loop over gains
			for( int g=0; g<2; g++){  
				// Loop over slots
				for(int sl=0; sl<22; sl++){

					// Make names for the histograms to open
					std::string f1name  = Form("h_cellTimeVsf1%s%d",gain[g].c_str(),sl);
					std::string f3name  = Form("h_cellTimeVsf3%s%d",gain[g].c_str(),sl);

					TH2F *t_f1  = (TH2F*)file->Get(f1name.c_str());
					TH2F *t_f3  = (TH2F*)file->Get(f3name.c_str());

					if( !t_f1->GetEntries() ) {
						std::cout <<	"  >> Slot index: " << sl << " has no data for " 
							<< gain[g] << " gain, no plots will be made!\n";
						continue;
					}

					// Summary plots
					h_f1_t[g][4]->Add(t_f1);
					h_f3_t[g][4]->Add(t_f3);
					if( sl < 4 || sl == 8 ){ 
						h_f1_t[g][0]->Add(t_f1);
						h_f3_t[g][0]->Add(t_f3);
					}
					else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
						h_f1_t[g][1]->Add(t_f1);
						h_f3_t[g][1]->Add(t_f3);
					}
					else if ( sl > 9 && sl < 16 ){
						h_f1_t[g][2]->Add(t_f1);
						h_f3_t[g][2]->Add(t_f3);
					}
					else if ( sl > 15 ){
						h_f1_t[g][3]->Add(t_f1);
						h_f3_t[g][3]->Add(t_f3);
					}

					// Rebin
					int binMinf1 = t_f1->GetEntries()/100;      
					int binMinf3 = t_f3->GetEntries()/100;
					t_f1 = rebin( t_f1, binMinf1, t_f1->GetXaxis()->FindBin(0.) -1 );
					t_f3 = rebin( t_f3, binMinf3, t_f3->GetXaxis()->FindBin(0.) -1 );
					t_f1->SetName( Form("%s%d%d",t_f1->GetName(),g,sl));   
					t_f3->SetName( Form("%s%d%d",t_f3->GetName(),g,sl)); 

					// Get Profile Plots 
					TProfile *f_f1   = t_f1->ProfileX(Form("f_f1%d%d",g,sl),-1,-1,"o");
					TProfile *f_f3   = t_f3->ProfileX(Form("f_f3%d%d",g,sl),-1,-1,"o");
					f_f1->GetYaxis()->SetRangeUser(tmin,tmax);
					f_f3->GetYaxis()->SetRangeUser(tmin,tmax);
					f_f1->GetYaxis()->SetTitle("Time [ns]");
					f_f3->GetYaxis()->SetTitle("Time [ns]");
					f_f1->GetXaxis()->SetTitle("f1");
					f_f3->GetXaxis()->SetTitle("f3");

					// Make each slot plot
					// f1
					TCanvas *cf1 = new TCanvas(Form("cf1%d_%d",g,sl),Form("cf1%d_%d",g,sl),800,700);
					f_f1->Draw();
					f_f1->GetXaxis()->SetRangeUser( f_f1->GetXaxis()->GetBinLowEdge( f_f1->GetXaxis()->FindBin(0.)) ,.6);
					drawSlInfo(g, sl);
					if( saveEPS )cf1->SaveAs(Form("plots/%s/efrac/Sl%dcellf1_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					// Store fits for pass5
					if( passNumber != 5) cf1->SaveAs(Form("plots/%s/efrac/Sl%dcellf1_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cf1;
					// f3
					TCanvas *cf3 = new TCanvas(Form("cf3%d_%d",g,sl),Form("cf3%d_%d",g,sl),800,700);
					f_f3->Draw();
					f_f3->GetXaxis()->SetRangeUser( f_f3->GetXaxis()->GetBinLowEdge( f_f3->GetXaxis()->FindBin(0.)),.2);
					drawSlInfo(g, sl);
					if( saveEPS )cf3->SaveAs(Form("plots/%s/efrac/Sl%dcellf3_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					// Store fits for pass5
					if( passNumber != 5) cf3->SaveAs(Form("plots/%s/efrac/Sl%dcellf3_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cf3;

				}// end loop over slots

				// Make summary plots
				for( int d=0; d<5; d++){
					// Rebin
					int binMinf1 = h_f1_t[g][d]->GetEntries()/100;      
					int binMinf3 = h_f3_t[g][d]->GetEntries()/100;
					h_f1_t[g][d] = rebin( h_f1_t[g][d], binMinf1, h_f1_t[g][d]->GetXaxis()->FindBin(0.) -1 );
					h_f3_t[g][d] = rebin( h_f3_t[g][d], binMinf3, h_f3_t[g][d]->GetXaxis()->FindBin(0.) -1 );

					// f1
					TCanvas *cf1 = new TCanvas(Form("cf1_%d_%d",g,d),Form("cf1_%d_%d",g,d),800,700);
					TProfile *ff_f1 = h_f1_t[g][d]->ProfileX(Form("ff_f1%d%d",g,d),-1,-1,"o");
					ff_f1->Draw();
					ff_f1->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_f1->GetXaxis()->SetRangeUser( ff_f1->GetXaxis()->GetBinLowEdge( ff_f1->GetXaxis()->FindBin(0.)), .60);
					ff_f1->GetYaxis()->SetTitle("Time [ns]");
					ff_f1->GetXaxis()->SetTitle("f1");
					drawSubDetInfo(g, d);
					if( saveEPS )cf1->SaveAs(Form("plots/%s/efrac/%s_cellf1_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cf1->SaveAs(Form("plots/%s/efrac/%s_cellf1_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cf1;
					// f3
					TCanvas *cf3 = new TCanvas(Form("cf3_%d_%d",g,d),Form("cf3_%d_%d",g,d),800,700);
					TProfile *ff_f3 = h_f3_t[g][d]->ProfileX(Form("ff_f3%d%d",g,d),-1,-1,"o");
					ff_f3->Draw();
					ff_f3->GetYaxis()->SetRangeUser(tmin,tmax);
					ff_f3->GetXaxis()->SetRangeUser( ff_f3->GetXaxis()->GetBinLowEdge( ff_f3->GetXaxis()->FindBin(0.)),.2);
					ff_f3->GetYaxis()->SetTitle("Time [ns]");
					ff_f3->GetXaxis()->SetTitle("f3");
					drawSubDetInfo(g, d);
					if( saveEPS )cf3->SaveAs(Form("plots/%s/efrac/%s_cellf3_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cf3->SaveAs(Form("plots/%s/efrac/%s_cellf3_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cf3;
				}

			}// end loop over gains

			file->Close();
			return;
		}



		//_____________________________________________
		//
		// Plot avg time vs energy 
		// by subdetector/slot
		//_____________________________________________
		void doETimePlot(){

			std::cout << "\nMaking plots for average energy timing\n\n";

			// Initialize the histograms
			for( int i=0; i<3; i++){
				for( int j=0; j<5; j++){
					//h_e_t[i][j]  = new TH2F(Form("h_e_t%d%d",j,i),Form("Cell Time Energy %s%d; Energy [GeV]; Time [ns]",subDet[j].c_str(),i),250,0,250.,500,-5,5);
					h_e_t[i][j]  = new TH2F(Form("h_e_t%d%d",j,i),Form("Cell Time Energy %s%d; Energy [GeV]; Time [ns]",subDet[j].c_str(),i),500,0,500.,500,-5,5);
				}
			}

			// Open the data file
			TFile *file;
			file = TFile::Open(Form("files/%s.root",sPassNumber.c_str()),"READ");

			// Loop over gains
			for( int g=0; g<2; g++){  
				// Loop over slots
				for(int sl=0; sl<22; sl++){

					// Make names for the histograms to open
					std::string ename  = Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),sl);

					TH2F *t_e  = (TH2F*)file->Get(ename.c_str());

					if( !t_e->GetEntries() ) {
						std::cout << "  >> Slot index: " << sl << " has no data for " 
							<< gain[g] << " gain, no plots will be made!\n";
						continue;
					}

					// Summary plots
					h_e_t[g][4]->Add(t_e);
					if( sl < 4 || sl == 8 ){ 
						h_e_t[g][0]->Add(t_e);
					}
					else if ( (sl > 3 && sl < 8) || sl == 9 ){ 
						h_e_t[g][1]->Add(t_e);
					}
					else if ( sl > 9 && sl < 16 || sl>21 &&sl< 26){
						h_e_t[g][2]->Add(t_e);
					}
					else if ( sl > 15 && sl<22 || sl >25  ){
						h_e_t[g][3]->Add(t_e);
					}

					// Rebin FIXME have to adjust to make data look reasonable
					int binMin = t_e->GetEntries() /  100; // each bin has at least 1/100th total entries
					if( g == 1 ) binMin = t_e->GetEntries() /  150;  // 1/150 for medium gain
					if( g == 2) binMin = 2; // 2 for lowgain
					t_e = rebin( t_e, binMin, 5); 
					t_e->SetName( Form("%s%d%d",t_e->GetName(),g,sl));

					// Get Profile Plots 
					TProfile *f_e   = t_e->ProfileX(Form("f_e%d%d",g,sl),-1,-1,"o");
					f_e->GetYaxis()->SetRangeUser(tmin,tmax);
					f_e->GetYaxis()->SetTitle("Time [ns]");
					f_e->GetXaxis()->SetTitle("Energy [GeV]");

					// Make each slot plot
					// e
					TCanvas *cce = new TCanvas(Form("cce%d_%d",g,sl),Form("cce%d_%d",g,sl),800,700);
					f_e->Draw();
					f_e->GetXaxis()->SetRangeUser( 5., 500); //FIXME
					//f_e->GetXaxis()->SetRangeUser( 5., 250);
					drawSlInfo(g, sl);
					if( saveEPS )cce->SaveAs(Form("plots/%s/energy/Sl%dcelle_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					// Save the fit version instead for pass3
					if( passNumber != 3) cce->SaveAs(Form("plots/%s/energy/Sl%dcelle_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					delete cce;

					//Make 1D histo also showing energy time
					TH1F *he = new TH1F(Form("1De_%d%d",g,sl),Form("1De_%d%d",g,sl),250,-5.,5.);
					for(int k=1; k<=f_e->GetXaxis()->GetNbins(); k++){
						if( f_e->GetBinEntries(k)) // if entries for energy, fill histogram
							he->Fill( f_e->GetBinContent(k) );
					}
					TCanvas *ce = new TCanvas(Form("ce%d%d",g,sl),Form("ce%d%d",g,sl),800,700);
					he->Draw();
					he->Rebin(5);
					he->GetXaxis()->SetTitle("Time [ns]");
					he->GetYaxis()->SetTitle("1 GeV bins / 0.2 ns"); // no rebin 0.04ns
					double mean = he->GetMean();
					double rms  = he->GetRMS();
					int entries = he->GetEntries();
					// Put info on plot
					drawSlInfo(g, sl);
					drawStats(entries, mean, rms);
					ce->SetLogy(1);
					// Save plots
					if( saveEPS )ce->SaveAs(Form("plots/%s/energy/1DSl%dcelle_%s.eps",sPassNumber.c_str(),sl,gain[g].c_str()));
					ce->SaveAs(Form("plots/%s/energy/1DSl%dcelle_%s.png",sPassNumber.c_str(),sl,gain[g].c_str()));
					ce->SetLogy(0);
					delete ce;

				}// end loop over slots

				// Make summary plots
				for( int d=0; d<5; d++){

					// Rebin FIXME have to adjust to make data look reasonable
					int binMin = h_e_t[g][d]->GetEntries() /  100; // each bin has at least 1/100th total entries
					if( g == 1 ) binMin = h_e_t[g][d]->GetEntries() /  150;  // 1/150 for medium gain
					if( g == 2 ) binMin = 5; // low gain 5
					h_e_t[g][d] = rebin( h_e_t[g][d], binMin, 5); 

					// e
					TCanvas *cce = new TCanvas(Form("ccce_%d_%d",g,d),Form("ce_%d_%d",g,d),800,700);
					TProfile *ff_e = h_e_t[g][d]->ProfileX(Form("ff_e%d%d",g,d),-1,-1,"o");
					ff_e->Draw();
					ff_e->GetYaxis()->SetRangeUser(tmin,tmax);
					// ff_e->GetXaxis()->SetRangeUser(5.,250); //FIXME
					ff_e->GetXaxis()->SetRangeUser(5.,500);
					ff_e->GetYaxis()->SetTitle("Time [ns]");
					ff_e->GetXaxis()->SetTitle("Energy [GeV]");
					drawSubDetInfo(g, d);
					if( saveEPS )cce->SaveAs(Form("plots/%s/energy/%s_celle_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					cce->SaveAs(Form("plots/%s/energy/%s_celle_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					delete cce;

					//Make 1D histo also showing energy time
					TH1F *he = new TH1F(Form("1De%d%d",g,d),Form("1De%d%d",g,d),250,-5.,5.);
					for(int k=1; k<=ff_e->GetXaxis()->GetNbins(); k++){
						if( ff_e->GetBinEntries(k)) // if entries for energy, fill histogram
							he->Fill( ff_e->GetBinContent(k) );
					}
					TCanvas *ce = new TCanvas(Form("ccce%d%d",g,d),Form("ce%d%d",g,d),800,700);
					he->Draw();
					he->Rebin(5);
					he->GetXaxis()->SetTitle("Time [ns]");
					he->GetYaxis()->SetTitle("1 GeV bins / 0.2 ns"); // no rebin .04ns
					double mean = he->GetMean();
					double rms  = he->GetRMS();
					int entries = he->GetEntries();
					// Put info on plot
					drawSubDetInfo(g, d);
					drawStats(entries, mean, rms);
					ce->SetLogy(1);
					// Save plots
					if( saveEPS )ce->SaveAs(Form("plots/%s/energy/1D%s_celle_%s.eps",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					ce->SaveAs(Form("plots/%s/energy/1D%s_celle_%s.png",sPassNumber.c_str(),subDet[d].c_str(),gain[g].c_str()));
					ce->SetLogy(0);
					delete ce;

				}

			}// end loop over gains

			file->Close();
			return;
		}

