#!/usr/bin/env python

import os,sys
from itertools import izip
import subprocess

#_________________
def writeCommands():
  f  = open('commands.txt','w')
  f1 = open('datasets.txt','r')

  for line in f1:
    line = line.strip()
    # Need to change data15 to whatever dataset you are looking at
    f.write('rucio add-rule data16_13TeV:%s --grouping DATASET 1 \"NEVIS_GRIDFTP\"\n'%line) 
  f.close()
  f1.close()
  print '\nFinished writing commands.txt'

#________________
def findDataSets():
  
  f1 = open('datasets.txt','w')
  f2 = open('runList.txt','r')
  
 # for line in f2:
  #  line = line.strip()
    # Need to change data15 to whatever dataset you are looking at
    # Use EGAM5 or EGAM1 if you are looking at W data sets or Z data sets
#    rucioCall = "rucio list-rules --account jue |grep data16_13TeV:|cut -d\  -f 1"
    #rucioCall = "rucio list-dids data16_13TeV.00%s.physics_Main.merge.DAOD_EGAM5*"%line
    #rucioCall = "rucio list-dids data15_13TeV.00%s.physics_Main.merge.DAOD_HIGG1D1*"%line 
    result = subprocess.Popen(rucioCall, stdout = subprocess.PIPE).communicate()[0]

    f1.write(rucioCall)
  f1.close()
  f2.close()
  print '\nFinished writing datasets.txt'


#_______________
def main():
 findDataSets()
 writeCommands()

#______________________
if __name__ == "__main__":
  main()

