#!/usr/bin/env python

import os,sys
from itertools import izip
import subprocess
from subprocess import Popen, PIPE  

#_________________
def runCommands():
  f1 = open('submit_job.txt','r')
  f2 = open('taskIds.txt','w')
  
  for line in f1:
    line = line.strip()
    print '\tSending command: \n\t >> %s'%line
    #result = subprocess.Popen(line.split(), stdout = subprocess.PIPE).communicate()[0]
    result = subprocess.Popen(line, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = result.communicate()
    f2.write('Command: %s\n'%line)
    f2.write('  Output >> %s\n'%out.rstrip())
    if err:
      f2.write('  Error! >> %s\n'%err.rstrip())

  
  f1.close()
  f2.close()
  print '\nFinished sending commands.txt'

#________________
def findDataSets():
  
  f1 = open('./runList.txt','r')
  #f2 = open('output_datasets.txt','w')
  f2 = open('submit_job.txt','w')
  for line in f1:
    line = line.strip()
#N    f3.write('rucio add-rule user.jue.data16_13TeV.00%s.physics_Main.CAOD_preSel.May19.inputs_v1_CxAOD.root  --grouping DATASET 1 \"NEVIS_GRIDFTP\"\n'%line) 
  #  f2.write('rucio add-rule user.jue.data16_13TeV.00%s.w_version4_WTiming_slimmed.root  --grouping DATASET 1 \"NEVIS_GRIDFTP\"\n'%line) 
    print int(line)
  f1.close()
  f2.close()
#  f3.close()
  print '\nFinished writing output_datasets.txt'


#_______________
def main():
 findDataSets()
 runCommands()


#______________________
if __name__ == "__main__":
  main()

