#!/usr/bin/env python

import os,sys
from itertools import izip
import subprocess
from subprocess import Popen, PIPE

#_______________
def submitCommands(fname):
  
  f  = open(fname,'r')
  f1 = open('taskIds.txt','w')
  
  for line in f:
    line = line.strip()
    print '\tSending command: \n\t >> %s'%line
    #result = subprocess.Popen(line.split(), stdout = subprocess.PIPE).communicate()[0]
    result = subprocess.Popen(line, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = result.communicate()
    f1.write('Command: %s\n'%line)
    f1.write('  Output >> %s\n'%out.rstrip())
    if err:
      f1.write('  Error! >> %s\n'%err.rstrip())

  
  f.close()
  f1.close()

#___________
def main():

  fname = 'delete'
  print 'Reading commands from %s'%fname
  submitCommands(fname)
  print 'TaskIds stored in taskIds.txt'

#______________________
if __name__ == "__main__":
  main()

