#!/usr/bin/env python

import os,sys
from itertools import izip
import subprocess

#_________________
def writeCommands():
  f  = open('commands.txt','w')
  f1 = open('datasets.txt','r')

  for line in f1:
    line = line.strip()
    # Need to change data15 to whatever dataset you are looking at
    f.write('rucio add-rule data16_13TeV:%s --grouping DATASET 1 \"NEVIS_GRIDFTP\"\n'%line) 
  f.close()
  f1.close()
  print '\nFinished writing commands.txt'

#________________
def findDataSets():
  
  f1 = open('datasets.txt','w')
  f2 = open('runList.txt','r')
  
  for line in f2:
    line = line.strip()
    # Need to change data15 to whatever dataset you are looking at
    # Use EGAM5 or EGAM1 if you are looking at W data sets or Z data sets
    rucioCall = "rucio list-dids data16_13TeV.00%s.physics_Main.merge.DAOD_EXOT3*"%line 
    #rucioCall = "rucio list-dids data16_13TeV.00%s.physics_Main.merge.DAOD_EGAM5*"%line
    #rucioCall = "rucio list-dids data15_13TeV.00%s.physics_Main.merge.DAOD_HIGG1D1*"%line 
    result = subprocess.Popen(rucioCall.split(), stdout = subprocess.PIPE).communicate()[0]

    splitResult = result.split()
    matching = [s for s in splitResult if "AOD" in s]
    f_tag =[]
    m_tag =[]
    p_tag =[]
    i_data =[]
    for i_match in matching:
      dataTag = i_match.split(":")[1]
      if 'tid' not in dataTag:
        i_data.append(dataTag)
        i_tag = dataTag.split('.')[5]
        tags  = i_tag.split('_')
	if 'f' in tags[0]:
		print tags[0]

        	f_tag.append( (int)(tags[0].replace('f','')) )
        	m_tag.append( (int)(tags[1].replace('m','')) )
        	p_tag.append( (int)(tags[2].replace('p','')) )
    print '\nFound %d tags for run %s'%(len(i_data),line)
    for ii in i_data:
      print '%s'%ii
    max_p_in = p_tag.index(max(p_tag))
    if f_tag[max_p_in] != max(f_tag):
      if p_tag[f_tag.index(max(f_tag))] == max(p_tag):
        max_p_in = f_tag.index(max(f_tag))
      else:
        print '==> WARNING larger f tag available: f%d'%max(f_tag)
    if m_tag[max_p_in] != max(m_tag):
      if p_tag[m_tag.index(max(m_tag))] == max(p_tag):
        max_p_in = m_tag.index(max(m_tag))
      else:
        print '==> WARNING larger m tag available: m%d'%max(m_tag)
    print '==> Using: %s'%i_data[max_p_in] 
    f1.write('%s\n'%i_data[max_p_in])
  f1.close()
  f2.close()
  print '\nFinished writing datasets.txt'


#_______________
def main():
 findDataSets()
 writeCommands()

#______________________
if __name__ == "__main__":
  main()

