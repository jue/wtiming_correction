//_________________________________
//
// Author : Ryne Carbone
// Date   : Oct 2015
// Contact: ryne.carbone@cern.ch
//________________________________
#include "help/runPassCalc.h"
#include "help/pass_help.cxx"
#include "help/plot_help.cxx"
#include "help/draw_help.cxx"
#include "help/fit_help.cxx"
#include "help/z_help.cxx"
#include "help/low_help.cxx"
#include "help/res_help.cxx"
//#include "rootlogon.C"
//_____________________________________________
//
// Main part of program
// pn is the passNumber, 0 by default
// mode [0] doPlot,doCorr [1]doPlot [2] doCorr
//_____________________________________________
void runPassCalc(int pn=0, int mode=1){
SetAtlasStyle(); 
#ifdef __CINT__
gROOT->LoadMacro("AtlasLabels.C");
gROOT->LoadMacro("AtlasStyle.C");  
#endif
  // Set Pass
  setPass(pn, mode);

  // Set Run List
  setRunList();
  cout<<runNumberList[0]<<endl;
  // Low gain correciton study
  // TODO, integrate into normal code
  // once finalized, if enough stats
  //
  //low_offset();
  //setTRange(-2.0,2.0);
  //low_zres();
  //low_pass4();
  //lowFEBTimePlot();
  //lowCellTimePlot();
  //lowAngularPlot();
  //lowEFracPlot();
  //lowETimePlot();
  
  //return;

  // Calculate pass specific corrections
  if( doCorr) {
    switch( passNumber ){
      case 0: 
        setTRange(-8.,8.);
        pass0();
        break;
      case 1: 
        setTRange(-1.5,1.5);
        pass1();
        break;
      case 2: 
        setTRange(-1.5,1.5);
        pass2();
        break;
      case 3:
        setTRange(-0.45,1);
        pass3();
        break;
      case 4:
        setTRange(-0.5,1);
        pass4();
        break;
      case 5:
        setTRange(-.3,.45);
        pass5();
        break;
      case 6:
        setTRange(-1,1);
        pass6();
        break;
      case 7:
        setTRange(-1,1);
	pass7();
        break;
      case 8:
        setTRange(-1,1);
        pass8();
        break;
	
      case 9:
        setTRange(-1,1);
//        pass9();
        break;
      case 10:
	 setTRange(-1,1);       
	 pass10();

    }
  }
  
  // Make plots
  if( doPlot ) {
	  setTRange(-1,1);
//	  doCellTimePlot();
//	  doAngularPlot();
//	  doEFracPlot();
//	  doETimePlot();
	  doFEBTimePlot();

  // doChannelPlot();
  }

  std::cout << "\nFinished processing pass " << passNumber << std::endl;
  
  return;

}


//_____________________________________________
//
// Set PassNumber and mode
//_____________________________________________
void setPass(int pn, int mode){
  
  passNumber  = pn;
  sPassNumber = Form("pass%d",passNumber);
  switch(mode){
    case 0: 
      doPlot = true;
      doCorr = true;
      break;
    case 1:
      doPlot = true;
      doCorr = false;
      break;
    case 2:
      doPlot = false;
      doCorr = true;
      break;
  }

  std::cout << "  > Pass number has been set: " << passNumber << std::endl;
  std::cout << "  > doPlot: " << doPlot << std::endl;
  std::cout << "  > doCorr: " << doCorr << std::endl;

  return;
}


//_____________________________________________
//
// Read list of run numbers from config file
//_____________________________________________
void setRunList(){
  
  // Run number config file
  ifstream f1("config/RunNumberList.txt");
  int tempRunNum,i=0;

  // Store each run into local vector
  while( f1 >> tempRunNum ){
      runNumberList[i]=tempRunNum;
      i++;
  } 
  
  f1.close();

  std::cout << "  > Added " << NRUNS << " runs successfully from "
            << runNumberList[0] << " to " << runNumberList[NRUNS-1] << "\n\n";
  
//  ifstream f2("config/channel.txt");  
//  int tempch, tempid;
//  i=0;
//  while( f2 >>tempch>>tempid){
//          channel[i] = tempch;
//          onlineid[i] = tempid;
//          i++;
//  }
//  f2.close();


  return;
}


//_____________________________________________
//
// Set tmin/tmax for plotting
//_____________________________________________
void setTRange(double t_l, double t_h){
  
  tmin = t_l;
  tmax = t_h;
  
  return;
}
