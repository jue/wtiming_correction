#!/usr/bin/env python
import os
import sys
import string
import getopt 
import random
import math
import time
import shutil
import pickle
import commands
from datetime import datetime

#____________________________________________________
def CopyTemplate(fname, passNumber,nRuns,IOV_Z):

  # get folder name based on IOV
  sIOV  = 'IOV_%d'%IOV_Z
  spath = 'pass%d'%passNumber

  # Make the folder if it doesn't exist 
  base = os.getcwd()
  if not os.path.exists('/'.join([base,sIOV])):
    os.makedirs('/'.join([base,sIOV]))
  if not os.path.exists('/'.join([base,sIOV,spath])):
    os.makedirs('/'.join([base,sIOV,spath]))
  if not os.path.exists('/'.join([base,sIOV,spath,spath])):
    os.makedirs('/'.join([base,sIOV,spath,spath]))

  # Open template file
  tempF = open('temp/%s'%fname,'r')
  
  # for iovHome.html
  if 'iovHome' in fname:
    shellF = open('%s/%s'%(sIOV,fname),'w')
    for line in tempF:
      shellF.write(line)
    tempF.close()
    shellF.close()
    return True
  
  # open output file
  shellF = open('%s/pass%d/%s'%(sIOV,passNumber,fname),'w')
  
  # Read template and replace lines with correct pass number
  for line in tempF:
    line = line.replace('"pass0','"pass%d'%passNumber)
    line = line.replace('Pass 0 Plots','Pass %d Plots'%passNumber)
    #line = line.replace('Pass0','Pass%d'%passNumber)
    if 'INSERT H RUNS HERE' in line:
      for iRun in range(0,nRuns):
        shellF.write('<tr>\n')
        shellF.write('  <td class="tg-d"> Average FEB Time High Gain</td>\n')
        shellF.write('  <td> <a href="pass%d/Run%dFebCorrections_High.png"><img src="pass%d/Run%dFebCorrections_High.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('  <td> <a href="pass%d/1DRun%dFebCorrections_High.png"><img src="pass%d/1DRun%dFebCorrections_High.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('</tr>')
    elif 'INSERT M RUNS HERE' in line:
      for iRun in range(0,nRuns):
        shellF.write('<tr>\n')
        shellF.write('  <td class="tg-d"> Average FEB Time Medium Gain</td>\n')
        shellF.write('  <td> <a href="pass%d/Run%dFebCorrections_Medium.png"><img src="pass%d/Run%dFebCorrections_Medium.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('  <td> <a href="pass%d/1DRun%dFebCorrections_Medium.png"><img src="pass%d/1DRun%dFebCorrections_Medium.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('</tr>')
    elif 'INSERT L RUNS HERE' in line:
      for iRun in range(0,nRuns):
        shellF.write('<tr>\n')
        shellF.write('  <td class="tg-d"> Average FEB Time Low Gain</td>\n')
        shellF.write('  <td> <a href="pass%d/Run%dFebCorrections_Low.png"><img src="pass%d/Run%dFebCorrections_Low.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('  <td> <a href="pass%d/1DRun%dFebCorrections_Low.png"><img src="pass%d/1DRun%dFebCorrections_Low.png" /></a> </td>\n'%(passNumber,iRun,passNumber,iRun))
        shellF.write('</tr>')
    elif 'INSERT RESOLUTION LINK' in line:
      if passNumber == 8:
        shellF.write('<tr>\n')
        shellF.write('<td class="tg-d"> Resolution and Correlation Plots  <br> (1) Leading vs Subleading Electron Time <br>(2) EMB Sl 11 High Gain Timing Resolution </td>\n')
        shellF.write('<td> <a href="pass8/Zplots/h_t1vst2Zee.png"><img src="pass8/Zplots/h_t1vst2Zee.png" /></a> </td>\n')
        shellF.write('<td> <a href="pass8/Zplots/h_eResolution_sl0_High.png"><img src="pass8/Zplots/h_eResolution_sl0_High.png" /></a> </td>\n')
        shellF.write('<td class="tg-d"> <a href="zeeTime.html">Time Resolution Plots, Energy Spectrum Plots</a>  </td>\n')
        shellF.write('</tr>\n')
      else:
        shellF.write('\n')
    else:
      shellF.write(line)

  tempF.close()
  shellF.close()

  return True


#____________________________________________________
def main():
#Note that the html pages reference mystyle.css which is included here
# You have to manually set:
#    nRuns to the number of runs in the Interval of Validity
#    passNumber to whichever pass you are making plots for
#    IOV_Z to which interval of validity you are analyzing

  nRuns = 51
  passNumber = 0
  IOV_Z = 1
  fileList = ['cellTime_embaM.html','cellTime_embaH.html',
              'cellTime_embcM.html','cellTime_embcH.html',
              'cellTime_emecaM.html','cellTime_emecaH.html',
              'cellTime_emeccM.html','cellTime_emeccH.html',
              'cellTime_SubM.html','cellTime_SubH.html',
              'ang_embaM.html','ang_embaH.html',
              'ang_embcM.html','ang_embcH.html',
              'ang_emecaM.html','ang_emecaH.html',
              'ang_emeccM.html','ang_emeccH.html',
              'angTime_SubM.html','angTime_SubH.html',
              'dang_embaM.html','dang_embaH.html',
              'dang_embcM.html','dang_embcH.html',
              'dang_emecaM.html','dang_emecaH.html',
              'dang_emeccM.html','dang_emeccH.html',
              'dangTime_SubM.html','dangTime_SubH.html',
              'efrac_embaM.html','efrac_embaH.html',
              'efrac_embcM.html','efrac_embcH.html',
              'efrac_emecaM.html','efrac_emecaH.html',
              'efrac_emeccM.html','efrac_emeccH.html',
              'efracTime_SubM.html','efracTime_SubH.html',
              'e_embaM.html','e_embaH.html',
              'e_embcM.html','e_embcH.html',
              'e_emecaM.html','e_emecaH.html',
              'e_emeccM.html','e_emeccH.html',
              'eTime_SubM.html','eTime_SubH.html',
              'runTime_RunM.html','runTime_RunH.html',
              'runTime_SubM.html','runTime_SubH.html',
              'main.html'] 
  if passNumber == 0:
    fileList.append('iovHome.html')
  if passNumber == 8:
    fileList.append('zeeTime.html')

  for fname in fileList:
    CopyTemplate(fname,passNumber,nRuns,IOV_Z)
  
  return



#____________________________________________________
if __name__ == "__main__":
  main()

